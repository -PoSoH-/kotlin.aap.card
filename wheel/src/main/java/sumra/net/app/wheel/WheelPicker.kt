package sumra.net.wheel.infinity_card.fragments

import android.content.Context
import android.content.res.TypedArray
import android.graphics.*
import android.os.Build
import android.os.Handler
import android.text.TextUtils
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.VelocityTracker
import android.view.View
import android.view.ViewConfiguration
import android.widget.Scroller
import sumra.net.app.wheel.R
import sumra.net.wheel.infinity_card.fragments.WheelPicker.OnWheelChangeListener

interface IDebug {
    fun setDebug(isDebug: Boolean)
}

interface IWheelPicker {

    fun getVisibleItemCount(): Int
    fun setVisibleItemCount(count: Int)
    fun isCyclic(): Boolean
    fun setCyclic(isCyclic: Boolean)
    fun setOnItemSelectedListener(listener: WheelPicker.OnItemSelectedListener?)
    fun getSelectedItemPosition(): Int
    fun setSelectedItemPosition(position: Int)
    fun getCurrentItemPosition(): Int
    fun getData(): List<Any>?
    fun setData(data: List<Any>?)
    fun setSameWidth(hasSameSize: Boolean)
    fun hasSameWidth(): Boolean
    fun setOnWheelChangeListener(listener: OnWheelChangeListener?)
    fun getMaximumWidthText(): String?
    fun setMaximumWidthText(text: String?)
    fun getMaximumWidthTextPosition(): Int
    fun setMaximumWidthTextPosition(position: Int)
    fun getSelectedItemTextColor(): Int
    fun setSelectedItemTextColor(color: Int)
    fun getItemTextColor(): Int
    fun setItemTextColor(color: Int)
    fun getItemTextSize(): Int
    fun setItemTextSize(size: Int)
    fun getItemSpace(): Int
    fun setItemSpace(space: Int)
    fun setIndicator(hasIndicator: Boolean)
    fun hasIndicator(): Boolean
    fun getIndicatorSize(): Int
    fun setIndicatorSize(size: Int)
    fun getIndicatorColor(): Int
    fun setIndicatorColor(color: Int)
    fun setCurtain(hasCurtain: Boolean)
    fun hasCurtain(): Boolean
    fun getCurtainColor(): Int
    fun setCurtainColor(color: Int)
    fun setAtmospheric(hasAtmospheric: Boolean)
    fun hasAtmospheric(): Boolean
    fun isCurved(): Boolean
    fun setCurved(isCurved: Boolean)
    fun getItemAlign(): Int
    fun setItemAlign(align: Int)
    fun getTypeface(): Typeface?
    fun setTypeface(tf: Typeface?)
}

open class WheelPicker constructor(context: Context, attrs: AttributeSet?) : View(context, attrs), IDebug,
    IWheelPicker, java.lang.Runnable {

    private val mHandler: Handler = Handler()
    private val mPaint: Paint?
    private val mScroller: Scroller
    private var mTracker: VelocityTracker? = null
    private var isTouchTriggered = false
    private var mOnItemSelectedListener: OnItemSelectedListener? = null
    private var mOnWheelChangeListener: OnWheelChangeListener? = null
    private val mRectDrawn: Rect
    private val mRectIndicatorHead: Rect
    private val mRectIndicatorFoot: Rect
    private val mRectCurrentItem: Rect
    private val mCamera: Camera
    private val mMatrixRotate: Matrix
    private val mMatrixDepth: Matrix
    private var mData: List<Any>?
    private var mMaxWidthText: String
    private var mVisibleItemCount: Int
    private var mDrawnItemCount = 0
    private var mHalfDrawnItemCount = 0
    private var mTextMaxWidth = 0
    private var mTextMaxHeight = 0
    private var mItemTextColor: Int
    private var mSelectedItemTextColor: Int
    private var mItemTextSize: Int
    private var mIndicatorSize: Int
    private var mIndicatorColor: Int
    private var mCurtainColor: Int
    private var mItemSpace: Int
    private var mItemAlign: Int
    private var mItemHeight = 0
    private var mHalfItemHeight = 0
    private var mHalfWheelHeight = 0
    private var mSelectedItemPosition: Int
//    private var mCurrentItemPosition = 0
    private var mCurrentItemPosition = 0
        private set

//    override fun getCurrentItemPosition(): Int {
//        TODO("Not yet implemented")
//    }

    private var mMinFlingY = 0
    private var mMaxFlingY = 0
    private var mMinimumVelocity = 50
    private var mMaximumVelocity = 8000
    private var mWheelCenterX = 0
    private var mWheelCenterY = 0
    private var mDrawnCenterX = 0
    private var mDrawnCenterY = 0
    private var mScrollOffsetY = 0
    private var mTextMaxWidthPosition: Int
    private var mLastPointY = 0
    private var mDownPointY = 0
    private var mTouchSlop = 8
    private var hasSameWidth: Boolean
    private var hasIndicator: Boolean
    private var hasCurtain: Boolean
    private var hasAtmospheric: Boolean
    private var isCyclic: Boolean = false
    private var isCurved: Boolean
    private var isClick = false
    private var isForceFinishScroll = false
    private val fontPath: String?
    private var isDebug = false

    constructor(context: Context) : this(context, null)

    private fun updateVisibleItemCount() {
        if (mVisibleItemCount < 2) throw java.lang.ArithmeticException("Wheel's visible item count can not be less than 2!")

        if (mVisibleItemCount % 2 == 0) mVisibleItemCount += 1
        mDrawnItemCount = mVisibleItemCount + 2
        mHalfDrawnItemCount = mDrawnItemCount / 2
    }

    private fun computeTextSize() {
        mTextMaxHeight = 0
        mTextMaxWidth = mTextMaxHeight
        if (hasSameWidth) {
            mTextMaxWidth = mPaint!!.measureText(mData!![0].toString()).toInt()
        } else if (isPosInRang(mTextMaxWidthPosition)) {
            mTextMaxWidth = mPaint!!.measureText(mData!![mTextMaxWidthPosition].toString()).toInt()
        } else if (!TextUtils.isEmpty(mMaxWidthText)) {
            mTextMaxWidth = mPaint!!.measureText(mMaxWidthText).toInt()
        } else {
            for (obj: Any in mData!!) {
                val text = obj.toString()
                val width = mPaint!!.measureText(text) as Int
                mTextMaxWidth = java.lang.Math.max(mTextMaxWidth, width)
            }
        }
        val metrics: Paint.FontMetrics = mPaint!!.getFontMetrics()
        mTextMaxHeight = ((metrics.bottom - metrics.top).toInt())
    }

    private fun updateItemTextAlign() {
        when (mItemAlign) {
            ALIGN_LEFT -> mPaint!!.setTextAlign(Paint.Align.LEFT)
            ALIGN_RIGHT -> mPaint!!.setTextAlign(Paint.Align.RIGHT)
            else -> mPaint!!.setTextAlign(Paint.Align.CENTER)
        }
    }

    init {
        val a: TypedArray = context.obtainStyledAttributes(attrs, R.styleable.WheelPicker)
        val idData: Int = a.getResourceId(R.styleable.WheelPicker_wheel_data, 0)
        mData = listOf("a", "b", "c")
//            Arrays.asList(
//            getResources()
//                .getStringArray(if (idData == 0) R.array.WheelArrayDefault else idData)
//        )
        mItemTextSize = a.getDimensionPixelSize(
            R.styleable.WheelPicker_wheel_item_text_size,
            getResources().getDimensionPixelSize(R.dimen.WheelItemTextSize)
        )
        mVisibleItemCount = a.getInt(R.styleable.WheelPicker_wheel_visible_item_count, 7)
        mSelectedItemPosition = a.getInt(R.styleable.WheelPicker_wheel_selected_item_position, 0)
        hasSameWidth = a.getBoolean(R.styleable.WheelPicker_wheel_same_width, false)
        mTextMaxWidthPosition = a.getInt(R.styleable.WheelPicker_wheel_maximum_width_text_position, -1)
        mMaxWidthText = a.getString(R.styleable.WheelPicker_wheel_maximum_width_text)!!
        mSelectedItemTextColor = a.getColor(R.styleable.WheelPicker_wheel_selected_item_text_color, -1)
        mItemTextColor = a.getColor(R.styleable.WheelPicker_wheel_item_text_color, -0x777778)
        mItemSpace = a.getDimensionPixelSize(
            R.styleable.WheelPicker_wheel_item_space,
            getResources().getDimensionPixelSize(R.dimen.WheelItemSpace)
        )
        isCyclic = a.getBoolean(R.styleable.WheelPicker_wheel_cyclic, false)
        hasIndicator = a.getBoolean(R.styleable.WheelPicker_wheel_indicator, false)
        mIndicatorColor = a.getColor(R.styleable.WheelPicker_wheel_indicator_color, -0x11cccd)
        mIndicatorSize = a.getDimensionPixelSize(
            R.styleable.WheelPicker_wheel_indicator_size,
            getResources().getDimensionPixelSize(R.dimen.WheelIndicatorSize)
        )
        hasCurtain = a.getBoolean(R.styleable.WheelPicker_wheel_curtain, false)
        mCurtainColor = a.getColor(R.styleable.WheelPicker_wheel_curtain_color, -0x77000001)
        hasAtmospheric = a.getBoolean(R.styleable.WheelPicker_wheel_atmospheric, false)
        isCurved = a.getBoolean(R.styleable.WheelPicker_wheel_curved, false)
        mItemAlign = a.getInt(R.styleable.WheelPicker_wheel_item_align, ALIGN_CENTER)
        fontPath = a.getString(R.styleable.WheelPicker_wheel_font_path)
        a.recycle()

        // Update relevant parameters when the count of visible item changed
        updateVisibleItemCount()
        mPaint = Paint(Paint.ANTI_ALIAS_FLAG or Paint.DITHER_FLAG or Paint.LINEAR_TEXT_FLAG)
        mPaint.setTextSize(mItemTextSize.toFloat())
        if (fontPath != null) {
            var typeface: Typeface = Typeface.createFromAsset(context.getAssets(), fontPath)
            typeface = typeface
        }

        // Update alignment of text
        updateItemTextAlign()

        // Correct sizes of text
        computeTextSize()
        mScroller = Scroller(getContext())
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.DONUT) {
            val conf: ViewConfiguration = ViewConfiguration.get(getContext())
            mMinimumVelocity = conf.getScaledMinimumFlingVelocity()
            mMaximumVelocity = conf.getScaledMaximumFlingVelocity()
            mTouchSlop = conf.getScaledTouchSlop()
        }
        mRectDrawn = Rect()
        mRectIndicatorHead = Rect()
        mRectIndicatorFoot = Rect()
        mRectCurrentItem = Rect()
        mCamera = Camera()
        mMatrixRotate = Matrix()
        mMatrixDepth = Matrix()
    }

    protected override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val modeWidth: Int = MeasureSpec.getMode(widthMeasureSpec)
        val modeHeight: Int = MeasureSpec.getMode(heightMeasureSpec)
        val sizeWidth: Int = MeasureSpec.getSize(widthMeasureSpec)
        val sizeHeight: Int = MeasureSpec.getSize(heightMeasureSpec)

        // Correct sizes of original content
        var resultWidth = mTextMaxWidth
        var resultHeight = mTextMaxHeight * mVisibleItemCount + mItemSpace * (mVisibleItemCount - 1)

        // Correct view sizes again if curved is enable
        if (isCurved) {
            resultHeight = (2 * resultHeight / java.lang.Math.PI) as Int
        }
        if (isDebug) Log.i(TAG, "Wheel's content size is ($resultWidth:$resultHeight)")

        // Consideration padding influence the view sizes
        resultWidth += getPaddingLeft() + getPaddingRight()
        resultHeight += getPaddingTop() + getPaddingBottom()
        if (isDebug) Log.i(TAG, "Wheel's size is ($resultWidth:$resultHeight)")

        // Consideration sizes of parent can influence the view sizes
        resultWidth = measureSize(modeWidth, sizeWidth, resultWidth)
        resultHeight = measureSize(modeHeight, sizeHeight, resultHeight)
        setMeasuredDimension(resultWidth, resultHeight)
    }

    private fun measureSize(mode: Int, sizeExpect: Int, sizeActual: Int): Int {
        var realSize: Int
        if (mode == MeasureSpec.EXACTLY) {
            realSize = sizeExpect
        } else {
            realSize = sizeActual
            if (mode == MeasureSpec.AT_MOST) realSize = java.lang.Math.min(realSize, sizeExpect)
        }
        return realSize
    }

    protected override fun onSizeChanged(w: Int, h: Int, oldW: Int, oldH: Int) {

        // Set content region
        mRectDrawn.set(
            getPaddingLeft(), getPaddingTop(), getWidth() - getPaddingRight(),
            getHeight() - getPaddingBottom()
        )
        if (isDebug) Log.i(
            TAG, "Wheel's drawn rect size is (" + mRectDrawn.width().toString() + ":" +
                    mRectDrawn.height().toString() + ") and location is (" + mRectDrawn.left.toString() + ":" +
                    mRectDrawn.top.toString() + ")"
        )

        // Get the center coordinates of content region
        mWheelCenterX = mRectDrawn.centerX()
        mWheelCenterY = mRectDrawn.centerY()

        // Correct item drawn center
        computeDrawnCenter()
        mHalfWheelHeight = mRectDrawn.height() / 2
        mItemHeight = mRectDrawn.height() / mVisibleItemCount
        mHalfItemHeight = mItemHeight / 2

        // Initialize fling max Y-coordinates
        computeFlingLimitY()

        // Correct region of indicator
        computeIndicatorRect()

        // Correct region of current select item
        computeCurrentItemRect()
    }

    private fun computeDrawnCenter() {
        when (mItemAlign) {
            ALIGN_LEFT -> mDrawnCenterX = mRectDrawn.left
            ALIGN_RIGHT -> mDrawnCenterX = mRectDrawn.right
            else -> mDrawnCenterX = mWheelCenterX
        }
        mDrawnCenterY = (mWheelCenterY - (mPaint!!.ascent() + mPaint!!.descent()) / 2) as Int
    }

    private fun computeFlingLimitY() {
        val currentItemOffset = mSelectedItemPosition * mItemHeight
        mMinFlingY = if (isCyclic) Int.MIN_VALUE else -mItemHeight * (mData!!.size - 1) + currentItemOffset
        mMaxFlingY = if (isCyclic) Int.MAX_VALUE else currentItemOffset
    }

    private fun computeIndicatorRect() {
        if (!hasIndicator) return
        val halfIndicatorSize = mIndicatorSize / 2
        val indicatorHeadCenterY = mWheelCenterY + mHalfItemHeight
        val indicatorFootCenterY = mWheelCenterY - mHalfItemHeight
        mRectIndicatorHead.set(
            mRectDrawn.left, indicatorHeadCenterY - halfIndicatorSize,
            mRectDrawn.right, indicatorHeadCenterY + halfIndicatorSize
        )
        mRectIndicatorFoot.set(
            mRectDrawn.left, indicatorFootCenterY - halfIndicatorSize,
            mRectDrawn.right, indicatorFootCenterY + halfIndicatorSize
        )
    }

    private fun computeCurrentItemRect() {
        if (!hasCurtain && mSelectedItemTextColor == -1) return
        mRectCurrentItem.set(
            mRectDrawn.left, mWheelCenterY - mHalfItemHeight, mRectDrawn.right,
            mWheelCenterY + mHalfItemHeight
        )
    }

    protected override fun onDraw(canvas: Canvas) {
        if (null != mOnWheelChangeListener) mOnWheelChangeListener!!.onWheelScrolled(mScrollOffsetY)
        if (mData!!.size == 0) return
        val drawnDataStartPos = -mScrollOffsetY / mItemHeight - mHalfDrawnItemCount
        var drawnDataPos = drawnDataStartPos + mSelectedItemPosition
        var drawnOffsetPos = -mHalfDrawnItemCount
        while (drawnDataPos < drawnDataStartPos + mSelectedItemPosition + mDrawnItemCount) {
            var data: String = ""
            if (isCyclic) {
                var actualPos = drawnDataPos % mData!!.size
                actualPos = if (actualPos < 0) actualPos + mData!!.size else actualPos
                data = mData!![actualPos].toString()
            } else {
                if (isPosInRang(drawnDataPos)) data = mData!![drawnDataPos].toString()
            }
            mPaint!!.setColor(mItemTextColor)
            mPaint!!.setStyle(Paint.Style.FILL)
            val mDrawnItemCenterY = mDrawnCenterY + drawnOffsetPos * mItemHeight + mScrollOffsetY % mItemHeight
            var distanceToCenter = 0
            if (isCurved) {

                // Correct ratio of item's drawn center to wheel center
                val ratio: Float = (mDrawnCenterY - java.lang.Math.abs(mDrawnCenterY - mDrawnItemCenterY) -
                        mRectDrawn.top) * 1.0f / (mDrawnCenterY - mRectDrawn.top)

                // Correct unit
                var unit = 0
                if (mDrawnItemCenterY > mDrawnCenterY) unit = 1 else if (mDrawnItemCenterY < mDrawnCenterY) unit = -1
                var degree = -(1 - ratio) * 90 * unit
                if (degree < -90) degree = -90f
                if (degree > 90) degree = 90f
                distanceToCenter = computeSpace(degree.toInt())
                var transX = mWheelCenterX
                when (mItemAlign) {
                    ALIGN_LEFT -> transX = mRectDrawn.left
                    ALIGN_RIGHT -> transX = mRectDrawn.right
                }
                val transY = mWheelCenterY - distanceToCenter
                mCamera.save()
                mCamera.rotateX(degree)
                mCamera.getMatrix(mMatrixRotate)
                mCamera.restore()
                mMatrixRotate.preTranslate((-transX).toFloat(), (-transY).toFloat())
                mMatrixRotate.postTranslate(transX.toFloat(), transY.toFloat())
                mCamera.save()
                mCamera.translate(0F, 0F, computeDepth(degree).toFloat())
                mCamera.getMatrix(mMatrixDepth)
                mCamera.restore()
                mMatrixDepth.preTranslate(-transX.toFloat(), -transY.toFloat())
                mMatrixDepth.postTranslate(transX.toFloat(), transY.toFloat())
                mMatrixRotate.postConcat(mMatrixDepth)
            }
            if (hasAtmospheric) {
                var alpha = ((mDrawnCenterY - java.lang.Math.abs(mDrawnCenterY - mDrawnItemCenterY)) *
                        1.0f / mDrawnCenterY * 255) as Int
                alpha = if (alpha < 0) 0 else alpha
                mPaint.setAlpha(alpha)
            }

            // Correct item's drawn centerY base on curved state
            val drawnCenterY = if (isCurved) mDrawnCenterY - distanceToCenter else mDrawnItemCenterY

            // Judges need to draw different color for current item or not
            if (mSelectedItemTextColor != -1) {
                canvas.save()
                if (isCurved) canvas.concat(mMatrixRotate)
                canvas.clipRect(mRectCurrentItem, Region.Op.DIFFERENCE)
                canvas.drawText(data, mDrawnCenterX.toFloat(), drawnCenterY.toFloat(), mPaint)
                canvas.restore()
                mPaint.setColor(mSelectedItemTextColor)
                canvas.save()
                if (isCurved) canvas.concat(mMatrixRotate)
                canvas.clipRect(mRectCurrentItem)
                canvas.drawText(data, mDrawnCenterX.toFloat(), drawnCenterY.toFloat(), mPaint)
                canvas.restore()
            } else {
                canvas.save()
                canvas.clipRect(mRectDrawn)
                if (isCurved) canvas.concat(mMatrixRotate)
                canvas.drawText(data, mDrawnCenterX.toFloat(), drawnCenterY.toFloat(), mPaint)
                canvas.restore()
            }
            if (isDebug) {
                canvas.save()
                canvas.clipRect(mRectDrawn)
                mPaint.setColor(-0x11cccd)
                val lineCenterY = mWheelCenterY + drawnOffsetPos * mItemHeight
                canvas.drawLine(
                    mRectDrawn.left.toFloat(),
                    lineCenterY.toFloat(),
                    mRectDrawn.right.toFloat(),
                    lineCenterY.toFloat(),
                    mPaint
                )
                mPaint.setColor(-0xcccc12)
                mPaint.setStyle(Paint.Style.STROKE)
                val top = lineCenterY - mHalfItemHeight
                canvas.drawRect(
                    mRectDrawn.left.toFloat(),
                    top.toFloat(),
                    mRectDrawn.right.toFloat(),
                    (top + mItemHeight).toFloat(),
                    mPaint
                )
                canvas.restore()
            }
            drawnDataPos++
            drawnOffsetPos++
        }

        // Need to draw curtain or not
        if (hasCurtain) {
            mPaint!!.setColor(mCurtainColor)
            mPaint.setStyle(Paint.Style.FILL)
            canvas.drawRect(mRectCurrentItem, mPaint)
        }

        // Need to draw indicator or not
        if (hasIndicator) {
            mPaint!!.setColor(mIndicatorColor)
            mPaint.setStyle(Paint.Style.FILL)
            canvas.drawRect(mRectIndicatorHead, mPaint)
            canvas.drawRect(mRectIndicatorFoot, mPaint)
        }
        if (isDebug) {
            mPaint!!.setColor(0x4433EE33)
            mPaint.setStyle(Paint.Style.FILL)
            canvas.drawRect(
                0F,
                0F, getPaddingLeft().toFloat(),
                getHeight().toFloat(),
                mPaint
            )
            canvas.drawRect(
                0F,
                0F, getWidth().toFloat(),
                getPaddingTop().toFloat(),
                mPaint
            )
            canvas.drawRect(
                (getWidth() - getPaddingRight()).toFloat(),
                0F,
                getWidth().toFloat(),
                getHeight().toFloat(),
                mPaint
            )
            canvas.drawRect(
                0F,
                (getHeight() - getPaddingBottom()).toFloat(),
                getWidth().toFloat(),
                getHeight().toFloat(),
                mPaint
            )
        }
    }

    private fun isPosInRang(position: Int): Boolean {
        return position >= 0 && position < mData!!.size
    }

    private fun computeSpace(degree: Int): Int {
        return (java.lang.Math.sin(java.lang.Math.toRadians(degree.toDouble())) * mHalfWheelHeight).toInt()
    }

    private fun computeDepth(degree: Float): Int {
        return (mHalfWheelHeight - java.lang.Math.cos(java.lang.Math.toRadians(degree.toDouble())) * mHalfWheelHeight) as Int
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.getAction()) {
            MotionEvent.ACTION_DOWN -> {
                isTouchTriggered = true
                if (null != getParent()) getParent().requestDisallowInterceptTouchEvent(true)
                if (null == mTracker) mTracker = VelocityTracker.obtain() else mTracker!!.clear()
                mTracker!!.addMovement(event)
                if (!mScroller.isFinished()) {
                    mScroller.abortAnimation()
                    isForceFinishScroll = true
                }
                run({
                    mLastPointY = event.getY().toInt()
                    mDownPointY = mLastPointY
                })
            }
            MotionEvent.ACTION_MOVE -> {
                if (java.lang.Math.abs(mDownPointY - event.getY()) < mTouchSlop) {
                    isClick = true
//                    break
                } else {
                    isClick = false
                }
                mTracker!!.addMovement(event)
                if (null != mOnWheelChangeListener) mOnWheelChangeListener!!.onWheelScrollStateChanged(
                    SCROLL_STATE_DRAGGING
                )

                // Scroll WheelPicker's content
                val move: Float = event.getY() - mLastPointY
//                if (java.lang.Math.abs(move) < 1) break
                mScrollOffsetY += move.toInt()
                mLastPointY = event.getY().toInt()
                invalidate()
            }
            MotionEvent.ACTION_UP -> {
                if (null != getParent()) getParent().requestDisallowInterceptTouchEvent(false)
//                if (isClick && !isForceFinishScroll) break
                mTracker!!.addMovement(event)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.DONUT) mTracker!!.computeCurrentVelocity(
                    1000,
                    mMaximumVelocity.toFloat()
                ) else mTracker!!.computeCurrentVelocity(1000)

                // Judges the WheelPicker is scroll or fling base on current velocity
                isForceFinishScroll = false
                val velocity = mTracker!!.getYVelocity() as Int
                if (java.lang.Math.abs(velocity) > mMinimumVelocity) {
                    mScroller.fling(0, mScrollOffsetY, 0, velocity, 0, 0, mMinFlingY, mMaxFlingY)
                    mScroller.setFinalY(
                        mScroller.getFinalY() +
                                computeDistanceToEndPoint(mScroller.getFinalY() % mItemHeight)
                    )
                } else {
                    mScroller.startScroll(
                        0, mScrollOffsetY, 0,
                        computeDistanceToEndPoint(mScrollOffsetY % mItemHeight)
                    )
                }

                // Correct coordinates
                if (!isCyclic) if (mScroller.getFinalY() > mMaxFlingY) mScroller.setFinalY(mMaxFlingY) else if (mScroller.getFinalY() < mMinFlingY) mScroller.setFinalY(
                    mMinFlingY
                )
                mHandler.post(this)
                if (null != mTracker) {
                    mTracker!!.recycle()
                    mTracker = null
                }
            }
            MotionEvent.ACTION_CANCEL -> {
                if (null != getParent()) getParent().requestDisallowInterceptTouchEvent(false)
                if (null != mTracker) {
                    mTracker!!.recycle()
                    mTracker = null
                }
            }
        }
        return true
    }

    private fun computeDistanceToEndPoint(remainder: Int): Int {
        if (java.lang.Math.abs(remainder) > mHalfItemHeight) return if (mScrollOffsetY < 0) -mItemHeight - remainder else mItemHeight - remainder else return -remainder
    }

    override fun run() {
        if (null == mData || mData!!.size == 0) return
        if (mScroller.isFinished() && !isForceFinishScroll) {
            if (mItemHeight == 0) return
            var position = (-mScrollOffsetY / mItemHeight + mSelectedItemPosition) % mData!!.size
            position = if (position < 0) position + mData!!.size else position
            if (isDebug) Log.i(TAG, position.toString() + ":" + mData!![position] + ":" + mScrollOffsetY)
            mCurrentItemPosition = position
            if (null != mOnItemSelectedListener && isTouchTriggered) mOnItemSelectedListener!!.onItemSelected(
                this,
                mData!![position], position
            )
            if (null != mOnWheelChangeListener && isTouchTriggered) {
                mOnWheelChangeListener!!.onWheelSelected(position)
                mOnWheelChangeListener!!.onWheelScrollStateChanged(SCROLL_STATE_IDLE)
            }
        }
        if (mScroller.computeScrollOffset()) {
            if (null != mOnWheelChangeListener) mOnWheelChangeListener!!.onWheelScrollStateChanged(
                SCROLL_STATE_SCROLLING
            )
            mScrollOffsetY = mScroller.getCurrY()
            postInvalidate()
            mHandler.postDelayed(this, 16)
        }
    }

    override fun setDebug(isDebug: Boolean) {
        this.isDebug = isDebug
    }

    override fun getVisibleItemCount(): Int {
        return mVisibleItemCount
    }

    override fun setVisibleItemCount(count: Int) {
        mVisibleItemCount = count
        updateVisibleItemCount()
        requestLayout()
    }

    override fun isCyclic(): Boolean {
        return isCyclic
    }

    override fun setCyclic(isCyclic: Boolean) {
        this.isCyclic = isCyclic
        computeFlingLimitY()
        invalidate()
    }

    override fun setOnItemSelectedListener(listener: OnItemSelectedListener?) {
        mOnItemSelectedListener = listener
    }

    override fun setSelectedItemPosition(position: Int) {
        setSelectedItemPosition(position, true)
    }

    override fun getSelectedItemPosition(): Int {
        return mSelectedItemPosition
    }

    open fun setSelectedItemPosition(position: Int, animated: Boolean) {
        var position = position
        isTouchTriggered = false
        if (animated && mScroller.isFinished) { // We go non-animated regardless of "animated" parameter if scroller is in motion
            val length = getData()!!.size
            var itemDifference: Int = position - mCurrentItemPosition
            if (itemDifference == 0) return
            if (isCyclic && Math.abs(itemDifference) > length / 2) { // Find the shortest path if it's cyclic
                itemDifference += if (itemDifference > 0) -length else length
            }
            mScroller.startScroll(0, mScroller.currY, 0, -itemDifference * mItemHeight)
            mHandler.post(this)
        } else {
            if (!mScroller.isFinished) mScroller.abortAnimation()
            position = Math.min(position, mData!!.size - 1)
            position = Math.max(position, 0)
            mSelectedItemPosition = position
            mCurrentItemPosition = position
            mScrollOffsetY = 0
            computeFlingLimitY()
            requestLayout()
            invalidate()
        }
    }

    override fun getCurrentItemPosition(): Int {
        return mCurrentItemPosition
    }

    override fun getData(): List<Any>? {
        return mData
    }

    override fun setData(data: List<Any>?) {
        if (null == data) throw java.lang.NullPointerException("WheelPicker's data can not be null!")
        mData = data

        // 重置位置
        if (mSelectedItemPosition > data.size - 1 || mCurrentItemPosition > data.size - 1) {
            mCurrentItemPosition = data.size - 1
            mSelectedItemPosition = mCurrentItemPosition
        } else {
            mSelectedItemPosition = mCurrentItemPosition
        }
        mScrollOffsetY = 0
        computeTextSize()
        computeFlingLimitY()
        requestLayout()
        invalidate()
    }

    override fun setSameWidth(hasSameWidth: Boolean) {
        this.hasSameWidth = hasSameWidth
        computeTextSize()
        requestLayout()
        invalidate()
    }

    override fun hasSameWidth(): Boolean {
        return hasSameWidth
    }

    override fun setOnWheelChangeListener(listener: OnWheelChangeListener?) {
        mOnWheelChangeListener = listener
    }

    override fun setMaximumWidthText(text: String?) {
        if (null == text) throw java.lang.NullPointerException("Maximum width text can not be null!")
        mMaxWidthText = text
        computeTextSize()
        requestLayout()
        invalidate()
    }

    override fun getMaximumWidthText(): String? {
        return mMaxWidthText
    }

//    override fun setMaximumWidthText(text: String?) {
//        TODO("Not yet implemented")
//    }

    override fun getMaximumWidthTextPosition(): Int {
        return mTextMaxWidthPosition
    }

    override fun setMaximumWidthTextPosition(position: Int) {
        if (!isPosInRang(position)) throw ArrayIndexOutOfBoundsException(
            "Maximum width text Position must in [0, " +
                    mData!!.size + "), but current is " + position
        )
        mTextMaxWidthPosition = position
        computeTextSize()
        requestLayout()
        invalidate()
    }

    override fun getSelectedItemTextColor(): Int {
        return mSelectedItemTextColor
    }

    override fun setSelectedItemTextColor(color: Int) {
        mSelectedItemTextColor = color
        computeCurrentItemRect()
        invalidate()
    }

    override fun getItemTextColor(): Int {
        return mItemTextColor
    }

    override fun setItemTextColor(color: Int) {
        mItemTextColor = color
        invalidate()
    }

    override fun getItemTextSize(): Int {
        return mItemTextSize
    }

    override fun setItemTextSize(size: Int) {
        mItemTextSize = size
        mPaint!!.textSize = mItemTextSize.toFloat()
        computeTextSize()
        requestLayout()
        invalidate()
    }

    override fun getItemSpace(): Int {
        return mItemSpace
    }

    override fun setItemSpace(space: Int) {
        mItemSpace = space
        requestLayout()
        invalidate()
    }

    override fun setIndicator(hasIndicator: Boolean) {
        this.hasIndicator = hasIndicator
        computeIndicatorRect()
        invalidate()
    }

    override fun hasIndicator(): Boolean {
        return hasIndicator
    }

    override fun getIndicatorSize(): Int {
        return mIndicatorSize
    }

    override fun setIndicatorSize(size: Int) {
        mIndicatorSize = size
        computeIndicatorRect()
        invalidate()
    }

    override fun getIndicatorColor(): Int {
        return mIndicatorColor
    }

    override fun setIndicatorColor(color: Int) {
        mIndicatorColor = color
        invalidate()
    }

    override fun setCurtain(hasCurtain: Boolean) {
        this.hasCurtain = hasCurtain
        computeCurrentItemRect()
        invalidate()
    }

    override fun hasCurtain(): Boolean {
        return hasCurtain
    }

    override fun getCurtainColor(): Int {
        return mCurtainColor
    }

    override fun setCurtainColor(color: Int) {
        mCurtainColor = color
        invalidate()
    }

    override fun setAtmospheric(hasAtmospheric: Boolean) {
        this.hasAtmospheric = hasAtmospheric
        invalidate()
    }

    override fun hasAtmospheric(): Boolean {
        return hasAtmospheric
    }

    override fun isCurved(): Boolean {
        return isCurved
    }

    override fun setCurved(isCurved: Boolean) {
        this.isCurved = isCurved
        requestLayout()
        invalidate()
    }

    override fun getItemAlign() = mItemAlign

    override fun setItemAlign(align: Int) {
        mItemAlign = align
        updateItemTextAlign()
        computeDrawnCenter()
        invalidate()
    }

    override fun getTypeface(): Typeface? {
        return mPaint?.typeface
    }

    override fun setTypeface(tf: Typeface?) {
        if (null != mPaint) mPaint.typeface = tf
        computeTextSize()
        requestLayout()
        invalidate()
    }

    interface OnItemSelectedListener {
        fun onItemSelected(picker: WheelPicker?, data: Any?, position: Int)
    }

    interface OnWheelChangeListener {
        fun onWheelScrolled(offset: Int)
        fun onWheelSelected(position: Int)
        fun onWheelScrollStateChanged(state: Int)
    }

    companion object {
        val SCROLL_STATE_IDLE = 0
        val SCROLL_STATE_DRAGGING = 1
        val SCROLL_STATE_SCROLLING = 2
        val ALIGN_CENTER = 0
        val ALIGN_LEFT = 1
        val ALIGN_RIGHT = 2
        private val TAG: String = WheelPicker::class.java.getSimpleName()
//        fun setMaximumWidthText(wheelPicker: WheelPicker, text: String) {
//            if (null == text) throw NullPointerException("Maximum width text can not be null!")
//            wheelPicker.mMaxWidthText = text
//            wheelPicker.computeTextSize()
//            wheelPicker.requestLayout()
//            wheelPicker.invalidate()
//        }
    }


}