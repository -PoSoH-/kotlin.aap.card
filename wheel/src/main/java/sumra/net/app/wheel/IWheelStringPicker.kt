package sumra.net.app.wheel

interface IWheelMonthPicker {
    fun getSelectedMonth(): Int
    fun setSelectedMonth(month: Int)
    fun getCurrentMonth(): Int



//    var selectedMonth: Int
//    val currentMonth: Int
}