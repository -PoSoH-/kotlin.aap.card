package sumra.net.app.wheel

import android.content.Context
import android.util.AttributeSet
import sumra.net.wheel.infinity_card.fragments.WheelPicker
import java.util.*
import kotlin.collections.ArrayList

class WheelMonthPicker constructor (context: Context, attrs: AttributeSet?) : WheelPicker(context, attrs), IWheelMonthPicker {

    private var mSelectedMonth = 0

    constructor(context: Context) : this(context, null)

    init{
        val data: MutableList<Int?> = ArrayList()
        for (i in 1..12) data.add(i)
        this.setData(data as List<Any>)
        mSelectedMonth = Calendar.getInstance().get(Calendar.MONTH) + 1
        updateSelectedYear()
    }

    private fun updateSelectedYear() {
        setSelectedItemPosition(mSelectedMonth - 1)
    }

    override fun setData(data: List<Any>?) {
        throw UnsupportedOperationException("You can not invoke setData in WheelMonthPicker")
    }

    override fun getSelectedMonth(): Int {
        return mSelectedMonth
    }

    override fun setSelectedMonth(month: Int) {
        mSelectedMonth = month
        updateSelectedYear()
    }

    override fun getCurrentMonth(): Int {
        return Integer.valueOf(getData()?.get(getCurrentItemPosition()).toString())
    }

}