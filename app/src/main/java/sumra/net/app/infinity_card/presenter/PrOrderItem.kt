package sumra.net.app.infinity_card.presenter

import kotlinx.coroutines.*
import moxy.MvpPresenter
import sumra.net.app.infinity_card.models.responses.Order
import sumra.net.app.infinity_card.models.responses._jsonRespOrder
import sumra.net.app.infinity_card.views.IViewOrderItem

class PrOrderItem : MvpPresenter<IViewOrderItem>() {

    //    private val mRepository = RepositoryOrderItem()
    private var mOrder: Order? = null

    fun fetchOrderItem(orderId: Long) {
        viewState.showProgress(true)
        CoroutineScope(Dispatchers.IO).launch {
            val resp = _jsonRespOrder(
                true,
                null,
                Order(
                    id = orderId,
                    createAt = "2020-12-20 13:17:54",
                    updateAt = "2020-12-24 09:45:12",
                    totalPrice = "1248.05",
                    cardTypeId = 9875125478,
                    currency = "usd",
                    status = 4,
                    count = 1
                )
            )

            delay(2_500L)
            withContext(Dispatchers.Main) {
                if (resp.success) {
                    mOrder = resp.data as Order
                    withContext(Dispatchers.Main) {
                        viewState.fetchOrderSuccess()
                    }
                } else {
                    withContext(Dispatchers.Main) {
                        viewState.fetchOrderFailure(resp.error)
                    }
                }
                viewState.showProgress(false)
            }
        }
    }

    fun getOrder() = mOrder
}