package sumra.net.app.infinity_card.adapters.holders

import android.text.Html
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.cell_card_series_show.view.*
import sumra.net.app.infinity_card.R
import sumra.net.app.infinity_card.enums.ECardType
import sumra.net.app.infinity_card.enums.ECurrency
import sumra.net.app.infinity_card.interfaces.ISeriesItemSelectListener
import sumra.net.app.infinity_card.models.CardTypeItem
import sumra.net.app.infinity_card.utils.UString

class HolderCardSeries(val cell: View): RecyclerView.ViewHolder(cell) {

    fun bindSeries(bind: CardTypeItem, listener: ISeriesItemSelectListener){

        cell.image_card_background.setImageResource(ECardType.compareType(bind.id))
        cell.cell_infinity_card_series.text = cell.context.resources.getString(R.string.not_series_name)
        bind.series?.let{
            cell.cell_infinity_card_series.text = it.name
        }
        cell.cell_infinity_card_type.text = bind.name
        val txt = cell.context.resources.getString(R.string.infinity_card_series_type_justify)
        cell.series_card_description.text = Html.fromHtml(txt.replace("#", bind.description), Html.FROM_HTML_MODE_LEGACY)
        cell.series_card_start_price.text = bind.priceStart.toString()
        cell.series_card_final_price.text = bind.priceFinish.toString()
        cell.series_card_start_price_currency.text = ECurrency.compareCurrencyByName(bind.currency)
        cell.series_card_final_price_currency.text = ECurrency.compareCurrencyByName(bind.currency)
        cell.series_card_update_date.text = cell.context.resources.getString(R.string.infinity_card_series_not_date)
        bind.series?.let{
            cell.series_card_update_date.text = UString.setupDateByTime(it.updatedAt)
        }
        cell.series_card_validity.text = bind.months.toString()
    }
}