package sumra.net.app.infinity_card.views

import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(value = AddToEndSingleStrategy::class)
interface IViewCard : IViewCommon {
    fun fetchCardsSuccess()
    fun fetchCardsFailure(error: String)
}