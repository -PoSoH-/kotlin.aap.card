package sumra.net.app.infinity_card.utils.extention

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.view.*
import sumra.net.app.infinity_card.R
import sumra.net.app.infinity_card.widgets.CustomSnackBar.CustomSnackBar

// show/hide simple left
fun View.showEmpty(){ this.visibility = View.VISIBLE }
fun View.hideEmpty(){ this.visibility = View.GONE }

// show/hide right to left
fun View.showRtoL(){
    this.animation = AnimationUtils.loadAnimation(context, R.anim.anim_show_r_to_l)
    this.animate()
    this.visibility = View.VISIBLE
}

fun View.hideRtoL(){
    this.animation = AnimationUtils.loadAnimation(context, R.anim.anim_hide_r_to_l)
    this.animate()
    this.visibility = View.GONE
}

// show/hide left to right
fun View.showLtoR(){
    this.animation = AnimationUtils.loadAnimation(context, R.anim.anim_show_l_to_r)
    this.animate()
    this.visibility = View.VISIBLE
}

fun View.hideLtoR(){
    this.animation = AnimationUtils.loadAnimation(context, R.anim.anim_hide_l_to_r)
    this.animate()
    this.visibility = View.GONE
}

// show/hide down to up
fun View.showDtoU(){
    this.animation = AnimationUtils.loadAnimation(context, R.anim.anim_show_d_to_u)
    this.animate()
    this.visibility = View.VISIBLE
}

fun View.hideUtoD(){
    this.animation = AnimationUtils.loadAnimation(context, R.anim.anim_hide_u_to_d)
    this.animate()
    this.visibility = View.GONE
}

// show/hide up to down
fun View.hideDtoU(){
    this.animation = AnimationUtils.loadAnimation(context, R.anim.anim_hide_d_to_u)
    this.animate()
    this.visibility = View.GONE
}

fun View.showUtoD(){
    this.animation = AnimationUtils.loadAnimation(context, R.anim.anim_show_u_to_d)
    this.animate()
    this.visibility = View.VISIBLE
}
// show/hide from scale view of screen
fun View.showScaleCtoO(){
    this.animation = AnimationUtils.loadAnimation(context, R.anim.anim_scale_show_c_to_o)
    this.animate()
    this.visibility = View.VISIBLE
}

fun View.hideScaleOtoC(){
    this.animation = AnimationUtils.loadAnimation(context, R.anim.anim_scale_hide_o_to_c)
    this.animate()
    this.visibility = View.GONE
}

// keyboard hide
fun View.hideKeyboard(){
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}

internal fun View?.findSuitableParent(): ViewGroup? {
    var view = this
    var fallback: ViewGroup? = null
    do {
        if (view is CoordinatorLayout) {
            // We've found a CoordinatorLayout, use it
            return view
        } else if (view is FrameLayout) {
            if (view.id == android.R.id.content) {
                // If we've hit the decor content view, then we didn't find a CoL in the
                // hierarchy, so use it.
                return view
            } else {
                // It's not the content view but we'll use it as our fallback
                fallback = view
            }
        }

        if (view != null) {
            // Else, we will loop and crawl up the view hierarchy and try to find a parent
            val parent = view.parent
            view = if (parent is View) parent else null
        }
    } while (view != null)

    // If we reach here then we didn't find a CoL or a suitable content view so we'll fallback
    return fallback
}

fun View.showSnackBarError(message:String){
    CustomSnackBar.make(
          view = this
        , message = message
        , duration = Snackbar.LENGTH_SHORT
        , listener = null
        , action_lable = ""
        , typeMessage = CustomSnackBar.EMessageType.ERROR
        , header = resources.getString(R.string.infinity_card_name_header)
    )?.show()
}

fun View.showSnackBarSuccess(message:String){
    CustomSnackBar.make(
          view = this
        , message = message
        , duration = Snackbar.LENGTH_SHORT
        , listener = null
        , action_lable = ""
        , typeMessage = CustomSnackBar.EMessageType.SUCCESS
        , header = resources.getString(R.string.infinity_card_name_header)
    )?.show()
}

fun View.showSnackBarInfo(message:String){
    CustomSnackBar.make(
          view = this
        , message = message
        , duration = Snackbar.LENGTH_SHORT
        , listener = null
        , action_lable = ""
        , typeMessage = CustomSnackBar.EMessageType.OTHER
        , header = resources.getString(R.string.infinity_card_name_header)
    )?.show()
}
