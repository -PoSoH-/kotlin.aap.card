package sumra.net.app.infinity_card.oAuth2tests

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.content.pm.PackageManager.NameNotFoundException
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.loader.app.LoaderManager
import androidx.loader.content.AsyncTaskLoader
import androidx.loader.content.Loader
import net.openid.appauth.browser.BrowserDescriptor
import net.openid.appauth.browser.BrowserSelector
import sumra.net.app.infinity_card.R
import java.util.*


class BrowserSelectionAdapter : BaseAdapter() {

    private val LOADER_ID = 101

    private var mContext: Context? = null
    private var mBrowsers: ArrayList<BrowserInfo?>? = null

    /**
     * Creates the adapter, using the loader manager from the specified activity.
     */
    fun BrowserSelectionAdapter(activity: Activity) {
        mContext = activity
        initializeItemList()
//        activity.loaderManager.initLoader(
//            LOADER_ID,
//            null,
//            BrowserLoaderCallbacks()
//        )
    }

    class BrowserInfo(val mDescriptor: BrowserDescriptor, val mLabel: CharSequence, val mIcon: Drawable?)

    override fun getCount(): Int {
        return mBrowsers!!.size
    }

    override fun getItem(position: Int): BrowserInfo? {
        return mBrowsers!![position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View, parent: ViewGroup?): View {
        var convertView = convertView
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext)
                .inflate(R.layout.browser_selector_layout, parent, false)
        }
        val info = mBrowsers!![position]
        val labelView = convertView.findViewById<View>(R.id.browser_label) as TextView
        val iconView = convertView.findViewById<View>(R.id.browser_icon) as ImageView
        if (info == null) {
            labelView.setText("Default browser") //R.string.browser_appauth_default_label)
            iconView.setImageResource(96) // R.drawable.appauth_96dp)
        } else {
            var label = info.mLabel
            if (info.mDescriptor.useCustomTab) {
                label = String.format("Custom tab"/*mContext!!.getString(R.string.custom_tab_label)*/, label)
            }
            labelView.text = label
            iconView.setImageDrawable(info.mIcon)
        }
        return convertView
    }

    private fun initializeItemList() {
        mBrowsers = ArrayList()
        mBrowsers!!.add(null)
    }

//    private class BrowserLoaderCallbacks : LoaderManager.LoaderCallbacks<List<BrowserInfo?>?> {
//
//        override fun onCreateLoader(id: Int, args: Bundle?): Loader<List<BrowserInfo>> {
//            return BrowserLoader(mContext)
//        }
//
//        override fun onLoadFinished(
//            loader: Loader<List<BrowserInfo?>?>?,
//            data: List<BrowserInfo?>?
//        ) {
//            initializeItemList()
//            mBrowsers.addAll(data)
//            notifyDataSetChanged()
//        }
//
//        override fun onLoaderReset(loader: Loader<List<BrowserInfo?>?>?) {
//            initializeItemList()
//            notifyDataSetChanged()
//        }
//    }

    private class BrowserLoader internal constructor(context: Context) :
        AsyncTaskLoader<List<BrowserInfo?>?>(context) {
        private var mResult: List<BrowserInfo>? = null
        override fun loadInBackground(): List<BrowserInfo> {
            val descriptors = BrowserSelector.getAllBrowsers(getContext())
            val infos = ArrayList<BrowserInfo>(descriptors.size)
            val pm: PackageManager = getContext().getPackageManager()
            for (descriptor in descriptors) {
                try {
                    val info = pm.getApplicationInfo(descriptor.packageName, 0)
                    val label = pm.getApplicationLabel(info)
                    val icon = pm.getApplicationIcon(descriptor.packageName)
                    infos.add(BrowserInfo(descriptor, label, icon))
                } catch (e: NameNotFoundException) {
                    e.printStackTrace()
                    infos.add(BrowserInfo(descriptor, descriptor.packageName, null))
                }
            }
            return infos
        }

        fun deliverResult(data: List<BrowserInfo>?) {
            if (isReset()) {
                mResult = null
                return
            }
            mResult = data
            super.deliverResult(mResult)
        }

        protected override fun onStartLoading() {
            if (mResult != null) {
                deliverResult(mResult)
            }
            forceLoad()
        }

        protected override fun onReset() {
            mResult = null
        }

        override fun onCanceled(data: List<BrowserInfo?>?) {
            mResult = null
            super.onCanceled(data)
        }
    }
}
