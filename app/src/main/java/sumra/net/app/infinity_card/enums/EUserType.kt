package sumra.net.app.infinity_card.enums

enum class EUserType (val typeId: Long){
    CLIENT (typeId = 1L),
    AGENT (typeId = 2L),
    MODERATOR (typeId = 3L);

    companion object {
        fun compareUserType(userTypeId: Long) :EUserType{
            return when(userTypeId){
                AGENT.typeId -> AGENT
                else -> CLIENT
            }
        }
    }
}