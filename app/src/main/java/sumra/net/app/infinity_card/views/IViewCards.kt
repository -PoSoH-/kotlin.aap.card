package sumra.net.app.infinity_card.views

import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(value = AddToEndSingleStrategy::class)
interface IViewCards : IViewCommon{

    fun loadAllCardTypesSuccess()
    fun loadAllCardTypesFailure(error: String?)
}