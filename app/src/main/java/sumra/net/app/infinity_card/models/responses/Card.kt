package sumra.net.app.infinity_card.models.responses


import com.google.gson.annotations.SerializedName

data class Card(
    @SerializedName("activate_before")
    val activateBefore: String,
    @SerializedName("activated_at")
    val activatedAt: String,
    @SerializedName("activation_code")
    val activationCode: String,
    @SerializedName("balance")
    val balance: String,
    @SerializedName("balance_at")
    val balanceAt: String,
    @SerializedName("code")
    val code: String,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("creator_id")
    val creatorId: Int,
    @SerializedName("currency")
    val currency: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("number")
    val number: String,
    @SerializedName("numberFormatted")
    val numberFormatted: String,
    @SerializedName("owner_id")
    val ownerId: Int,
    @SerializedName("status")
    val status: Int,
    @SerializedName("type_id")
    val typeId: Int,
    @SerializedName("updated_at")
    val updatedAt: String
)