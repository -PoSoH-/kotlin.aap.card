package sumra.net.app.infinity_card.widgets.CustomSnackBar

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.BaseTransientBottomBar
import sumra.net.app.infinity_card.R
import sumra.net.app.infinity_card.utils.UtilLog
import sumra.net.app.infinity_card.utils.extention.findSuitableParent
import sumra.net.app.infinity_card.utils.extention.showEmpty

class CustomSnackBar (
    parent: ViewGroup,
    content: CustomSnackBarView
) : BaseTransientBottomBar<CustomSnackBar>(parent, content, content) {


    init {
        getView().setBackgroundColor(ContextCompat.getColor(view.context, android.R.color.transparent))
        getView().setPadding(0, 0, 0, 0)
    }

    companion object {

        fun make(
            view: View
            , typeMessage: EMessageType = EMessageType.OTHER
            , header: String?
            , message : String
            , duration : Int
            , listener : View.OnClickListener?
//            , icon : Int
            , action_lable : String? ): CustomSnackBar?{
//            , bg_color : Int): CustomSnackBar? {

            // First we find a suitable parent for our custom view
            val parent = view.findSuitableParent() ?: throw IllegalArgumentException(
                "No suitable parent found from the given view. Please provide a valid view."
            )

            // We inflate our custom view
            try{
                val customView = LayoutInflater.from(view.context).inflate(
                    R.layout.lay_custom_snack_bar_inflation,
                    parent,
                    false
                ) as CustomSnackBarView
                // We create and return our Snackbar
                when(typeMessage){
                    EMessageType.ERROR -> {
                        customView.tvHeader.setTextColor(Color.RED)
                        customView.tvMessage.setTextColor(Color.RED)
                    }
                    EMessageType.SUCCESS -> {
                        customView.tvHeader.setTextColor(Color.GREEN)
                        customView.tvMessage.setTextColor(Color.GREEN)}
                    EMessageType.OTHER -> {}
                }

                customView.tvMessage.text = message

                header?.let{
                    customView.tvHeader.text = header
                    customView.tvHeader.showEmpty()
                }

                action_lable?.let {
                    customView.tvAction.text = action_lable
                    customView.tvAction.setOnClickListener {
                        listener?.onClick(customView.tvAction)
                    }
                }
//                customView.imLeft.setImageResource(icon)
//                customView.layRoot.setBackgroundColor(bg_color)

                return CustomSnackBar(
                    parent,
                    customView).setDuration(duration)
            }catch ( e: Exception){
                UtilLog.error("exception ",e.message.toString())
            }

            return null
        }

    }
    enum class EMessageType{
        ERROR,
        SUCCESS,
        OTHER
    }
}