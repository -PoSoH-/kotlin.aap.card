package sumra.net.app.infinity_card.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import kotlinx.android.synthetic.main.frg_card_types_screen.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter
import sumra.net.app.infinity_card.AppCard
import sumra.net.app.infinity_card.R
import sumra.net.app.infinity_card.adapters.viewPagers.AdapterCardTypesPage
import sumra.net.app.infinity_card.presenter.PrCards
import sumra.net.app.infinity_card.utils.UtilLog
import sumra.net.app.infinity_card.views.IViewCards

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FrgCardTypesScreen : MvpAppCompatFragment(R.layout.frg_card_types_screen)
    , IViewCards {

    private val TAG = this::class.java.simpleName

    @InjectPresenter
    lateinit var mPresenterCards : PrCards

    private lateinit var mAdapterPage : AdapterCardTypesPage

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        AppCard.hideBottomNavView()
        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true /* enabled by default */) {
                override fun handleOnBackPressed() {
                    UtilLog.error(TAG, "BackPressed -> Click")
                    onBackPressed()
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mAdapterPage = AdapterCardTypesPage(requireActivity()).apply {
//            items = listOf(0)
        }

        view_pager_card_types.adapter = mAdapterPage
        mPresenterCards.fetchCardTypes("Bearer token-codes...")

    }

    private fun onBackPressed(){
//        AppCard.showBottomNavView()
        AppCard.getNavigation().popBackStack()
    }

    override fun loadAllCardTypesSuccess() {
        mAdapterPage.apply {
            items = mPresenterCards.getCountMapCollections()
        }
    }

    override fun loadAllCardTypesFailure(error: String?) {
        when(error){
            null -> Toast.makeText(requireContext(), "NOT KNOW ERROR...", Toast.LENGTH_SHORT).show()
            else -> Toast.makeText(requireContext(), error, Toast.LENGTH_SHORT).show()
        }
    }

    override fun showProgress(state: Boolean) {
        when(state){
            true -> AppCard.showProgress() // Toast.makeText(requireContext(), "SHOW TYPE PROGRESS", Toast.LENGTH_SHORT).show()
            else -> AppCard.hideProgress() // Toast.makeText(requireContext(), "HIDE TYPE PROGRESS", Toast.LENGTH_SHORT).show()
        }
    }
}