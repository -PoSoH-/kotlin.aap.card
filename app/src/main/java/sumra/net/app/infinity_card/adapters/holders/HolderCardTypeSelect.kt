package sumra.net.app.infinity_card.adapters.holders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.cell_card_type_select_show.view.*
import sumra.net.app.infinity_card.R
import sumra.net.app.infinity_card.enums.ECurrency
import sumra.net.app.infinity_card.interfaces.ICardTypeSelectListener
import sumra.net.app.infinity_card.models.Balance
import sumra.net.app.infinity_card.models.CardTypeItem
import sumra.net.app.infinity_card.utils.UString
import sumra.net.app.infinity_card.utils.extention.*

class HolderCardTypeSelect(val cell: View) : RecyclerView.ViewHolder(cell) {
    fun bindCard(cardType: CardTypeItem, listener: ICardTypeSelectListener, balance: Balance){

        cell.cell_infinity_card_series.text = cell.context.resources.getString(R.string.not_series_name)
        cardType.series?.let{
            cell.cell_infinity_card_series.text = it.name
        }
        cell.cell_infinity_card_series_type.text = cardType.name
        cell.series_card_validity.text = cardType.months.toString()
        cell.series_card_start_price.text = cardType.priceStart
        cell.series_card_start_price_currency.text = ECurrency.compareCurrencyByName(cardType.currency)

        cell.check_box_selected_card.apply {
            setOnClickListener { view ->
                if(UString.getBalance(balance) < cardType.priceStart.toDouble()&&cardType.countSend > 0) {
                    cardType.countSend--
                    listener.cardItemTypeSelect(cardType, false)
                }else{
                    if(UString.getBalance(balance) >= cardType.priceStart.toDouble()){
                        if(cardType.countSend > 0){
                            cardType.countSend--
                            listener.cardItemTypeSelect(cardType, false)
                        }else {
                            cardType.countSend++
                            listener.cardItemTypeSelect(cardType, true)
                        }
                    }
                }
            }
        }

        if(balance != null) {
            if (UString.getBalance(balance) >= cardType.priceStart.toDouble()) {
                cell.cell_infinity_card_dont_select.hideDtoU()
            } else {
                cell.cell_infinity_card_dont_select.showUtoD()
            }
        }

        if(cardType.countSend > 0){
            cell.check_box_selected_card_disable.showEmpty()
            cell.check_box_selected_card_enable .hideEmpty()
        }else{
            cell.check_box_selected_card_disable.hideEmpty()
            cell.check_box_selected_card_enable .showEmpty()
        }
    }
}