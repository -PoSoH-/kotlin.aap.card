package sumra.net.app.infinity_card.enums

import sumra.net.app.infinity_card.utils.UString

enum class ECurrency(val id: Long, val compare: String, val curType: String) {
    CUR(id = 0L, compare = "cur", curType = "\u00A4"),
    DIV(id = 0L, compare = "div", curType = "d"),
    USA(id = 1L, compare = "usd", curType = "\u0024"),
    EUR(id = 2L, compare = "eur", curType = "\u20ac"),
    GBP(id = 3L, compare = "gbp", curType = "\u00a3");

    companion object{
        fun configureResult(compare: String, balance: String) : String{
            return when(compare){
                DIV.compare  -> "${DIV.curType} ${UString.setupBalanceShow(balance)}"
                USA.compare  -> "${USA.curType} ${UString.setupBalanceShow(balance)}"
                EUR.compare  -> "${EUR.curType} ${UString.setupBalanceShow(balance)}"
                GBP.compare  -> "${GBP.curType} ${UString.setupBalanceShow(balance)}"
                else         -> "${CUR.curType} ${UString.setupBalanceShow(balance)}"
            }
        }
        fun compareCurrencyByName(currency: String?) : String{
//            var cur = CUR.compare
//            currency?.let { cur = it}
            return when (currency) {
                DIV.compare  -> DIV.curType
                USA.compare  -> USA.curType
                EUR.compare  -> EUR.curType
                GBP.compare  -> GBP.curType
                else         -> CUR.curType
            }
        }
        fun compareCurrencyById(id: Long) : String{
            return when (id) {
                DIV.id  -> DIV.curType
                USA.id  -> USA.curType
                EUR.id  -> EUR.curType
                GBP.id  -> GBP.curType
                else    -> CUR.curType
            }
        }
    }
}

//const val symUSD = "\u0024"
//const val symEUR = "\u20ac"
//const val symGBP = "\u00a3"
//const val symBitCoin  = "\u20BF"