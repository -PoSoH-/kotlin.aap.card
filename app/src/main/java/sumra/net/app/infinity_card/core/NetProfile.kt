package sumra.net.app.infinity_card.core

import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.Response
import org.apache.http.HttpException
import sumra.net.app.infinity_card.BuildConfig

object NetProfile {

    private val localHostUrl = "http://192.168.188.137:8095/v1/infinity"   //"https://api.sumra.net/files/1/v1/files"
    private val currentUser = 2

    fun getClientUsers(token: String):String {
        val host = BuildConfig.SUMRA_API_INITY_URL.toHttpUrlOrNull()!!
        val httpUrl = HttpUrl.Builder().apply {
            scheme(EHost.HTTPS.value)
            host(host.host)
//            addPathSegment(BuildConfig.SUMRA_VER_INTY_URL)
            addPathSegment("infinity")
            addPathSegment("users")
            addPathSegment("1")
        }
            .build()
        try{
//            val response = OkHttpClient.builder()
//                .newCall(NetworkCore.GET_RequestContentByToken(httpUrl.toString(), token)).execute()
            val response = OkHttpClient.builder()
                .newCall(NetworkCore.GET_RequestContentByUserId("${localHostUrl}/users/${currentUser}")).execute()
            if(response.isSuccessful){
                return NetworkCore.resultSuccess(response)
            }else{
                return NetworkCore.resultFailure(response)
            }
        } catch (e : HttpException) {
            return NetworkCore.resultFailure(e as Response)
        }
    }

    fun patchProfileUser(token: String, dataBody: String):String {
        val host = BuildConfig.SUMRA_API_INITY_URL.toHttpUrlOrNull()!!
//        val host = "192.168.188.137:8095".toHttpUrlOrNull()!!
        val httpUrl = HttpUrl.Builder().apply {
            scheme(EHost.HTTPS.value)
            host(host.host)
//            addPathSegment(BuildConfig.SUMRA_VER_INTY_URL)
            addPathSegment("infinity")
            addPathSegment("users")
            addPathSegment("1")
        }
            .build()
        try{
//            val response = OkHttpClient.builder()
//                .newCall(NetworkCore.PATCH_RequestContent(
//                    url = httpUrl.toString(),
//                    token = token,
//                    dataBody = dataBody)).execute()
            val response = OkHttpClient.builder()
                .newCall(NetworkCore.PATCH_RequestContentByUserId(
                    url = "${localHostUrl}/users/${currentUser}",
                    token = token,
                    dataBody = dataBody)).execute()
            if(response.isSuccessful){
                return NetworkCore.resultSuccess(response)
            }else{
                return NetworkCore.resultFailure(response)
            }
        } catch (e : HttpException) {
            return NetworkCore.resultFailure(e as Response)
        }
    }
}