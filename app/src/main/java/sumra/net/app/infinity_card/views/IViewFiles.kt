package sumra.net.app.infinity_card.views

import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(value = AddToEndSingleStrategy::class)
interface IViewFiles : IViewCommon {

    fun uploadFileSuccess()
    fun uploadFileFailure(error: String?)

    fun patchFileSuccess()
    fun patchFileFailure(error: String?)

    fun deleteFileSuccess()
    fun deleteFileFailure(error: String?)
}