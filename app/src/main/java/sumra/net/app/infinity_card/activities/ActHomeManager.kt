//package sumra.net.app.infinity_card.activities
//
//import android.accounts.Account
//import android.accounts.AccountManager
//import android.accounts.AccountManagerCallback
//import android.net.Uri
//import android.os.Bundle
//import android.text.TextUtils
//import android.util.Log
//import kotlinx.coroutines.CoroutineScope
//import kotlinx.coroutines.Dispatchers
//import kotlinx.coroutines.launch
//import moxy.MvpAppCompatActivity
//import sumra.net.app.infinity_card.R
//import sumra.net.app.infinity_card.openIdmanagers.Config
//import sumra.net.app.infinity_card.openIdmanagers.authenticator.AuthenticatorActivity
//import sumra.net.app.infinity_card.utils.UtilLog
//import java.time.Instant
//
//class ActHomeManager : MvpAppCompatActivity(R.layout.act_home_manager) {
//
//    val TAG = this::class.java.simpleName
//
////    val am = AccountManager.get(this)
////    val options = Bundle()
//
//    companion object {
//        val KEY_AUTH_URL = "umra.net.app.infinity_card.KEY_AUTH_URL"
//        val KEY_IS_NEW_ACCOUNT = "umra.net.app.infinity_card.KEY_IS_NEW_ACCOUNT"
//        val KEY_ACCOUNT_OBJECT = "umra.net.app.infinity_card.KEY_ACCOUNT_OBJECT"
//    }
//
//    private var accountManager: AccountManager? = null
//    private var account: Account? = null
//    private var isNewAccount = false
//    private var extras : Bundle? = null
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        accountManager = AccountManager.get(this)
//        extras = if(intent.extras == null) Bundle() else intent.extras
//    }
//
//    override fun onStart() {
//        super.onStart()
//
//        // Are we supposed to create a new account or renew the authorisation of an old one?
//        isNewAccount = extras!!.getBoolean(AuthenticatorActivity.KEY_IS_NEW_ACCOUNT, false)
//
//        // In case we're renewing authorisation, we also got an Account object that we're supposed
//        // to work with.
//        account = extras!!.getParcelable(AuthenticatorActivity.KEY_ACCOUNT_OBJECT)
//
//        // Fetch the authentication URL that was given to us by the calling activity
//        val authUrl = extras!!.getString(AuthenticatorActivity.KEY_AUTH_URL)
//        UtilLog.info(TAG, "Initiated activity for getting authorisation with URL $authUrl.")
//
////        when (Config.flowType) {
////            Config.Flows.Implicit -> {
//////                if (!TextUtils.isEmpty(extractedFragment)) {
////////                                val task = CreateIdTokenFromFragmentPartTask()
////////                                task.execute(extractedFragment)
//////
//////
//////                } else {
//////                    Log.e(
//////                        TAG, String.format(
//////                            "urlString '%1\$s' doesn't contain fragment part; can't extract tokens",
//////                            urlString
//////                        )
//////                    )
//////                }
////            }
////            Config.Flows.Hybrid -> {
////                if (!TextUtils.isEmpty(extractedFragment)) {
//////                                val task = RequestIdTokenFromFragmentPartTask()
//////                                task.execute(extractedFragment)
//////                    val fragmentPart = args[0]
////                    val tokenExtrationUrl = Uri.Builder().encodedQuery(fragmentPart).build()
////                    val idToken = tokenExtrationUrl.getQueryParameter("id_token")
////                    val authCode = tokenExtrationUrl.getQueryParameter("code")
////                    if (TextUtils.isEmpty(idToken) || TextUtils.isEmpty(authCode)) {
////                        return
////                    } else {
////                        val response: IdTokenResponse
////                        Log.i("TAG", "Requesting access_token with AuthCode : $authCode")
////                        response = try {
////                            OIDCUtils.requestTokens(
////                                Config.tokenServerUrl,
////                                Config.redirectUrl,
////                                Config.clientId,
////                                Config.clientSecret,
////                                authCode
////                            )
////                        } catch (e: IOException) {
////                            Log.e("TAG", "Could not get response.")
////                            e.printStackTrace()
////                            return false
////                        }
////                        if (isNewAccount) {
////                            createAccount(response)
////                        } else {
////                            setTokens(response)
////                        }
////                    }
////                    return true
////                }
////
////                override fun onPostExecute(wasSuccess: Boolean) {
////                    if (wasSuccess) {
////                        // The account manager still wants the following information back
////                        val intent = Intent()
////                        intent.putExtra(AccountManager.KEY_ACCOUNT_NAME, account.name)
////                        intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, account.type)
////                        setAccountAuthenticatorResult(intent.extras)
////                        setResult(RESULT_OK, intent)
////                        finish()
////                    } else {
////                        showErrorDialog("Could not get ID Token.")
////
////
////
////
////
////
////
////
////                    CoroutineScope(Dispatchers.IO).launch {
////
////                    }
////                } else {
////                    Log.e(TAG,"urlString '%1\$s' doesn't contain fragment part; can't request tokens") // $urlString")
////                }
////            }
////            Config.Flows.AuthorizationCode -> {
////                // The URL will contain a `code` parameter when the user has been authenticated
////                if (parameterNames.contains("code")) {
////                    val authToken = url.getQueryParameter("code")
////
////                    // Request the ID token
//////                                val task = RequestIdTokenTask()
//////                                task.execute(authToken)
////
////
////
////                } else {
////                    Log.e(
////                        TAG, String.format(
////                            "urlString '%1\$s' doesn't contain code param; can't extract authCode",
////                            urlString
////                        )
////                    )
////                }
////            }
////            else -> {
//////                if (parameterNames.contains("code")) {
//////                    val authToken = url.getQueryParameter("code")
////////                                val task = RequestIdTokenTask()
////////                                task.execute(authToken)
//////
//////
//////                } else {
//////                    Log.e(
//////                        TAG, String.format(
//////                            "urlString '%1\$s' doesn't contain code param; can't extract authCode",
//////                            urlString
//////                        )
//////                    )
//////                }
////            }
////        }
//
//    // else : should be an intermediate url, load it and keep going
//
//
////        am.getAuthToken(
////            myAccount,
////            "Manage your tasks",
////            options,
////            this,
////            AccountManagerCallback {  },
////            OnError)
//
//    }
//
//}