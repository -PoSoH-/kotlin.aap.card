package sumra.net.app.infinity_card.models

import com.google.gson.annotations.SerializedName

data class _jsonTariffs(
    @SerializedName("current_page")
    val currentPage: Int,
    @SerializedName("data")
    val tariffs: List<Tariff>,
    @SerializedName("fields")
    val fields: List<TariffMap>,
    @SerializedName("first_page_url")
    val firstPageUrl: String,
    @SerializedName("from")
    val from: Int,
    @SerializedName("last_page")
    val lastPage: Int,
    @SerializedName("last_page_url")
    val lastPageUrl: String,
    @SerializedName("next_page_url")
    val nextPageUrl: String,
    @SerializedName("path")
    val path: String,
    @SerializedName("per_page")
    val perPage: Int,
    @SerializedName("prev_page_url")
    val prevPageUrl: String,
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("to")
    val to: Int,
    @SerializedName("total")
    val total: Int
)

data class Tariff(
    @SerializedName("description")
    val description: String,
    @SerializedName("id")
    val id: Long,
    @SerializedName("name")
    val name: String,
    @SerializedName("percent")
    val percent: String
)

data class TariffMap(
    @SerializedName("key")
    val key: String,
    @SerializedName("label")
    val label: String
)

enum class DataKey(val key: String){
    KEY (key = "key"),
    LABEL(key = "label")
}