package sumra.net.app.infinity_card.enums

enum class EFileLoadType(val value: String, val filePrefix: String) {
    ADVERT (value = "advert", filePrefix = "infinity_card_advert_#.jpeg"),
    AVATAR (value = "avatar", filePrefix = "infinity_card_avatar_#.jpeg"),
    GIFT_CARD (value = "giftCardQR", filePrefix = "infinity_card_gift_card_#.jpeg"),
    DOCUMENT (value = "document", filePrefix = "infinity_card_document_#.jpeg");
}

/*
    const ENTITY_ADVERT = 'advert';
    const ENTITY_AVATAR = 'avatar';
    const ENTITY_GIFT_CARD_QR = 'giftCardQR';
    const ENTITY_DOCUMENT = 'document';
 */