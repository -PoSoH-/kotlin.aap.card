package sumra.net.app.infinity_card.views

import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(value = AddToEndSingleStrategy::class)
interface IViewOrderItem: IViewCommon {

    fun fetchOrderSuccess()
    fun fetchOrderFailure(error: String?)

    // request for repeat order
    fun repeatOrderSuccess()
    fun repeatOrderFailure(error: String?)
}