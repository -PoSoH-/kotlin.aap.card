package sumra.net.app.infinity_card.views

import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(value = AddToEndSingleStrategy::class)
interface IViewCheckCard: IViewCommon {

    fun fetchCardCheckSuccess()
    fun fetchCardCheckFailure()

}