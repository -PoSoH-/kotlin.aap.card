package sumra.net.app.infinity_card.repositoryes

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import sumra.net.app.infinity_card.core.NetCards
import sumra.net.app.infinity_card.models.responses._respCardTypes
import sumra.net.app.infinity_card.repositoryes.converters.ConverterCard

class RepositoryCardTypes {

    fun fetchCardTypes(token: String): Deferred<_respCardTypes> {
        val resp = MockCard.getCardTypesSuccess()     //getCardTypesFailure()   //NetCards.getCardTypes(token)
        return GlobalScope.async {
            ConverterCard.convertToUseApp(resp)
        }
    }

    private object MockCard{

        fun getCardTypesSuccess() = "{\n" +
                "    \"success\": true,\n" +
                "    \"fields\": [\n" +
                "        {\n" +
                "            \"key\": \"id\",\n" +
                "            \"label\": \"ID\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"key\": \"name\",\n" +
                "            \"label\": \"Name\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"key\": \"description\",\n" +
                "            \"label\": \"Description\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"key\": \"series_id\",\n" +
                "            \"label\": \"Series\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"key\": \"price_start\",\n" +
                "            \"label\": \"Price Start\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"key\": \"price_finish\",\n" +
                "            \"label\": \"Price Finish\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"key\": \"months\",\n" +
                "            \"label\": \"Months\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"key\": \"currency\",\n" +
                "            \"label\": \"Currency\"\n" +
                "        }\n" +
                "    ],\n" +
                "    \"current_page\": 1,\n" +
                "    \"data\": [\n" +
                "        {\n" +
                "            \"id\": 7,\n" +
                "            \"name\": \"Advanced\",\n" +
                "            \"description\": \"Quos quia necessitatibus deleniti excepturi esse quo ad.\",\n" +
                "            \"series_id\": 3,\n" +
                "            \"price_start\": \"217.00\",\n" +
                "            \"price_finish\": \"317.00\",\n" +
                "            \"months\": 26,\n" +
                "            \"currency\": \"usd\",\n" +
                "            \"series\": {\n" +
                "                \"id\": 3,\n" +
                "                \"name\": \"Sublime\",\n" +
                "                \"created_at\": \"2020-11-05 16:05:34\",\n" +
                "                \"updated_at\": \"2020-11-05 16:05:34\"\n" +
                "            }\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 5,\n" +
                "            \"name\": \"Gold\",\n" +
                "            \"description\": \"Officia placeat assumenda similique Officia placeat assumenda similique Officia placeat assumenda similique Officia placeat assumenda similique Officia placeat assumenda similique.\",\n" +
                "            \"series_id\": 2,\n" +
                "            \"price_start\": \"238.00\",\n" +
                "            \"price_finish\": \"431.00\",\n" +
                "            \"months\": 17,\n" +
                "            \"currency\": \"usd\",\n" +
                "            \"series\": {\n" +
                "                \"id\": 2,\n" +
                "                \"name\": \"Series\",\n" +
                "                \"created_at\": \"2020-11-05 16:05:34\",\n" +
                "                \"updated_at\": \"2020-11-05 16:05:34\"\n" +
                "            }\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 1,\n" +
                "            \"name\": \"Neon\",\n" +
                "            \"description\": \"Sequi quibusdam doloribus culpa quidem.\",\n" +
                "            \"series_id\": 1,\n" +
                "            \"price_start\": \"162.00\",\n" +
                "            \"price_finish\": \"448.00\",\n" +
                "            \"months\": 36,\n" +
                "            \"currency\": \"usd\",\n" +
                "            \"series\": {\n" +
                "                \"id\": 1,\n" +
                "                \"name\": \"Range\",\n" +
                "                \"created_at\": \"2020-11-05 16:05:34\",\n" +
                "                \"updated_at\": \"2020-11-05 16:05:34\"\n" +
                "            }\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 6,\n" +
                "            \"name\": \"Platinum\",\n" +
                "            \"description\": \"Nisi reiciendis ut aliquam velit ullam ut ex.\",\n" +
                "            \"series_id\": 2,\n" +
                "            \"price_start\": \"128.00\",\n" +
                "            \"price_finish\": \"433.00\",\n" +
                "            \"months\": 24,\n" +
                "            \"currency\": \"usd\",\n" +
                "            \"series\": {\n" +
                "                \"id\": 2,\n" +
                "                \"name\": \"Series\",\n" +
                "                \"created_at\": \"2020-11-05 16:05:34\",\n" +
                "                \"updated_at\": \"2020-11-05 16:05:34\"\n" +
                "            }\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 8,\n" +
                "            \"name\": \"Premier\",\n" +
                "            \"description\": \"Perspiciatis assumenda omnis cum velit libero et earum ut.\",\n" +
                "            \"series_id\": 3,\n" +
                "            \"price_start\": \"286.00\",\n" +
                "            \"price_finish\": \"531.00\",\n" +
                "            \"months\": 13,\n" +
                "            \"currency\": \"usd\",\n" +
                "            \"series\": {\n" +
                "                \"id\": 3,\n" +
                "                \"name\": \"Sublime\",\n" +
                "                \"created_at\": \"2020-11-05 16:05:34\",\n" +
                "                \"updated_at\": \"2020-11-05 16:05:34\"\n" +
                "            }\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 4,\n" +
                "            \"name\": \"Silver\",\n" +
                "            \"description\": \"Id qui provident repellendus nulla consequatur.\",\n" +
                "            \"series_id\": 2,\n" +
                "            \"price_start\": \"108.00\",\n" +
                "            \"price_finish\": \"555.00\",\n" +
                "            \"months\": 20,\n" +
                "            \"currency\": \"usd\",\n" +
                "            \"series\": {\n" +
                "                \"id\": 2,\n" +
                "                \"name\": \"Series\",\n" +
                "                \"created_at\": \"2020-11-05 16:05:34\",\n" +
                "                \"updated_at\": \"2020-11-05 16:05:34\"\n" +
                "            }\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 2,\n" +
                "            \"name\": \"Strata\",\n" +
                "            \"description\": \"Autem dolores consequuntur dolor et.\",\n" +
                "            \"series_id\": 1,\n" +
                "            \"price_start\": \"186.00\",\n" +
                "            \"price_finish\": \"556.00\",\n" +
                "            \"months\": 17,\n" +
                "            \"currency\": \"usd\",\n" +
                "            \"series\": {\n" +
                "                \"id\": 1,\n" +
                "                \"name\": \"Range\",\n" +
                "                \"created_at\": \"2020-11-05 16:05:34\",\n" +
                "                \"updated_at\": \"2020-11-05 16:05:34\"\n" +
                "            }\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 9,\n" +
                "            \"name\": \"Supreme\",\n" +
                "            \"description\": \"Vel eaque voluptatem labore molestiae.\",\n" +
                "            \"series_id\": 3,\n" +
                "            \"price_start\": \"185.00\",\n" +
                "            \"price_finish\": \"445.00\",\n" +
                "            \"months\": 29,\n" +
                "            \"currency\": \"usd\",\n" +
                "            \"series\": {\n" +
                "                \"id\": 3,\n" +
                "                \"name\": \"Sublime\",\n" +
                "                \"created_at\": \"2020-11-05 16:05:34\",\n" +
                "                \"updated_at\": \"2020-11-05 16:05:34\"\n" +
                "            }\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 3,\n" +
                "            \"name\": \"Ultima\",\n" +
                "            \"description\": \"Quia quos corrupti sed labore sit quam.\",\n" +
                "            \"series_id\": 1,\n" +
                "            \"price_start\": \"243.00\",\n" +
                "            \"price_finish\": \"364.00\",\n" +
                "            \"months\": 26,\n" +
                "            \"currency\": \"usd\",\n" +
                "            \"series\": {\n" +
                "                \"id\": 1,\n" +
                "                \"name\": \"Range\",\n" +
                "                \"created_at\": \"2020-11-05 16:05:34\",\n" +
                "                \"updated_at\": \"2020-11-05 16:05:34\"\n" +
                "            }\n" +
                "        }\n" +
                "    ],\n" +
                "    \"first_page_url\": \"http://192.168.188.137:8095/v1/giftcards/cardtypes?page=1\",\n" +
                "    \"from\": 1,\n" +
                "    \"last_page\": 1,\n" +
                "    \"last_page_url\": \"http://192.168.188.137:8095/v1/giftcards/cardtypes?page=1\",\n" +
                "    \"next_page_url\": null,\n" +
                "    \"path\": \"http://192.168.188.137:8095/v1/giftcards/cardtypes\",\n" +
                "    \"per_page\": 20,\n" +
                "    \"prev_page_url\": null,\n" +
                "    \"to\": 9,\n" +
                "    \"total\": 9\n" +
                "}"

        fun getCardTypesFailure() = "{\n" +
                "    \"data\": {\n" +
                "        \"type\": \"error\",\n" +
                "        \"title\": \"Auth error\",\n" +
                "        \"message\": \"Unauthorized access\"\n" +
                "    }\n" +
                "}"

    }
}