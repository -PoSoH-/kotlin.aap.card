package sumra.net.app.infinity_card.wsoi;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.domain.wsoi.WalletConst;

import net.openid.appauth.*;

import org.json.JSONException;

import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicReference;

/**
 * A mechanism for handling the AuthState.
 */
public class AuthStateManager {
    public static String REFRESH_TOKEN = "refresh_token";
    public static String ACCESS_TOKEN = "access_token";

    private static final AtomicReference<WeakReference<AuthStateManager>> INSTANCE_REF =
            new AtomicReference<>(new WeakReference<AuthStateManager>(null));
    private final SharedPreferences prefs;
    private final AtomicReference<AuthState> currentAuthState;
    private final String TAG = AuthStateManager.class.getSimpleName();

    private AuthStateManager(SharedPreferences preferences) {

        prefs = preferences;
        currentAuthState = new AtomicReference<>();
    }

    /**
     * public static AuthStateManager getInstance(SharedPreferences preferences) {
     * <p>
     * AuthStateManager manager = INSTANCE_REF.get().get();
     * if (manager == null) {
     * manager = new AuthStateManager(preferences);
     * INSTANCE_REF.set(new WeakReference<>(manager));
     * }
     * <p>
     * return manager;
     * }
     */
    public static AuthStateManager getInstance(Context context) {

        AuthStateManager manager = INSTANCE_REF.get().get();
        if (manager == null) {
            manager = new AuthStateManager(context.getSharedPreferences("shared_pref_key", Context.MODE_PRIVATE));
            INSTANCE_REF.set(new WeakReference<>(manager));
        }

        return manager;
    }

    /**
     * Returns the current AuthState instance.
     *
     * @return Current AuthState instance.
     */

    public AuthState getCurrentState() {


        AuthState current;
        if (currentAuthState.get() != null) {
            current = currentAuthState.get();
        } else {
            AuthState state = readState();
            if (currentAuthState.compareAndSet(null, state)) {
                current = state;
            } else {
                current = currentAuthState.get();
            }
        }
        Log.d(TAG, "------getCurrentState(), current.getAccessToken()--------- "+current.getAccessToken());
        return current;
    }

    /**
     * Replaces the current AuthState with a new AuthState.
     *
     * @param state AuthState object that is to replace the existing one.
     */

    public void replaceState(AuthState state) {
        writeState(state);
        currentAuthState.set(state);
    }

    /**
     * Updates the current AuthState with authorization response and exception.
     *
     * @param response Authorization response.
     * @param ex       Authorization exception.
     */

    public void updateAfterAuthorization(AuthorizationResponse response,
                                         AuthorizationException ex) {

        AuthState current = getCurrentState();
        current.update(response, ex);
        replaceState(current);
    }

    /**
     * Updates the current AuthState with token response and exception.
     *
     * @param response Token response.
     * @param ex       Authorization exception.
     */

    public void updateAfterTokenResponse(TokenResponse response, AuthorizationException ex) {

        AuthState current = getCurrentState();
        current.update(response, ex);
        replaceState(current);
    }

    /**
     * Reads the AuthState.
     *
     * @return AuthState object.
     */

    private AuthState readState() {

        AuthState auth;
        String currentState = prefs.getString(WalletConst.KEY_STATE, null);

        if (currentState == null) {
            auth = new AuthState();
        } else {
            try {
                auth = AuthState.jsonDeserialize(currentState);
            } catch (JSONException ex) {
                Log.e(TAG, "-------------------Failed to deserialize stored auth state - discarding: ", ex);
                auth = new AuthState();
            }
        }
        Log.d(TAG, "----------------- readState(), auth.getAccessToken()=" + auth.getAccessToken());
        Log.d(TAG, "----------------- readState(), auth.getRefreshToken=" + auth.getRefreshToken());
        return auth;
    }

    /**
     * Writes the AuthState.
     *
     * @param state AuthState object.
     */

    private void writeState(AuthState state) {
        Log.d(TAG, "----------------- writeState(), auth.getAccessToken()=" + state.getAccessToken());
        Log.d(TAG, "----------------- writeState(), auth.getRefreshToken=" + state.getRefreshToken());
        SharedPreferences.Editor editor = prefs.edit();
        if (state == null) {
            editor.remove(WalletConst.KEY_STATE);
        } else {
            editor.putString(WalletConst.KEY_STATE, state.jsonSerializeString());
        }
        if (!editor.commit()) {
            Log.e(TAG, "---------------------Failed to write state to shared prefs.");
        }
        prefs.edit().putString(ACCESS_TOKEN, state.getAccessToken()).apply();
        prefs.edit().putString(REFRESH_TOKEN, state.getRefreshToken()).apply();
    }

    public AuthState updateAfterRegistration(
            RegistrationResponse response,
            AuthorizationException ex) {
        AuthState current = getCurrentState();
        if (ex != null) {
            return current;
        }

        current.update(response);
        replaceState(current);
        return current;
    }
}
