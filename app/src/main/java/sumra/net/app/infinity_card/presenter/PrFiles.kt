package sumra.net.app.infinity_card.presenter

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import moxy.MvpPresenter
import sumra.net.app.infinity_card.AppCard
import sumra.net.app.infinity_card.enums.ERespCode
import sumra.net.app.infinity_card.models.AttributeFiles
import sumra.net.app.infinity_card.repositoryes.RepositoryFiles
import sumra.net.app.infinity_card.views.IViewFiles

class PrFiles : MvpPresenter<IViewFiles>() {

    private val mRepositoryFiles = RepositoryFiles()
    private lateinit var userAvatarUrl: AttributeFiles

    fun uploadFile(image:String, name:String){
        viewState.showProgress(true)
        CoroutineScope(Dispatchers.IO).launch {
            val resp = mRepositoryFiles.postFilesAvatar(AppCard.getCheckToken(), image, name).await()

            if((ERespCode.compareType(resp.success))) {
                userAvatarUrl = resp.data!!
                withContext(Dispatchers.Main) {
                    viewState.uploadFileSuccess()
                }
            }else {
                withContext(Dispatchers.Main) {
                    viewState.uploadFileFailure(null)
                }
            }
            withContext(Dispatchers.Main){
                viewState.showProgress(false)
            }
        }
    }

    fun updateFile(token: String, image:String, name:String){
        viewState.showProgress(true)
        CoroutineScope(Dispatchers.IO).launch {
//            val resp = mRepositoryFiles.patchFilesAvatar(token, image, name).await()
//
//            withContext(Dispatchers.Main){
//                viewState.patchFileSuccess()
//            }
//
//            withContext(Dispatchers.Main){
//                viewState.patchFileFailure(null)
//            }
//
//            withContext(Dispatchers.Main){
//                viewState.showProgress(false)
//            }
        }
    }

    fun deleteFile(token: String, imageId:Int){
        viewState.showProgress(true)
        CoroutineScope(Dispatchers.IO).launch {
//            val resp = mRepositoryFiles.deleteFilesAvatar(token, imageId).await()
//
//            withContext(Dispatchers.Main){
//                viewState.deleteFileSuccess()
//            }
//
//            withContext(Dispatchers.Main){
//                viewState.deleteFileFailure(null)
//            }
//
//            withContext(Dispatchers.Main){
//                viewState.showProgress(false)
//            }
        }
    }

    fun getUserAvatar() = userAvatarUrl.path

}