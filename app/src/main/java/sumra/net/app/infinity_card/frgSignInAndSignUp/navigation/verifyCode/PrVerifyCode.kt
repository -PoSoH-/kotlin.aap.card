package sumra.net.app.infinity_card.frgSignInAndSignUp.navigation.verifyCode

import kotlinx.coroutines.*
import moxy.MvpPresenter

class PrVerifyCode: MvpPresenter<IViewVerifyCode>() {

    private val repository = RepositoryVerifyCode()

    fun sendVerifyCode(phone: String, code: String, userName: String){
        viewState.showProgress(true)
        CoroutineScope( Dispatchers.IO).launch {
            val resp = repository.sendVerifyCode(phone, code, userName).await()
            if(resp.success){
                withContext(Dispatchers.Main){
                    viewState.showProgress(false)
                    viewState.sendVerifyCodeSuccess()
                }
            }else{
                withContext(Dispatchers.Main){
                    viewState.showProgress(false)
                    viewState.sendVerifyCodeFailure(resp.error)
                }
            }
        }
    }

}