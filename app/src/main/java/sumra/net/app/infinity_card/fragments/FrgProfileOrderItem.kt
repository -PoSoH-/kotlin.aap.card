package sumra.net.app.infinity_card.fragments

import android.os.Bundle
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.navigation.Navigation
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.frg_profile_order_item.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter
import sumra.net.app.infinity_card.AppCard
import sumra.net.app.infinity_card.R
import sumra.net.app.infinity_card.enums.ECurrency
import sumra.net.app.infinity_card.enums.EKeys
import sumra.net.app.infinity_card.enums.EOrderStatus
import sumra.net.app.infinity_card.models.responses.Order
import sumra.net.app.infinity_card.presenter.PrOrderItem
import sumra.net.app.infinity_card.utils.UString
import sumra.net.app.infinity_card.utils.UtilLog
import sumra.net.app.infinity_card.views.IViewOrderItem
import sumra.net.app.infinity_card.widgets.CustomSnackBar.CustomSnackBar

class FrgProfileOrderItem:
    MvpAppCompatFragment(R.layout.frg_profile_order_item)
    , IViewOrderItem{

    private val TAG = "FragmentProfileOrderItem ->"

    @InjectPresenter
    lateinit var mPrOrderItem: PrOrderItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppCard.hideBottomNavView()
        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true /* enabled by default */) {
                override fun handleOnBackPressed() {
                    UtilLog.error(TAG, "BackPressed -> Click")
                    onBackPressed()
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val orderId = arguments?.getLong(EKeys.PROFILE_ORDER_ITEM_ID, -1)!!
        if(orderId >= 0) mPrOrderItem.fetchOrderItem(orderId)
    }

    private fun onBackPressed(){
        UtilLog.info(TAG
            , "Button back click..."
            , rootProfileOrderItem)
        AppCard.showBottomNavView()
        Navigation.findNavController(
            requireActivity(),
            R.id.nav_host_profile
        ).popBackStack()
    }

    private fun updateOrderView(order: Order){
        txt_profile_order_number_vls.text = order.id.toString()

        txt_profile_order_created_vls.text = UString.setupDateOrderState(order.createAt)
        txt_profile_order_updated_vls.text = UString.setupDateOrderState(order.updateAt)

        txt_profile_order_price_vls.text = UString.setupOrderPriceShow(order.totalPrice)
        txt_profile_order_currency_vls.text = ECurrency.compareCurrencyByName(order.currency)

        txt_profile_order_status_vls.apply {
            text = EOrderStatus.checkStatus(order.status)
            setTextColor(resources.getColor(EOrderStatus.checkColor(order.status), resources.newTheme()))
        }
    }

    private fun showErrorOfScreen(message: String){
        CustomSnackBar.make(
            view = rootProfileOrderItem,
            message = message,
            duration = Snackbar.LENGTH_SHORT,
            listener = null,
            action_lable = "",
            typeMessage = CustomSnackBar.EMessageType.ERROR,
            header = resources.getString(R.string.error_infinity_card_message_in_header)
        )?.show()
    }

    override fun fetchOrderSuccess() {
        UtilLog.info(TAG, "Order loaded success...", rootProfileOrderItem)
        updateOrderView(mPrOrderItem.getOrder()!!)
    }

    override fun fetchOrderFailure(error: String?) {
        when(error){
            null -> showErrorOfScreen(resources.getString(R.string.error_network_dont_know))
            else-> showErrorOfScreen(error)
        }
    }

    override fun repeatOrderSuccess() {
        UtilLog.info(TAG, "Order repeat request success...", rootProfileOrderItem)
    }

    override fun repeatOrderFailure(error: String?) {
        when(error){
            null -> showErrorOfScreen(resources.getString(R.string.error_network_dont_know))
            else-> showErrorOfScreen(error)
        }
    }

    override fun showProgress(state: Boolean) {
        when(state){
            true -> AppCard.showProgress()
            else -> AppCard.hideProgress()
        }
    }
}