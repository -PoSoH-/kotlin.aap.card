package sumra.net.app.infinity_card.fragments

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.aigestudio.wheelpicker.WheelPicker
import kotlinx.android.synthetic.main.lay_date_picker_show.*
import moxy.MvpAppCompatDialogFragment
import sumra.net.app.infinity_card.AppCard
import sumra.net.app.infinity_card.R
import sumra.net.app.infinity_card.interfaces.IDialogMessageListener
import sumra.net.app.infinity_card.utils.extention.showSnackBarInfo
import kotlin.ClassCastException


class FrgDialogDatePicker : MvpAppCompatDialogFragment() {

    private lateinit var listener: IDialogMessageListener

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.lay_date_picker_show, container, false)
        root.layoutParams =
            ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        return root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is IDialogMessageListener) {
            listener = context
        } else {
            throw ClassCastException("Error interface implementation!")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        datePickerSelectYear.apply {
//            data = mutableListOf("2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029")
            yearStart = 2020
            yearEnd = 2020 + 50
            setOnItemSelectedListener(object : WheelPicker.OnItemSelectedListener {
                override fun onItemSelected(picker: WheelPicker?, data: Any?, position: Int) {
                    baseDataPickerDialogFragment.showSnackBarInfo("$position")
                    datePickerSelectDay.month = datePickerSelectMonth.currentMonth
                    datePickerSelectDay.year = datePickerSelectYear.currentYear
                }
            })
        }

        datePickerSelectMonth.apply {
//            data = mutableListOf("jan", "feb", "mar", "apr", "mai", "jun", "jul", "aug", "sep", "okt", "nov", "dec")
            setOnItemSelectedListener(object : WheelPicker.OnItemSelectedListener {
                override fun onItemSelected(picker: WheelPicker?, data: Any?, position: Int) {
                    baseDataPickerDialogFragment.showSnackBarInfo("$position")
                    datePickerSelectDay.month = datePickerSelectMonth.currentMonth
                    datePickerSelectDay.year = datePickerSelectYear.currentYear
                }
            })
        }

        datePickerSelectDay.apply {
//            data = mutableListOf(
//                "01","02","03","04","05","06","07","08","09","10",
//                "11","12","13","14","15","16","17","18","19","20",
//                "21","22","23","24","25","26","27","28","29","30",
//                "31")
            setOnItemSelectedListener(object : WheelPicker.OnItemSelectedListener {
                override fun onItemSelected(picker: WheelPicker?, data: Any?, position: Int) {
                    baseDataPickerDialogFragment.showSnackBarInfo("$position")
                }
            })
        }

        passportExpireDateCancel.apply {
            setOnClickListener {
                AppCard.getNavigation().popBackStack()
            }
        }

        passportExpireDateConfirm.apply {
            setOnClickListener {
                AppCard.getNavigation().popBackStack()
                listener.dialogMessage(
                    id = baseDataPickerDialogFragment.id,
                    message = "${datePickerSelectMonth.selectedMonth}-${datePickerSelectDay.selectedDay}-${datePickerSelectYear.selectedYear}"
                )
            }
        }

    }

    override fun onDetach() {
        listener == null
        super.onDetach()
    }

//    private fun setupDobContainerYear() {
//        container[AccountKey.DOB]
//            ?.findViewById<ConstraintLayout>(R.id.layoutContainerDob2)
//            ?.findViewById<RecyclerView>(R.id.selectYear)?.apply {
//                adapter = adapterYear
//                layoutManager =
//                    CarouselLayoutManager(CarouselLayoutManager.HORIZONTAL, true).apply {
//                        setPostLayoutListener(CarouselZoomPostLayoutListener())
//                        maxVisibleItems = 1
//                        addOnItemSelectionListener(
//                            object : CarouselLayoutManager.OnCenterItemSelectionListener {
//                                override fun onCenterItemChanged(adapterPosition: Int) {
//                                    UtilLog.error(
//                                        "LOG YEAR", "Current year: -" +
//                                                "> ${adapterYear.getCurrentYear(adapterPosition)}"
//                                    )
//                                    mCustomerPresenter.inputAccount.year = adapterYear
//                                        .getCurrentYear(adapterPosition).toString()
//
//                                    adapterDay.bindDayOfMonth(
//                                        mCustomerPresenter.inputAccount.year,
//                                        if (mCustomerPresenter.inputAccount.month.equals("")) "1" else mCustomerPresenter.inputAccount.month
//                                    )
//                                }
//                            })
//                    }
//                addOnScrollListener(object : CenterScrollListener() {
//                    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
//                        super.onScrollStateChanged(recyclerView, newState)
//                    }
//                })
//                scrollToPosition(15)
//                setHasFixedSize(true)
//            }
//    }
//
//    private fun setupDobContainerMonth() {
//        container[AccountKey.DOB]
//            ?.findViewById<ConstraintLayout>(R.id.layoutContainerDob2)
//            ?.findViewById<RecyclerView>(R.id.selectMonth)?.apply {
//                adapter = adapterMonth
//                layoutManager =
//                    CarouselLayoutManager(CarouselLayoutManager.HORIZONTAL, true).apply {
//                        setPostLayoutListener(CarouselZoomPostLayoutListener())
//                        maxVisibleItems = 1
//                        addOnItemSelectionListener(
//                            object : CarouselLayoutManager.OnCenterItemSelectionListener {
//                                override fun onCenterItemChanged(adapterPosition: Int) {
//                                    mCustomerPresenter.inputAccount.month = adapterMonth
//                                        .getNumberMonth(adapterPosition).toString()
//                                    UtilLog.error(
//                                        "LOG MONTH", "Current month: -" +
//                                                "> ${adapterMonth.getNumberMonth(adapterPosition)} -" +
//                                                "> ${adapterMonth.getNameMonthFull(adapterPosition)}"
//                                    )
//                                    adapterDay.bindDayOfMonth(
//                                        mCustomerPresenter.inputAccount.year,
//                                        mCustomerPresenter.inputAccount.month
//                                    )
//                                }
//                            })
//                    }
//                addOnScrollListener(object : CenterScrollListener() {
//                    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
//                        super.onScrollStateChanged(recyclerView, newState)
//                    }
//                })
//                scrollToPosition(5)
//                setHasFixedSize(true)
//            }
//    }
//
//    private fun setupDobContainerDay() {
//        val carousel = CarouselLayoutManager(
//            com.azoft.carousellayoutmanager.CarouselLayoutManager.HORIZONTAL,
//            true
//        ).apply {
//            setPostLayoutListener(object : CarouselZoomPostLayoutListener() {})
//            maxVisibleItems = 1
//            addOnItemSelectionListener(
//                object : CarouselLayoutManager.OnCenterItemSelectionListener {
//                    override fun onCenterItemChanged(adapterPosition: Int) {
//                        mCustomerPresenter.inputAccount.day = adapterDay.getDay(adapterPosition)
//                    }
//                })
//        }
//        container[AccountKey.DOB]
//            ?.findViewById<ConstraintLayout>(R.id.layoutContainerDob2)
//            ?.findViewById<RecyclerView>(R.id.selectDay)?.apply {
//                adapter = adapterDay
//                addOnScrollListener(object : CenterScrollListener() {})
//                layoutManager = carousel
//                scrollToPosition(14)
//                setHasFixedSize(true)
//            }
//    }


//    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
////        return super.onCreateDialog(savedInstanceState)
//        val Q = LayoutInflater.from(requireActivity()).inflate(R.layout.lay_date_picker_show, null)
//        val root = Q // FrameLayout(requireActivity())  // LayoutInflater.from(requireActivity()).inflate(R.layout.lay_date_picker_show, requireActivity(), false) as ConstraintLayout  //RelativeLayout(requireActivity())
//        root.layoutParams =
//            ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
//
//        // creating the fullscreen dialog
//        val dialog = Dialog(requireActivity())
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        dialog.setContentView(root)
//        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.YELLOW))
//        dialog.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
//
//
////        root.addView(Q)
//
//        return dialog
//    }

}

