package sumra.net.app.infinity_card.models.responses

import com.google.gson.annotations.SerializedName
import sumra.net.app.infinity_card.models.AttributeFiles

data class RespNetVerifySign (
    val success: Boolean, // type response
    val data: Any?,       // link for file of server
    val error: String?    // message error...
)

data class VerifyPhone(
    val link: String,
    val code: String,
    val id_code: String,
)

data class VerifyCode(
    val success: Boolean,
    val error: String?,
)

data class AuthUser(
    val user_name : String,
    val email     : String,
    val password  : String,
)

//data class RespUserAuth(
//    val success: Boolean,
//    val error  : String
//)

data class RespVerifyPhone(
    val success: Boolean,
    val data   : VerifyAuth?,
    val error  : String?,
)

data class VerifyAuth(
    @SerializedName("token")
    val token: String,
    @SerializedName("token_expire")
    val tokenExpire: String,
    @SerializedName("refresh_token")
    val tokenRefresh: String,
    @SerializedName("scope")
    val scope: String
)