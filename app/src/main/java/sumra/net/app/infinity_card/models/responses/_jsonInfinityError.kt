package sumra.net.app.infinity_card.models.responses


import com.google.gson.annotations.SerializedName

data class _jsonInfinityError(
    @SerializedName("data")
    val error: InfinityError?
) {
    data class InfinityError (
        @SerializedName("message")
        val message: String?,
        @SerializedName("title")
        val title: String?,
        @SerializedName("type")  //error  //success
        val type: String?
    )
}

//3:02 / 3:13