package sumra.net.app.infinity_card.core

import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.Response
import org.apache.http.HttpException
import sumra.net.app.infinity_card.BuildConfig

object NetClientUsers {
    fun getClientUsers(limits:String?, agents: String?, page: String?):String {
        val host = BuildConfig.SUMRA_API_INITY_URL.toHttpUrlOrNull()!!
        val httpUrl = HttpUrl.Builder().apply {
            scheme(EHost.HTTPS.value)
            host(host.host)
//            addPathSegment(BuildConfig.SUMRA_VER_INTY_URL)
            addPathSegment("giftcards")
            addPathSegment("users")

            addQueryParameter("limit", "20")
            addQueryParameter("agents_only", "0")
            addQueryParameter("page", "1")
        }
            .build()
        try{
            val response = OkHttpClient.builder()
                .newCall(NetworkCore.GET_RequestContent(httpUrl.toString())).execute()
            if(response.isSuccessful){
                return NetworkCore.resultSuccess(response)
            }else{
                return NetworkCore.resultFailure(response)
            }
        } catch (e : HttpException) {
            return NetworkCore.resultFailure(e as Response)
        }
    }
}