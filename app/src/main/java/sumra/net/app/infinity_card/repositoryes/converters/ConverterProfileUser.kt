package sumra.net.app.infinity_card.repositoryes.converters

import com.google.gson.Gson
import sumra.net.app.infinity_card.enums.ERespCode
import sumra.net.app.infinity_card.models.User
import sumra.net.app.infinity_card.models.UserPatch
import sumra.net.app.infinity_card.models._jsonRespProfile
import sumra.net.app.infinity_card.models.responses.*
import java.lang.RuntimeException

object ConverterProfileUser {
    fun convertToUserModel(args: String): _respNetProfile {
        var isGreat: _jsonRespProfile? = null
        var isError: _jsonRespErrorMin? = null
        try {
            isGreat = Gson().fromJson(args, _jsonRespProfile::class.java)
//            isGreat = Gson().fromJson(args, _USER_NEW_PARSER_::class.java)
        }catch (ex: RuntimeException){
            return _respNetProfile(success = false, data = null, error = "Error parsing data json object!...")
        }
        if(isGreat != null && isGreat.success){
//            if(isGreat.user.isEmpty()){
//                return _respNetProfile(success = false, data = null, error = null)
//            }else {
                return _respNetProfile(success = isGreat.success, data = isGreat.user, error = null)
//            }
        }else{
            try {
                isError = Gson().fromJson(args, _jsonRespErrorMin::class.java)
            }catch (ex: RuntimeException) {
                return _respNetProfile(success = false, data = null, error = "Error parsing failure json object!...")
            }
            if(isError == null){
                return _respNetProfile(success = false, data = null, error = null)
            }else{
                return _respNetProfile(success = isError.success, error = isError.error, data = null)
            }
        }
    }

    fun convertRequestUserPatch(args: UserPatch): String {
        return Gson().toJson(args)
    }

    fun convertRespUserPatch(args: String): _respUserPatch {
        var isError: _respUserPatchError? = null
        var isGreat: _respUserPatchError? = null

        try{
            isGreat = Gson().fromJson(args, _respUserPatchError::class.java)
        }catch(erGreat: Exception){  }

        if(isGreat != null){
            return _respUserPatch(success = ERespCode.SUCCESS.type, data = isGreat.user, error = null)
        }else {
            try {
                isError = Gson().fromJson(args, _respUserPatchError::class.java)
            } catch (erError: Exception) {
                isError = _respUserPatchError(false, erError.message.toString(), null)
            }
        }
        return _respUserPatch(ERespCode.ERROR.type, isError?.user, isError?.error)
//            _jsonInfinityError.InfinityError(message = null, title = ",", type = ""))
    }

    private data class _respUserPatchError(val success: Boolean, val error: String, val user: User?)
}