package sumra.net.app.infinity_card.presenter

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import moxy.MvpPresenter
import sumra.net.app.infinity_card.models.Country
import sumra.net.app.infinity_card.repositoryes.RepositoryCountry
import sumra.net.app.infinity_card.enums.ERespCode
import sumra.net.app.infinity_card.views.IViewCountries

class PresenterCountries: MvpPresenter<IViewCountries>() {

    private val mListCountry = mutableListOf<Country>()
    private val mRepository = RepositoryCountry()

    fun fetchAllCountries(token:String){
        viewState.showProgress(true)
        CoroutineScope(Dispatchers.IO).launch {
            val resp = mRepository.fetchCountries(token).await()
            if (ERespCode.compareType(resp.success)) {
                mListCountry.clear()
                mListCountry.addAll(resp.data as MutableList)
                withContext(Dispatchers.Main) {
                    viewState.fetchAllCountrySuccess()
                }
            } else {
                withContext(Dispatchers.Main) {
                    viewState.fetchAllCountryFailure(resp.error!!)
                }
            }
            withContext(Dispatchers.Main) {
                viewState.showProgress(false)
            }
        }
    }

    fun getAllCountries() = mListCountry
}