package sumra.net.app.infinity_card.frgSignInAndSignUp.navigation.verifyPhone

import android.os.Bundle
import android.view.View
import androidx.navigation.Navigation
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.frg_auth_phone.*
import kotlinx.android.synthetic.main.frg_login_screen.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter
import sumra.net.app.infinity_card.AppCard
import sumra.net.app.infinity_card.R
import sumra.net.app.infinity_card.enums.EKeys
import sumra.net.app.infinity_card.utils.extention.hideKeyboard
import sumra.net.app.infinity_card.utils.extention.inputText
import sumra.net.app.infinity_card.widgets.CustomSnackBar.CustomSnackBar

class FrgInfoLogin : MvpAppCompatFragment(R.layout.frg_info_login) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

}