package sumra.net.app.infinity_card.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.Button
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.frg_splash_screen.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.DisposableHandle
import kotlinx.coroutines.launch
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter
import sumra.net.app.infinity_card.R
import sumra.net.app.infinity_card.adapters.AdapterCardList
import sumra.net.app.infinity_card.presenter.PAllCards
import sumra.net.app.infinity_card.views.IViewAllCards

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FrgSplashScreen : MvpAppCompatFragment(R.layout.frg_splash_screen){

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        image_logo_not_text.apply{
            animate()
                .rotationX(360F*10)
                .setInterpolator(AccelerateDecelerateInterpolator())
                .duration = 100000
        }

    }
}