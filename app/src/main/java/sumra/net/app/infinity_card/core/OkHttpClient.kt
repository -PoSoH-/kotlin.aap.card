package sumra.net.app.infinity_card.core

import com.google.common.net.HttpHeaders
import okhttp3.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit
object OkHttpClient {
    fun builder() = OkHttpClient
        .Builder()
        .addInterceptor(
            HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            }
        )
        .writeTimeout((30 * 1000).toLong(), TimeUnit.MILLISECONDS)
        .connectTimeout((30 * 1000).toLong(), TimeUnit.MILLISECONDS)
        .readTimeout((30 * 1000).toLong(), TimeUnit.MILLISECONDS)
        .build()

    fun builderByAuthoryze() = OkHttpClient
        .Builder()
        .addInterceptor(
            HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            }
        )
        .writeTimeout((30 * 1000).toLong(), TimeUnit.MILLISECONDS)
        .connectTimeout((30 * 1000).toLong(), TimeUnit.MILLISECONDS)
        .readTimeout((30 * 1000).toLong(), TimeUnit.MILLISECONDS)
        .authenticator(object : Authenticator {
            override fun authenticate(route: Route?, response: Response): Request? {
                if (response.request.header(HttpHeaders.AUTHORIZATION) != null)
                    return null //if you've tried to authorize and failed, give up
                val credential = Credentials.basic("username", "pass")
                return response.request.newBuilder().header(HttpHeaders.AUTHORIZATION, credential).build()
            }
        })
        .build()
}