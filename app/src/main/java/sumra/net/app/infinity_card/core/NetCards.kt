package sumra.net.app.infinity_card.core

import okhttp3.Headers
import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.Response
import okhttp3.internal.addHeaderLenient
import org.apache.http.HttpException
import sumra.net.app.infinity_card.BuildConfig

object NetCards {

    private val localHostUrl = "http://192.168.188.137:8095/v1/infinity"   //"https://api.sumra.net/files/1/v1/files"

    fun getCardTypes(token: String):String {
        val host = BuildConfig.SUMRA_API_INITY_URL.toHttpUrlOrNull()!!
        val httpUrl = HttpUrl.Builder().apply {
            scheme(EHost.HTTPS.value)
            host(host.host)
            addPathSegment(BuildConfig.SUMRA_API_INITY_INITY)
            addPathSegment(BuildConfig.SUMRA_API_INITY_NUM)
            addPathSegment(BuildConfig.SUMRA_API_INITY_VER)
            addPathSegment("infinity")  // /v1/infinity/reference/card-types
//            addPathSegment("reference")
//            addPathSegment("card-types")
            addPathSegment("cardtypes")
        }.build()

        try{
//            val response = OkHttpClient.builder()
//                .newCall(NetworkCore.GET_RequestContentByToken(httpUrl.toString(), token)).execute()
            val response = OkHttpClient.builder()
                .newCall(NetworkCore.GET_RequestContentByUserId("${localHostUrl}/cardtypes")).execute()
            if(response.isSuccessful){
                return NetworkCore.resultSuccess(response)
            }else{
                return NetworkCore.resultFailure(response)
            }
        } catch (e : HttpException) {
            return NetworkCore.resultFailure(e as Response)
        }
    }

    fun postCardGeneration(token: String, cardType: String):String {
        val host = BuildConfig.SUMRA_API_INITY_URL.toHttpUrlOrNull()!!
        val httpUrl = HttpUrl.Builder().apply {
            scheme(EHost.HTTPS.value)
            host(host.host)
            addPathSegment(BuildConfig.SUMRA_API_INITY_INITY)
            addPathSegment(BuildConfig.SUMRA_API_INITY_NUM)
            addPathSegment(BuildConfig.SUMRA_API_INITY_VER)
            addPathSegment("infinity")  // /infinity/cards
            addPathSegment("cards")
        }.build()

        try{
//            val response = OkHttpClient.builder()
//                .newCall(NetworkCore.POST_RequestContent(httpUrl.toString(), token, cardType)).execute()
            val response = OkHttpClient.builder()
                .newCall(NetworkCore.POST_RequestContentByUserID("${localHostUrl}/cards", token, cardType)).execute()
            if(response.isSuccessful){
                return NetworkCore.resultSuccess(response)
            }else{
                return NetworkCore.resultFailure(response)
            }
        } catch (e : HttpException) {
            return NetworkCore.resultFailure(e as Response)
        }
    }

    fun getCheckCard(token: String, arg: String):String {
        val host = BuildConfig.SUMRA_API_INITY_URL.toHttpUrlOrNull()!!
        val httpUrl = HttpUrl.Builder().apply {
            scheme(EHost.HTTPS.value)
            host(host.host)
            addPathSegment(BuildConfig.SUMRA_API_INITY_INITY)
            addPathSegment(BuildConfig.SUMRA_API_INITY_NUM)
            addPathSegment(BuildConfig.SUMRA_API_INITY_VER)
            addPathSegment("infinity")
            addPathSegment("cards")
            addPathSegment("check")
            addPathSegment("arg")
        }.build()

        try{
//            val response = OkHttpClient.builder()
//                .newCall(NetworkCore.GET_RequestContentByToken(httpUrl.toString(), token)).execute()
            val response = OkHttpClient.builder()
                .newCall(NetworkCore.GET_RequestContentByUserId(
                    "${localHostUrl}/cards/check/arg"
                )).execute()
            if(response.isSuccessful){
                return NetworkCore.resultSuccess(response)
            }else{
                return NetworkCore.resultFailure(response)
            }
        } catch (e : HttpException) {
            return NetworkCore.resultFailure(e as Response)
        }
    }

    fun getActivateCard(token: String, arg: String):String {
        val host = BuildConfig.SUMRA_API_INITY_URL.toHttpUrlOrNull()!!
        val httpUrl = HttpUrl.Builder().apply {
            scheme(EHost.HTTPS.value)
            host(host.host)
            addPathSegment(BuildConfig.SUMRA_API_INITY_INITY)
            addPathSegment(BuildConfig.SUMRA_API_INITY_NUM)
            addPathSegment(BuildConfig.SUMRA_API_INITY_VER)
            addPathSegment("infinity")
            addPathSegment("cards")
            addPathSegment("activated")
            addPathSegment("arg")
        }.build()

        try{
//            val response = OkHttpClient.builder()
//                .newCall(NetworkCore.GET_RequestContentByToken(httpUrl.toString(), token)).execute()
            val response = OkHttpClient.builder()
                .newCall(NetworkCore.GET_RequestContentByUserId(
                    "${localHostUrl}/cards/activated/arg"
                )).execute()
            if(response.isSuccessful){
                return NetworkCore.resultSuccess(response)
            }else{
                return NetworkCore.resultFailure(response)
            }
        } catch (e : HttpException) {
            return NetworkCore.resultFailure(e as Response)
        }
    }

}