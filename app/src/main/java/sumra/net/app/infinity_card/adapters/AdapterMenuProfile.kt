package sumra.net.app.infinity_card.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import sumra.net.app.infinity_card.R
import sumra.net.app.infinity_card.adapters.holders.HolderMenuProfile
import sumra.net.app.infinity_card.enums.EProfileMenu
import sumra.net.app.infinity_card.interfaces.IProfileMenuSelectListener

class AdapterMenuProfile(val listener: IProfileMenuSelectListener): RecyclerView.Adapter<HolderMenuProfile>() {

    private val listMenuItems = mutableListOf<EProfileMenu>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderMenuProfile {
        return HolderMenuProfile(LayoutInflater.from(parent.context)
            .inflate(R.layout.cell_menu_profile_item, parent, false))
    }

    override fun onBindViewHolder(holder: HolderMenuProfile, position: Int) {
        holder.bindMenu(listMenuItems[position], listener)
    }

    override fun getItemCount(): Int {
        return listMenuItems.size
    }

    fun updateMenuItems(typeID: Long){
        listMenuItems.clear()
        listMenuItems.addAll(EProfileMenu.getMenuByTypeId(typeID))
        notifyDataSetChanged()
        notifyDataSetChanged()
    }

}