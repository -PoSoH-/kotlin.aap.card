package sumra.net.app.infinity_card.core

import android.util.Log
import com.google.gson.Gson
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.Response
import java.lang.NullPointerException
import java.nio.charset.Charset


object NetworkCore {

    private fun getMediaCntApplicationJson() = "application/json; charset=utf-8".toMediaTypeOrNull()!!
    private val mediaType = "application/json".toMediaType()

    private val CURRENT_USER_ID = "2"

    fun PATCH_RequestContent(url:String, token:String, dataBody:String) = Request
        .Builder().apply {

            header("Authorization", token)
            patch(getResponseBodyPOST(dataBody))
            url(url)
        }.build()

    fun PATCH_RequestContentByUserId(url:String, token:String, dataBody:String) = Request
        .Builder()
        .url(url)
        .header("user-id", CURRENT_USER_ID)
//        .patch(getResponseBodyPOST(dataBody))
        .patch(dataBody.toRequestBody(mediaType))
        .build()

    fun GET_RequestContentByToken(url:String, token:String) = Request
        .Builder()
        .header("Authorization", token)
        .get()
        .url(url)
        .build()

    fun GET_RequestContentByUserId(url:String) = Request
        .Builder()
        .header("user-id", CURRENT_USER_ID)
        .get()
        .url(url)
        .build()

    fun GET_RequestContent(url:String) = Request
        .Builder()
        .get()
        .url(url)
        .build()

    fun DELETE_RequestContent(url:String, token:String) = Request
        .Builder()
        .header("Authorization", token)
        .delete()
        .url(url)
        .build()

    fun POST_RequestContent(url:String, token:String, dataBody:String?)= Request
        .Builder().apply {
            header("Authorization", token)
            if(dataBody !=  null) post(getResponseBodyPOST(dataBody))
            url(url)
        }.build()

    fun POST_RequestContentByUserID(url:String, token:String?, dataBody:String?)= Request
        .Builder().apply {
            token?.let{header("user-id", CURRENT_USER_ID)}
            if(dataBody !=  null) post(getResponseBodyPOST(dataBody))
            url(url)
        }.build()


    private fun getResponseBodyPOST(bodyData:String) = bodyData
        .toRequestBody(
            getMediaCntApplicationJson()
        )

    private fun readResponseBuffer(response:Response):String{
        try {
            val charset: Charset? =
                response.body?.contentType()?.charset(Charset.forName("UTF-8"))
            val tmp = response.body?.source()?.buffer?.clone()?.readString(charset!!)!!
            Log.e(this::class.java.simpleName, "Code line 108 success")
            return tmp
        }catch (ex: NullPointerException){
            Log.e(this::class.java.simpleName, "Code line 108 error -> ${ex.message}")
            return ""
        }
    }

    fun resultSuccess(response:Response):String{
        return readResponseBuffer(response)
    }

    fun resultFailure(response:Response):String{
        var tmp = ""
        if(response != null)
             tmp = readResponseBuffer(response)
        val tmp_trim = StringBuilder(tmp)
        tmp.trim(' ')
        if(tmp_trim[0] == '<'){
            return typeErrorResponse(response.code)
        }else if(tmp_trim[0] == '{' || tmp_trim[0] == '['){
            val temp : ResponseMessage? = Gson().fromJson<ResponseMessage>(tmp, ResponseMessage::class.java)
            if(temp != null){
                return Gson().toJson(temp)
            }else{
                return Gson().toJson(MessageError(NetError.error(type = -1)))
            }
        }else{
            return typeErrorResponse(response.code)
        }
    }

    private fun typeErrorResponse(code:Int):String{
        return when(code){
            400  -> Gson().toJson(MessageError(NetError.error(type = 400)))
            401  -> Gson().toJson(MessageError(NetError.error(type = 401)))
            402  -> Gson().toJson(MessageError(NetError.error(type = 402)))
            403  -> Gson().toJson(MessageError(NetError.error(type = 403)))
            404  -> Gson().toJson(MessageError(NetError.error(type = 404)))
            405  -> Gson().toJson(MessageError(NetError.error(type = 405)))
            500  -> Gson().toJson(MessageError(NetError.error(type = 500)))
            else -> Gson().toJson(MessageError(NetError.error(type = -1)))
        }
    }

//    fun resultFailure(ex:HttpException) = Gson().toJson(ex.message())
//
    private data class MessageError (val error:String){val success:Boolean = false}
    private data class ResponseMessage (val success:Boolean, val error:String)
}

enum class EHost(val value: String){
    HTTP  (value = "http" ),
    HTTPS (value = "https")
}


