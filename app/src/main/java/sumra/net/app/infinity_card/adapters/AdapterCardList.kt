package sumra.net.app.infinity_card.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import sumra.net.app.infinity_card.R
import sumra.net.app.infinity_card.adapters.holders.HolderCardItem
import sumra.net.app.infinity_card.enums.ECardType
import sumra.net.app.infinity_card.models.Card
import sumra.net.app.infinity_card.models.CardTypeItem

class AdapterCardList : RecyclerView.Adapter<HolderCardItem>() {

    private val cards = mutableListOf<Card>()
    private val types = arrayListOf<CardTypeItem>()
    private var mUserNameFirst: String = ""
    private var mUserNameLast : String = ""
    private lateinit var ctx : Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderCardItem {
        ctx = parent.context
        return HolderCardItem(LayoutInflater.from(ctx).inflate(R.layout.cell_card_show, parent, false))
    }

    override fun onBindViewHolder(holder: HolderCardItem, position: Int) {
        lateinit var nm: CardTypeItem
        types.forEach {
            if(it.id == cards[position].typeId) {
                nm = it
            }
        }
        holder.bind(cards[position]
            , nm.series!!.name
            , nm.name
            , mUserNameFirst
            , mUserNameLast
            , cards[position].code
            , ECardType.compareType(nm.id))
    }

    override fun getItemCount(): Int {
        return cards.size
    }

    fun bindCards(cards : MutableList<Card>
                  , types: ArrayList<CardTypeItem>
                  , userNameFirst: String
                  , userNameLast: String){
        this.cards.clear()
        this.cards.addAll(cards)
        this.types.clear()
        this.types.addAll(types)
        this.mUserNameFirst = userNameFirst
        this.mUserNameLast  = userNameLast
        notifyDataSetChanged()
    }
}