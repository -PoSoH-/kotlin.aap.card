package sumra.net.app.infinity_card.presenter

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import moxy.MvpPresenter
import sumra.net.app.infinity_card.models.CardTypeItem
import sumra.net.app.infinity_card.repositoryes.RepositoryCardTypes
import sumra.net.app.infinity_card.enums.ERespCode
import sumra.net.app.infinity_card.views.IViewCards

class PrCards : MvpPresenter<IViewCards>() {

    private val mRepoCardTypes = RepositoryCardTypes()

    private var mCardsTypes = mutableMapOf<Long, MutableList<CardTypeItem>>()
    private var mCardsTypesList = mutableListOf<CardTypeItem>()

    fun fetchCardTypes(token: String){
        viewState.showProgress(true)
        CoroutineScope(Dispatchers.IO).launch {
            val resp = mRepoCardTypes.fetchCardTypes(token).await()
            if(ERespCode.compareType(resp.success)){
                // this point
                val col = resp.data!!.cardTypes
                mCardsTypesList = col as MutableList
                    col.forEach {
                    if(mCardsTypes.isNullOrEmpty()){
                        if(mCardsTypes[it.seriesId].isNullOrEmpty()){
                            mCardsTypes.put(it.seriesId, mutableListOf(it))
                        }else{
                            mCardsTypes[it.seriesId]!!.add(it)
                        }
                    }else{
                        if(mCardsTypes[it.seriesId].isNullOrEmpty()){
                            mCardsTypes.put(it.seriesId, mutableListOf(it))
                        }else{
                            mCardsTypes[it.seriesId]!!.add(it)
                        }
                    }
//                    if(mCardsTypes.size > 0){
//
//                    }else{
//                        mCardsTypes. = it
//                    }
                }
                withContext(Dispatchers.Main) {
                    viewState.loadAllCardTypesSuccess()
                }
            }else{
                var er : String? = null
                if(resp.error != null) er = resp.error
                withContext(Dispatchers.Main) {
                    viewState.loadAllCardTypesFailure(er)
                }
            }
            withContext(Dispatchers.Main) {
                viewState.showProgress(false)
            }
        }
    }

    fun getCountMapCollections() = mCardsTypes

    fun getListCardType() = mCardsTypesList


}