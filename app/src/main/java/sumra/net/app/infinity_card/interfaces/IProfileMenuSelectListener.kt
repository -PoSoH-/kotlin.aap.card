package sumra.net.app.infinity_card.interfaces

import sumra.net.app.infinity_card.enums.EProfileMenu

interface IProfileMenuSelectListener {
    fun selectMenuItem(menuItem: EProfileMenu)
}