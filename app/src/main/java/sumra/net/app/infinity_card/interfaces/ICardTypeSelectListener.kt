package sumra.net.app.infinity_card.interfaces

import sumra.net.app.infinity_card.models.CardTypeItem

interface ICardTypeSelectListener {
    fun cardItemTypeSelect(itemTypeSelect: CardTypeItem, typeOperation: Boolean)
}