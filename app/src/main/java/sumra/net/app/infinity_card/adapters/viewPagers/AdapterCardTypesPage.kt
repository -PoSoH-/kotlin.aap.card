package sumra.net.app.infinity_card.adapters.viewPagers

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import sumra.net.app.infinity_card.fragments.FrgSeriesScreen
import sumra.net.app.infinity_card.models.CardTypeItem
import sumra.net.app.infinity_card.enums.EKeys
import java.util.ArrayList

class AdapterCardTypesPage(frgManager: FragmentActivity): FragmentStateAdapter(frgManager) {

    private lateinit var keys: MutableSet<Long>

    var items: MutableMap<Long, MutableList<CardTypeItem>>
        set(value){
            mutableItems = value
            notifyDataSetChanged()
        }
        get() = mutableItems

    private var mutableItems = mutableMapOf<Long, MutableList<CardTypeItem>>()

    override fun getItemCount(): Int {
        keys = mutableItems.keys
        return keys.size
    }

    override fun createFragment(position: Int): Fragment {
        val bundle = Bundle()
        val k = keys.toLongArray() //   : MutableList<Long> = mutableListOf(keys)[position]
        val temp = mutableItems[keys.toLongArray().get(position)]
        keys.remove(position)
        bundle.putSerializable(EKeys.CARD_TYPES_SHOW, temp as ArrayList )
        val frg = FrgSeriesScreen()
        frg.apply {
            arguments = bundle
        }
        return frg
    }

    override fun getItemId(position: Int): Long {
        return super.getItemId(position)
    }

    override fun containsItem(itemId: Long): Boolean {
        return super.containsItem(itemId)
    }

    fun addAfter(position: Int) {
        val size = mutableItems.size
//        if (size == 0 || position == size) mutableItems.put(size) else mutableItems.add(position + 1, size)
        notifyItemInserted(position)
    }

    fun deleteAt(position: Int) {
//        mutableItems.removeAt(position)
        notifyItemRemoved(position)
    }

}