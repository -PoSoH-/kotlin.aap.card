package sumra.net.app.infinity_card.models.requests

import com.google.gson.annotations.SerializedName

data class ModelCardGenerate (
    @SerializedName("type_id")
    val idCardType: String
)