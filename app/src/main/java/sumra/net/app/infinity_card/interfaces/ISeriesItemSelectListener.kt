package sumra.net.app.infinity_card.interfaces

import sumra.net.app.infinity_card.models.CardTypeItem

interface ISeriesItemSelectListener {
    fun cardItemSelect(itemSelect: CardTypeItem)
}