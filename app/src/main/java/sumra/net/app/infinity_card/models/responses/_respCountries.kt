package sumra.net.app.infinity_card.models.responses

import sumra.net.app.infinity_card.models.Country

data class _respCountries(val success: String, var data: List<Country>?, var error: _jsonInfinityError.InfinityError?)