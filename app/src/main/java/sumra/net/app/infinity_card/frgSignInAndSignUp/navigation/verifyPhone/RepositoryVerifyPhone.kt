package sumra.net.app.infinity_card.frgSignInAndSignUp.navigation.verifyPhone

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import sumra.net.app.infinity_card.frgSignInAndSignUp.ConverterVerify
import sumra.net.app.infinity_card.frgSignInAndSignUp.NetVerify
import sumra.net.app.infinity_card.models.responses.RespNetVerifySign

class RepositoryVerifyPhone {
    suspend fun sendVerifyPhone(token: String, phone: String) : Deferred<RespNetVerifySign> {
        val resp = NetVerify.postVerifyPhone(token, phone)
        return GlobalScope.async{
            ConverterVerify.convertRespPhoneToModel(resp)
        }
    }
}