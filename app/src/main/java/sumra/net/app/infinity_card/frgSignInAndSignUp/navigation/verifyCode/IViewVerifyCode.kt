package sumra.net.app.infinity_card.frgSignInAndSignUp.navigation.verifyCode

import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType
import sumra.net.app.infinity_card.views.IViewCommon

@StateStrategyType(value = AddToEndSingleStrategy::class)
interface IViewVerifyCode: IViewCommon {
    fun sendVerifyCodeSuccess()
    fun sendVerifyCodeFailure(error: String?)
}