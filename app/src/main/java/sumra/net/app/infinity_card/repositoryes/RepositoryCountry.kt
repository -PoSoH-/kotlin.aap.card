package sumra.net.app.infinity_card.repositoryes

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import sumra.net.app.infinity_card.core.NetReferenceAll
import sumra.net.app.infinity_card.models.responses._respCountries
import sumra.net.app.infinity_card.repositoryes.converters.ConverterReferenceData

class RepositoryCountry {

    suspend fun fetchCountries(token: String) : Deferred<_respCountries>{
        val resp = MockCountry.getMockCountrySuccess()   // NetReferenceAll.getCountries(token)   // MockCountry.getMockCountrySuccess() //
        return GlobalScope.async {
            ConverterReferenceData.convertToCountriesModel(resp)
        }
    }

    private object MockCountry{

        fun getMockCountrySuccess() = "[{\n" +
                "        \"value\": 1,\n" +
                "        \"label\": \"-- Universal --\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 2,\n" +
                "        \"label\": \"Afghanistan\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 3,\n" +
                "        \"label\": \"Åland Islands\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 4,\n" +
                "        \"label\": \"Albania\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 5,\n" +
                "        \"label\": \"Algeria\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 6,\n" +
                "        \"label\": \"American Samoa\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 7,\n" +
                "        \"label\": \"Andorra\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 8,\n" +
                "        \"label\": \"Angola\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 9,\n" +
                "        \"label\": \"Anguilla\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 10,\n" +
                "        \"label\": \"Antarctica\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 11,\n" +
                "        \"label\": \"Antigua and Barbuda\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 12,\n" +
                "        \"label\": \"Argentina\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 13,\n" +
                "        \"label\": \"Armenia\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 14,\n" +
                "        \"label\": \"Aruba\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 15,\n" +
                "        \"label\": \"Australia\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 16,\n" +
                "        \"label\": \"Austria\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 17,\n" +
                "        \"label\": \"Azerbaijan\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 18,\n" +
                "        \"label\": \"Bahamas\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 19,\n" +
                "        \"label\": \"Bahrain\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 20,\n" +
                "        \"label\": \"Bangladesh\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 21,\n" +
                "        \"label\": \"Barbados\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 22,\n" +
                "        \"label\": \"Belarus\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 23,\n" +
                "        \"label\": \"Belgium\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 24,\n" +
                "        \"label\": \"Belize\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 25,\n" +
                "        \"label\": \"Benin\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 26,\n" +
                "        \"label\": \"Bermuda\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 27,\n" +
                "        \"label\": \"Bhutan\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 28,\n" +
                "        \"label\": \"Bolivia (Plurinational State of)\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 29,\n" +
                "        \"label\": \"BonaireSint Eustatius and Saba\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 30,\n" +
                "        \"label\": \"Bosnia and Herzegovina\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 31,\n" +
                "        \"label\": \"Botswana\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 32,\n" +
                "        \"label\": \"Bouvet Island\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 33,\n" +
                "        \"label\": \"Brazil\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 34,\n" +
                "        \"label\": \"British Indian Ocean Territory\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 38,\n" +
                "        \"label\": \"Brunei Darussalam\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"value\": 39,\n" +
                "        \"label\": \"Bulgaria\"\n" +
                "    }]"

        fun getCardTypesFailure() = "{\n" +
                "    \"data\": {\n" +
                "        \"type\": \"error\",\n" +
                "        \"title\": \"Auth error\",\n" +
                "        \"message\": \"Unauthorized access\"\n" +
                "    }\n" +
                "}"

    }
}