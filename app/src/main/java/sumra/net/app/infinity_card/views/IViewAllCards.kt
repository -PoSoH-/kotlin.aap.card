package sumra.net.app.infinity_card.views

import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(value = AddToEndSingleStrategy::class)
interface IViewAllCards : IViewCommon {

    fun loadAllCardsSuccess()
    fun loadAllCardsFailure(error: String?)

    fun loadAllCardTypesSuccess()
    fun loadAllCardTypesFailure(error: String?)

}