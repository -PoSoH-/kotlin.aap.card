package sumra.net.app.infinity_card.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import sumra.net.app.infinity_card.R
import sumra.net.app.infinity_card.adapters.holders.HolderCardItem
import sumra.net.app.infinity_card.adapters.holders.HolderClientsItem
import sumra.net.app.infinity_card.models.Client

class AdapterClientsList : RecyclerView.Adapter<HolderClientsItem>() {

    private val cards = mutableListOf<Client>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderClientsItem {
        return HolderClientsItem(LayoutInflater.from(parent.context).inflate(R.layout.cell_user_client_show, parent, false))
    }

    override fun onBindViewHolder(holder: HolderClientsItem, position: Int) {
        holder.bind(cards[position])
    }

    override fun getItemCount(): Int {
        return cards.size
    }

    fun bindClientsList(cards : MutableList<Client>){
        this.cards.clear()
        this.cards.addAll(cards)
        notifyDataSetChanged()
    }
}