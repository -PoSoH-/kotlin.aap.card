package sumra.net.app.infinity_card.enums

import sumra.net.app.infinity_card.R

enum class EProfileMenu(val id_type: Int) {
    MENU_PROF_ORDERS(id_type = R.string.menu_name_prof_orders),
    MENU_PROF_CONTACTS(id_type = R.string.menu_name_prof_contacts),
    MENU_PROF_TARIFFS(id_type = R.string.menu_name_prof_tariffs),
    MENU_PROF_CLIENTS(id_type = R.string.menu_name_prof_clients),
    MENU_PROF_USER_CARD(id_type = R.string.menu_name_prof_cards),
//    MENU_PROF_USER_LOGOUT(id_type = R.string.menu_name_prof_cards),
//    MENU_PROF_USER_CARD(id_type = R.string.menu_name_prof_cards),
    MENU_PROF_USER_LOGOUT(id_type = R.string.menu_name_prof_logout);

    companion object {
        fun getNameMenu(type: EProfileMenu): Int {
            return when (type) {
                MENU_PROF_USER_LOGOUT -> MENU_PROF_USER_LOGOUT.id_type
                MENU_PROF_USER_CARD -> MENU_PROF_USER_CARD.id_type
                MENU_PROF_ORDERS -> MENU_PROF_ORDERS.id_type
                MENU_PROF_CONTACTS -> MENU_PROF_CONTACTS.id_type
                MENU_PROF_TARIFFS -> MENU_PROF_TARIFFS.id_type
                else -> R.string.menu_name_not_know_undefined
            }
        }

        fun getMenuByTypeId(userType: Long): MutableList<EProfileMenu> {
            return when(EUserType.compareUserType(userType)) {
                EUserType.AGENT -> mutableListOf(
                    MENU_PROF_USER_LOGOUT,
                    MENU_PROF_ORDERS,
//                    MENU_PROF_TARIFFS,
                    MENU_PROF_CONTACTS,
                    MENU_PROF_CLIENTS
                )
                else -> mutableListOf(
                    MENU_PROF_USER_LOGOUT
//                    MENU_PROF_CARD_TYPE,
//                    MENU_PROF_TARIFFS,
//                    MENU_PROF_CONTACTS,
//                    MENU_PROF_CLIENTS
                )
            }
        }
    }
}