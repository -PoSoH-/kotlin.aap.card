package sumra.net.app.infinity_card.models

import sumra.net.app.infinity_card.R

data class BottomNavigationMenuItem(val label: Int, val icon: Int, val id: Int)
object UserTypeMenuAgent {
    fun menuUserAgentEnable(): List<BottomNavigationMenuItem> {
        return listOf<BottomNavigationMenuItem>(
            BottomNavigationMenuItem(
                label = R.string.lbl_menu_item_all_cards,
                icon = R.drawable.ic_down_menu_my_card_list,
                id = R.id._fragment_all_card_screen
            ),
            BottomNavigationMenuItem(
                label = R.string.lbl_menu_item_card_pay,
                icon = R.drawable.ic_down_menu_pay_card,
                id = R.id._fragment_card_pay_screen
            ),
            BottomNavigationMenuItem(
                label = R.string.lbl_menu_item_card_type,
                icon = R.drawable.ic_down_menu_type_card,
                id = R.id._fragment_card_type_screen
            ),
            BottomNavigationMenuItem(
                label = R.string.lbl_menu_item_profile,
                icon = R.drawable.ic_down_menu_settings,
                id = R.id._fragment_profile_screen
            )
        )
    }

    fun menuUserAgentDisable():  List<BottomNavigationMenuItem> {
        return listOf<BottomNavigationMenuItem>(
            BottomNavigationMenuItem(
                label = R.string.lbl_menu_item_all_cards,
                icon = R.drawable.ic_down_menu_my_card_list,
                id = R.id._fragment_all_card_screen
            ),
            BottomNavigationMenuItem(
                label = R.string.lbl_menu_item_card_type,
                icon = R.drawable.ic_down_menu_type_card,
                id = R.id._fragment_card_type_screen
            ),
            BottomNavigationMenuItem(
                label = R.string.lbl_menu_item_profile,
                icon = R.drawable.ic_down_menu_settings,
                id = R.id._fragment_profile_screen
            )
        )
    }
}

data class UserTypeMenuEasy(val label: String, val icon: String, val id: String)