package sumra.net.app.infinity_card.presenter

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import moxy.MvpPresenter
import sumra.net.app.infinity_card.enums.ERespCode
import sumra.net.app.infinity_card.models.User
import sumra.net.app.infinity_card.models.UserPatch
import sumra.net.app.infinity_card.repositoryes.RepositoryProfile
import sumra.net.app.infinity_card.views.IViewProfile

class PresenterProfile : MvpPresenter<IViewProfile>() {

    private val mRepository = RepositoryProfile()
    private lateinit var mUser : User

    fun fetchMeProfile(token: String){
        viewState.showProgress(true)
        CoroutineScope(Dispatchers.IO).launch {
            val resp = mRepository.fetchProfileUser("_0000_").await()
            if(resp.success){
                withContext(Dispatchers.Main){
                    if(resp.data != null) {
                        mUser = resp.data
                        viewState.fetchMeSuccess()
                    }else{
                        viewState.fetchMeFailure("No user loaded...")
                    }
                }
            }else{
                withContext(Dispatchers.Main){
                    if(resp == null){
                        viewState.fetchMeFailure(null)
                    }else{
                        viewState.fetchMeFailure(resp.error)
                    }
                }
            }
            withContext(Dispatchers.Main){
                viewState.showProgress(false)
            }
        }
    }

    fun patchUserInformation(token: String, userData: UserPatch){
        viewState.showProgress(true)
        CoroutineScope(Dispatchers.IO).launch {
            val resp = mRepository.patchProfileUser(token, userData).await()
            if(ERespCode.compareType(resp.success)){
                if(resp.data != null) {
                    mUser = resp.data as User
                    withContext(Dispatchers.Main) {
                        viewState.fetchMeSuccess()
                    }
                }else{
                    withContext(Dispatchers.Main){
                        viewState.fetchMeFailure(resp.error)
                    }
                }
            }else{
                if(resp == null){
                    withContext(Dispatchers.Main) {
                        viewState.fetchMeFailure(null)
                    }
                }else{
                    withContext(Dispatchers.Main) {
                        viewState.fetchMeFailure(resp.error)
                    }
                }
            }
            withContext(Dispatchers.Main){
                viewState.showProgress(false)
            }
        }
    }

    fun getUserInfo() = mUser

//    fun fetchMeTariffs(token: String){
//        viewState.showProgress(true)
//        CoroutineScope(Dispatchers.IO).launch {
//            val resp = mRepository.fetchProfileUser("_0000_").await()
//            if(resp.success){
//                withContext(Dispatchers.Main){
//                    viewState.fetchMeSuccess()
//                }
//            }else{
//                withContext(Dispatchers.Main){
//
//
//                    viewState.fetchMeFailure()
//                }
//            }
//            withContext(Dispatchers.Main){
//                viewState.showProgress(false)
//            }
//        }
//    }
//
//    fun fetchMePayDetails(token: String){
//
//    }

}