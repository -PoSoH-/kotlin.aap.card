package sumra.net.app.infinity_card.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import sumra.net.app.infinity_card.R
import sumra.net.app.infinity_card.adapters.holders.HolderOrderItem
import sumra.net.app.infinity_card.interfaces.OnOrderItemClickListener
import sumra.net.app.infinity_card.models.responses.OrderItem

class AdapterOrders(val listener: OnOrderItemClickListener) : RecyclerView.Adapter<HolderOrderItem>() {

    private val mOrders = mutableListOf<OrderItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderOrderItem {
        return HolderOrderItem(LayoutInflater.from(parent.context)
            .inflate(R.layout.cell_ordr_item_show, parent, false))
    }

    override fun onBindViewHolder(holder: HolderOrderItem, position: Int) {
        holder.bindData(order = mOrders[position], listener = listener)
    }

    override fun getItemCount() = mOrders.size

    fun bind(orders: MutableList<OrderItem>){
        this.mOrders.clear()
        this.mOrders.addAll(orders)
        notifyDataSetChanged()
    }
}