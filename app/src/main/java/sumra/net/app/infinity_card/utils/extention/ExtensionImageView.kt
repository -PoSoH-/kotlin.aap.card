package sumra.net.app.infinity_card.utils.extention

import android.graphics.Bitmap
import android.widget.ImageView
import com.bumptech.glide.Glide
import sumra.net.app.infinity_card.R

fun ImageView.showImageByResources(resImg: Int){
    Glide.with(context)
        .load(resImg)
        .placeholder(context.resources.getDrawable(R.drawable.ic_profile_account_empty, context.theme))
        .into(this)
}

fun ImageView.showImageByUrl(urlImg: String){
    Glide.with(context)
        .load(urlImg)
        .placeholder(context.resources.getDrawable(R.drawable.ic_profile_account_empty, context.theme))
        .into(this)
}

fun ImageView.showImageByBitmap(bitmap: Bitmap){
    Glide.with(context)
        .load(bitmap)
        .placeholder(context.resources.getDrawable(R.drawable.ic_profile_account_empty, context.theme))
        .into(this)
}


/*
        Glide.with(view.context)
                .load(url)
                .placeholder(view.context.getDrawable(R.drawable.ic_wallet_app))
                .into(view)
        }

        fun imageAddBitmap(view: ImageView, url: Bitmap) {
            Glide.with(view.context)
                .load(url)
                .placeholder(view.context.getDrawable(R.drawable.ic_wallet_app))
                .into(view)
        }

        fun imageAddByRadius(view: ImageView, url: Any) {
            Glide.with(view.context)
                .load(url)
                .transform(TransformationsRadius(48, 0))
                .placeholder(view.context.getDrawable(R.drawable.ic_wallet_app))
                .into(view)
        }

        fun imageAddByRadius(view: ImageView, url: Any, radius : Int) {
            Glide.with(view.context)
                .load(url)
                .transform(TransformationsRadius(radius, 0))
                .placeholder(view.context.getDrawable(R.drawable.ic_wallet_app))
                .into(view)
        }

        fun imageAddByCircle(view: ImageView, url: Any) {
            Glide.with(view.context)
                .load(url)
                .transform(TransformationsCircle())
                .placeholder(view.context.getDrawable(R.drawable.ic_wallet_app))
                .into(view)
        }
* */