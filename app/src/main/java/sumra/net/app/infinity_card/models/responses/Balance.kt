package sumra.net.app.infinity_card.models.responses


import com.google.gson.annotations.SerializedName

data class Balance(
    @SerializedName("balance")
    val balance: String,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("currency")
    val currency: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("updated_at")
    val updatedAt: String,
    @SerializedName("user_id")
    val userId: Int
)