package sumra.net.app.infinity_card.models.responses


import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("avatar")
    val avatar: String,
    @SerializedName("balances")
    val balances: List<Balance>,
    @SerializedName("cards")
    val cards: List<Card>,
    @SerializedName("contacts")
    val contacts: List<Contact>,
    @SerializedName("country")
    val country: String,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("first_name")
    val firstName: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("last_name")
    val lastName: String,
    @SerializedName("ownership_id")
    val ownershipId: Int,
    @SerializedName("parent_id")
    val parentId: Int,
    @SerializedName("passport_date_issue")
    val passportDateIssue: String,
    @SerializedName("passport_number")
    val passportNumber: String,
    @SerializedName("phone")
    val phone: String,
    @SerializedName("requisites")
    val requisites: List<Requisite>,
    @SerializedName("status")
    val status: Int,
    @SerializedName("tariff_id")
    val tariffId: Int,
    @SerializedName("type_id")
    val typeId: Int,
    @SerializedName("updated_at")
    val updatedAt: String
)