package sumra.net.app.infinity_card.repositoryes

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import sumra.net.app.infinity_card.core.NetCards
import sumra.net.app.infinity_card.models.requests.ModelCardGenerate
import sumra.net.app.infinity_card.repositoryes.converters.ConverterCard

class RepositoryBuyCard {

    fun postFetch(token: String, model: ModelCardGenerate) : Deferred<String> {
        val resp = NetCards.postCardGeneration(token, ConverterCard.fromTypeToSend(model))
        return GlobalScope.async {
            "_0000_0000_0000_0000_"
        }
    }

}