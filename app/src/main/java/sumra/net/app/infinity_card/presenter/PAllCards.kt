package sumra.net.app.infinity_card.presenter

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import moxy.MvpPresenter
import sumra.net.app.infinity_card.models.CardTypeItem
import sumra.net.app.infinity_card.repositoryes.RepositoryAllCards
import sumra.net.app.infinity_card.enums.ERespCode
import sumra.net.app.infinity_card.views.IViewAllCards

class PAllCards : MvpPresenter<IViewAllCards>() {

    private val repository = RepositoryAllCards()
    private val mCards = mutableListOf<String>("More", "More" ,"More" ,"More" ,"More" ,"More" ,"More")
    private val mCardTypes = arrayListOf<CardTypeItem>()

    fun fetchCardsAll(token: String){
        viewState.showProgress(true)
        CoroutineScope(Dispatchers.IO).launch {
            withContext(Dispatchers.Main){
                viewState.showProgress(false)
                viewState.loadAllCardsSuccess()
            }
        }
    }

    fun fetchCardTypes(token: String){
        viewState.showProgress(true)
        CoroutineScope(Dispatchers.IO).launch {
            val resp = repository.fetchCardTypes(token).await()
            if(resp.success.equals(ERespCode.SUCCESS.type)){
                mCardTypes.clear()
                mCardTypes.addAll(resp.data!!.cardTypes)
                withContext(Dispatchers.Main){
                    viewState.loadAllCardTypesSuccess()
                }
            }else{
                val er : String?
                if(resp.error == null){
                    er = null
                }else{
                    er = resp.error
                }
                withContext(Dispatchers.Main){
                    viewState.loadAllCardTypesFailure(er)
                }
            }
            withContext(Dispatchers.Main){
                viewState.showProgress(false)
            }
        }
    }

    fun getAllCards() = mCards
    fun getCardTypes() = mCardTypes

}