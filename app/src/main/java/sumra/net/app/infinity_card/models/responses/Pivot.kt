package sumra.net.app.infinity_card.models.responses


import com.google.gson.annotations.SerializedName

data class Pivot(
    @SerializedName("requisite_id")
    val requisiteId: Int,
    @SerializedName("user_id")
    val userId: Int,
    @SerializedName("value")
    val value: String
)