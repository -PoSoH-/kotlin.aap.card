package sumra.net.app.infinity_card.openIdmanagers

final object Config {

//    "client_id": "8cvnmMyUR1MhCSxNQu3jYXaK0Sga",
//    "client_secret": "B4pjhmVW6SAr6tJ5rFpr1nGJdq4a",
//    "redirect_uri": "sumra.infinity://oauth",
//    "authorization_scope": "openid, profile",
//    "end_session_endpoint": "https://apim.sumra.net/oidc/logout",
//    "authorization_endpoint_uri": "https://apim.sumra.net/oauth2/authorize",
//    "token_endpoint_uri": "https://apim.sumra.net/oauth2/token",
//    "user_info_endpoint_uri": "https://apim.sumra.net/oauth2/userinfo",
//    "https_required": true

    // TODO: Add the information you received from your OIDC provider below.
    // TODO: Add the information you received from your OIDC provider below.
    val clientId = "8cvnmMyUR1MhCSxNQu3jYXaK0Sga"
    val clientSecret = "B4pjhmVW6SAr6tJ5rFpr1nGJdq4a"

    val authorizationServerUrl = "https://apim.sumra.net/oauth2/authorize"
    val tokenServerUrl = "https://apim.sumra.net/oauth2/token"
    val userInfoUrl = "https://apim.sumra.net/oauth2/userinfo"
    val userLogout = "https://apim.sumra.net/oidc/logout"

    // This URL doesn't really have a use with native apps and basically just signifies the end
    // of the authorisation process. It doesn't have to be a real URL, but it does have to be the
    // same URL that is registered with your provider.
    val redirectUrl = "sumra.infinity://oauth"

    // The `offline_access` scope enables us to request Refresh Tokens, so we don't have to ask the
    // user to authorise us again every time the tokens expire. Some providers might have an
    // `offline` scope instead. If you get an `invalid_scope` error when trying to authorise the
    // app, try changing it to `offline`.
    val scopes :Array<String?> = arrayOf("openid, profile")

    enum class Flows {
        AuthorizationCode,  //http://openid.net/specs/openid-connect-core-1_0.html#CodeFlowAuth
        Implicit,  //http://openid.net/specs/openid-connect-core-1_0.html#ImplicitFlowAuth
        Hybrid //http://openid.net/specs/openid-connect-core-1_0.html#HybridFlowAuth
    }

    // The authorization flow type that determine the response_type authorization request should use.
    // One of the supported flows AuthorizationCode, Implicit or Hybrid.
    // For more info see http://openid.net/specs/openid-connect-core-1_0.html#Authentication
    val flowType = Flows.Hybrid
}