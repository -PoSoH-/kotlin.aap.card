package sumra.net.app.infinity_card.adapters.holders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.cell_ordr_item_show.view.*
import sumra.net.app.infinity_card.enums.ECurrency
import sumra.net.app.infinity_card.enums.EOrderStatus
import sumra.net.app.infinity_card.interfaces.OnOrderItemClickListener
import sumra.net.app.infinity_card.models.responses.OrderItem

class HolderOrderItem(val cellView: View): RecyclerView.ViewHolder(cellView) {

    fun bindData(order: OrderItem, listener: OnOrderItemClickListener){
        cellView.txt_profile_order_item_number_vls.text = order.id.toString()
        cellView.txt_profile_order_item_date_created_vls.text = order.createAt
        cellView.txt_profile_order_item_total_price_vls.text = order.totalPrice

        cellView.txt_profile_order_item_total_currency_vls.text = ECurrency.compareCurrencyByName(order.currency)

        cellView.txt_profile_order_item_order_status_vls.apply {
            text = EOrderStatus.checkStatus(order.status)
            setTextColor(resources.getColor(EOrderStatus.checkColor(order.status), resources.newTheme()))
        }

        cellView.apply {
            setOnClickListener {
                listener.onOrderItemClick(order.id)
            }
        }
    }
}