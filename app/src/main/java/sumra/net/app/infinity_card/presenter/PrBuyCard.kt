package sumra.net.app.infinity_card.presenter

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import moxy.MvpPresenter
import sumra.net.app.infinity_card.models.requests.ModelCardGenerate
import sumra.net.app.infinity_card.repositoryes.RepositoryBuyCard
import sumra.net.app.infinity_card.views.IViewCard

class PrBuyCard : MvpPresenter<IViewCard>(){

    private val mRepoCard = RepositoryBuyCard()
    private val mIDCardsTokens = mutableListOf<ModelCardGenerate>()

    fun postGenerateCard (token: String) { // model: ModelCardGenerate){
        viewState.showProgress(true)
        CoroutineScope(Dispatchers.IO).launch {
            mIDCardsTokens.forEach {
                val resp = mRepoCard.postFetch(token= token, model = it).await()
            }
            withContext(Dispatchers.Main) {
                viewState.showProgress(false)
            }
        }
    }

    fun addTypeCard(idType: String){
        mIDCardsTokens.add(ModelCardGenerate(idType))
    }

    fun removeTypeCard(idType: String){
        mIDCardsTokens.remove(ModelCardGenerate(idType))
    }

    fun emptyCardList() = !mIDCardsTokens.isNullOrEmpty()


}