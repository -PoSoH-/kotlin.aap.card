package sumra.net.app.infinity_card.presenter

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import moxy.MvpPresenter
import sumra.net.app.infinity_card.models._jsonRespUserClient
import sumra.net.app.infinity_card.repositoryes.RepositoryClientUsers
import sumra.net.app.infinity_card.views.IViewClientUsers

class PClientUsers :MvpPresenter<IViewClientUsers>() {

    private val repository = RepositoryClientUsers()
    private lateinit var info : _jsonRespUserClient

    fun fetchClientUsers(){
        viewState.showProgress(true)
        CoroutineScope(Dispatchers.IO).launch {
            val resp = repository.fetchClientUsers().await()
            info = resp

            withContext(Dispatchers.Main){
                viewState.loadClientsSuccess()
                viewState.showProgress(false)
            }
        }
    }

    fun getClients() = info.clients as MutableList
}