//package sumra.net.app.infinity_card.openIdmanagers.authenticator
//
//import android.accounts.*
//import android.content.Context
//import android.content.Intent
//import android.os.Bundle
//import android.text.TextUtils
//import android.util.Log
//import com.google.api.client.auth.oauth2.TokenResponseException
//import com.google.api.client.auth.openidconnect.IdTokenResponse
//import sumra.net.app.infinity_card.openIdmanagers.Config
//import java.io.IOException
//import java.net.HttpURLConnection
//
//class Authenticator(private var context: Context?) : AbstractAccountAuthenticator(context) {
//
//    private val TAG = javaClass.simpleName
//
//    private var accountManager: AccountManager? = null
//
//    companion object {
//        val TOKEN_TYPE_ID = "com.lnikkila.oidcsample.TOKEN_TYPE_ID"
//        val TOKEN_TYPE_ACCESS = "com.lnikkila.oidcsample.TOKEN_TYPE_ACCESS"
//        val TOKEN_TYPE_REFRESH = "com.lnikkila.oidcsample.TOKEN_TYPE_REFRESH"
//    }
//
//    init {
//        accountManager = AccountManager.get(context)
//        Log.d(TAG, "Authenticator created.")
//    }
//
//    /**
//     * Called when the user adds a new account through Android's system settings or when an app
//     * explicitly calls this.
//     */
//    override fun addAccount(
//        response: AccountAuthenticatorResponse, accountType: String?,
//        authTokenType: String?, requiredFeatures: Array<String?>?, options: Bundle?
//    ): Bundle? {
//        Log.d(
//            TAG, String.format(
//                "addAccount called with accountType %s, authTokenType %s.",
//                accountType, authTokenType
//            )
//        )
//        val result = Bundle()
//        val intent = createIntentForAuthorization(response)
//
//        // We're creating a new account, not just renewing our authorisation
//        intent.putExtra(AuthenticatorActivity.KEY_IS_NEW_ACCOUNT, true)
//        result.putParcelable(AccountManager.KEY_INTENT, intent)
//        return result
//    }
//
//    /**
//     * Tries to retrieve a previously stored token of any type. If the token doesn't exist yet or
//     * has been invalidated, we need to request a set of replacement tokens.
//     */
//    override fun getAuthToken(
//        response: AccountAuthenticatorResponse, account: Account,
//        authTokenType: String?, options: Bundle?
//    ): Bundle? {
//        Log.d(
//            TAG, String.format(
//                "getAuthToken called with account.type '%s', account.name '%s', " +
//                        "authTokenType '%s'.", account.type, account.name, authTokenType
//            )
//        )
//
//        // Try to retrieve a stored token
//        var token = accountManager!!.peekAuthToken(account, authTokenType)
//        if (TextUtils.isEmpty(token)) {
//            // If we don't have one or the token has been invalidated, we need to check if we have
//            // a refresh token
//            Log.d(TAG, "Token empty, checking for refresh token.")
//            val refreshToken = accountManager!!.peekAuthToken(account, TOKEN_TYPE_REFRESH)
//            if (TextUtils.isEmpty(refreshToken)) {
//                // If we don't even have a refresh token, we need to launch an intent for the user
//                // to get us a new set of tokens by authorising us again.
//                Log.d(TAG, "Refresh token empty, launching intent for renewing authorisation.")
//                val result = Bundle()
//                val intent = createIntentForAuthorization(response)
//
//                // Provide the account that we need re-authorised
//                intent.putExtra(AuthenticatorActivity.KEY_ACCOUNT_OBJECT, account)
//                result.putParcelable(AccountManager.KEY_INTENT, intent)
//                return result
//            } else {
//                // Got a refresh token, let's use it to get a fresh set of tokens
//                Log.d(TAG, "Got refresh token, getting new tokens.")
//                val tokenResponse: IdTokenResponse?
//                try {
//                    tokenResponse = OIDCUtils.refreshTokens(
//                        tokenServerUrl = Config.tokenServerUrl,
//                        clientId = Config.clientId,
//                        clientSecret = Config.clientSecret,
//                        scopes = Config.scopes,
//                        refreshToken = refreshToken
//                    )
//                    Log.d(TAG, "Got new tokens.")
//                    accountManager!!.setAuthToken(account, TOKEN_TYPE_ID, tokenResponse!!.getIdToken())
//                    accountManager!!.setAuthToken(account, TOKEN_TYPE_ACCESS, tokenResponse!!.getAccessToken())
//                    accountManager!!.setAuthToken(account, TOKEN_TYPE_REFRESH, tokenResponse!!.getRefreshToken())
//                } catch (e: TokenResponseException) {
//                    if (e.getStatusCode() === HttpURLConnection.HTTP_BAD_REQUEST && e.getContent()
//                            .contains("invalid_grant")
//                    ) {
//                        // If the refresh token has expired, we need to launch an intent for the user
//                        // to get us a new set of tokens by authorising us again.
//                        Log.d(TAG, "Refresh token expired, launching intent for renewing authorisation.")
//                        val result = Bundle()
//                        val intent = createIntentForAuthorization(response)
//
//                        // Provide the account that we need re-authorised
//                        intent.putExtra(AuthenticatorActivity.KEY_ACCOUNT_OBJECT, account)
//                        result.putParcelable(AccountManager.KEY_INTENT, intent)
//                        return result
//                    } else {
//                        // There's not much we can do if we get here
//                        Log.e(TAG, "Couldn't get new tokens.", e)
//                    }
//                } catch (e: IOException) {
//                    // There's not much we can do if we get here
//                    Log.e(TAG, "Couldn't get new tokens.", e)
//                }
//
//                // Now, let's return the token that was requested
//                token = accountManager!!.peekAuthToken(account, authTokenType)
//            }
//        }
//        Log.d(TAG, String.format("Returning token '%s' of type '%s'.", token, authTokenType))
//        val result = Bundle()
//        result.putString(AccountManager.KEY_ACCOUNT_NAME, account.name)
//        result.putString(AccountManager.KEY_ACCOUNT_TYPE, account.type)
//        result.putString(AccountManager.KEY_AUTHTOKEN, token)
//        return result
//    }
//
//    /**
//     * Create an intent for showing the authorisation web page.
//     */
//    private fun createIntentForAuthorization(response: AccountAuthenticatorResponse): Intent {
//        val intent = Intent(context, AuthenticatorActivity::class.java)
//
//        // Generate a new authorisation URL
//        val authUrl: String?
//        authUrl = when (Config.flowType) {
//            Config.Flows.AuthorizationCode -> OIDCUtils.codeFlowAuthenticationUrl(
//                Config.authorizationServerUrl,
//                Config.clientId,
//                Config.redirectUrl,
//                Config.scopes
//            )
//            Config.Flows.Implicit -> OIDCUtils.implicitFlowAuthenticationUrl(
//                Config.authorizationServerUrl,
//                Config.clientId,
//                Config.redirectUrl,
//                Config.scopes
//            )
//            Config.Flows.Hybrid -> OIDCUtils.hybridFlowAuthenticationUrl(
//                Config.authorizationServerUrl,
//                Config.clientId,
//                Config.redirectUrl,
//                Config.scopes
//            )
//            else -> {
//                Log.d(TAG, "Requesting unsupported flowType! Using CodeFlow instead")
//                OIDCUtils.codeFlowAuthenticationUrl(
//                    Config.authorizationServerUrl,
//                    Config.clientId,
//                    Config.redirectUrl,
//                    Config.scopes
//                )
//            }
//        }
//        Log.d(TAG, String.format("Created new intent with authorisation URL '%s'.", authUrl))
//        intent.putExtra(AuthenticatorActivity.KEY_AUTH_URL, authUrl)
//        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response)
//        return intent
//    }
//
//    override fun getAuthTokenLabel(authTokenType: String?): String? {
//        return null
//    }
//
//    @Throws(NetworkErrorException::class)
//    override fun hasFeatures(
//        response: AccountAuthenticatorResponse?, account: Account?,
//        features: Array<String?>?
//    ): Bundle? {
//        return null
//    }
//
//    override fun editProperties(response: AccountAuthenticatorResponse?, accountType: String?): Bundle? {
//        return null
//    }
//
//    @Throws(NetworkErrorException::class)
//    override fun confirmCredentials(
//        response: AccountAuthenticatorResponse?, account: Account?,
//        options: Bundle?
//    ): Bundle? {
//        return null
//    }
//
//    @Throws(NetworkErrorException::class)
//    override fun updateCredentials(
//        response: AccountAuthenticatorResponse?, account: Account?,
//        authTokenType: String?, options: Bundle?
//    ): Bundle? {
//        return null
//    }
//}