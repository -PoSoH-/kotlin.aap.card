package sumra.net.app.infinity_card.models.responses


import com.google.gson.annotations.SerializedName

data class _USER_NEW_PARSER_(
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("user")
    val user: User
)