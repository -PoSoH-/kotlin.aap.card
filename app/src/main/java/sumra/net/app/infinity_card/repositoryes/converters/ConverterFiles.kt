package sumra.net.app.infinity_card.repositoryes.converters

import com.google.gson.Gson
import sumra.net.app.infinity_card.enums.EFileLoadType
import sumra.net.app.infinity_card.enums.ERespCode
import sumra.net.app.infinity_card.models._jsonNetRespFiles
import sumra.net.app.infinity_card.models.requests.ModelPostRequest
import sumra.net.app.infinity_card.models.responses._jsonRespErrorMin
import sumra.net.app.infinity_card.models.responses._respErrorMax
import sumra.net.app.infinity_card.models.responses._respNetFiles
import java.lang.ClassCastException
import kotlin.random.Random

class ConverterFiles {
    companion object{

        fun convertLinkForRequest(image:String, name:String):String{
            return Gson().toJson(ModelPostRequest(
                entity = EFileLoadType.AVATAR.value,
//                entityId = "",
                image = image,
                name = name))
        }

        fun convertRespToLink(args: String) : _respNetFiles{

            var isGreat: _jsonNetRespFiles? = null
            var isError: _respErrorMax? = null

            try{
                isGreat = Gson().fromJson<_jsonNetRespFiles>(args, _jsonNetRespFiles::class.java)
            }catch(erGreat: ClassCastException){
                try {
                    isError = Gson().fromJson<_respErrorMax>(args, _respErrorMax::class.java)
                }catch(erError: ClassCastException){
                    isError = _respErrorMax(type = ERespCode.ERROR.type, title = "Error", message = erError.message!!)
                }
            }

            if(isGreat != null){
                return _respNetFiles(success = ERespCode.SUCCESS.type, data =isGreat.respFiles.attributes, error = null)
            }else{
                return _respNetFiles(success = isError!!.type, data = null, error = isError.message)
            }

//            val mockURL = listOf(
//                "http://s11.favim.com/orig/7/758/7589/75894/cute-anime-girl-Favim.com-7589410.jpg",
//                "http://p.favim.com/orig/2018/09/26/blue-eyes-neko-smile-Favim.com-6366919.jpg",
//                "http://p.favim.com/orig/2018/12/15/blush-anime-avi-anime-Favim.com-6660353.jpg",
//                "http://p.favim.com/orig/2018/08/27/cute-anime-girl-kawaii-red-aesthetic-Favim.com-6185385.jpg",
//                "http://s8.favim.com/orig/150117/anime-anime-girl-cute-anime-cute-anime-girl-Favim.com-2392019.jpg",
//                "http://s6.favim.com/orig/140710/cute-anime-girl-face-pretty-art-dangan-ronpa-Favim.com-1904126.png"
//            )
//            val s = 0
//            val f = mockURL.size-1
//            val r = Random.Default.nextInt(s, f)
//            return _respNetFiles(
//                success = ERespCode.SUCCESS.type,
//                data = mockURL[r],
//                error = null)
        }
    }
}


/*
* reserve image links
* https://www.shutterstock.com/image-illustration/anime-girl-587949341
* https://www.shutterstock.com/image-illustration/anime-girl-comic-cartoon-character-t-1654537423
* https://www.shutterstock.com/image-vector/anime-girl-on-white-background-1789524179
* https://www.shutterstock.com/image-illustration/anime-girl-comic-cartoon-character-t-1654536457
* */