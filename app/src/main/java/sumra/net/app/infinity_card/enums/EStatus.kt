package sumra.net.app.infinity_card.enums

enum class EStatus {
    CREATED,
    CONFIRM,
    BLOCKED
}