package sumra.net.app.infinity_card.frgSignInAndSignUp.navigation.verifyCode

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.navigation.Navigation
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.frg_auth_code.*
import kotlinx.android.synthetic.main.frg_auth_phone.*
import kotlinx.coroutines.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter
import sumra.net.app.infinity_card.AppCard
import sumra.net.app.infinity_card.R
import sumra.net.app.infinity_card.enums.EKeys
import sumra.net.app.infinity_card.utils.extention.hideKeyboard
import sumra.net.app.infinity_card.utils.extention.inputText
import sumra.net.app.infinity_card.widgets.CustomSnackBar.CustomSnackBar

class FrgVerifyCode : MvpAppCompatFragment(R.layout.frg_auth_code),
    IViewVerifyCode {

    @InjectPresenter
    lateinit var mPresenter: PrVerifyCode
    private lateinit var bndl: Bundle

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let { bndl = it }

        val phone = bndl.getString(EKeys.AUTH_USER_PHONE, "")!!
        val code = bndl.getString(EKeys.AUTH_USER_CODE, "")!!
        val codeID = bndl.getString(EKeys.AUTH_USER_CODE_ID, "")!!

        initInputText()
        updateCodePhone(code)

        btnFrgVerifyCODE.apply {
            setOnClickListener {
                if (txtFrgNumberVerify0.text.isNullOrEmpty()
                    || txtFrgNumberVerify1.text.isNullOrEmpty()
                    || txtFrgNumberVerify2.text.isNullOrEmpty()
                    || txtFrgNumberVerify3.text.isNullOrEmpty()
                    || txtFrgNumberVerify4.text.isNullOrEmpty()
                    || txtFrgNumberVerify5.text.isNullOrEmpty()
                ) {
                    CustomSnackBar.make(
                        view = rootFrgAuthCODE,
                        typeMessage = CustomSnackBar.EMessageType.ERROR,
                        header = resources.getString(R.string.txt_login_section_header_phone),
                        message = resources.getString(R.string.error_phone_not_code),
                        duration = Snackbar.LENGTH_SHORT,
                        listener = null,
                        action_lable = null
                    )?.show()
                } else {

                    if(txtFrgNumberUserName.text.isNullOrEmpty()) {
                        val show = CustomSnackBar.make(
                            view = rootFrgAuthCODE,
                            typeMessage = CustomSnackBar.EMessageType.ERROR,
                            header = resources.getString(R.string.txt_login_section_header_phone),
                            message = resources.getString(R.string.frg_user_name_hint),
                            duration = Snackbar.LENGTH_SHORT,
                            listener = null,
                            action_lable = null
                        )?.show()
                    }else {

                        hideKeyboard()

                        bndl.putString(EKeys.AUTH_USER_PHONE, phone)
                        bndl.putString(EKeys.AUTH_USER_CODE, code)
                        bndl.putString(EKeys.AUTH_USER_NAME, txtFrgNumberUserName.text.toString())
                        bndl.putString(EKeys.AUTH_USER_CODE_ID, codeID)

                        mPresenter.sendVerifyCode(phone, code,txtFrgNumberUserName.text.toString())
                    }

//                    showProgress(true)
//                    CoroutineScope(Dispatchers.IO).launch {
//                        delay(2_500)
//                        withContext(Dispatchers.Main){
//                            showProgress(false)
//                            Navigation.findNavController(
//                                requireActivity(),
//                                R.id.nav_host_auth
//                            ).navigate(R.id._frg_auth_info_added, bundle)
//                            removeAllCode()
//                        }
//                    }
//                    mPrPhone.sendPhoneForVerify(
//                        "_000_",
//                        "${cppNumberPhoneInput.selectedCountryCode}${
//                            txtFrgLoginUserPhone.text.toString()
//                                .replace(" ", "")
//                        }"
//                    )
                }
            }
        }

    }

    private fun initInputText() {
        txtFrgNumberVerify0.apply {
            addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    s?.let {
                        if (it.length == 2) txtFrgNumberVerify1.requestFocus()
//                        if(it.length == 0) txtFrgNumberFirst.requestFocus()
                    }

                    4
                }

                override fun afterTextChanged(s: Editable?) {
//                    s?.let {txtFrgNumberSecond.setSelection(it.length) }
                }
            })
        }
        txtFrgNumberVerify1.apply {
            addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    s?.let {
                        if (it.length == 2) txtFrgNumberVerify2.requestFocus()
                        if (it.length == 0) txtFrgNumberVerify0.requestFocus()
                    }

                }

                override fun afterTextChanged(s: Editable?) {
//                    s?.let {txtFrgNumberSecond.setSelection(it.length)}
                }
            })
        }
        txtFrgNumberVerify2.apply {
            addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    s?.let {
                        if (it.length == 2) txtFrgNumberVerify3.requestFocus()
                        if (it.length == 0) txtFrgNumberVerify1.requestFocus()
//                        txtFrgNumberSecond.setSelection(it.length)
                    }
                }

                override fun afterTextChanged(s: Editable?) {}
            })
        }
        txtFrgNumberVerify3.apply {
            addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    s?.let {
                        if (it.length == 0) txtFrgNumberVerify2.requestFocus()
//                        txtFrgNumberSecond.setSelection(it.length)
                    }

                }

                override fun afterTextChanged(s: Editable?) {}
            })
        }
    }

    fun updateCodePhone(code: String) {
        txtFrgNumberVerify0.inputText(code[0])
        txtFrgNumberVerify1.inputText(code[1])
        txtFrgNumberVerify2.inputText(code[2])
        txtFrgNumberVerify3.inputText(code[3])
        txtFrgNumberVerify4.inputText(code[4])
        txtFrgNumberVerify5.inputText(code[5])
    }

    private fun removeAllCode() {
        txtFrgNumberVerify0.inputText("")
        txtFrgNumberVerify1.inputText("")
        txtFrgNumberVerify2.inputText("")
        txtFrgNumberVerify3.inputText("")
        txtFrgNumberVerify4.inputText("")
        txtFrgNumberVerify5.inputText("")
    }

    override fun sendVerifyCodeSuccess() {
        Navigation.findNavController(
            requireActivity(),
            R.id.nav_host_authorization
        ).navigate(R.id._frg_auth_info_added, bndl)
        removeAllCode()
    }

    override fun sendVerifyCodeFailure(error: String?) {
        CustomSnackBar.make(
            view = rootFrgAuthCODE,
            typeMessage = CustomSnackBar.EMessageType.ERROR,
            header = resources.getString(R.string.txt_login_section_header_phone),
            message = error!!,
            duration = Snackbar.LENGTH_SHORT,
            listener = null,
            action_lable = null
        )?.show()
    }

    override fun showProgress(state: Boolean) {
        when (state) {
            true ->
                AppCard.showProgress()
            else ->
                AppCard.hideProgress()
        }
    }
}
