package sumra.net.app.infinity_card.adapters.holders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.cell_menu_profile_item.view.*
import sumra.net.app.infinity_card.enums.EProfileMenu
import sumra.net.app.infinity_card.interfaces.IProfileMenuSelectListener

class HolderMenuProfile(val cell: View) : RecyclerView.ViewHolder(cell) {
    fun bindMenu(idResource: EProfileMenu, listener: IProfileMenuSelectListener){
        cell.cell_profile_menu_item.text = cell.context.resources.getString(idResource.id_type)
        cell.apply {
            setOnClickListener {
                listener.selectMenuItem(idResource)
            }
        }
    }
}