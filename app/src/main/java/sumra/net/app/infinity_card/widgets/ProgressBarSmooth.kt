package sumra.net.app.infinity_card.widgets

import android.content.Context
import android.content.res.Resources
import android.graphics.*
import android.graphics.drawable.Animatable
import android.graphics.drawable.Drawable
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.Shape
import android.os.SystemClock
import android.util.AttributeSet
import android.view.animation.*
import android.view.animation.Interpolator
import android.widget.ProgressBar
import androidx.annotation.UiThread
import sumra.net.app.infinity_card.R
import sumra.net.app.infinity_card.widgets.ProgressBarSmooth.SmoothProgressBarUtils.checkColors
import sumra.net.app.infinity_card.widgets.ProgressBarSmooth.SmoothProgressBarUtils.checkPositive
import sumra.net.app.infinity_card.widgets.ProgressBarSmooth.SmoothProgressBarUtils.checkPositiveOrZero
import sumra.net.app.infinity_card.widgets.ProgressBarSmooth.SmoothProgressBarUtils.checkSpeed
import java.util.*


class ProgressBarSmooth constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = R.attr.spbStyle
) :
    ProgressBar(context, attrs, defStyle) {
    fun applyStyle(styleResId: Int) {
        val a = context.obtainStyledAttributes(null, R.styleable.SmoothProgressBar, 0, styleResId)
        if (a.hasValue(R.styleable.SmoothProgressBar_spb_color)) {
            setSmoothProgressDrawableColor(a.getColor(R.styleable.SmoothProgressBar_spb_color, 0))
        }
        if (a.hasValue(R.styleable.SmoothProgressBar_spb_colors)) {
            val colorsId = a.getResourceId(R.styleable.SmoothProgressBar_spb_colors, 0)
            if (colorsId != 0) {
                val colors = resources.getIntArray(colorsId)
                if (colors != null && colors.size > 0) setSmoothProgressDrawableColors(colors)
            }
        }
        if (a.hasValue(R.styleable.SmoothProgressBar_spb_sections_count)) {
            setSmoothProgressDrawableSectionsCount(a.getInteger(R.styleable.SmoothProgressBar_spb_sections_count, 0))
        }
        if (a.hasValue(R.styleable.SmoothProgressBar_spb_stroke_separator_length)) {
            setSmoothProgressDrawableSeparatorLength(
                a.getDimensionPixelSize(
                    R.styleable.SmoothProgressBar_spb_stroke_separator_length,
                    0
                )
            )
        }
        if (a.hasValue(R.styleable.SmoothProgressBar_spb_stroke_width)) {
            setSmoothProgressDrawableStrokeWidth(a.getDimension(R.styleable.SmoothProgressBar_spb_stroke_width, 0f))
        }
        if (a.hasValue(R.styleable.SmoothProgressBar_spb_speed)) {
            setSmoothProgressDrawableSpeed(a.getFloat(R.styleable.SmoothProgressBar_spb_speed, 0f))
        }
        if (a.hasValue(R.styleable.SmoothProgressBar_spb_progressiveStart_speed)) {
            setSmoothProgressDrawableProgressiveStartSpeed(
                a.getFloat(
                    R.styleable.SmoothProgressBar_spb_progressiveStart_speed,
                    0f
                )
            )
        }
        if (a.hasValue(R.styleable.SmoothProgressBar_spb_progressiveStop_speed)) {
            setSmoothProgressDrawableProgressiveStopSpeed(
                a.getFloat(
                    R.styleable.SmoothProgressBar_spb_progressiveStop_speed,
                    0f
                )
            )
        }
        if (a.hasValue(R.styleable.SmoothProgressBar_spb_reversed)) {
            setSmoothProgressDrawableReversed(a.getBoolean(R.styleable.SmoothProgressBar_spb_reversed, false))
        }
        if (a.hasValue(R.styleable.SmoothProgressBar_spb_mirror_mode)) {
            setSmoothProgressDrawableMirrorMode(a.getBoolean(R.styleable.SmoothProgressBar_spb_mirror_mode, false))
        }
        if (a.hasValue(R.styleable.SmoothProgressBar_spb_progressiveStart_activated)) {
            setProgressiveStartActivated(
                a.getBoolean(
                    R.styleable.SmoothProgressBar_spb_progressiveStart_activated,
                    false
                )
            )
        }
        if (a.hasValue(R.styleable.SmoothProgressBar_spb_progressiveStart_activated)) {
            setProgressiveStartActivated(
                a.getBoolean(
                    R.styleable.SmoothProgressBar_spb_progressiveStart_activated,
                    false
                )
            )
        }
        if (a.hasValue(R.styleable.SmoothProgressBar_spb_gradients)) {
            setSmoothProgressDrawableUseGradients(a.getBoolean(R.styleable.SmoothProgressBar_spb_gradients, false))
        }
        if (a.hasValue(R.styleable.SmoothProgressBar_spb_generate_background_with_colors)) {
            if (a.getBoolean(R.styleable.SmoothProgressBar_spb_generate_background_with_colors, false)) {
                setSmoothProgressDrawableBackgroundDrawable(
                    SmoothProgressBarUtils.generateDrawableWithColors(
                        checkIndeterminateDrawable().colors,
                        checkIndeterminateDrawable().strokeWidth
                    )
                )
            }
        }
        if (a.hasValue(R.styleable.SmoothProgressBar_spb_interpolator)) {
            val iInterpolator = a.getInteger(R.styleable.SmoothProgressBar_spb_interpolator, -1)
            val interpolator: Interpolator?
            interpolator =
                when (iInterpolator) {
                    INTERPOLATOR_ACCELERATEDECELERATE -> AccelerateDecelerateInterpolator()
                    INTERPOLATOR_DECELERATE -> DecelerateInterpolator()
                    INTERPOLATOR_LINEAR -> LinearInterpolator()
                    INTERPOLATOR_ACCELERATE -> AccelerateInterpolator()
                    else -> null
                }
            if (interpolator != null) {
                setInterpolator(interpolator)
            }
        }
        a.recycle()
    }

    @Synchronized
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        if (isIndeterminate && indeterminateDrawable is SmoothProgressDrawable &&
            !(indeterminateDrawable as SmoothProgressDrawable).isRunning
        ) {
            indeterminateDrawable.draw(canvas)
        }
    }

    private fun checkIndeterminateDrawable(): SmoothProgressDrawable {
        val ret = indeterminateDrawable
        if (ret == null || ret !is SmoothProgressDrawable) throw RuntimeException("The drawable is not a SmoothProgressDrawable")
        return ret as SmoothProgressDrawable
    }

    override fun setInterpolator(interpolator: Interpolator?) {
        super.setInterpolator(interpolator)
        val ret = indeterminateDrawable
        if (ret != null && ret is SmoothProgressDrawable) (ret as SmoothProgressDrawable).setInterpolator(interpolator)
    }

    fun setSmoothProgressDrawableInterpolator(interpolator: Interpolator?) {
        checkIndeterminateDrawable().setInterpolator(interpolator)
    }

    fun setSmoothProgressDrawableColors(colors: IntArray?) {
        checkIndeterminateDrawable().colors = colors
    }

    fun setSmoothProgressDrawableColor(color: Int) {
        checkIndeterminateDrawable().setColor(color)
    }

    fun setSmoothProgressDrawableSpeed(speed: Float) {
        checkIndeterminateDrawable().setSpeed(speed)
    }

    fun setSmoothProgressDrawableProgressiveStartSpeed(speed: Float) {
        checkIndeterminateDrawable().setProgressiveStartSpeed(speed)
    }

    fun setSmoothProgressDrawableProgressiveStopSpeed(speed: Float) {
        checkIndeterminateDrawable().setProgressiveStopSpeed(speed)
    }

    fun setSmoothProgressDrawableSectionsCount(sectionsCount: Int) {
        checkIndeterminateDrawable().setSectionsCount(sectionsCount)
    }

    fun setSmoothProgressDrawableSeparatorLength(separatorLength: Int) {
        checkIndeterminateDrawable().setSeparatorLength(separatorLength)
    }

    fun setSmoothProgressDrawableStrokeWidth(strokeWidth: Float) {
        checkIndeterminateDrawable().strokeWidth = strokeWidth
    }

    fun setSmoothProgressDrawableReversed(reversed: Boolean) {
        checkIndeterminateDrawable().setReversed(reversed)
    }

    fun setSmoothProgressDrawableMirrorMode(mirrorMode: Boolean) {
        checkIndeterminateDrawable().setMirrorMode(mirrorMode)
    }

    fun setProgressiveStartActivated(progressiveStartActivated: Boolean) {
        checkIndeterminateDrawable().setProgressiveStartActivated(progressiveStartActivated)
    }

    fun SmoothProgressDrawable.Callbacks?.setSmoothProgressDrawableCallbacks() {
        this@ProgressBarSmooth.checkIndeterminateDrawable().setCallbacks(this)
    }

    fun setSmoothProgressDrawableBackgroundDrawable(drawable: Drawable?) {
        if (drawable != null) {
            checkIndeterminateDrawable().setBackgroundDrawable(drawable)
        }
    }

    fun setSmoothProgressDrawableUseGradients(useGradients: Boolean) {
        checkIndeterminateDrawable().setUseGradients(useGradients)
    }

    fun progressiveStart() {
        checkIndeterminateDrawable().progressiveStart()
    }

    fun progressiveStop() {
        checkIndeterminateDrawable().progressiveStop()
    }

    companion object {
        private const val INTERPOLATOR_ACCELERATE = 0
        private const val INTERPOLATOR_LINEAR = 1
        private const val INTERPOLATOR_ACCELERATEDECELERATE = 2
        private const val INTERPOLATOR_DECELERATE = 3
    }

    init {
        if (isInEditMode) {
            indeterminateDrawable = SmoothProgressDrawable.Builder(context, true).build()
        }else {
            val res: Resources = context.resources
            val a = context.obtainStyledAttributes(attrs, R.styleable.SmoothProgressBar, defStyle, 0)
            val color = a.getColor(R.styleable.SmoothProgressBar_spb_color, res.getColor(R.color.spb_default_color))
            val sectionsCount = a.getInteger(
                R.styleable.SmoothProgressBar_spb_sections_count,
                res.getInteger(R.integer.spb_default_sections_count)
            )
            val separatorLength = a.getDimensionPixelSize(
                R.styleable.SmoothProgressBar_spb_stroke_separator_length,
                res.getDimensionPixelSize(R.dimen.spb_default_stroke_separator_length)
            )
            val strokeWidth = a.getDimension(
                R.styleable.SmoothProgressBar_spb_stroke_width,
                res.getDimension(R.dimen.spb_default_stroke_width)
            )
            val speed =
                a.getFloat(R.styleable.SmoothProgressBar_spb_speed, res.getString(R.string.spb_default_speed).toFloat())
            val speedProgressiveStart = a.getFloat(R.styleable.SmoothProgressBar_spb_progressiveStart_speed, speed)
            val speedProgressiveStop = a.getFloat(R.styleable.SmoothProgressBar_spb_progressiveStop_speed, speed)
            val iInterpolator = a.getInteger(R.styleable.SmoothProgressBar_spb_interpolator, -1)
            val reversed =
                a.getBoolean(R.styleable.SmoothProgressBar_spb_reversed, res.getBoolean(R.bool.spb_default_reversed))
            val mirrorMode =
                a.getBoolean(
                    R.styleable.SmoothProgressBar_spb_mirror_mode,
                    res.getBoolean(R.bool.spb_default_mirror_mode)
                )
            val colorsId = a.getResourceId(R.styleable.SmoothProgressBar_spb_colors, 0)
            val progressiveStartActivated = a.getBoolean(
                R.styleable.SmoothProgressBar_spb_progressiveStart_activated,
                res.getBoolean(R.bool.spb_default_progressiveStart_activated)
            )
            val backgroundDrawable = a.getDrawable(R.styleable.SmoothProgressBar_spb_background)
            val generateBackgroundWithColors =
                a.getBoolean(R.styleable.SmoothProgressBar_spb_generate_background_with_colors, false)
            val gradients = a.getBoolean(R.styleable.SmoothProgressBar_spb_gradients, false)
            a.recycle()

            //interpolator
            var interpolator: Interpolator? = null
            if (iInterpolator == -1) {
                interpolator = getInterpolator()
            }
            if (interpolator == null) {
                interpolator =
                    when (iInterpolator) {
                        INTERPOLATOR_ACCELERATEDECELERATE -> AccelerateDecelerateInterpolator()
                        INTERPOLATOR_DECELERATE -> DecelerateInterpolator()
                        INTERPOLATOR_LINEAR -> LinearInterpolator()
                        INTERPOLATOR_ACCELERATE -> AccelerateInterpolator()
                        else -> AccelerateInterpolator()
                    }
            }
            var colors: IntArray? = null
            //colors
            if (colorsId != 0) {
                colors = res.getIntArray(colorsId)
            }
            val builder: SmoothProgressDrawable.Builder = SmoothProgressDrawable.Builder(context)
                .speed(speed)
                .progressiveStartSpeed(speedProgressiveStart)
                .progressiveStopSpeed(speedProgressiveStop)
                .interpolator(interpolator)
                .sectionsCount(sectionsCount)
                .separatorLength(separatorLength)
                .strokeWidth(strokeWidth)
                .reversed(reversed)
                .mirrorMode(mirrorMode)
                .progressiveStart(progressiveStartActivated)
                .gradients(gradients)
            if (backgroundDrawable != null) {
                builder.backgroundDrawable(backgroundDrawable)
            }
            if (generateBackgroundWithColors) {
                builder.generateBackgroundUsingColors()
            }
            if (colors != null && colors.size > 0) builder.colors(colors) else builder.color(color)
            val d: SmoothProgressDrawable = builder.build()
            indeterminateDrawable = d
        }
    }

    internal object SmoothProgressBarUtils {
        fun generateDrawableWithColors(colors: IntArray?, strokeWidth: Float): Drawable? {
            return if (colors == null || colors.size == 0) null else ShapeDrawable(ColorsShape(strokeWidth, colors))
        }

        fun checkSpeed(speed: Float) {
            require(speed > 0f) { "Speed must be >= 0" }
        }

        fun checkColors(colors: IntArray?) {
            require(!(colors == null || colors.size == 0)) { "You must provide at least 1 color" }
        }

        fun checkAngle(angle: Int) {
            require(!(angle < 0 || angle > 360)) {
                String.format(
                    Locale.US,
                    "Illegal angle %d: must be >=0 and <= 360",
                    angle
                )
            }
        }

        fun checkPositiveOrZero(number: Float, name: String?) {
            require(number >= 0) { String.format(Locale.US, "%s %d must be positive", name, number) }
        }

        fun checkPositive(number: Int, name: String?) {
            require(number > 0) { String.format(Locale.US, "%s must not be null", name) }
        }

        fun checkNotNull(o: Any?, name: String?) {
            requireNotNull(o) { String.format(Locale.US, "%s must be not null", name) }
        }
    }

    internal class ColorsShape(var strokeWidth: Float, var colors: IntArray) : Shape() {

        override fun draw(canvas: Canvas, paint: Paint) {
            val ratio = 1f / colors.size
            var i = 0
            paint.strokeWidth = strokeWidth
            for (color in colors) {
                paint.color = color
                canvas.drawLine(i * ratio * width, height / 2, ++i * ratio * width, height / 2, paint)
            }
        }
    }

    class SmoothProgressDrawable private constructor(
        private var mInterpolator: Interpolator?,
        private var mSectionsCount: Int,
        separatorLength: Int,
        colors: IntArray,
        strokeWidth: Float,
        speed: Float,
        progressiveStartSpeed: Float,
        progressiveStopSpeed: Float,
        reversed: Boolean,
        mirrorMode: Boolean,
        callbacks: Callbacks?,
        progressiveStartActivated: Boolean,
        backgroundDrawable: Drawable?,
        useGradients: Boolean
    ) : Drawable(), Animatable {
        interface Callbacks {
            fun onStop()
            fun onStart()
        }

        private val fBackgroundRect = Rect()
        private var mCallbacks: Callbacks?
        private var mBounds: Rect? = null
        private val mPaint: Paint
        private var mColors: IntArray
        private var mColorsIndex: Int  = 0
        private var isRunning = false
            private set

        override fun isRunning(): Boolean {
            TODO("Not yet implemented")
        }

        private var mCurrentOffset = 0f
        private var mFinishingOffset = 0f
        private var mSeparatorLength: Int = 0
        private var mSpeed: Float = 0F
        private var mProgressiveStartSpeed: Float = 10F
        private var mProgressiveStopSpeed: Float = 10F
        private var mReversed: Boolean
        private var mNewTurn = false
        private var mMirrorMode: Boolean
        private var mMaxOffset: Float = 0F
        var isFinishing: Boolean = false
            private set
        private var mProgressiveStartActivated: Boolean
        private var mStartSection = 0
        private var mCurrentSections: Int = 0
        private var mStrokeWidth: Float = 0F
        var backgroundDrawable: Drawable?
            private set
        private var mUseGradients: Boolean
        private var mLinearGradientColors: IntArray? = null
        private var mLinearGradientPositions: FloatArray? = null

        ////////////////////////////////////////////////////////////////////////////
        ///////////////////         SETTERS
        @UiThread
        fun setInterpolator(interpolator: Interpolator?) {
            requireNotNull(interpolator) { "Interpolator cannot be null" }
            mInterpolator = interpolator
            invalidateSelf()
        }

        @UiThread
        fun setColor(color: Int) {
            colors = intArrayOf(color)
        }

        @UiThread
        fun setSpeed(speed: Float) {
            require(speed >= 0) { "Speed must be >= 0" }
            mSpeed = speed
            invalidateSelf()
        }

        @UiThread
        fun setProgressiveStartSpeed(speed: Float) {
            require(speed >= 0) { "SpeedProgressiveStart must be >= 0" }
            mProgressiveStartSpeed = speed
            invalidateSelf()
        }

        @UiThread
        fun setProgressiveStopSpeed(speed: Float) {
            require(speed >= 0) { "SpeedProgressiveStop must be >= 0" }
            mProgressiveStopSpeed = speed
            invalidateSelf()
        }

        @UiThread
        fun setSectionsCount(sectionsCount: Int) {
            require(sectionsCount > 0) { "SectionsCount must be > 0" }
            mSectionsCount = sectionsCount
            mMaxOffset = 1f / mSectionsCount
            mCurrentOffset %= mMaxOffset
            refreshLinearGradientOptions()
            invalidateSelf()
        }

        @UiThread
        fun setSeparatorLength(separatorLength: Int) {
            require(separatorLength >= 0) { "SeparatorLength must be >= 0" }
            mSeparatorLength = separatorLength
            invalidateSelf()
        }

        @UiThread
        fun setReversed(reversed: Boolean) {
            if (mReversed == reversed) return
            mReversed = reversed
            invalidateSelf()
        }

        @UiThread
        fun setMirrorMode(mirrorMode: Boolean) {
            if (mMirrorMode == mirrorMode) return
            mMirrorMode = mirrorMode
            invalidateSelf()
        }

        @UiThread
        fun setBackgroundDrawable(backgroundDrawable: Drawable) {
            if (this.backgroundDrawable === backgroundDrawable) return
            this.backgroundDrawable = backgroundDrawable
            invalidateSelf()
        }

        @set:UiThread
        var colors: IntArray?
            get() = mColors
            set(colors) {
                require(!(colors == null || colors.size == 0)) { "Colors cannot be null or empty" }
                mColorsIndex = 0
                mColors = colors
                refreshLinearGradientOptions()
                invalidateSelf()
            }

        @set:UiThread
        var strokeWidth: Float
            get() = mStrokeWidth
            set(strokeWidth) {
                require(strokeWidth >= 0) { "The strokeWidth must be >= 0" }
                mPaint.strokeWidth = strokeWidth
                invalidateSelf()
            }

        @UiThread
        fun setProgressiveStartActivated(progressiveStartActivated: Boolean) {
            mProgressiveStartActivated = progressiveStartActivated
        }

        @UiThread
        fun setUseGradients(useGradients: Boolean) {
            if (mUseGradients == useGradients) return
            mUseGradients = useGradients
            refreshLinearGradientOptions()
            invalidateSelf()
        }

        private fun refreshLinearGradientOptions() {
            if (mUseGradients) {
                mLinearGradientColors = IntArray(mSectionsCount + 2)
                mLinearGradientPositions = FloatArray(mSectionsCount + 2)
            } else {
                mPaint.shader = null
                mLinearGradientColors = null
                mLinearGradientPositions = null
            }
        }

        ////////////////////////////////////////////////////////////////////////////
        ///////////////////         DRAW
        override fun draw(canvas: Canvas) {
            mBounds = bounds
            canvas.clipRect(mBounds!!)

            //new turn
            if (mNewTurn) {
                mColorsIndex = decrementColor(mColorsIndex)
                mNewTurn = false
                if (isFinishing) {
                    mStartSection++
                    if (mStartSection > mSectionsCount) {
                        stop()
                        return
                    }
                }
                if (mCurrentSections < mSectionsCount) {
                    mCurrentSections++
                }
            }
            if (mUseGradients) prepareGradient()
            drawStrokes(canvas)
        }

        @UiThread
        private fun prepareGradient() {
            val xSectionWidth = 1f / mSectionsCount
            var currentIndexColor = mColorsIndex
            mLinearGradientPositions!![0] = 0f
            mLinearGradientPositions!![mLinearGradientPositions!!.size - 1] = 1f
            var firstColorIndex = currentIndexColor - 1
            if (firstColorIndex < 0) firstColorIndex += mColors.size
            mLinearGradientColors!![0] = mColors[firstColorIndex]
            for (i in 0 until mSectionsCount) {
                val position = mInterpolator!!.getInterpolation(i * xSectionWidth + mCurrentOffset)
                mLinearGradientPositions!![i + 1] = position
                mLinearGradientColors!![i + 1] = mColors[currentIndexColor]
                currentIndexColor = (currentIndexColor + 1) % mColors.size
            }
            mLinearGradientColors!![mLinearGradientColors!!.size - 1] = mColors[currentIndexColor]
            val left =
                if (mReversed) (if (mMirrorMode) Math.abs(mBounds!!.left - mBounds!!.right) / 2 else mBounds!!.left).toFloat() else mBounds!!.left.toFloat()
            val right =
                if (mMirrorMode) (if (mReversed) mBounds!!.left else Math.abs(mBounds!!.left - mBounds!!.right) / 2).toFloat() else mBounds!!.right.toFloat()
            val top = mBounds!!.centerY() - mStrokeWidth / 2
            val bottom = mBounds!!.centerY() + mStrokeWidth / 2
            val linearGradient = LinearGradient(
                left, top, right, bottom,
                mLinearGradientColors!!, mLinearGradientPositions,
                if (mMirrorMode) Shader.TileMode.MIRROR else Shader.TileMode.CLAMP
            )
            mPaint.shader = linearGradient
        }

        @UiThread
        private fun drawStrokes(canvas: Canvas) {
            if (mReversed) {
                canvas.translate(mBounds!!.width().toFloat(), 0f)
                canvas.scale(-1f, 1f)
            }
            var prevValue = 0f
            var boundsWidth = mBounds!!.width()
            if (mMirrorMode) boundsWidth /= 2
            val width = boundsWidth + mSeparatorLength + mSectionsCount
            val centerY = mBounds!!.centerY()
            val xSectionWidth = 1f / mSectionsCount
            var startX: Float
            var endX: Float
            var firstX = 0f
            var lastX = 0f
            var prev: Float
            var end: Float
            var spaceLength: Float
            var xOffset: Float
            var ratioSectionWidth: Float
            var sectionWidth: Float
            var drawLength: Float
            var currentIndexColor = mColorsIndex
            if (mStartSection == mCurrentSections && mCurrentSections == mSectionsCount) {
                firstX = canvas.width.toFloat()
            }
            for (i in 0..mCurrentSections) {
                xOffset = xSectionWidth * i + mCurrentOffset
                prev = Math.max(0f, xOffset - xSectionWidth)
                ratioSectionWidth = Math.abs(
                    mInterpolator!!.getInterpolation(prev) -
                            mInterpolator!!.getInterpolation(Math.min(xOffset, 1f))
                )
                sectionWidth = (width * ratioSectionWidth).toFloat()
                spaceLength =
                    if (sectionWidth + prev < width) Math.min(sectionWidth, mSeparatorLength.toFloat()) else 0f
                drawLength = if (sectionWidth > spaceLength) sectionWidth - spaceLength else 0F
                end = prevValue + drawLength
                if (end > prevValue && i >= mStartSection) {
                    val xFinishingOffset = mInterpolator!!.getInterpolation(Math.min(mFinishingOffset, 1f))
                    startX = Math.max(xFinishingOffset * width, Math.min(boundsWidth.toFloat(), prevValue))
                    endX = Math.min(boundsWidth.toFloat(), end)
                    drawLine(canvas, boundsWidth, startX, centerY.toFloat(), endX, centerY.toFloat(), currentIndexColor)
                    if (i == mStartSection) { // first loop
                        firstX = startX - mSeparatorLength
                    }
                }
                if (i == mCurrentSections) {
                    lastX = prevValue + sectionWidth //because we want to keep the separator effect
                }
                prevValue = end + spaceLength
                currentIndexColor = incrementColor(currentIndexColor)
            }
            drawBackgroundIfNeeded(canvas, firstX, lastX)
        }

        @UiThread
        private fun drawLine(
            canvas: Canvas,
            canvasWidth: Int,
            startX: Float,
            startY: Float,
            stopX: Float,
            stopY: Float,
            currentIndexColor: Int
        ) {
            mPaint.color = mColors[currentIndexColor]
            if (!mMirrorMode) {
                canvas.drawLine(startX, startY, stopX, stopY, mPaint)
            } else {
                if (mReversed) {
                    canvas.drawLine(canvasWidth + startX, startY, canvasWidth + stopX, stopY, mPaint)
                    canvas.drawLine(canvasWidth - startX, startY, canvasWidth - stopX, stopY, mPaint)
                } else {
                    canvas.drawLine(startX, startY, stopX, stopY, mPaint)
                    canvas.drawLine(canvasWidth * 2 - startX, startY, canvasWidth * 2 - stopX, stopY, mPaint)
                }
            }
        }

        @UiThread
        private fun drawBackgroundIfNeeded(canvas: Canvas, firstX: Float, lastX: Float) {
            var firstX = firstX
            var lastX = lastX
            if (backgroundDrawable == null) return
            fBackgroundRect.top = ((canvas.height - mStrokeWidth) / 2).toInt()
            fBackgroundRect.bottom = ((canvas.height + mStrokeWidth) / 2).toInt()
            fBackgroundRect.left = 0
            fBackgroundRect.right = if (mMirrorMode) canvas.width / 2 else canvas.width
            backgroundDrawable!!.bounds = fBackgroundRect

            //draw the background if the animation is over
            if (!isRunning) {
                if (mMirrorMode) {
                    canvas.save()
                    canvas.translate(canvas.width / 2.toFloat(), 0f)
                    drawBackground(canvas, 0f, fBackgroundRect.width().toFloat())
                    canvas.scale(-1f, 1f)
                    drawBackground(canvas, 0f, fBackgroundRect.width().toFloat())
                    canvas.restore()
                } else {
                    drawBackground(canvas, 0f, fBackgroundRect.width().toFloat())
                }
                return
            }
            if (!isFinishing && !isStarting) return
            if (firstX > lastX) {
                val temp = firstX
                firstX = lastX
                lastX = temp
            }
            if (firstX > 0) {
                if (mMirrorMode) {
                    canvas.save()
                    canvas.translate(canvas.width / 2.toFloat(), 0f)
                    if (mReversed) {
                        drawBackground(canvas, 0f, firstX)
                        canvas.scale(-1f, 1f)
                        drawBackground(canvas, 0f, firstX)
                    } else {
                        drawBackground(canvas, canvas.width / 2 - firstX, canvas.width / 2.toFloat())
                        canvas.scale(-1f, 1f)
                        drawBackground(canvas, canvas.width / 2 - firstX, canvas.width / 2.toFloat())
                    }
                    canvas.restore()
                } else {
                    drawBackground(canvas, 0f, firstX)
                }
            }
            if (lastX <= canvas.width) {
                if (mMirrorMode) {
                    canvas.save()
                    canvas.translate(canvas.width / 2.toFloat(), 0f)
                    if (mReversed) {
                        drawBackground(canvas, lastX, canvas.width / 2.toFloat())
                        canvas.scale(-1f, 1f)
                        drawBackground(canvas, lastX, canvas.width / 2.toFloat())
                    } else {
                        drawBackground(canvas, 0f, canvas.width / 2 - lastX)
                        canvas.scale(-1f, 1f)
                        drawBackground(canvas, 0f, canvas.width / 2 - lastX)
                    }
                    canvas.restore()
                } else {
                    drawBackground(canvas, lastX, canvas.width.toFloat())
                }
            }
        }

        @UiThread
        private fun drawBackground(canvas: Canvas, fromX: Float, toX: Float) {
            val count = canvas.save()
            canvas.clipRect(
                fromX, ((canvas.height - mStrokeWidth) / 2),
                toX, ((canvas.height + mStrokeWidth) / 2)
            )
            backgroundDrawable!!.draw(canvas)
            canvas.restoreToCount(count)
        }

        @UiThread
        private fun incrementColor(colorIndex: Int): Int {
            var colorIndex = colorIndex
            ++colorIndex
            if (colorIndex >= mColors.size) colorIndex = 0
            return colorIndex
        }

        @UiThread
        private fun decrementColor(colorIndex: Int): Int {
            var colorIndex = colorIndex
            --colorIndex
            if (colorIndex < 0) colorIndex = mColors.size - 1
            return colorIndex
        }

        /**
         * Start the animation with the first color.
         * Calls progressiveStart(0)
         */
        @UiThread
        fun progressiveStart() {
            progressiveStart(0)
        }

        /**
         * Start the animation from a given color.
         *
         * @param index
         */
        @UiThread
        fun progressiveStart(index: Int) {
            resetProgressiveStart(index)
            start()
        }

        @UiThread
        private fun resetProgressiveStart(index: Int) {
            checkColorIndex(index)
            mCurrentOffset = 0f
            isFinishing = false
            mFinishingOffset = 0f
            mStartSection = 0
            mCurrentSections = 0
            mColorsIndex = index
        }

        /**
         * Finish the animation by animating the remaining sections.
         */
        @UiThread
        fun progressiveStop() {
            isFinishing = true
            mStartSection = 0
        }

        override fun setAlpha(alpha: Int) {
            mPaint.alpha = alpha
        }

        override fun setColorFilter(cf: ColorFilter?) {
            mPaint.colorFilter = cf
        }

        override fun getOpacity(): Int {
            return PixelFormat.TRANSPARENT
        }

        ///////////////////////////////////////////////////////////////////////////
        ///////////////////         Animation: based on http://cyrilmottier.com/2012/11/27/actionbar-on-the-move/
        override fun start() {
            if (mProgressiveStartActivated) {
                resetProgressiveStart(0)
            }
            if (isRunning) return
            if (mCallbacks != null) {
                mCallbacks!!.onStart()
            }
            scheduleSelf(mUpdater, SystemClock.uptimeMillis() + FRAME_DURATION)
            invalidateSelf()
        }

        override fun stop() {
            if (!isRunning) return
            if (mCallbacks != null) {
                mCallbacks!!.onStop()
            }
            isRunning = false
            unscheduleSelf(mUpdater)
        }

        override fun scheduleSelf(what: Runnable, `when`: Long) {
            isRunning = true
            super.scheduleSelf(what, `when`)
        }

        val isStarting: Boolean
            get() = mCurrentSections < mSectionsCount
        private val mUpdater: Runnable = object : Runnable {
            override fun run() {
                if (isFinishing) {
                    mFinishingOffset += OFFSET_PER_FRAME * mProgressiveStopSpeed
                    mCurrentOffset += OFFSET_PER_FRAME * mProgressiveStopSpeed
                    if (mFinishingOffset >= 1f) {
                        stop()
                    }
                } else if (isStarting) {
                    mCurrentOffset += OFFSET_PER_FRAME * mProgressiveStartSpeed
                } else {
                    mCurrentOffset += OFFSET_PER_FRAME * mSpeed
                }
                if (mCurrentOffset >= mMaxOffset) {
                    mNewTurn = true
                    mCurrentOffset -= mMaxOffset
                }
                if (isRunning) scheduleSelf(this, SystemClock.uptimeMillis() + FRAME_DURATION)
                invalidateSelf()
            }
        }

        ////////////////////////////////////////////////////////////////////////////
        ///////////////////     Listener
        fun setCallbacks(callbacks: Callbacks?) {
            mCallbacks = callbacks
        }

        ////////////////////////////////////////////////////////////////////////////
        ///////////////////     Checks
        private fun checkColorIndex(index: Int) {
            require(!(index < 0 || index >= mColors.size)) { String.format(Locale.US, "Index %d not valid", index) }
        }
        ////////////////////////////////////////////////////////////////////////////
        ///////////////////         BUILDER
        /**
         * Builder for SmoothProgressDrawable! You must use it!
         */
        class Builder @JvmOverloads constructor(context: Context, editMode: Boolean = false) {
            private var mInterpolator: Interpolator? = null
            private var mSectionsCount = 0
            private var mColors: IntArray = intArrayOf()
            private var mSpeed = 0f
            private var mProgressiveStartSpeed = 0f
            private var mProgressiveStopSpeed = 0f
            private var mReversed = false
            private var mMirrorMode = false
            private var mStrokeWidth = 0f
            private var mStrokeSeparatorLength = 0
            private var mProgressiveStartActivated = false
            private var mGenerateBackgroundUsingColors = false
            private var mGradients = false
            private var mBackgroundDrawableWhenHidden: Drawable? = null
            private var mOnProgressiveStopEndedListener: Callbacks? = null
            fun build(): SmoothProgressDrawable {
                if (mGenerateBackgroundUsingColors) {
                    mBackgroundDrawableWhenHidden =
                        SmoothProgressBarUtils.generateDrawableWithColors(mColors, mStrokeWidth)
                }
                return SmoothProgressDrawable(
                    mInterpolator,
                    mSectionsCount,
                    mStrokeSeparatorLength,
                    mColors,
                    mStrokeWidth,
                    mSpeed,
                    mProgressiveStartSpeed,
                    mProgressiveStopSpeed,
                    mReversed,
                    mMirrorMode,
                    mOnProgressiveStopEndedListener,
                    mProgressiveStartActivated,
                    mBackgroundDrawableWhenHidden,
                    mGradients
                )
            }

            private fun initValues(context: Context, editMode: Boolean) {
                val res = context.resources
                mInterpolator = AccelerateInterpolator()
                if (!editMode) {
                    mSectionsCount = res.getInteger(R.integer.spb_default_sections_count)
                    mSpeed = res.getString(R.string.spb_default_speed).toFloat()
                    mReversed = res.getBoolean(R.bool.spb_default_reversed)
                    mProgressiveStartActivated = res.getBoolean(R.bool.spb_default_progressiveStart_activated)
                    mColors = intArrayOf(res.getColor(R.color.spb_default_color))
                    mStrokeSeparatorLength = res.getDimensionPixelSize(R.dimen.spb_default_stroke_separator_length)
                    mStrokeWidth = res.getDimensionPixelOffset(R.dimen.spb_default_stroke_width).toFloat()
                } else {
                    mSectionsCount = 4
                    mSpeed = 1f
                    mReversed = false
                    mProgressiveStartActivated = false
                    mColors = intArrayOf(-0xcc4a1b)
                    mStrokeSeparatorLength = 4
                    mStrokeWidth = 4f
                }
                mProgressiveStartSpeed = mSpeed
                mProgressiveStopSpeed = mSpeed
                mGradients = false
            }

            fun interpolator(interpolator: Interpolator?): Builder {
                checkNotNull(interpolator, { "Interpolator" })
                mInterpolator = interpolator
                return this
            }

            fun sectionsCount(sectionsCount: Int): Builder {
                checkPositive(sectionsCount, "Sections count")
                mSectionsCount = sectionsCount
                return this
            }

            fun separatorLength(separatorLength: Int): Builder {
                checkPositiveOrZero(separatorLength.toFloat(), "Separator length")
                mStrokeSeparatorLength = separatorLength
                return this
            }

            fun color(color: Int): Builder {
                mColors = intArrayOf(color)
                return this
            }

            fun colors(colors: IntArray): Builder {
                checkColors(colors)
                mColors = colors
                return this
            }

            fun strokeWidth(width: Float): Builder {
                checkPositiveOrZero(width, "Width")
                mStrokeWidth = width
                return this
            }

            fun speed(speed: Float): Builder {
                checkSpeed(speed)
                mSpeed = speed
                return this
            }

            fun progressiveStartSpeed(progressiveStartSpeed: Float): Builder {
                checkSpeed(progressiveStartSpeed)
                mProgressiveStartSpeed = progressiveStartSpeed
                return this
            }

            fun progressiveStopSpeed(progressiveStopSpeed: Float): Builder {
                checkSpeed(progressiveStopSpeed)
                mProgressiveStopSpeed = progressiveStopSpeed
                return this
            }

            fun reversed(reversed: Boolean): Builder {
                mReversed = reversed
                return this
            }

            fun mirrorMode(mirrorMode: Boolean): Builder {
                mMirrorMode = mirrorMode
                return this
            }

            fun progressiveStart(progressiveStartActivated: Boolean): Builder {
                mProgressiveStartActivated = progressiveStartActivated
                return this
            }

            fun callbacks(onProgressiveStopEndedListener: Callbacks?): Builder {
                mOnProgressiveStopEndedListener = onProgressiveStopEndedListener
                return this
            }

            fun backgroundDrawable(backgroundDrawableWhenHidden: Drawable?): Builder {
                mBackgroundDrawableWhenHidden = backgroundDrawableWhenHidden
                return this
            }

            fun generateBackgroundUsingColors(): Builder {
                mGenerateBackgroundUsingColors = true
                return this
            }

            @JvmOverloads
            fun gradients(useGradients: Boolean = true): Builder {
                mGradients = useGradients
                return this
            }

            init {
                initValues(context, editMode)
            }
        }

        companion object {
            private const val FRAME_DURATION = 1000 / 60.toLong()
            private const val OFFSET_PER_FRAME = 0.01f
        }

        init {
            mCurrentSections = mSectionsCount
            mSeparatorLength = separatorLength
            mSpeed = speed
            mProgressiveStartSpeed = progressiveStartSpeed
            mProgressiveStopSpeed = progressiveStopSpeed
            mReversed = reversed
            mColors = colors
            mColorsIndex = 0
            mMirrorMode = mirrorMode
            isFinishing = false
            this.backgroundDrawable = backgroundDrawable
            this.mStrokeWidth = strokeWidth
            mMaxOffset = 1f / mSectionsCount
            mPaint = Paint()
            mPaint.strokeWidth = strokeWidth
            mPaint.style = Paint.Style.STROKE
            mPaint.isDither = false
            mPaint.isAntiAlias = false
            mProgressiveStartActivated = progressiveStartActivated
            mCallbacks = callbacks
            mUseGradients = useGradients
            refreshLinearGradientOptions()
        }
    }

}

