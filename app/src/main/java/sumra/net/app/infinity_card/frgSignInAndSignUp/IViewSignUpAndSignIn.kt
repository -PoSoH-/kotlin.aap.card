package sumra.net.app.infinity_card.frgSignInAndSignUp

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(value = AddToEndSingleStrategy::class)
interface IViewSignUpAndSignIn: MvpView {
    fun showProgressView()
    fun hideProgressView()

    fun signUpSuccess()
    fun signUpFailure()

    fun signInSuccess()
    fun signInFailure()

    fun updateTokenSuccess()
    fun updateTokenFailure()
}