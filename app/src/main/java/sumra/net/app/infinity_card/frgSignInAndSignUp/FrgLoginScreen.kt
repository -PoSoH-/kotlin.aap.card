package sumra.net.app.infinity_card.frgSignInAndSignUp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.navigation.findNavController
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.frg_login_screen.*
import kotlinx.android.synthetic.main.frg_profile_screen.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter
import sumra.net.app.infinity_card.R
import sumra.net.app.infinity_card.widgets.CustomSnackBar.CustomSnackBar

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FrgLoginScreen : MvpAppCompatFragment(R.layout.frg_login_screen)
    , IViewSignUpAndSignIn{

//    @InjectPresenter
//    lateinit var mPrSignUpAndSignIn: PrSignUpAndSignIn

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        initCodeCCP()
//
//        initListeners()
    }

    override fun showProgressView() {
        CustomSnackBar.make(view = rootSignUpAndSignIn
            , typeMessage = CustomSnackBar.EMessageType.OTHER
            , header = null
            , message = "Show progress!"
            , duration = Snackbar.LENGTH_SHORT
            , action_lable = null
            , listener = null)
    }

    override fun hideProgressView() {
        CustomSnackBar.make(view = rootSignUpAndSignIn
            , typeMessage = CustomSnackBar.EMessageType.OTHER
            , header = null
            , message = "Hide progress!"
            , duration = Snackbar.LENGTH_SHORT
            , action_lable = null
            , listener = null)
    }

    override fun signUpSuccess() {
        CustomSnackBar.make(view = rootSignUpAndSignIn
        , typeMessage = CustomSnackBar.EMessageType.SUCCESS
            , header = null
            , message = "SignUp SUCCESS"
            , duration = Snackbar.LENGTH_SHORT
            , action_lable = null
            , listener = null)
    }

    override fun signUpFailure() {
        CustomSnackBar.make(view = rootSignUpAndSignIn
            , typeMessage = CustomSnackBar.EMessageType.ERROR
            , header = null
            , message = "SignUp FAILURE"
            , duration = Snackbar.LENGTH_SHORT
            , action_lable = null
            , listener = null)
    }

    override fun signInSuccess() {
        CustomSnackBar.make(view = rootSignUpAndSignIn
            , typeMessage = CustomSnackBar.EMessageType.SUCCESS
            , header = null
            , message = "SignIn SUCCESS"
            , duration = Snackbar.LENGTH_SHORT
            , action_lable = null
            , listener = null)
    }

    override fun signInFailure() {
        CustomSnackBar.make(view = rootSignUpAndSignIn
            , typeMessage = CustomSnackBar.EMessageType.ERROR
            , header = null
            , message = "SignIn FAILURE"
            , duration = Snackbar.LENGTH_SHORT
            , action_lable = null
            , listener = null)
    }

    override fun updateTokenSuccess() {
        CustomSnackBar.make(view = rootSignUpAndSignIn
            , typeMessage = CustomSnackBar.EMessageType.SUCCESS
            , header = null
            , message = "UPDATE TOKEN SUCCESS"
            , duration = Snackbar.LENGTH_SHORT
            , action_lable = null
            , listener = null)
    }

    override fun updateTokenFailure() {
        CustomSnackBar.make(view = rootSignUpAndSignIn
            , typeMessage = CustomSnackBar.EMessageType.ERROR
            , header = null
            , message = "UPDATE TOKEN FAILURE"
            , duration = Snackbar.LENGTH_SHORT
            , action_lable = null
            , listener = null)
    }

    private fun showInfo(message: String){
        CustomSnackBar.make(view = rootSignUpAndSignIn,
            header = "INFINITY CARD",
            message = message,
            duration = Snackbar.LENGTH_SHORT,
            listener = null,
            action_lable = null
        )?.show()
    }

//    private fun initCodeCCP(){
//        cppNumberPhoneInput.apply {
//            registerCarrierNumberEditText(txtFrgLoginUserPhone)
//        }
//    }
//
//    private fun initListeners(){
//        txtTextButtonRegistration.apply {
//            setOnClickListener {
//                showInfo("start registration process")
//            }
//        }
//
//        txtTextButtonLogin.apply {
//            setOnClickListener {
//                showInfo("start login process")
//            }
//        }
//
//        txtTextButtonForgot.apply {
//            setOnClickListener {
//                showInfo("start forgot process")
//            }
//        }

//        //=========================
//        txtTextButtonForgot.apply {
//            setOnClickListener {
//                findNavController(R.id.nav_host_auth)
//            }
//        }
//    }

}