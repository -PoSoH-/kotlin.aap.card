package sumra.net.app.infinity_card.fragments

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.aigestudio.wheelpicker.WheelPicker
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.frg_profile_screen.*
import kotlinx.android.synthetic.main.frg_profile_screen.profile_user_avatar
import kotlinx.android.synthetic.main.lay_balance_show.*
import kotlinx.android.synthetic.main.lay_date_picker_show.*
import kotlinx.android.synthetic.main.lay_infinity_card_camera_gallery_permissions.*
import kotlinx.android.synthetic.main.lay_select_countries_show.*
import kotlinx.android.synthetic.main.lay_user_info_editable_show.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter
import sumra.net.app.infinity_card.AppCard
import sumra.net.app.infinity_card.BuildConfig
import sumra.net.app.infinity_card.R
import sumra.net.app.infinity_card.activities.ActHomeScreen
import sumra.net.app.infinity_card.adapters.AdapterCountry
import sumra.net.app.infinity_card.adapters.AdapterMenuProfile
import sumra.net.app.infinity_card.enums.*
import sumra.net.app.infinity_card.interfaces.ICountryItemSelectListener
import sumra.net.app.infinity_card.interfaces.IProfileMenuSelectListener
import sumra.net.app.infinity_card.models.Country
import sumra.net.app.infinity_card.models.User
import sumra.net.app.infinity_card.models.UserPatch
import sumra.net.app.infinity_card.models.responses._jsonInfinityError
import sumra.net.app.infinity_card.presenter.PrFiles
import sumra.net.app.infinity_card.presenter.PresenterCountries
import sumra.net.app.infinity_card.presenter.PresenterProfile
import sumra.net.app.infinity_card.utils.UtilLog
import sumra.net.app.infinity_card.utils.extention.*
import sumra.net.app.infinity_card.views.IViewCountries
import sumra.net.app.infinity_card.views.IViewFiles
import sumra.net.app.infinity_card.views.IViewProfile
import sumra.net.app.infinity_card.widgets.CustomSnackBar.CustomSnackBar
import sumra.net.app.infinity_card.widgets.DividerItem
import java.io.ByteArrayOutputStream
import java.util.*

class FrgProfileScreen : MvpAppCompatFragment(R.layout.frg_profile_screen), IViewProfile, IViewCountries, IViewFiles {

    private val TAG = this::class.java.simpleName

    @InjectPresenter
    lateinit var mPrProfile: PresenterProfile

    @InjectPresenter
    lateinit var mPrCountry: PresenterCountries

    @InjectPresenter
    lateinit var mPrFiles: PrFiles

    private lateinit var mUser: User
    private lateinit var mCountryes: Country

//    private lateinit var pictureTransfer : IPictureTransfer

    private lateinit var mAdapterMenu: AdapterMenuProfile
    private val mAdapterCountries = AdapterCountry(object : ICountryItemSelectListener {
        override fun countryItemSelect(itemSelect: Country) {
            mCountryes = itemSelect
            profile_user_country_edit.text = mCountryes.label
            infinity_card_profile_countries_container.hideUtoD()
        }
    })

//    override fun onAttach(context: Context) {
//        super.onAttach(context)
//        if(context is IPictureTransfer){
//            pictureTransfer = context
//        }else{
//            try{
//                throw ClassCastException(resources.getString(R.string.error_picture_interface_implementation))
//            }catch (ex: ClassCastException){
//                UtilLog.error(TAG, ex.message!!)
//            }
//        }
//    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mPrCountry.fetchAllCountries("_0000_")

//        mUser = arguments?.getParcelable<User>(EKeys.PROF_USER) as User

        mUser = (requireActivity() as ActHomeScreen).getCurrentUser()



        updateUserInformation()

//        mProfilePresenter.fetchMeProfile("_0000_")
        initProfileMenu(mUser.typeId)
        list_profile_menu.apply {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, true)
            adapter = mAdapterMenu
            addItemDecoration(DividerItem(requireContext(), R.drawable.xml_item_divider))
        }

        profile_user_action_edit_button.apply {
            setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    showUserEditable()
                }
            })
        }

        profile_user_country_edit.apply {
            setOnClickListener {
                showViewSelectCountry()
            }
        }

        profile_photo_image_container_edit.apply {
            setOnClickListener {
                Toast.makeText(requireContext(), ".....photo select/capture.....", Toast.LENGTH_SHORT).show()
                infinity_card_camera_gallery_permissions.showScaleCtoO()
            }
        }

        infinity_card_action_cancel_photo.apply {
            setOnClickListener {
                CustomSnackBar.make(
                    view = baseProfileView,
                    header = "INFINITY CARD - PHOT",
                    message = "...photo => capture -> cancel...",
                    duration = Snackbar.LENGTH_SHORT,
                    listener = null,
                    action_lable = null
                )?.show()
                infinity_card_camera_gallery_permissions.hideScaleOtoC()
            }
        }

        profile_user_passport_expire_edit.apply {
            setOnClickListener {
//                AppCard.getNavigation().navigate(R.id._fragment_date_picker)
                datePickerPassportSetup()
            }
        }

        mPrCountry.fetchAllCountries("_0000_")

        var type = mUser.typeId

        if (BuildConfig.DEBUG) {
            profile_user_action_change_user_type.apply {
//                initProfileMenu()
                showScaleCtoO()
                setOnClickListener {

                    if(EUserType.compareUserType(type) == EUserType.CLIENT) type = EUserType.AGENT.typeId
                    else  type = EUserType.CLIENT.typeId
//                    initProfileMenu(type)
                    mAdapterMenu.updateMenuItems(type)
                    (requireActivity() as ActHomeScreen ).notActionByClick(profile_user_action_change_user_type)
                }
            }
        } else {
            profile_user_action_change_user_type.apply {
                hideScaleOtoC()
            }
        }

    }

    fun updateUserInformation() { //}   (user: User){
        profile_user_first_name.text = mUser.firstName //.split(" ")[0]
        profile_user_second_name.text = mUser.lastName  //name.split(" ")[1]
        profile_user_passport_number.text = mUser.passportNumber

        if (mUser.passportDateIssue.isNullOrEmpty())
            profile_user_passport_expire.text = ""
        else
            profile_user_passport_expire.text = mUser.passportDateIssue!!

        profile_user_contact_phone.text = mUser.phone
        profile_user_contact_email.text = mUser.email
        profile_user_country.text = mUser.country
        profile_user_description.text = mUser.description

        if (mUser.avatar.isNullOrEmpty())
            profile_user_avatar.showImageByResources(R.drawable.ic_infinity_card)
        else
            profile_user_avatar.showImageByUrl(mUser.avatar!!)

        //balance section...
//        if (!mUser.balances.isNullOrEmpty()) {
//            mUser.balances.let {
//                it.forEach {
////                    updateBalance(it.currency, it.balance)
//                    if (it.balance.isNullOrEmpty()) {
//                        lbl_all_cards_balance_aa.text = ECurrency.configureResult(ECurrency.USA.compare, "0.0")
//                        lbl_all_cards_balance_bb.text = ECurrency.configureResult(ECurrency.EUR.compare, "0.0")
//                        lbl_all_cards_balance_cc.text = ECurrency.configureResult(ECurrency.GBP.compare, "0.0")
//                    } else {
//                        updateBalance(it.currency, it.balance)
//                    }
//                }
//            }
//        }

        infinity_card_profile_action_save.apply {
            setOnClickListener {
                // saving process
                saveNewUserData()
                hideKeyboard()
                infinity_card_profile_user_edit_container.hideUtoD()
            }
        }

        infinity_card_profile_action_cancel.apply {
            setOnClickListener {
                // cancel process
                hideKeyboard()
                infinity_card_profile_user_edit_container.hideUtoD()
            }
        }

    }

    /*
    * this function read bitmap file with disk
    * change the file size to less than one megabyte
    * encode modified file in Base 64
    * upload the encrypted file to the server
    */
    fun updatePictureState(bm: Uri) {

        val inputStream = requireContext().contentResolver.openInputStream(bm)
        val bitmap: Bitmap = BitmapFactory.decodeStream(inputStream)
        val size_file: Long = bitmap.byteCount.toLong() //  File(bm).length()
        val perc = ((1_000_000 / size_file.toDouble()) * 100).toInt()
        val stream = ByteArrayOutputStream()
        if (perc < 100) {
            bitmap.compress(Bitmap.CompressFormat.JPEG, perc, stream)
        } else {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
        }

        mPrFiles.uploadFile(
            Base64.getEncoder().encodeToString(stream.toByteArray()),
            EFileLoadType.AVATAR.filePrefix.replace("#", System.currentTimeMillis().toString(), true)
        )

    }

    fun updatePassportState(message: String) {
        profile_user_passport_number.text = message
    }

    fun showUserEditable() { //}   (user: User){
        profile_user_first_name_edit.inputHint(resources.getString(R.string.infinity_card_profile_hint_first_name))
        mUser.firstName?.let {
            profile_user_first_name_edit.inputText(it)
        }

        profile_user_second_name_edit.inputHint(resources.getString(R.string.infinity_card_profile_hint_last_name))
        mUser.lastName?.let {
            profile_user_second_name_edit.inputText(it)
        }

        profile_user_passport_number_edit.inputHint(resources.getString(R.string.infinity_card_profile_hint_passport_number))
        mUser.passportNumber?.let {
            profile_user_passport_number_edit.inputHint(it)
        }

        profile_user_passport_expire_edit.inputHint(resources.getString(R.string.infinity_card_profile_hint_passport_issues))
        mUser.passportDateIssue?.let {
            profile_user_passport_expire_edit.inputHint(it)
        }

        profile_user_contact_phone_edit.inputHint(resources.getString(R.string.infinity_card_profile_hint_phone))
        mUser.phone?.let {
            profile_user_contact_phone_edit.inputHint(it)
        }

        profile_user_contact_email_edit.inputHint(resources.getString(R.string.infinity_card_profile_hint_email))
        mUser.email?.let {
            profile_user_contact_email_edit.inputHint(it)
        }

        profile_user_country_edit.setText(resources.getString(R.string.infinity_card_profile_hint_currency))
        mUser.country?.let {
            profile_user_country_edit.text = it
        }

        profile_user_description_edit.inputHint(resources.getString(R.string.infinity_card_profile_hint_description))
        mUser.description?.let {
            profile_user_description_edit.inputHint(it)
        }
        infinity_card_profile_user_edit_container.showDtoU()
    }

    fun saveNewUserData() {
//        mUser.firstName         = profile_user_first_name_edit.text.toString()
//        mUser.lastName          = profile_user_second_name_edit.text.toString()
//        mUser.passportNumber    = profile_user_passport_number_edit.text.toString()
//        mUser.passportDateIssue = profile_user_passport_expire_edit.text.toString()
//        mUser.phone             = profile_user_contact_phone_edit.text.toString()
//        mUser.email             = profile_user_contact_email_edit.text.toString()
//        mUser.country           = profile_user_country_edit.text.toString()
//        mUser.description       = profile_user_description_edit.text.toString()

        mPrProfile.patchUserInformation(
            "_0000_",
            UserPatch(
                id = mUser.id,
                first_name = profile_user_first_name_edit.text.toString(),
                last_name = profile_user_second_name_edit.text.toString(),
                avatar = if (mUser.avatar.isNullOrEmpty()) mUser.avatar!! else "",
                description = profile_user_description_edit.text.toString(),
                country_id = mCountryes.value,   // profile_user_country_edit.text.toString(),
                phone = profile_user_contact_phone_edit.text.toString(),
                email = profile_user_contact_email_edit.text.toString(),
                passport_number = profile_user_passport_number_edit.text.toString(),
                passport_date_issue = "${datePickerSelectYear.currentYear}" +
                        "-${datePickerSelectMonth.currentMonth}" +
                        "-${datePickerSelectDay.currentDay}", //profile_user_passport_expire_edit.text.toString().replace("/", "-"),
                type_id = mUser.typeId,
//                parentId = ,
//                ownershipId = ,
//                tariffId = ,
                status = mUser.status
            )
        )
    }

    private fun updateBalance(currency: String, balance: String) {
        when (currency) {
            ECurrency.USA.compare -> lbl_all_cards_balance_aa.text = ECurrency.configureResult(currency, balance)
            ECurrency.EUR.compare -> lbl_all_cards_balance_bb.text = ECurrency.configureResult(currency, balance)
            ECurrency.GBP.compare -> lbl_all_cards_balance_cc.text = ECurrency.configureResult(currency, balance)
        }
    }

    private fun showViewSelectCountry() {
        list_all_countries.apply {
            adapter = mAdapterCountries
            layoutManager = LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.VERTICAL,
                false
            )
        }
        mAdapterCountries.bindCountries(mPrCountry.getAllCountries())

        infinity_card_profile_countries_container.showDtoU()
    }

    private fun datePickerPassportSetup() {

        subViewProfilePassportExpireDate.showScaleCtoO()

        datePickerSelectYear.apply {
            yearStart = Calendar.getInstance().get(Calendar.YEAR)
            yearEnd = yearStart + 50
            setOnItemSelectedListener(object : WheelPicker.OnItemSelectedListener {
                override fun onItemSelected(picker: WheelPicker?, data: Any?, position: Int) {
//                    baseDataPickerDialogFragment.showSnackBarInfo("$position")
                    datePickerSelectDay.month = datePickerSelectMonth.currentMonth
                    datePickerSelectDay.year = datePickerSelectYear.currentYear
                }
            })
        }

        datePickerSelectMonth.apply {
            setOnItemSelectedListener(object : WheelPicker.OnItemSelectedListener {
                override fun onItemSelected(picker: WheelPicker?, data: Any?, position: Int) {
//                    baseDataPickerDialogFragment.showSnackBarInfo("$position")
                    datePickerSelectDay.month = datePickerSelectMonth.currentMonth
                    datePickerSelectDay.year = datePickerSelectYear.currentYear
                }
            })
        }

        datePickerSelectDay.apply {
            setOnItemSelectedListener(object : WheelPicker.OnItemSelectedListener {
                override fun onItemSelected(picker: WheelPicker?, data: Any?, position: Int) {
//                    baseDataPickerDialogFragment.showSnackBarInfo("$position")
                }
            })
        }

        passportExpireDateCancel.apply {
            setOnClickListener {
                subViewProfilePassportExpireDate.hideScaleOtoC()
            }
        }

        passportExpireDateConfirm.apply {
            setOnClickListener {
                subViewProfilePassportExpireDate.hideScaleOtoC()
                profile_user_passport_expire_edit.text = "${datePickerSelectMonth.currentMonth}" +
                        "/${datePickerSelectDay.currentDay}" +
                        "/${datePickerSelectYear.currentYear}"
            }
        }
    }

    override fun fetchMeSuccess() {
        UtilLog.info(TAG, "SUCCESS FETCH ME")
        mUser = mPrProfile.getUserInfo()
        updateUserInformation()
    }

    override fun fetchMeFailure(error: String?) {
        UtilLog.info(TAG, "FAILURE FETCH ME")
    }

    override fun fetchTariffsSuccess() {
        UtilLog.info(TAG, "SUCCESS TARIFFS")
    }

    override fun fetchTariffsFailure(error: String?) {
        UtilLog.info(TAG, "FAILURE TARIFFS")
    }

    override fun fetchPayDetailsSuccess() {
        UtilLog.info(TAG, "SUCCESS PAY DETAILS")
    }

    override fun fetchPayDetailsFailure(error: String?) {
        UtilLog.info(TAG, "FAILURE PAY DETAILS")
    }

    override fun fetchAllCountrySuccess() {
//        mAdapterCountries = AdapterCountry()
    }

    override fun fetchAllCountryFailure(error: _jsonInfinityError.InfinityError) {
        Toast.makeText(requireContext(), "${error.title}\n${error.message}", Toast.LENGTH_SHORT).show()
    }

    override fun uploadFileSuccess() {
        mUser.avatar = mPrFiles.getUserAvatar()

        if (mUser.avatar.isNullOrEmpty()) {
            (profile_photo_image_container_edit.getChildAt(0) as ImageView)
                .showImageByResources(R.drawable.ic_infinity_card)
            profile_user_avatar.showImageByResources(R.drawable.ic_infinity_card)
        } else {
            (profile_photo_image_container_edit.getChildAt(0) as ImageView).showImageByUrl(mUser.avatar!!)
            profile_user_avatar.showImageByUrl(mUser.avatar!!)
        }
    }

    override fun uploadFileFailure(error: String?) {
        when (error) {
            null -> baseProfileView.showSnackBarError("Unknown error!") // Toast.makeText(requireContext(), "Unknown error!", Toast.LENGTH_SHORT).show()
            else -> baseProfileView.showSnackBarError(error) //Toast.makeText(requireContext(), error, Toast.LENGTH_SHORT).show()
        }
    }

    override fun patchFileSuccess() {
        TODO("Not yet implemented")
    }

    override fun patchFileFailure(error: String?) {
        TODO("Not yet implemented")
    }

    override fun deleteFileSuccess() {
        TODO("Not yet implemented")
    }

    override fun deleteFileFailure(error: String?) {
        TODO("Not yet implemented")
    }

    override fun showProgress(state: Boolean) {
        when (state) {
            true -> AppCard.showProgress() // Toast.makeText(requireContext(), "SHOW PROGRESS", Toast.LENGTH_SHORT).show()
            else -> AppCard.hideProgress() // Toast.makeText(requireContext(), "HIDE PROGRESS", Toast.LENGTH_SHORT).show()
        }
    }

    private fun initProfileMenu(type_id: Long){
        mAdapterMenu = AdapterMenuProfile(object : IProfileMenuSelectListener {
            override fun selectMenuItem(menuItem: EProfileMenu) {
                when (menuItem) {
                    EProfileMenu.MENU_PROF_CONTACTS -> {
                        UtilLog.info(TAG
                            , "This item menu ${menuItem.name} is development..."
                            , baseProfileView)
                        Navigation.findNavController(
                            requireActivity(),
                            R.id.nav_host_profile
                        ).navigate(R.id._frg_profile_contacts)
                    }
                    EProfileMenu.MENU_PROF_TARIFFS -> {
                        UtilLog.info(TAG
                            , "This item menu ${menuItem.name} is development..."
                            , baseProfileView)
                    }
                    EProfileMenu.MENU_PROF_USER_LOGOUT -> {
                        UtilLog.info(TAG
                            , "This item menu ${menuItem.name} is development..."
                            , baseProfileView)
                    }
                    EProfileMenu.MENU_PROF_ORDERS -> {
                        Navigation.findNavController(
                            requireActivity(),
                            R.id.nav_host_profile
                        ).navigate(R.id._frg_profile_orders)
                    }
                    EProfileMenu.MENU_PROF_CLIENTS -> {
                        Navigation.findNavController(
                            requireActivity(),
                            R.id.nav_host_profile
                        ).navigate(R.id._frg_profile_clients)
                    }
                    EProfileMenu.MENU_PROF_USER_CARD -> {
                        // this functionality has been deleted
                    }
                }
            }
        })
        mAdapterMenu.updateMenuItems(type_id)
    }
}