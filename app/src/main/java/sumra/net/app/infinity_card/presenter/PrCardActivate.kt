package sumra.net.app.infinity_card.presenter

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import moxy.MvpPresenter
import sumra.net.app.infinity_card.repositoryes.RepositoryActivate
import sumra.net.app.infinity_card.views.IViewActivateCard

class PrCardActivate : MvpPresenter<IViewActivateCard>() {

    private val mRepository = RepositoryActivate()


    fun fetchActivateCard(token: String, activationCode: String) {
        viewState.showProgress(true)
        CoroutineScope(Dispatchers.IO).launch {
            val resp = mRepository.fetchActivateCardNet(token, activationCode).await()
            if(!resp.isNullOrEmpty()){
                withContext(Dispatchers.Main){
                    viewState.fetchCardActivateSuccess()
                }
            }else{
                withContext(Dispatchers.Main){
                    viewState.fetchCardActivateFailure(null)
                }
            }
            withContext(Dispatchers.Main){
                viewState.showProgress(false)
            }
        }
    }

}