package sumra.net.app.infinity_card.models

import com.google.gson.annotations.SerializedName

class _jsonCountries : ArrayList<Country>()

data class Country(
    @SerializedName("label")
    val label: String,
    @SerializedName("value")
    val value: Long
)