package sumra.net.app.infinity_card.interfaces

import android.os.Bundle

interface IPictureTransfer {
    fun picturesTransfer(picture: String)
    fun picturesTransfer(picture: Bundle)
}