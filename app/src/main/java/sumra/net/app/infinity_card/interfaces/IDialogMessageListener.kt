package sumra.net.app.infinity_card.interfaces

interface IDialogMessageListener {
    fun dialogMessage(id : Int, message: String)
}