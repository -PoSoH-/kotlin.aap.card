package sumra.net.app.infinity_card.frgSignInAndSignUp.navigation.authUser

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import sumra.net.app.infinity_card.frgSignInAndSignUp.ConverterVerify
import sumra.net.app.infinity_card.frgSignInAndSignUp.NetVerify
import sumra.net.app.infinity_card.models.responses.AuthUser
import sumra.net.app.infinity_card.models.responses.RespVerifyPhone

class RepositoryVerifyAuth {

    suspend fun fetchAuthSignUp(user: AuthUser): Deferred<RespVerifyPhone>{
        val resp = NetVerify.postAuthSignUp(ConverterVerify.convertReqstModelToCode(user))
        return GlobalScope.async {
            ConverterVerify.convertRespCodeToUser(resp)
        }
    }

    suspend fun fetchAuthSignIn(user: AuthUser): Deferred<RespVerifyPhone>{
        val resp = NetVerify.postAuthSignIn(ConverterVerify.convertReqstModelToCode(user))
        return GlobalScope.async {
            ConverterVerify.convertRespCodeToUser(resp)
        }
    }

    suspend fun fetchTokenRefresh(token: String, tokenRefresh: String): Deferred<RespVerifyPhone>{
        val resp = NetVerify.postRefreshToken(token, tokenRefresh)
        return GlobalScope.async {
            ConverterVerify.convertRespCodeToUser(resp)
        }
    }

    suspend fun fetchAuthSignOut(tokenBearer: String): Deferred<RespVerifyPhone>{
        val resp = NetVerify.postAuthSignOut(tokenBearer)
        return GlobalScope.async {
            ConverterVerify.convertRespCodeToUser(resp)
        }
    }

}