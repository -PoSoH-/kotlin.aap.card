package sumra.net.app.infinity_card.oAuth2tests

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Resources
import android.net.Uri
import android.text.TextUtils
import androidx.annotation.Nullable
import net.openid.appauth.connectivity.ConnectionBuilder
import net.openid.appauth.connectivity.DefaultConnectionBuilder
import okio.Buffer
import okio.BufferedSource
import okio.buffer
import okio.source
import org.json.JSONException
import org.json.JSONObject
import sumra.net.app.infinity_card.R
import sumra.net.app.infinity_card.wsoi.ConnectionBuilderForTesting
import java.io.IOException
import java.lang.ref.WeakReference
import java.nio.charset.Charset
import kotlin.jvm.Throws


class Configuration private constructor(context: Context){

    private val TAG = "Configuration"

    private val PREFS_NAME = "config"
    private val KEY_LAST_HASH = "lastHash"

    private var mContext: Context? = null
    private var mPrefs: SharedPreferences? = null
    private var mResources: Resources? = null

    private var mConfigJson: JSONObject? = null
    private var mConfigHash: String? = null
    private var mConfigError: String? = null

    private var mClientId: String? = null
    private var mSecretId: String? = null
    private var mScope: String? = null
    private var mRedirectUri: Uri? = null
//    private var mDiscoveryUri: Uri? = null
    private var mAuthEndpointUri: Uri? = null
    private var mTokenEndpointUri: Uri? = null
    private var mLogoutEndpointUri: Uri? = null
//    private var mRegistrationEndpointUri: Uri? = null
    private var mUserInfoEndpointUri: Uri? = null
    private var mHttpsRequired = false

    init {
        mContext = context
        mPrefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        mResources = context.resources
        try {
            readConfiguration()
        } catch (ex: InvalidConfigurationException) {
            mConfigError = ex.message
        }
    }

    companion object {
        fun getInstance(context: Context): Configuration? {
            var sInstance = WeakReference<Configuration?>(null)
            var config = sInstance.get()
            if (config == null) {
                config = Configuration(context.applicationContext)
                sInstance = WeakReference(config)
            }
            return config
        }
    }

    /**
     * Indicates whether the configuration has changed from the last known valid state.
     */
    fun hasConfigurationChanged(): Boolean {
        val lastHash = getLastKnownConfigHash()
        return mConfigHash != lastHash
    }

    /**
     * Indicates whether the current configuration is valid.
     */
    fun isValid(): Boolean {
        return mConfigError == null
    }

    /**
     * Returns a description of the configuration error, if the configuration is invalid.
     */
    @Nullable
    fun getConfigurationError(): String? {
        return mConfigError
    }

    /**
     * Indicates that the current configuration should be accepted as the "last known valid"
     * configuration.
     */
    fun acceptConfiguration() {
        mPrefs!!.edit().putString(KEY_LAST_HASH, mConfigHash).apply()
    }

    @Nullable
    fun getClientId(): String? {
        return mClientId
    }

    @Nullable
    fun getSecretId(): String? {
        return mSecretId
    }

    fun getScope(): String {
        return mScope!!
    }

    fun getRedirectUri(): Uri {
        return mRedirectUri!!
    }

//    @Nullable
//    fun getDiscoveryUri(): Uri? {
//        return mDiscoveryUri
//    }

    @Nullable
    fun getAuthEndpointUri(): Uri? {
        return mAuthEndpointUri
    }

    @Nullable
    fun getTokenEndpointUri(): Uri? {
        return mTokenEndpointUri
    }

    @Nullable
    fun getLogoutEndpointUri(): Uri? {
        return mLogoutEndpointUri
    }

//    @Nullable
//    fun getRegistrationEndpointUri(): Uri? {
//        return mRegistrationEndpointUri
//    }

    @Nullable
    fun getUserInfoEndpointUri(): Uri? {
        return mUserInfoEndpointUri
    }

    fun isHttpsRequired(): Boolean {
        return mHttpsRequired
    }

    fun getConnectionBuilder(): ConnectionBuilder? {
        return if (isHttpsRequired()) {
            DefaultConnectionBuilder.INSTANCE
        } else ConnectionBuilderForTesting.INSTANCE
    }

    private fun getLastKnownConfigHash(): String? {
        return mPrefs!!.getString(KEY_LAST_HASH, null)
    }

    @Throws(InvalidConfigurationException::class)
    private fun readConfiguration() {
        val configSource: BufferedSource = mResources!!.openRawResource(R.raw.auth_config).source().buffer()
        val configData = Buffer()
        try {
            configSource.readAll(configData)
            mConfigJson = JSONObject(configData.readString(Charset.forName("UTF-8")))
        } catch (ex: IOException) {
            throw InvalidConfigurationException(
                "Failed to read configuration: " + ex.message
            )
        } catch (ex: JSONException) {
            throw InvalidConfigurationException(
                "Unable to parse configuration: " + ex.message
            )
        }
        mConfigHash = configData.sha256().base64()
        mClientId = getConfigString("client_id")
        mSecretId = getConfigString("client_secret")
        mScope = getRequiredConfigString("authorization_scope")
        mRedirectUri = getRequiredConfigUri("redirect_uri")
        if (!isRedirectUriRegistered()) {
            throw InvalidConfigurationException(
                "redirect_uri is not handled by any activity in this app! "
                        + "Ensure that the appAuthRedirectScheme in your build.gradle file "
                        + "is correctly configured, or that an appropriate intent filter "
                        + "exists in your app manifest."
            )
        }
//        if (getConfigString("discovery_uri") == null) {
//            mAuthEndpointUri = getRequiredConfigWebUri("authorization_endpoint_uri")
//            mTokenEndpointUri = getRequiredConfigWebUri("token_endpoint_uri")
//            mUserInfoEndpointUri = getRequiredConfigWebUri("user_info_endpoint_uri")
//            if (mClientId == null) {
//                mRegistrationEndpointUri = getRequiredConfigWebUri("registration_endpoint_uri")
//            }
//        } else {
//            mDiscoveryUri = getRequiredConfigWebUri("discovery_uri")
//        }
        mAuthEndpointUri = getRequiredConfigWebUri("authorization_endpoint_uri")
        mTokenEndpointUri = getRequiredConfigWebUri("token_endpoint_uri")
        mUserInfoEndpointUri = getRequiredConfigWebUri("user_info_endpoint_uri")
        mLogoutEndpointUri = getRequiredConfigWebUri("end_session_endpoint")
        mHttpsRequired = mConfigJson!!.optBoolean("https_required", true)
    }

    @Nullable
    fun getConfigString(propName: String?): String? {
        var value = mConfigJson!!.optString(propName) ?: return null
        value = value.trim { it <= ' ' }
        return if (TextUtils.isEmpty(value)) {
            null
        } else value
    }

    @Throws(InvalidConfigurationException::class)
    private fun getRequiredConfigString(propName: String): String {
        return getConfigString(propName)!!
            ?: throw InvalidConfigurationException(
                "$propName is required but not specified in the configuration"
            )
    }

    @Throws(InvalidConfigurationException::class)
    fun getRequiredConfigUri(propName: String): Uri {
        val uriStr = getRequiredConfigString(propName)
        val uri: Uri
        uri = try {
            Uri.parse(uriStr)
        } catch (ex: Throwable) {
            throw InvalidConfigurationException("$propName could not be parsed", ex)
        }
        if (!uri.isHierarchical || !uri.isAbsolute) {
            throw InvalidConfigurationException(
                "$propName must be hierarchical and absolute"
            )
        }
        if (!TextUtils.isEmpty(uri.encodedUserInfo)) {
            throw InvalidConfigurationException("$propName must not have user info")
        }
        if (!TextUtils.isEmpty(uri.encodedQuery)) {
            throw InvalidConfigurationException("$propName must not have query parameters")
        }
        if (!TextUtils.isEmpty(uri.encodedFragment)) {
            throw InvalidConfigurationException("$propName must not have a fragment")
        }
        return uri
    }

    @Throws(InvalidConfigurationException::class)
    fun getRequiredConfigWebUri(propName: String): Uri? {
        val uri = getRequiredConfigUri(propName)
        val scheme = uri.scheme
        if (TextUtils.isEmpty(scheme) || !("http" == scheme || "https" == scheme)) {
            throw InvalidConfigurationException(
                "$propName must have an http or https scheme"
            )
        }
        return uri
    }

    private fun isRedirectUriRegistered(): Boolean {
        // ensure that the redirect URI declared in the configuration is handled by some activity
        // in the app, by querying the package manager speculatively
        val redirectIntent = Intent()
        redirectIntent.setPackage(mContext!!.packageName)
        redirectIntent.action = Intent.ACTION_VIEW
        redirectIntent.addCategory(Intent.CATEGORY_BROWSABLE)
        redirectIntent.data = mRedirectUri
        return !mContext!!.packageManager.queryIntentActivities(redirectIntent, 0).isEmpty()
    }

    class InvalidConfigurationException : Exception {
        internal constructor(reason: String?) : super(reason) {}
        internal constructor(reason: String?, cause: Throwable?) : super(reason, cause) {}
    }

}