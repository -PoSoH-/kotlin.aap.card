package sumra.net.app.infinity_card.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.annotations.SerializedName
import sumra.net.app.infinity_card.R
import sumra.net.app.infinity_card.adapters.holders.HolderCardTypeSelect
import sumra.net.app.infinity_card.interfaces.ICardTypeSelectListener
import sumra.net.app.infinity_card.models.Balance
import sumra.net.app.infinity_card.models.CardTypeItem

class AdapterCardTypeSelect(val listener: ICardTypeSelectListener) : RecyclerView.Adapter<HolderCardTypeSelect>(){

    private val mCardTypes = mutableListOf<CardTypeItem>()
    private var mCurrentBalance : Balance? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderCardTypeSelect {
        return HolderCardTypeSelect(LayoutInflater.from(parent.context).inflate(R.layout.cell_card_type_select_show, parent, false))
    }

    override fun onBindViewHolder(holder: HolderCardTypeSelect, position: Int) {
        holder.bindCard(cardType = mCardTypes[position], listener = listener, balance= mCurrentBalance!!)
    }

    override fun getItemCount() = mCardTypes.size

    fun bindTypes(typeCard: MutableList<CardTypeItem>, balance: Balance){
        mCurrentBalance = balance
        mCardTypes.clear()
        mCardTypes.addAll(typeCard)
        notifyDataSetChanged()
    }

    fun updateBalance(nowBalance: Double){
        this.mCurrentBalance = Balance(
            id        = this.mCurrentBalance!!.id,
            userId    = this.mCurrentBalance!!.userId,
            createdAt = this.mCurrentBalance!!.createdAt,
            updatedAt = this.mCurrentBalance!!.updatedAt,
            balance   = nowBalance.toString(),
            currency  = this.mCurrentBalance!!.currency
        )
        notifyDataSetChanged()
    }

}