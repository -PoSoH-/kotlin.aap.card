package sumra.net.app.infinity_card.models.responses

import com.google.gson.annotations.SerializedName
import sumra.net.app.infinity_card.models.User

data class OrderItem(
      val id: Long
    , val createAt: String
    , val updateAt: String
    , val totalPrice: String
    , val currency: String
    , val status: Int
)

data class Order(
      val id: Long
    , val createAt: String
    , val updateAt: String
    , val totalPrice: String
    , val cardTypeId: Long
    , val currency: String
    , val status: Int
    , val count: Int
)

data class _jsonRespOrderItems (
      val success: Boolean
    , val error: String?
    , val data: List<OrderItem>?)

data class _jsonRespOrder (
      val success: Boolean
    , val error: String?
    , val data: Order?)



