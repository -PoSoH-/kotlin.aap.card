package sumra.net.app.infinity_card.utils.extention

import android.text.Editable
import android.widget.EditText
import android.widget.TextView
import java.lang.RuntimeException
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

fun TextView.showDateWithTime(resText: String){
    val originalFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)  //2020-10-20 11:37:27
    val targetFormat: DateFormat = SimpleDateFormat("MM/yyyy")
    val date = originalFormat.parse(resText)
    text = "not parse"
    date.let {
        text = targetFormat.format(it!!)
    }
}

fun TextView.textDate(resText: String){
    text = correctParseDateWithTime(resText) //targetFormat.format(date)
}

fun TextView.textFullDate(resText: String){
    text = correctParseDateWithTimeFull(resText) //targetFormat.format(date)
}

private fun correctParseDateWithTime(resText: String): String{
    var originalFormat: DateFormat? = null
    try{
        originalFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
    }catch (ex: RuntimeException){
        try{
            originalFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        }catch (ex: RuntimeException){

        }
    }
    val targetFormat: DateFormat = SimpleDateFormat("MM/yyyy")
    val date = originalFormat!!.parse(resText)
    return targetFormat.format(date!!)
}

private fun correctParseDateWithTimeFull(resText: String): String{
    var originalFormat: DateFormat? = null
    try{
        originalFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
    }catch (ex: RuntimeException){
        try{
            originalFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        }catch (ex: RuntimeException){

        }
    }
    val targetFormat: DateFormat = SimpleDateFormat("dd/MM/yyyy")
    val date = originalFormat!!.parse(resText)
    return targetFormat.format(date!!)
}

fun TextView.inputHint(text: String){
    this.hint = Editable.Factory.getInstance().newEditable(text)
}