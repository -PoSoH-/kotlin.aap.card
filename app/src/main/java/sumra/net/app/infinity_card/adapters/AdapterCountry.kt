package sumra.net.app.infinity_card.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import sumra.net.app.infinity_card.R
import sumra.net.app.infinity_card.adapters.holders.HolderCountryItem
import sumra.net.app.infinity_card.interfaces.ICountryItemSelectListener
import sumra.net.app.infinity_card.models.Country

class AdapterCountry (val listener: ICountryItemSelectListener): RecyclerView.Adapter<HolderCountryItem>() {

    private val countries = mutableListOf<Country>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderCountryItem {
        return HolderCountryItem(
            LayoutInflater.from(parent.context).inflate(R.layout.cell_country_item_show, parent, false)
        )
    }

    override fun onBindViewHolder(holder: HolderCountryItem, position: Int) {
        holder.bindItem(country = countries[position], listener = listener)
    }

    override fun getItemCount() = countries.size

    fun bindCountries(countries: MutableList<Country>){
        this.countries.clear()
        this.countries.addAll(countries)
        notifyDataSetChanged()
    }

}