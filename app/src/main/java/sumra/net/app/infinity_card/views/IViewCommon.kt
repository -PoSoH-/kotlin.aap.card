package sumra.net.app.infinity_card.views

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(value = AddToEndSingleStrategy::class)
interface IViewCommon: MvpView {
    fun showProgress(state: Boolean)
}