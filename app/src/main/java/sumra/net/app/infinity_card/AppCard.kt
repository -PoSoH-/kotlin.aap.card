package sumra.net.app.infinity_card

import android.app.Activity
import android.app.Application
import android.graphics.Rect
import android.view.View
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.lay_infinity_card_progress_show.view.*
import sumra.net.app.infinity_card.utils.extention.hideScaleOtoC
import sumra.net.app.infinity_card.utils.extention.showScaleCtoO
import sumra.net.app.progresscircle.CircleDrawable


class AppCard : Application() {

    override fun onCreate() {
        super.onCreate()
    }

    companion object{
        private lateinit var navController: NavController
        private lateinit var homeBottomNavigationView: BottomNavigationView
        private lateinit var progressBar: View

        fun instance(activity: Activity) {
            navController = Navigation.findNavController(activity, R.id.nav_host_fragment);
        }
        fun instanceBottomNav(activity: Activity) {
            homeBottomNavigationView = activity.findViewById(R.id.navigation_bottom_bar)
        }
        fun setupNavController(){
            NavigationUI.setupWithNavController(homeBottomNavigationView, navController)
        }

        fun getNavigation() = navController
        fun showBottomNavView(){ homeBottomNavigationView.visibility = View.VISIBLE}
        fun hideBottomNavView(){ homeBottomNavigationView.visibility = View.GONE}
        fun getBottomNavigationView() = homeBottomNavigationView

        fun addViewProgress(progress: View) {
            progressBar = progress
            var bounds: Rect
            progressBar.progressCircleFour.apply {
                bounds = this.indeterminateDrawable.getBounds()
                this.indeterminateDrawable = CircleDrawable.Builder(progress.context)
//                    .colors(arrayOf(progress.context.resources.getColor(R.color.red)))
                    .build()
                this.indeterminateDrawable.setBounds(bounds)
            }
//            progressBar.apply {
//                getIndeterminateDrawable().getBounds()
//            }
        }
        fun showProgress(){ progressBar.showScaleCtoO()}
        fun hideProgress(){ progressBar.hideScaleOtoC()}

        fun getCheckToken(): String {
            //this method checked, and update, and return all success token or never
            return "_0000_"
        }

    }
}