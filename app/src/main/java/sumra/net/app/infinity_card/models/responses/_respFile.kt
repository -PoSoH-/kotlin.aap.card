package sumra.net.app.infinity_card.models.responses

import com.google.gson.annotations.SerializedName
import sumra.net.app.infinity_card.models.Country

data class _respFiles(
    val success: String,
    var data: List<Country>?,
    var error: _jsonInfinityError.InfinityError?)