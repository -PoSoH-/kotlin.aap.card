package sumra.net.app.infinity_card.presenter

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import moxy.MvpPresenter
import sumra.net.app.infinity_card.core.NetOauthTest
import sumra.net.app.infinity_card.views.IViewHomeScreen

class PHomeScreen : MvpPresenter<IViewHomeScreen>() {

    fun fetchTokens(){
        CoroutineScope(Dispatchers.IO).launch {
            val resp = NetOauthTest.postFetchToken()
            // записати тут токени та час дії....
        }
    }

}