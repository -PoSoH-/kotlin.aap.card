package sumra.net.app.infinity_card.frgSignInAndSignUp.navigation.authUser

import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType
import sumra.net.app.infinity_card.views.IViewCommon

@StateStrategyType(value = AddToEndSingleStrategy::class)
interface IViewVerifyAuth: IViewCommon {

    fun fetchUserAuthSuccess()
    fun fetchUserAuthFailure(error: String?)

    fun fetchTokenRefreshSuccess()
    fun fetchTokenRefreshFailure(error: String?)

    fun fetchLogoutSuccess()
    fun fetchLogoutFailure(error: String?)

}

