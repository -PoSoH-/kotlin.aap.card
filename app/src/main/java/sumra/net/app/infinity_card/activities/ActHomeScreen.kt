package sumra.net.app.infinity_card.activities

import android.Manifest
import android.accounts.Account
import android.accounts.AccountManager
import android.annotation.TargetApi
import android.app.Activity
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import android.os.*
import android.provider.ContactsContract
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.frg_profile_screen.*
import kotlinx.android.synthetic.main.lay_infinity_card_camera_gallery_permissions.*
import kotlinx.coroutines.*
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter
import net.openid.appauth.*
import net.openid.appauth.AuthorizationServiceConfiguration.RetrieveConfigurationCallback
import net.openid.appauth.browser.AnyBrowserMatcher
import net.openid.appauth.browser.BrowserMatcher
import sumra.net.app.infinity_card.AppCard
import sumra.net.app.infinity_card.R
import sumra.net.app.infinity_card.enums.EKeys
import sumra.net.app.infinity_card.enums.EUserType
import sumra.net.app.infinity_card.fragments.FrgProfileScreen
import sumra.net.app.infinity_card.models.UserTypeMenuAgent
import sumra.net.app.infinity_card.oAuth2tests.AuthStateManager
import sumra.net.app.infinity_card.oAuth2tests.BrowserSelectionAdapter
import sumra.net.app.infinity_card.oAuth2tests.Configuration
import sumra.net.app.infinity_card.presenter.PHomeScreen
import sumra.net.app.infinity_card.presenter.PresenterProfile
import sumra.net.app.infinity_card.utils.UtilLog
import sumra.net.app.infinity_card.utils.extention.hideScaleOtoC
import sumra.net.app.infinity_card.utils.extention.showSnackBarError
import sumra.net.app.infinity_card.views.IViewHomeScreen
import sumra.net.app.infinity_card.views.IViewProfile
import java.io.File
import java.lang.Runnable
import java.util.concurrent.CountDownLatch
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicReference


class ActHomeScreen : MvpAppCompatActivity()
//    , IDialogMessageListener
    , IViewHomeScreen
    , IViewProfile { //R.layout.activity_main) {

    private val TAG = this::class.java.simpleName
    private val EXTRA_FAILED = "failed"
    private val RC_AUTH = 100
    private val CODE_PERMISSION_CAMERA = 0x000001
    private val CODE_PERMISSION_CARD = 0x000003

    private val CODE_TAKE_PHOTO = 0x000101
    private val CODE_READ_PHOTO = 0x000103

    private var isFinalMenu = false

    private var currentScreen: Int = R.id._fragment_all_card_screen

    private var currentImagePath: File? = null
    private var currentImageName: String = ""

    @InjectPresenter
    lateinit var mPresenterHome: PHomeScreen

    @InjectPresenter
    lateinit var mPrUserProfile: PresenterProfile

    private var mAuthService: AuthorizationService? = null
    private var mAuthStateManager: AuthStateManager? = null
    private var mConfiguration: Configuration? = null

    private val mClientId = AtomicReference<String>()
    private val mAuthRequest = AtomicReference<AuthorizationRequest?>()
    private val mAuthIntent = AtomicReference<CustomTabsIntent?>()
    private var mAuthIntentLatch = CountDownLatch(1)

    private var mUsePendingIntents = false

    private var mBrowserMatcher: BrowserMatcher = AnyBrowserMatcher.INSTANCE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        mExecutor = Executors.newSingleThreadExecutor()
        setContentView(R.layout.activity_main)
        //init nav controler
        AppCard.instance(this)
        AppCard.instanceBottomNav(this)
        AppCard.setupNavController()

//        val manager: AccountManager = AccountManager.get(this)
//        val accounts: Array<Account> = manager.getAccountsByType("com.google")
//        val size = accounts.size
//        val names = arrayOfNulls<String>(size)
//        for (i in 0 until size) {
//            names[i] = accounts[i].name
//        }
//
//        CoroutineScope(Dispatchers.IO).launch {
//            val c: Cursor? =
//                application.contentResolver.query(ContactsContract.Profile.CONTENT_URI, null, null, null, null)
//            c!!.moveToFirst()
//            val res = c.getString(c.getColumnIndex("display_name"))
//            c.close()
//        }

        AppCard.addViewProgress(progress_infinity_card_state)

        mAuthStateManager = AuthStateManager.getInstance(this)
        mConfiguration = Configuration.getInstance(this)
        if (mAuthStateManager?.getCurrent()!!.isAuthorized()
            && !mConfiguration?.hasConfigurationChanged()!!
        ) {
            UtilLog.info(TAG, "User is already authenticated, proceeding to token activity", activity_parent_view)
            AppCard.getNavigation().navigate(R.id._fragment_all_card_screen)
            return
        } else {
            UtilLog.info(TAG, "User is already not authenticated", activity_parent_view)
            CoroutineScope(Dispatchers.IO).launch {
                delay(2_500L)
                withContext(Dispatchers.Main){
//                    AppCard.getNavigation().navigate(R.id._fragment_all_card_screen)
//                    AppCard.getNavigation().navigate(R.id._frg_auth_phone_verify)
//                    startAuth()
                    loadProfileUser()
                }
            }
        }

        //кнопка
//        findViewById<View>(R.id.retry).setOnClickListener { view: View? ->
//            mExecutor.submit(
//                Runnable { initializeAppAuth() }
//            )
//        }
//        findViewById<View>(R.id.start_auth).setOnClickListener { view: View? -> startAuth() }
//        (findViewById<View>(R.id.login_hint_value) as EditText).addTextChangedListener(
//            LoginHintChangeHandler()
//        )


        if (!mConfiguration!!.isValid()) {
            displayError(mConfiguration!!.getConfigurationError()!!, false)
            return
        }
//        configureBrowserSelector()
        if (mConfiguration!!.hasConfigurationChanged()) {
            // discard any existing authorization state due to the change of configuration
            Log.i(TAG, "Configuration change detected, discarding old state")
            mAuthStateManager!!.replace(AuthState())
            mConfiguration!!.acceptConfiguration()
        }
        if (intent.getBooleanExtra(EXTRA_FAILED, false)) {
            displayAuthCancelled()
        }
        displayLoading("Initializing")
        CoroutineScope(Dispatchers.IO).launch {
            initializeAppAuth()
        }

        AppCard.getBottomNavigationView().apply {
            setOnNavigationItemSelectedListener(object : BottomNavigationView.OnNavigationItemSelectedListener {
                override fun onNavigationItemSelected(item: MenuItem): Boolean {
                    when (item.itemId) {
                        R.id._fragment_all_card_screen -> {
                            UtilLog.info(TAG, "SELECT ALL CARD")
//                            if (currentScreen != R .id._fragment_all_card_screen)
                            navigateToAllCards()
//                            AppCard.getNavigation().navigate(R.id._frg,  navigateToAllCards())
//                            AppCard.getNavigation().navigate(R.id._fragment_all_card_screen)
                        }
                        R.id._fragment_card_pay_screen -> {
                            UtilLog.info(TAG, "SELECT PAY CARD")
                            val bundleBuy = Bundle()
                            bundleBuy.putParcelableArrayList(
                                EKeys.PROF_BALANCE,
                                mPrUserProfile.getUserInfo().balances as ArrayList
                            )
                            AppCard.getNavigation().navigate(R.id._fragment_card_pay_screen, bundleBuy)
                        }
                        R.id._fragment_card_type_screen -> {
                            UtilLog.info(TAG, "SELECT TYPE CARD")
//                            if (currentScreen != R.id._fragment_card_type_screen) {
                            AppCard.getNavigation().navigate(R.id._fragment_card_type_screen)
//                                currentScreen = R.id._fragment_card_type_screen
//                            }
                        }
                        R.id._fragment_profile_screen -> {
                            UtilLog.info(TAG, "SELECT PROFILE")
//                            if (currentScreen != R.id._fragment_profile_screen) {
                            val bundleUser = Bundle()
                            bundleUser.putParcelable(EKeys.PROF_USER, mPrUserProfile.getUserInfo())
                            AppCard.getNavigation().navigate(R.id._fragment_profile_screen, bundleUser)
//                                currentScreen = R.id._fragment_profile_screen
//                            }
                        }
                    }
                    return false
                }
            })
            setOnNavigationItemReselectedListener {
                object : BottomNavigationView.OnNavigationItemReselectedListener {
                    override fun onNavigationItemReselected(item: MenuItem) {
                        UtilLog.error(TAG, item.itemId.toString())
                    }
                }
            }
        }
    }

    fun onClickListener(view: View) {
        when (view.id) {
//            btn_action_press_login.id -> {
//                startAuth()
//            }
//            btn_action_press_registration.id -> {
//                startAuth()
//            }
//             -> UtilLog.error(TAG, "Data ")
        }
    }

    fun onClickRegistration(view: View) {
        startAuth()
        //navigateToAllCards()
    }

//    override fun onStart() {
//        super.onStart()
////        if (mExecutor!!.isShutdown) {
////            mExecutor = Executors.newSingleThreadExecutor()
////        }
//    }

//    override fun onStop() {
//        super.onStop()
////        mExecutor!!.shutdownNow()
//    }

    override fun onDestroy() {
        super.onDestroy()
        if (mAuthService != null) {
            mAuthService!!.dispose()
        }
    }

    private fun loadProfileUser() {
        mPrUserProfile.fetchMeProfile("_0000_")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        UtilLog.error(TAG, "Start activity result")
        displayAuthOptions()
        if (resultCode == Activity.RESULT_CANCELED) {
            when(requestCode){
                RC_AUTH -> {
                    displayAuthCancelled(); loadProfileUser()
                }
                CODE_TAKE_PHOTO -> errorTakePhoto()
                CODE_READ_PHOTO -> errorChoosePhoto()
            }
        } else {
            when (requestCode) {
                RC_AUTH -> {
                    Toast.makeText(this, "Start fragment for all card views", Toast.LENGTH_LONG).show()
                    //            AppCard.getNavigation().navigate(R.id._action_frg_splash_to_frg_all_cards)
                    val dt = data?.data!!.toString()
                    val rs = dt!!.split("?")[1].split("=")[1].split("&")[0]
                    val acc = mAuthStateManager!!.getCurrent().accessToken
                    val ref = mAuthStateManager!!.getCurrent().refreshToken
                    //            val typ = mAuthStateManager!!.getCurrent().
                    val scp = mAuthStateManager!!.getCurrent().scope
                    val exp = mAuthStateManager!!.getCurrent().accessTokenExpirationTime

                    //            mPresenterHome.fetchTokens()

                    //            val intent = Intent(this, TokenActivity::class.java)
                    //            intent.putExtras(data.extras!!)
                    //            startActivity(intent)

                    val response = AuthorizationResponse.fromIntent(data)
                    val ex = AuthorizationException.fromIntent(data)

                    if (response != null || ex != null) {
                        mAuthStateManager!!.updateAfterAuthorization(response, ex)
                    }

                    if (response != null && response.authorizationCode != null) {
                        // authorization code exchange is required
                        mAuthStateManager!!.updateAfterAuthorization(response, ex)

                        val acc = mAuthStateManager!!.getCurrent().accessToken
                        val ref = mAuthStateManager!!.getCurrent().refreshToken
                        //            val typ = mAuthStateManager!!.getCurrent().
                        val scp = mAuthStateManager!!.getCurrent().scope
                        val exp = mAuthStateManager!!.getCurrent().accessTokenExpirationTime

                        exchangeAuthorizationCode(response)
                    } else if (ex != null) {
                        Toast.makeText(this, "Authorization flow failed: ${ex.message}", Toast.LENGTH_LONG).show()
                        //                displayNotAuthorized("Authorization flow failed: " + ex.getMessage())
                    } else {
                        Toast.makeText(
                            this,
                            "No authorization state retained - reauthorization required",
                            Toast.LENGTH_LONG
                        ).show()
                        //                displayNotAuthorized("No authorization state retained - reauthorization required")
                    }
                    loadProfileUser()
                }
                CODE_TAKE_PHOTO -> {
                    Toast.makeText(this, "TAKE PHOTO SUCCESS -> ${currentImagePath!!.absoluteFile}", Toast.LENGTH_SHORT).show()
                    val fjkd = Uri.fromFile(currentImagePath!!)
                    goToPhotoByFragmentProfile(fjkd)
                }
                CODE_READ_PHOTO -> {
                    Toast.makeText(this, "READ PHOTO SUCCESS -> ${data!!.dataString}", Toast.LENGTH_SHORT).show()
                    if(data.data != null) {
                        goToPhotoByFragmentProfile(data.data!!)
                    }else{
                        errorChoosePhoto()
                    }
                }
            }
        }

        UtilLog.error(TAG, "Final activity result")
    }

    private fun goToPhotoByFragmentProfile(pictureUri: Uri){
        val frg = supportFragmentManager.fragments
        val cur = frg[0].childFragmentManager.fragments[0] as FrgProfileScreen
        cur.updatePictureState(pictureUri)
    }

    private fun exchangeAuthorizationCode(authorizationResponse: AuthorizationResponse) {
        displayLoading("Exchanging authorization code")
        performTokenRequest(
            authorizationResponse.createTokenExchangeRequest(),
            object : AuthorizationService.TokenResponseCallback {
                override fun onTokenRequestCompleted(
                    tokenResponse: TokenResponse?,
                    authException: AuthorizationException?
                ) {
                    mAuthStateManager!!.updateAfterTokenResponse(tokenResponse, authException)
                    if (!mAuthStateManager!!.getCurrent().isAuthorized()) {
                        val message = ("Authorization Code exchange failed"
                                + if (authException != null) authException.error else "")

                        // WrongThread inference is incorrect for lambdas
//                        displayNotAuthorized(message)
                        displayError(message, false)
                    } else {
//                        runOnUiThread(this::displayAuthorized)
                        displayLoading("Authorization success")
                    }
                }
            }
        )
    }

    private fun performTokenRequest(
        request: TokenRequest,
        callback: AuthorizationService.TokenResponseCallback
    ) {
        val clientAuthentication: ClientAuthentication
        clientAuthentication = try {
            mAuthStateManager!!.getCurrent().getClientAuthentication()
        } catch (ex: ClientAuthentication.UnsupportedAuthenticationMethod) {
            Log.d(
                TAG, "Token request cannot be made, client authentication for the token "
                        + "endpoint could not be constructed (%s)", ex
            )
            Toast.makeText(this, "Client authentication method is unsupported", Toast.LENGTH_LONG).show()

            return
        }
        mAuthService!!.performTokenRequest(
            request,
            clientAuthentication,
            callback
        )
    }

    fun startAuth() {
        UtilLog.error(TAG, "Start do auth")
        displayLoading("Making authorization request")
//        mUsePendingIntents = (findViewById<View>(R.id.pending_intents_checkbox) as CheckBox).isChecked

        // WrongThread inference is incorrect for lambdas
        // noinspection WrongThread
//        mExecutor!!.submit { doAuth() }
        CoroutineScope(Dispatchers.IO).launch {
            doAuth()
        }
        UtilLog.error(TAG, "Final do auth")
    }

    /**
     * Initializes the authorization service configuration if necessary, either from the local
     * static values or by retrieving an OpenID discovery document.
     */
//    @WorkerThread
    private suspend fun initializeAppAuth() {
        UtilLog.error(TAG, "Initializing AppAuth")
        recreateAuthorizationService()
        if (mAuthStateManager!!.getCurrent().getAuthorizationServiceConfiguration() != null) {
            // configuration is already created, skip to client initialization
            Log.i(TAG, "auth config already established")
            initializeClient()
            return
        }

        // if we are not using discovery, build the authorization service configuration directly
        // from the static configuration values.
//        if (mConfiguration!!.getDiscoveryUri() == null) {
//            Log.i(TAG, "Creating auth config from res/raw/auth_config.json")
        val config = AuthorizationServiceConfiguration(
            mConfiguration!!.getAuthEndpointUri()!!,
            mConfiguration!!.getTokenEndpointUri()!!
//                mConfiguration.getRegistrationEndpointUri()
        )
        mAuthStateManager!!.replace(AuthState(config))
        initializeClient()

//        }

        // WrongThread inference is incorrect for lambdas
        // noinspection WrongThread

        withContext(Dispatchers.Main) { displayLoading("Retrieving discovery document") }
        UtilLog.error(TAG, "Retrieving OpenID discovery doc")
        AuthorizationServiceConfiguration.fetchFromUrl(
            mConfiguration?.getAuthEndpointUri()!!,  //mConfiguration.getDiscoveryUri(),
            RetrieveConfigurationCallback { config: AuthorizationServiceConfiguration?, ex: AuthorizationException? ->
                object : AuthorizationServiceConfiguration.RetrieveConfigurationCallback {
                    override fun onFetchConfigurationCompleted(
                        serviceConfiguration: AuthorizationServiceConfiguration?,
                        ex: AuthorizationException?
                    ) {
                        if (config == null) {
                            Log.i(TAG, "Failed to retrieve discovery document", ex)
                            displayError("Failed to retrieve discovery document: " + ex!!.message, true)
                            return
                        }
                        Log.i(TAG, "Discovery document retrieved")
                        mAuthStateManager!!.replace(AuthState(config))
                        CoroutineScope(Dispatchers.IO).launch { initializeClient() }
                    }

                }
//                handleConfigurationRetrievalResult(
//                    config,
//                    ex
            })
        mConfiguration!!.getConnectionBuilder()
    }

////    @MainThread
//    private fun handleConfigurationRetrievalResult(
//        config: AuthorizationServiceConfiguration?,
//        ex: AuthorizationException?
//    ) {
//        if (config == null) {
//            Log.i(TAG, "Failed to retrieve discovery document", ex)
//            displayError("Failed to retrieve discovery document: " + ex!!.message, true)
//            return
//        }
//        Log.i(TAG, "Discovery document retrieved")
//        mAuthStateManager!!.replace(AuthState(config))
//        CoroutineScope(Dispatchers.IO).launch { initializeClient() }
//    }

    /**
     * Initiates a dynamic registration request if a client ID is not provided by the static
     * configuration.
     */

    private suspend fun initializeClient() {
        UtilLog.error(TAG, "initialize client...")
        if (mConfiguration!!.getClientId() != null) {
            Log.i(TAG, "Using static client ID: " + mConfiguration!!.getClientId())
            // use a statically configured client ID
            mClientId.set(mConfiguration!!.getClientId())
//            withContext(Dispatchers.Main) { initializeAuthRequest() }
            initializeAuthRequest()
            return
        }
        val lastResponse: RegistrationResponse = mAuthStateManager!!.getCurrent().getLastRegistrationResponse()!!
        if (lastResponse != null) {
            Log.i(TAG, "Using dynamic client ID: " + lastResponse.clientId)
            // already dynamically registered a client ID
            mClientId.set(lastResponse.clientId)
//            withContext(Dispatchers.Main) {
            initializeAuthRequest()
//        }
            return
        }

        // WrongThread inference is incorrect for lambdas
        // noinspection WrongThread
        withContext(Dispatchers.Main) { displayLoading("Dynamically registering client") }
        Log.i(TAG, "Dynamically registering client")
        val registrationRequest = RegistrationRequest.Builder(
            mAuthStateManager!!.getCurrent().getAuthorizationServiceConfiguration()!!,
            listOf(mConfiguration!!.getRedirectUri())
        )
            .setTokenEndpointAuthenticationMethod(ClientSecretBasic.NAME)
            .build()
        mAuthService!!.performRegistrationRequest(
            registrationRequest
        ) { response: RegistrationResponse?, ex: AuthorizationException? ->
            object : AuthorizationService.RegistrationResponseCallback {
                override fun onRegistrationRequestCompleted(
                    response: RegistrationResponse?,
                    ex: AuthorizationException?
                ) {
                    mAuthStateManager!!.updateAfterRegistration(response, ex)
                    if (response == null) {
//                        displayErrorLater("Failed to register client: " + ex!!.message, true)
                        return
                    }
                    UtilLog.error(TAG, "Dynamically registered client: " + response.clientId)
                    mClientId.set(response.clientId)
                    initializeAuthRequest()
                }
            }
//            handleRegistrationResponse(
//                response,
//                ex
//            )
        }
    }

//    private suspend fun initAuthRequest(){
//        withContext(Dispatchers.Main){initializeAuthRequest()}
//    }

////    @MainThread
//    private suspend fun handleRegistrationResponse(
//        response: RegistrationResponse?,
//        ex: AuthorizationException?
//    ) {
//        mAuthStateManager!!.updateAfterRegistration(response, ex)
//        if (response == null) {
//            Log.i(TAG, "Failed to dynamically register client", ex)
//            displayErrorLater("Failed to register client: " + ex!!.message, true)
//            return
//        }
//        Log.i(TAG, "Dynamically registered client: " + response.clientId)
//        mClientId.set(response.clientId)
//        initializeAuthRequest()
//    }

    /**
     * Enumerates the browsers installed on the device and populates a spinner, allowing the
     * demo user to easily test the authorization flow against different browser and custom
     * tab configurations.
     */
//    @MainThread
    private fun configureBrowserSelector() {
//        val spinner = findViewById<View>(R.id.browser_selector) as Spinner
        val adapter = BrowserSelectionAdapter()
//        spinner.adapter = adapter
//        spinner.onItemSelectedListener = object : OnItemSelectedListener {
//            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
//                val info = adapter.getItem(position)
//                if (info == null) {
//                    mBrowserMatcher = AnyBrowserMatcher.INSTANCE
//                    return
//                } else {
//                    mBrowserMatcher = ExactBrowserMatcher(info.mDescriptor)
//                }
//                recreateAuthorizationService()
//                createAuthRequest(getLoginHint())
//                warmUpBrowser()
//            }

//            override fun onNothingSelected(parent: AdapterView<*>?) {
//                mBrowserMatcher = AnyBrowserMatcher.INSTANCE
//            }
//        }
    }

    /**
     * Performs the authorization request, using the browser selected in the spinner,
     * and a user-provided `login_hint` if available.
     */
//    @WorkerThread
    private fun doAuth() {
        UtilLog.error(TAG, "private fun doAuth...")
//        try {
//            mAuthIntentLatch.await()
//        } catch (ex: InterruptedException) {
//            UtilLog.error(TAG, "Interrupted while waiting for auth intent")
//        }
        if (mUsePendingIntents) { //mUsePendingIntents) {
            val completionIntent = Intent(this, null) //TokenActivity::class.java)
            val cancelIntent = Intent(this, null) //LoginActivity::class.java)
            cancelIntent.putExtra(EXTRA_FAILED, true)
            cancelIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            mAuthService!!.performAuthorizationRequest(
                mAuthRequest.get()!!,
                PendingIntent.getActivity(this, 0, completionIntent, 0),
                PendingIntent.getActivity(this, 0, cancelIntent, 0),
                mAuthIntent.get()!!
            )
        } else {
            val intent = mAuthService!!.getAuthorizationRequestIntent(
                mAuthRequest.get()!!,
                mAuthIntent.get()!!
            )
            startActivityForResult(intent, RC_AUTH)
        }
    }

    private fun recreateAuthorizationService() {
        UtilLog.error(TAG, "recreate authorization...")
        if (mAuthService != null) {
            Log.i(TAG, "Discarding existing AuthService instance")
            mAuthService!!.dispose()
        }
        mAuthService = createAuthorizationService()
        mAuthRequest.set(null)
        mAuthIntent.set(null)
    }

    private fun createAuthorizationService(): AuthorizationService? {
        UtilLog.error(TAG, "Creating authorization service")
        val builder = AppAuthConfiguration.Builder()
        builder.setBrowserMatcher(mBrowserMatcher)
        builder.setConnectionBuilder(mConfiguration!!.getConnectionBuilder()!!)
        return AuthorizationService(this, builder.build())
    }

    //    @MainThread
    private fun displayLoading(loadingMessage: String) {
        UtilLog.error("DISP_LOAD", "Display load message -> $loadingMessage")
        Toast.makeText(this, loadingMessage, Toast.LENGTH_LONG).show()
//        findViewById<View>(R.id.loading_container).visibility = View.VISIBLE
//        findViewById<View>(R.id.auth_container).visibility = View.GONE
//        findViewById<View>(R.id.error_container).visibility = View.GONE
//        (findViewById<View>(R.id.loading_description) as TextView).text = loadingMessage
    }

    //    @MainThread
    private fun displayError(error: String, recoverable: Boolean) {
        UtilLog.error("DISP_LOAD", "Display error message -> $error")



        Toast.makeText(this, error, Toast.LENGTH_LONG).show()
//        findViewById<View>(R.id.error_container).visibility = View.VISIBLE
//        findViewById<View>(R.id.loading_container).visibility = View.GONE
//        findViewById<View>(R.id.auth_container).visibility = View.GONE
//        (findViewById<View>(R.id.error_description) as TextView).text = error
//        findViewById<View>(R.id.retry).visibility = if (recoverable) View.VISIBLE else View.GONE
    }

    // WrongThread inference is incorrect in this case
//    @AnyThread
    private suspend fun displayErrorLater(error: String, recoverable: Boolean) {
        withContext(Dispatchers.Main) {
            displayError(error, recoverable)
        }
    }

    //    @MainThread
    private fun initializeAuthRequest() {
        createAuthRequest()   //  (getLoginHint())
        warmUpBrowser()
        displayAuthOptions()
    }

    //    @MainThread
    private fun displayAuthOptions() {
        UtilLog.error("DISP_LOAD", "Display oauth options...")
//        findViewById<View>(R.id.auth_container).visibility = View.VISIBLE
//        findViewById<View>(R.id.loading_container).visibility = View.GONE
//        findViewById<View>(R.id.error_container).visibility = View.GONE
        val state: AuthState = mAuthStateManager!!.getCurrent()
        val config = state.authorizationServiceConfiguration
        var authEndpointStr: String
        authEndpointStr = if (config!!.discoveryDoc != null) {
            "Discovered auth endpoint: \n"
        } else {
            "Static auth endpoint: \n"
        }
        authEndpointStr += config.authorizationEndpoint
//        (findViewById<View>(R.id.auth_endpoint) as TextView).text = authEndpointStr
        var clientIdStr: String
        clientIdStr = if (state.lastRegistrationResponse != null) {
            "Dynamic client ID: \n"
        } else {
            "Static client ID: \n"
        }
        clientIdStr += mClientId
//        (findViewById<View>(R.id.client_id) as TextView).text = clientIdStr
    }

    private fun displayAuthCancelled() {
        UtilLog.error(TAG, "Display auth canceled...")

        Toast.makeText(this, "Authorization canceled", Toast.LENGTH_LONG).show()
    }

    private fun warmUpBrowser() {
        UtilLog.error(TAG, "warm up browser...")
        mAuthIntentLatch = CountDownLatch(1)
        CoroutineScope(Dispatchers.IO).launch {
            UtilLog.error(TAG, "Warming up browser instance for auth request")
            val intentBuilder =
                mAuthService!!.createCustomTabsIntentBuilder(mAuthRequest.get()!!.toUri())
            intentBuilder.setToolbarColor(getColorCompat(R.color.colorPrimary))
            mAuthIntent.set(intentBuilder.build())
            mAuthIntentLatch.countDown()
            UtilLog.error(TAG, "End coroutine works ... warm up browser...")
        }
    }

    private fun createAuthRequest() {
        UtilLog.error(TAG, "Creating auth request for login hint: login hint...")

        val codeVerifier = CodeVerifierUtil.generateRandomCodeVerifier()
        val codeChallenge = CodeVerifierUtil.deriveCodeVerifierChallenge(codeVerifier)

        val authRequestBuilder = AuthorizationRequest.Builder(
            mAuthStateManager!!.getCurrent().getAuthorizationServiceConfiguration()!!,
            mClientId.get(),
            ResponseTypeValues.CODE,
            mConfiguration!!.getRedirectUri()
        ).apply {
            setScope(mConfiguration!!.getScope())
            setCodeVerifier(
                codeVerifier,
                codeChallenge,
                AuthorizationRequest.CODE_CHALLENGE_METHOD_S256
            )
        }

//        if (!TextUtils.isEmpty(loginHint)) {
//            authRequestBuilder.setLoginHint(loginHint)
//        }
        mAuthRequest.set(authRequestBuilder.build())
    }

//    private fun createAuthRequest() {
//        UtilLog.info(TAG, "Creating authorization request.")
//
//        val codeVerifier = CodeVerifierUtil.generateRandomCodeVerifier()
//        val codeChallenge = CodeVerifierUtil.deriveCodeVerifierChallenge(codeVerifier)
//
//        val authRequestBuilder = AuthorizationRequest.Builder(
//            mAuthStateManager!!.getCurrent().authorizationServiceConfiguration!!,
//            mClientId.get(),
//            ResponseTypeValues.CODE,
//            mConfiguration!!.getRedirectUri()
//        ).setScope(
//            mConfiguration!!.getScope()).apply {
//            setCodeVerifier(
//                codeVerifier,
//                codeChallenge,
//                AuthorizationRequest.CODE_CHALLENGE_METHOD_S256)
//        }.build()
//        mAuthRequest.set(authRequestBuilder)
//    }


    private fun getLoginHint(): String {
        return "Login hint"
//        (findViewById<View>(R.id.login_hint_value) as EditText)
//            .text
//            .toString()
//            .trim { it <= ' ' }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private fun getColorCompat(@ColorRes color: Int): Int {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getColor(color)
        } else {
            resources.getColor(color)
        }
    }

    /**
     * Responds to changes in the login hint. After a "debounce" delay, warms up the browser
     * for a request with the new login hint; this avoids constantly re-initializing the
     * browser while the user is typing.
     */
    private class LoginHintChangeHandler internal constructor() : TextWatcher {
        private val mHandler: Handler
        private var mTask: RecreateAuthRequestTask
        override fun beforeTextChanged(
            cs: CharSequence,
            start: Int,
            count: Int,
            after: Int
        ) {
        }

        override fun onTextChanged(
            cs: CharSequence,
            start: Int,
            before: Int,
            count: Int
        ) {
            mTask.cancel()
            mTask = RecreateAuthRequestTask()
            mHandler.postDelayed(mTask, DEBOUNCE_DELAY_MS)
        }

        override fun afterTextChanged(ed: Editable) {}

        companion object {
            private const val DEBOUNCE_DELAY_MS = 500L
        }

        init {
            mHandler = Handler(Looper.getMainLooper())
            mTask = RecreateAuthRequestTask()
        }
    }

    private class RecreateAuthRequestTask : Runnable {
        private val mCanceled = AtomicBoolean()
        override fun run() {
            if (mCanceled.get()) {
                return
            }
//            createAuthRequest(getLoginHint())
//            warmUpBrowser()
        }

        fun cancel() {
            mCanceled.set(true)
        }
    }

    private var type: Long? = null

    private fun navigateToAllCards() { // : Bundle{
        UtilLog.info(TAG, "SELECT ALL CARD")
        val bundle = Bundle()
        val card = mPrUserProfile.getUserInfo().cards as ArrayList
        type = mPrUserProfile.getUserInfo().typeId
        bundle.putParcelableArrayList(EKeys.PROF_CARD, card)
        bundle.putParcelableArrayList(EKeys.PROF_BALANCE, mPrUserProfile.getUserInfo().balances as ArrayList)
        bundle.putString(EKeys.PROF_USER_NAME_FIRST, mPrUserProfile.getUserInfo().firstName)
        bundle.putString(EKeys.PROF_USER_NAME_LAST, mPrUserProfile.getUserInfo().lastName)
        AppCard.getNavigation().navigate(R.id._fragment_all_card_screen, bundle)

        checkUserType()
    }

    fun notActionByClick(view: View) {

        when (view.id) {
            infinity_card_action_take_photo.id -> {
                UtilLog.info(TAG, "Check permissions for take camera...")
                infinity_card_camera_gallery_permissions.hideScaleOtoC()

                if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED){
                    // this call action because have permission CAMERA
                    Toast.makeText(this, "CAMERA GRANTED 1", Toast.LENGTH_SHORT).show()
                    currentImageName = "avatar"
                    takePhotoIntent(this)
                } else {
                    // this call dialog request permission CAMERA
                    ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), CODE_PERMISSION_CAMERA)
                }
            }
            infinity_card_action_choose_photo.id -> {
                UtilLog.info(TAG, "Check permissions for read photo from disk...")
                infinity_card_camera_gallery_permissions.hideScaleOtoC()
                choosePhotoIntent()
            }
            profile_user_action_change_user_type.id -> {
                // this command check Bottom bar menu but 1 or 2 user id type

                if(type == EUserType.AGENT.typeId) type = EUserType.CLIENT.typeId else type = EUserType.AGENT.typeId
                AppCard.getBottomNavigationView().menu.clear()
                type?.let {
                    if (EUserType.compareUserType(it) == EUserType.AGENT) {
                        // user agent enable menu
                        UserTypeMenuAgent.menuUserAgentEnable().forEachIndexed { index, item ->
                            AppCard.getBottomNavigationView().menu.add(
                                Menu.NONE,
                                item.id,
                                index,
                                item.label,
                            ).icon = resources.getDrawable(item.icon, resources.newTheme())
                        }
                    } else {
                        UserTypeMenuAgent.menuUserAgentDisable().forEachIndexed { index, item ->
                            AppCard.getBottomNavigationView().menu.add(
                                Menu.NONE,
                                item.id,
                                index,
                                item.label
                            ).icon = resources.getDrawable(item.icon, resources.newTheme())
                        }
                        // user agent disable menu
                    }
                }
            }
            else -> UtilLog.info(TAG, resources.getString(R.string.not_another_action_call_this_function))
        }
    }

    fun getCurrentUser() = mPrUserProfile.getUserInfo()

    private fun checkUserType(){
        if(!isFinalMenu) {
            AppCard.getBottomNavigationView().menu.clear()
            if (EUserType.compareUserType(mPrUserProfile.getUserInfo().typeId) == EUserType.AGENT) {
                // user agent enable menu
                UserTypeMenuAgent.menuUserAgentEnable().forEachIndexed { index, item ->
                    AppCard.getBottomNavigationView().menu.add(
                        Menu.NONE,
                        item.id,
                        index,
                        item.label
                    ).icon = resources.getDrawable(item.icon, resources.newTheme())
                }
            } else {
                UserTypeMenuAgent.menuUserAgentDisable().forEachIndexed { index, item ->
                    AppCard.getBottomNavigationView().menu.add(
                        Menu.NONE,
                        item.id,
                        index,
                        item.label
                    ).icon = resources.getDrawable(item.icon, resources.newTheme())
                }
                // user agent disable menu
            }
            isFinalMenu = true
        }
    }

    /*function create file name*/
    private fun getImageFile(name: String): File {
        val storageDir = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        currentImagePath = File.createTempFile("$name", ".jpeg", storageDir)
//        currentImagePath = imageFile.absolutePath
        return currentImagePath!!
    }

    private fun takePhotoIntent(context: Context){ // name: String) {
//        try {
        CoroutineScope(Dispatchers.IO).launch {
            val takePhotoIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            val file: File = getImageFile(currentImageName)
            val uri = FileProvider.getUriForFile(context, "sumra.net.app.infinity_card_file_provider", file)
            takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
            withContext(Dispatchers.Main) {
                startActivityForResult(takePhotoIntent, CODE_TAKE_PHOTO)
            }
        }
//        }catch (ex: IllegalStateException){
//            Toast.makeText(this, "TAKE PHOTO FAILURE \n ${ex.message}", Toast.LENGTH_SHORT).show()
//        }
    }

    private fun choosePhotoIntent() {
        val getIntent = Intent(Intent.ACTION_GET_CONTENT)
        getIntent.type = "image/*"

        val pickIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        pickIntent.type = "image/*"

        val chooserIntent = Intent.createChooser(getIntent, "Select Image")
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(pickIntent))

        startActivityForResult(chooserIntent, CODE_READ_PHOTO)
    }

    private fun errorTakePhoto(){
        Toast.makeText(this, "TAKE PHOTO FAILURE", Toast.LENGTH_SHORT).show()
    }

    private fun errorChoosePhoto(){
        Toast.makeText(this, "CHOOSE PHOTO FAILURE", Toast.LENGTH_SHORT).show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            CODE_PERMISSION_CAMERA -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "CAMERA GRANTED 2", Toast.LENGTH_SHORT).show()
                    takePhotoIntent(this) //, currentImageName)
                } else {
                    // request for permission camera failed
                    Toast.makeText(this, "PERMISSION FOR CAMERA DENIED", Toast.LENGTH_SHORT).show()
                }
            }
            else -> {
                // request for permission failed
                Toast.makeText(this, "PERMISSION DENIED", Toast.LENGTH_SHORT).show()
            }
        }

//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun oauthTokenSuccess() {
//        TODO("Not yet implemented")
    }

    override fun oauthTokenFailure() {
//        TODO("Not yet implemented")
    }

    override fun fetchMeSuccess() {
        navigateToAllCards()
    }

    override fun fetchMeFailure(error: String?) {
        when(error){
            null -> activity_parent_view.showSnackBarError("Unknown error!")
            else -> activity_parent_view.showSnackBarError(error)
        }
    }

    override fun fetchTariffsSuccess() {
        TODO("Not yet implemented")
    }

    override fun fetchTariffsFailure(error: String?) {
        TODO("Not yet implemented")
    }

    override fun fetchPayDetailsSuccess() {
        TODO("Not yet implemented")
    }

    override fun fetchPayDetailsFailure(error: String?) {
        TODO("Not yet implemented")
    }

    override fun showProgress(state: Boolean) {
//        TODO("Not yet implemented")
    }

//    override fun dialogMessage(id: Int, message: String) {
//        when(id){
//            baseDataPickerDialogFragment.id -> {
//                val frg = supportFragmentManager.fragments
////                val cur = frg[0].childFragmentManager.fragments[0] as FrgProfileScreen
//
//                frg[1].childFragmentManager.fragments.forEach {
//                    val name = it::class.java.simpleName
//                    activity_parent_view.showSnackBarInfo(name)
//                }
//
//                frg[0].childFragmentManager.fragments.forEach {
//                    val name = it::class.java.simpleName
//                    activity_parent_view.showSnackBarInfo(name)
//                }
//
////                cur.updatePassportState(message)
//            }
//            else -> activity_parent_view.showSnackBarError("Unknown dialog error!")
//        }
//    }
}