package sumra.net.app.infinity_card.frgSignInAndSignUp.navigation.verifyCode

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import sumra.net.app.infinity_card.frgSignInAndSignUp.ConverterVerify
import sumra.net.app.infinity_card.frgSignInAndSignUp.NetVerify
import sumra.net.app.infinity_card.models.responses.VerifyCode

class RepositoryVerifyCode {

    suspend fun sendVerifyCode(token: String, code: String, userName: String) : Deferred<VerifyCode> {
        val resp = NetVerify.postVerifyCode(token, code, userName)
        return GlobalScope.async {
            ConverterVerify.convertRespCodeToModel(resp)
        }
    }

}