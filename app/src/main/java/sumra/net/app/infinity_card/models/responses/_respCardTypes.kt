package sumra.net.app.infinity_card.models.responses

import sumra.net.app.infinity_card.models._jsonCardModels

class _respCardTypes(val success: String, val data: _jsonCardModels?, val error: String?)