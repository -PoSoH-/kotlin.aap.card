package sumra.net.app.infinity_card.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.api.client.googleapis.util.Utils
import kotlinx.android.synthetic.main.frg_all_card_screen.*
import kotlinx.android.synthetic.main.frg_all_card_screen.infinity_card_check_container
import kotlinx.android.synthetic.main.frg_card_pay_screen.*
import kotlinx.android.synthetic.main.lay_balance_show.*
import kotlinx.android.synthetic.main.lay_infinity_card_active_show.*
import kotlinx.android.synthetic.main.lay_infinity_card_check_show.*
import kotlinx.android.synthetic.main.lay_infinity_card_check_show.infinity_card_action_number_cancel
import kotlinx.android.synthetic.main.lay_user_info_editable_show.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter
import sumra.net.app.infinity_card.AppCard
import sumra.net.app.infinity_card.R
import sumra.net.app.infinity_card.adapters.AdapterCardList
import sumra.net.app.infinity_card.enums.ECurrency
import sumra.net.app.infinity_card.models.Balance
import sumra.net.app.infinity_card.models.Card
import sumra.net.app.infinity_card.presenter.PAllCards
import sumra.net.app.infinity_card.enums.EKeys
import sumra.net.app.infinity_card.presenter.PrCardActivate
import sumra.net.app.infinity_card.presenter.PrCardCheck
import sumra.net.app.infinity_card.utils.UtilLog
import sumra.net.app.infinity_card.utils.extention.*
import sumra.net.app.infinity_card.views.IViewActivateCard
import sumra.net.app.infinity_card.views.IViewAllCards
import sumra.net.app.infinity_card.views.IViewCheckCard
import java.lang.StringBuilder
import kotlinx.android.synthetic.main.lay_infinity_card_check_show.infinity_card_check_container as infinity_card_check_container1

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FrgAllCardsScreen : MvpAppCompatFragment(R.layout.frg_all_card_screen)
    , IViewAllCards
    , IViewCheckCard
    , IViewActivateCard {

    private val TAG = this::class.java.simpleName

    @InjectPresenter
    lateinit var mPresenter : PAllCards
    @InjectPresenter
    lateinit var mPrCheckCard : PrCardCheck
    @InjectPresenter
    lateinit var mPrActivateCard : PrCardActivate
//    @InjectPresenter
//    lateinit var mPrActiveCard : PrCardActive

    private val mAdapterCard = AdapterCardList()
    private lateinit var mCards : ArrayList<Card>
    private lateinit var mUserNameFirst : String
    private lateinit var mUserNameLast  : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppCard.showBottomNavView()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val arg = arguments
        val data = arg!!.getParcelableArrayList<Card>(EKeys.PROF_CARD) // as ArrayList<Card>
        mCards = data!! // as ArrayList<Card>
        mUserNameFirst = arg.getString(EKeys.PROF_USER_NAME_FIRST, "")
        mUserNameLast  = arg.getString(EKeys.PROF_USER_NAME_LAST,  "")
        // out balances user of screen ....
        val balances = arg.getParcelableArrayList<Balance>(EKeys.PROF_BALANCE) // as ArrayList<Card>
        balances?.forEach {
            if(it.balance.isNullOrEmpty()){
                lbl_all_cards_balance_aa.text = ECurrency.configureResult(ECurrency.USA.compare, "0.0")
                lbl_all_cards_balance_bb.text = ECurrency.configureResult(ECurrency.EUR.compare, "0.0")
                lbl_all_cards_balance_cc.text = ECurrency.configureResult(ECurrency.GBP.compare, "0.0")
            }else {
                updateBalance(it.currency, it.balance)
            }
        }
        // init recycler view list cards
        list_all_cards.apply {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            adapter = mAdapterCard
        }

        if(data.size > 0){
            mPresenter.fetchCardTypes("")
        }

        addListener()
        addTextWatcherListener()

    }

    private fun showSubmenuButtons(){
        infinity_card_action_buttons_container.showEmpty()
    }

    private fun hideSubmenuButtons(){
        infinity_card_action_buttons_container.hideEmpty()
    }

    private fun updateBalance(currency: String, balance: String){
        when (currency) {
            ECurrency.USA.compare -> infinity_card_bay_card_balance.findViewById<TextView>(R.id.lbl_all_cards_balance_aa).text = ECurrency.configureResult(currency, balance)
            ECurrency.EUR.compare -> infinity_card_bay_card_balance.findViewById<TextView>(R.id.lbl_all_cards_balance_bb).text = ECurrency.configureResult(currency, balance)
            ECurrency.GBP.compare -> infinity_card_bay_card_balance.findViewById<TextView>(R.id.lbl_all_cards_balance_cc).text = ECurrency.configureResult(currency, balance)
        }
    }

    private fun addListener(){
        action_operation_after_card.apply {
            setOnClickListener {
                if(infinity_card_action_buttons_container.isVisible){
                    hideSubmenuButtons()
                }else{
                    showSubmenuButtons()
                }
            }
        }

        action_operation_of_check.apply {
            setOnClickListener {
                Toast.makeText(requireContext(), "press check card", Toast.LENGTH_SHORT).show()
                hideSubmenuButtons()
                hideButtonFloatAction()
                showDialogCardCheck()
                showDialogContainer()
//                mPrCheckCard.fetchCardCheck("_0000_", "kjskljflk jaskd jaslkdja slkja")
            }
        }

        action_operation_of_activate.apply {
            setOnClickListener {
                Toast.makeText(requireContext(), "press active card", Toast.LENGTH_SHORT).show()
                hideSubmenuButtons()
                hideButtonFloatAction()
                showDialogCardActivate()
                showDialogContainer()
            }
        }



        infinity_card_action_dialogs_container.apply {
            setOnClickListener {
                hideSubmenuButtons()
            }
        }

        infinity_card_action_number_check.apply {
            setOnClickListener {
                Toast.makeText(requireContext(), "press action check card", Toast.LENGTH_SHORT).show()
//                mPrCheckCard.fetchCardCheck(
//                    token = "_0000_",
//                    arg =   "${infinity_card_check_input_bloc_a.text}" +
//                            "${infinity_card_check_input_bloc_b.text}" +
//                            "${infinity_card_check_input_bloc_c.text}" +
//                            "${infinity_card_check_input_bloc_d.text}"
//                )
                hideKeyboard()
                hideDialogContainer()
                hideDialogCardCheck()
                showButtonFloatAction()
                removeEditField()
            }
        }

        infinity_card_action_number_cancel.apply {
            setOnClickListener {
                Toast.makeText(requireContext(), "press action check card", Toast.LENGTH_SHORT).show()
                hideKeyboard()
                hideDialogContainer()
                hideDialogCardCheck()
                showButtonFloatAction()
                removeEditField()
            }
        }

        infinity_card_action_number_activate.apply {
            setOnClickListener {
                Toast.makeText(requireContext(), "press action check card", Toast.LENGTH_SHORT).show()
                mPrActivateCard.fetchActivateCard(
                    token = "_0000_",
                    activationCode = "${infinity_card_activation_code_input.text}"
                )
                hideKeyboard()
                hideDialogContainer()
                hideDialogCardActivate()
                showButtonFloatAction()
                removeActivateCodeField()
            }
        }

        infinity_card_action_number_activate_cancel.apply {
            setOnClickListener {
                Toast.makeText(requireContext(), "press action check card", Toast.LENGTH_SHORT).show()
                hideKeyboard()
                hideDialogContainer()
                hideDialogCardActivate()
                showButtonFloatAction()
                removeActivateCodeField()
            }
        }

        infinity_card_action_buttons_container.apply {
            setOnClickListener {
                hideSubmenuButtons()
//                infinity_card_action_buttons_container.hideEmpty()
            }
        }

        infinity_card_active_container.apply {
            setOnClickListener {
                Toast.makeText(requireContext(), "press action active card", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun addTextWatcherListener(){
        //check number card...
        infinity_card_check_input_bloc_a.addTextChangedListener(object :TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
            override fun afterTextChanged(s: Editable?) {
                if(s!!.length == 4) infinity_card_check_input_bloc_b.requestFocus()
            }
        })
        infinity_card_check_input_bloc_b.addTextChangedListener(object :TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
            override fun afterTextChanged(s: Editable?) {
                if(s!!.length == 4) infinity_card_check_input_bloc_c.requestFocus()
                if(s.length == 0) infinity_card_check_input_bloc_a.requestFocus()

            }
        })
        infinity_card_check_input_bloc_c.addTextChangedListener(object :TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
            override fun afterTextChanged(s: Editable?) {
                if(s!!.length == 4) infinity_card_check_input_bloc_d.requestFocus()
                if(s.length == 0) infinity_card_check_input_bloc_b.requestFocus()
            }
        })
        infinity_card_check_input_bloc_d.addTextChangedListener(object :TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
            override fun afterTextChanged(s: Editable?) {
                if(s!!.length == 4) infinity_card_action_number_check.requestFocus()
            }
        })
        // activate card block
        infinity_card_activation_code_input.addTextChangedListener(object : TextWatcher {
            var beforeCount = 0
            var afterCount = 0
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                UtilLog.info(
                    TAG, "beforeTextChanged:\n " +
                            "- start symbol count -> ${start}\n " +  // count symbol of add or remove symbol
                            "- count symbol count -> ${count}\n " +  // count of remove symbol
                            "- after symbol count -> ${after}\n"
                )    // count of add symbol....
                beforeCount = start
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                if (beforeCount < s!!.length) {
                    UtilLog.info(
                        TAG,
                        "SYMBOL ADD... => lenght -> ${s.length} \n ============================================= \n"
                    )
                    val str = StringBuilder(s)
                    if (s.length == 9) {
                        str.insert(8, "-")
                        infinity_card_activation_code_input.inputText(str)
                        infinity_card_activation_code_input.setSelection(str.length)
                    }
                } else {
                    UtilLog.info(
                        TAG,
                        "SYMBOL REMOVE... => lenght -> ${s.length} \n ============================================= \n"
                    )
                    val str = StringBuilder(s)
                    if (s.length == 9) {
                        str.deleteCharAt(8)
                        infinity_card_activation_code_input.inputText(str)
                        infinity_card_activation_code_input.setSelection(str.length)
                    }
                }
            }
        })
    }

    private fun showDialogContainer(){
        infinity_card_action_dialogs_container.showScaleCtoO()
    }

    private fun hideDialogContainer(){
        infinity_card_action_dialogs_container.hideScaleOtoC()
    }

    private fun showDialogCardCheck(){
        infinity_card_check_container.showEmpty()
    }

    private fun hideDialogCardCheck(){
        infinity_card_check_container.hideEmpty()
    }

    private fun showDialogCardActivate(){
        infinity_card_active_container.showEmpty()
    }

    private fun hideDialogCardActivate(){
        infinity_card_active_container.hideEmpty()
    }

    private fun showButtonFloatAction(){
        action_operation_after_card.showEmpty()
    }

    private fun hideButtonFloatAction(){
        action_operation_after_card.hideEmpty()
    }

    private fun removeEditField(){
        infinity_card_check_input_bloc_a.inputText("")
        infinity_card_check_input_bloc_b.inputText("")
        infinity_card_check_input_bloc_c.inputText("")
        infinity_card_check_input_bloc_d.inputText("")
    }

    private fun removeActivateCodeField(){
        infinity_card_activation_code_input.inputText("")
    }

    override fun loadAllCardsSuccess() {
        mAdapterCard.bindCards(mCards,  mPresenter.getCardTypes(), mUserNameFirst, mUserNameLast)
    }

    override fun loadAllCardsFailure(error: String?) {
        when(error){
            null -> {
                Toast.makeText(requireContext(), "UNKNOWN ERROR!..", Toast.LENGTH_SHORT).show()}
            else -> {
                Toast.makeText(requireContext(), error, Toast.LENGTH_SHORT).show()}
        }
    }

    override fun loadAllCardTypesSuccess() {
        mAdapterCard.bindCards(mCards, mPresenter.getCardTypes(), mUserNameFirst, mUserNameLast)
    }

    override fun loadAllCardTypesFailure(error: String?) {
        when(error){
            null -> {
                Toast.makeText(requireContext(), "UNKNOWN ERROR!..", Toast.LENGTH_SHORT).show()}
            else -> {
                Toast.makeText(requireContext(), error, Toast.LENGTH_SHORT).show()}
        }
    }

    override fun fetchCardCheckSuccess() {
//        TODO("Not yet implemented")
    }

    override fun fetchCardCheckFailure() {
//        TODO("Not yet implemented")
    }

    override fun fetchCardActivateSuccess() {
//        TODO("Not yet implemented")
    }

    override fun fetchCardActivateFailure(error: String?) {
//        TODO("Not yet implemented")
    }

    override fun showProgress(state: Boolean) {
        when(state){
            true -> AppCard.showProgress() // Toast.makeText(requireContext(), "PROGRESS SHOW", Toast.LENGTH_SHORT).show()
            else -> AppCard.hideProgress() // Toast.makeText(requireContext(), "PROGRESS HIDE", Toast.LENGTH_SHORT).show()
        }

    }
}