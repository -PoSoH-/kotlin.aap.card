package sumra.net.app.infinity_card.enums

import sumra.net.app.infinity_card.R

enum class ECardType(val id: Long, val type: Int) {
    INFINITY_RANGE_NEON(id = 1L, type = R.drawable.bg_infinity_range_neon),
    INFINITY_RANGE_STRATA(id = 2L, type = R.drawable.bg_infinity_range_strata),
    INFINITY_RANGE_ULTIMA(id = 3L, type = R.drawable.bg_infinity_range_ultima),
    INFINITY_SERIES_SILVER(id = 4L, type = R.drawable.bg_infinity_serires_silver),
    INFINITY_SERIES_GOLD(id = 5L, type = R.drawable.bg_infinity_series_gold),
    INFINITY_SERIES_PLATINUM(id = 6L, type = R.drawable.bg_infinity_serires_platinum),
    INFINITY_SUBLIME_ADVANCED(id = 7L, type = R.drawable.bg_infinity_sublime_advanced),
    INFINITY_SUBLIME_PREMIER(id = 8L, type = R.drawable.bg_infinity_sublime_premier),
    INFINITY_SUBLIME_SUPREME(id = 9L, type = R.drawable.bg_infinity_serires_platinum),
    INFINITY_CARD_UNKNOWN(id = -1L, type = R.drawable.bg_infinity_card_unknown);

    companion object {
        fun compareType(id: Long): Int {
            return when (id) {
                INFINITY_RANGE_NEON.id -> INFINITY_RANGE_NEON.type
                INFINITY_RANGE_STRATA.id -> INFINITY_RANGE_STRATA.type
                INFINITY_RANGE_ULTIMA.id -> INFINITY_RANGE_ULTIMA.type
                INFINITY_SERIES_SILVER.id -> INFINITY_SERIES_SILVER.type
                INFINITY_SERIES_GOLD.id -> INFINITY_SERIES_GOLD.type
                INFINITY_SERIES_PLATINUM.id -> INFINITY_SERIES_PLATINUM.type
                INFINITY_SUBLIME_ADVANCED.id -> INFINITY_SUBLIME_ADVANCED.type
                INFINITY_SUBLIME_PREMIER.id -> INFINITY_SUBLIME_PREMIER.type
                INFINITY_SUBLIME_SUPREME.id -> INFINITY_SUBLIME_SUPREME.type
                else -> INFINITY_CARD_UNKNOWN.type
            }
        }
    }
}