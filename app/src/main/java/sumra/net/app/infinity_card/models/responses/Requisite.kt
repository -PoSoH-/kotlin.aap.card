package sumra.net.app.infinity_card.models.responses


import com.google.gson.annotations.SerializedName

data class Requisite(
    @SerializedName("country")
    val country: String,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("pivot")
    val pivot: Pivot,
    @SerializedName("updated_at")
    val updatedAt: String
)