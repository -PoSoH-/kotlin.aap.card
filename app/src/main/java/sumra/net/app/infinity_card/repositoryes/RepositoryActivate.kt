package sumra.net.app.infinity_card.repositoryes

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import sumra.net.app.infinity_card.core.NetCards

class RepositoryActivate {
    fun fetchActivateCardNet(token: String, arg: String): Deferred<String>{
        val resp = NetCards.getActivateCard(token, arg)
        return GlobalScope.async {
            "---"
        }
    }
}