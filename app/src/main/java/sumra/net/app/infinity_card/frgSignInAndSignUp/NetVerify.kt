package sumra.net.app.infinity_card.frgSignInAndSignUp

import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.Response
import org.apache.http.HttpException
import sumra.net.app.infinity_card.BuildConfig
import sumra.net.app.infinity_card.core.EHost
import sumra.net.app.infinity_card.core.NetworkCore
import sumra.net.app.infinity_card.core.OkHttpClient

object NetVerify {

    private val localHostUrl = "http://192.168.188.137:8095/v1/infinity"   //"https://api.sumra.net/files/1/v1/files"
//private val localHostUrl = "https://api.sumra.net/infinity/1/v1/infinity/users/"

    fun postVerifyPhone(token: String, phone: String):String {
        val host = BuildConfig.SUMRA_API_INITY_URL.toHttpUrlOrNull()!!
        val httpUrl = HttpUrl.Builder().apply {
            scheme(EHost.HTTPS.value)
            host(host.host)
            addPathSegment(BuildConfig.SUMRA_API_INITY_INITY)
            addPathSegment(BuildConfig.SUMRA_API_INITY_NUM)
            addPathSegment(BuildConfig.SUMRA_API_INITY_VER)
            addPathSegment("infinity")  // /v1/infinity/reference/card-types
//            addPathSegment("reference")
//            addPathSegment("card-types")
            addPathSegment("cardtypes")
        }.build()

        try{
//            val response = OkHttpClient.builder()
//                .newCall(NetworkCore.GET_RequestContentByToken(httpUrl.toString(), token)).execute()
            val response = OkHttpClient.builder()
                .newCall(NetworkCore.GET_RequestContentByUserId("$localHostUrl/auth-phone")).execute()
            if(response.isSuccessful){
                return NetworkCore.resultSuccess(response)
            }else{
                return NetworkCore.resultFailure(response)
            }
        } catch (e : HttpException) {
            return NetworkCore.resultFailure(e as Response)
        }
    }

    fun postVerifyCode(code: String, phone: String, userName: String):String {
        val host = BuildConfig.SUMRA_API_INITY_URL.toHttpUrlOrNull()!!
        val httpUrl = HttpUrl.Builder().apply {
            scheme(EHost.HTTPS.value)
            host(host.host)
            addPathSegment(BuildConfig.SUMRA_API_INITY_INITY)
            addPathSegment(BuildConfig.SUMRA_API_INITY_NUM)
            addPathSegment(BuildConfig.SUMRA_API_INITY_VER)
            addPathSegment("infinity")  // /v1/infinity/reference/card-types
            addPathSegment("cardtypes")
        }.build()

        try{
//            val response = OkHttpClient.builder()
//                .newCall(NetworkCore.GET_RequestContentByToken(httpUrl.toString(), token)).execute()
            val response = OkHttpClient.builder()
                .newCall(NetworkCore.POST_RequestContentByUserID(
                    url = "$localHostUrl/refresh",
                    token = null,
                    dataBody = """{"code": ${code}, "phone": ${phone}, "user_name": ${userName}}""")).execute()
            if(response.isSuccessful){
                return NetworkCore.resultSuccess(response)
            }else{
                return NetworkCore.resultFailure(response)
            }
        } catch (e : HttpException) {
            return NetworkCore.resultFailure(e as Response)
        }
    }

    fun postAuthSignUp(body: String):String {
        val host = BuildConfig.SUMRA_API_INITY_URL.toHttpUrlOrNull()!!
        val httpUrl = HttpUrl.Builder().apply {
            scheme(EHost.HTTPS.value)
            host(host.host)
            addPathSegment(BuildConfig.SUMRA_API_INITY_INITY)
            addPathSegment(BuildConfig.SUMRA_API_INITY_NUM)
            addPathSegment(BuildConfig.SUMRA_API_INITY_VER)
            addPathSegment("infinity")  // /v1/infinity/reference/card-types
            addPathSegment("cardtypes")
        }.build()

        try{
//            val response = OkHttpClient.builder()
//                .newCall(NetworkCore.GET_RequestContentByToken(httpUrl.toString(), token)).execute()
            val response = OkHttpClient.builder()
                .newCall(NetworkCore.POST_RequestContentByUserID(
                    url = "$localHostUrl/sign-up",
                    token = "",
                    dataBody = body)).execute()
            if(response.isSuccessful){
                return NetworkCore.resultSuccess(response)
            }else{
                return NetworkCore.resultFailure(response)
            }
        } catch (e : HttpException) {
            return NetworkCore.resultFailure(e as Response)
        }
    }

    fun postAuthSignIn(body: String):String {
        val host = BuildConfig.SUMRA_API_INITY_URL.toHttpUrlOrNull()!!
        val httpUrl = HttpUrl.Builder().apply {
            scheme(EHost.HTTPS.value)
            host(host.host)
            addPathSegment(BuildConfig.SUMRA_API_INITY_INITY)
            addPathSegment(BuildConfig.SUMRA_API_INITY_NUM)
            addPathSegment(BuildConfig.SUMRA_API_INITY_VER)
            addPathSegment("infinity")  // /v1/infinity/reference/card-types
            addPathSegment("cardtypes")
        }.build()

        try{
//            val response = OkHttpClient.builder()
//                .newCall(NetworkCore.GET_RequestContentByToken(httpUrl.toString(), token)).execute()
            val response = OkHttpClient.builder()
                .newCall(NetworkCore.POST_RequestContentByUserID(
                    url = "$localHostUrl/sign-in",
                    token = "",
                    dataBody = body)).execute()
            if(response.isSuccessful){
                return NetworkCore.resultSuccess(response)
            }else{
                return NetworkCore.resultFailure(response)
            }
        } catch (e : HttpException) {
            return NetworkCore.resultFailure(e as Response)
        }
    }

    fun postAuthSignOut(token: String):String {
        val host = BuildConfig.SUMRA_API_INITY_URL.toHttpUrlOrNull()!!
        val httpUrl = HttpUrl.Builder().apply {
            scheme(EHost.HTTPS.value)
            host(host.host)
            addPathSegment(BuildConfig.SUMRA_API_INITY_INITY)
            addPathSegment(BuildConfig.SUMRA_API_INITY_NUM)
            addPathSegment(BuildConfig.SUMRA_API_INITY_VER)
            addPathSegment("infinity")  // /v1/infinity/reference/card-types
            addPathSegment("cardtypes")
        }.build()

        try{
            val response = OkHttpClient.builder()
                .newCall(NetworkCore.POST_RequestContentByUserID(
                    url = "$localHostUrl/sign-out",
                    token = token,
                    dataBody = null)).execute()
            if(response.isSuccessful){
                return NetworkCore.resultSuccess(response)
            }else{
                return NetworkCore.resultFailure(response)
            }
        } catch (e : HttpException) {
            return NetworkCore.resultFailure(e as Response)
        }
    }

    fun postRefreshToken(token: String, refreshToken: String):String {
        val host = BuildConfig.SUMRA_API_INITY_URL.toHttpUrlOrNull()!!
        val httpUrl = HttpUrl.Builder().apply {
            scheme(EHost.HTTPS.value)
            host(host.host)
            addPathSegment(BuildConfig.SUMRA_API_INITY_INITY)
            addPathSegment(BuildConfig.SUMRA_API_INITY_NUM)
            addPathSegment(BuildConfig.SUMRA_API_INITY_VER)
            addPathSegment("infinity")  // /v1/infinity/reference/card-types
            addPathSegment("cardtypes")
        }.build()

        try{
//            val response = OkHttpClient.builder()
//                .newCall(NetworkCore.GET_RequestContentByToken(httpUrl.toString(), token)).execute()
            val response = OkHttpClient.builder()
                .newCall(NetworkCore.POST_RequestContentByUserID(
                    url = "$localHostUrl/refresh",
                    token = token,
                    dataBody = refreshToken)).execute()
            if(response.isSuccessful){
                return NetworkCore.resultSuccess(response)
            }else{
                return NetworkCore.resultFailure(response)
            }
        } catch (e : HttpException) {
            return NetworkCore.resultFailure(e as Response)
        }
    }


}