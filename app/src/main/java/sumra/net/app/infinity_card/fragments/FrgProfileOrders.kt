package sumra.net.app.infinity_card.fragments

import android.os.Bundle
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.frg_profile_contacts.*
import kotlinx.android.synthetic.main.frg_profile_order_item.*
import kotlinx.android.synthetic.main.frg_profile_orders.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter
import sumra.net.app.infinity_card.AppCard
import sumra.net.app.infinity_card.R
import sumra.net.app.infinity_card.adapters.AdapterOrders
import sumra.net.app.infinity_card.enums.EKeys
import sumra.net.app.infinity_card.interfaces.OnOrderItemClickListener
import sumra.net.app.infinity_card.presenter.PrOrderList
import sumra.net.app.infinity_card.utils.UtilLog
import sumra.net.app.infinity_card.views.IViewOrders
import sumra.net.app.infinity_card.widgets.CustomSnackBar.CustomSnackBar

class FrgProfileOrders:MvpAppCompatFragment(R.layout.frg_profile_orders), IViewOrders {

    private val TAG = "FragmentProfileOrders ->"

    @InjectPresenter
    lateinit var mPrOrders: PrOrderList

    private val mAdapterOrders = AdapterOrders(
        object : OnOrderItemClickListener{
            override fun onOrderItemClick(orderId: Long) {
                UtilLog.info(TAG, "Order selected id ${orderId}", rootProfileOrders)
                val data = Bundle()
                data.putLong(EKeys.PROFILE_ORDER_ITEM_ID, orderId)
                Navigation
                    .findNavController(requireActivity(), R.id.nav_host_profile)
                    .navigate(R.id.action_frg_order_items_to_frg_order_current, data)
            }
        }
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppCard.hideBottomNavView()
        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true /* enabled by default */) {
                override fun handleOnBackPressed() {
                    UtilLog.error(TAG, "BackPressed -> Click")
                    onBackPressed()
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewListOrders.apply {
            adapter = mAdapterOrders
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        }

        mPrOrders.fetchOrderLists()
    }

    override fun fetchOrdersSuccess() {
        UtilLog.info(TAG, "Fetch orders success wit count - ${mPrOrders.getOrders().size}")
        mAdapterOrders.bind(mPrOrders.getOrders())
    }

    override fun fetchOrdersFailure(error: String?) {
        when(error){
            null -> showErrorOfScreen(resources.getString(R.string.error_network_dont_know))
            else -> showErrorOfScreen(error)
        }
    }

    override fun showProgress(state: Boolean) {
        when(state){
            true -> AppCard.showProgress()
            else -> AppCard.hideProgress()
        }
    }

    private fun showErrorOfScreen(message: String){
        CustomSnackBar.make(
            view = rootProfileOrders,
            message = message,
            duration = Snackbar.LENGTH_SHORT,
            listener = null,
            action_lable = "",
            typeMessage = CustomSnackBar.EMessageType.ERROR,
            header = resources.getString(R.string.error_infinity_card_message_in_header)
        )?.show()
    }

    private fun onBackPressed(){
        UtilLog.info(TAG
            , "Button back click..."
            , rootProfileOrders)
        AppCard.showBottomNavView()
        Navigation.findNavController(
            requireActivity(),
            R.id.nav_host_profile
        ).popBackStack()
    }
}