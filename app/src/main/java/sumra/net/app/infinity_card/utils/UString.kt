package sumra.net.app.infinity_card.utils

import sumra.net.app.infinity_card.models.Balance
import java.math.RoundingMode
import java.text.DateFormat
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

object UString {

    fun setupBalanceShow(balance: String?) : String{
        var info = "0.0"
        balance?.let { info = it }
        val resultat = DecimalFormat("###,###,###,###,###.##").apply {
            roundingMode = RoundingMode.CEILING
        }.format(info.toDouble())
        return resultat.replace("\u00A0", "`")
    }

    fun setupOrderPriceShow(balance: String?) : String{
        var info = "0.0"
        balance?.let { info = it }
        val resultat = DecimalFormat("###,###,###,###,###.##").apply {
            roundingMode = RoundingMode.CEILING
        }.format(info.toDouble())
        return resultat.replace(",", "`")
    }

    fun setupDateByTime(value: String?) : String{
        var temp = value
        if(temp.isNullOrEmpty()){
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = System.currentTimeMillis()
            temp = calendar.time.toString()
        }
        return setupDateByTimeInner(temp!!)
    }

    private fun setupDateByTimeInner(value: String) : String{
        val originalFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)  //2020-10-20 11:37:27
        val targetFormat: DateFormat = SimpleDateFormat("MM/yyyy")
        val date = originalFormat.parse(value)
        val formattedDate = targetFormat.format(date)
        return formattedDate
    }

    fun setupDateNotTime(dateString: String) : String{
        val originalFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)  //2020-10-20 11:37:27
        val targetFormat: DateFormat = SimpleDateFormat("MM/yyyy")
        val date = originalFormat.parse(dateString)
        val formattedDate = targetFormat.format(date)
        return formattedDate
    }

    fun setupDateOrderState(dateString: String) : String{
        val originalFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)  //2020-10-20 11:37:27
        val targetFormat: DateFormat = SimpleDateFormat("MM/dd/yyyy HH:mm")
        val date = originalFormat.parse(dateString)
        val formattedDate = targetFormat.format(date)
        return formattedDate
    }

    fun isEmptyBalance(balance: String) = if(balance.toDouble() > 0.0) true else false

    fun getBalance(balance: Balance) = if(balance.balance == null) 0.0 else balance.balance.toDouble()
}