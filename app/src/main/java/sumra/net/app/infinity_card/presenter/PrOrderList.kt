package sumra.net.app.infinity_card.presenter

import kotlinx.coroutines.*
import moxy.MvpPresenter
import sumra.net.app.infinity_card.models.responses.OrderItem
import sumra.net.app.infinity_card.models.responses._jsonRespOrderItems
import sumra.net.app.infinity_card.views.IViewOrderItem
import sumra.net.app.infinity_card.views.IViewOrders

class PrOrderList: MvpPresenter<IViewOrders>() {

    private val mOrders = mutableListOf<OrderItem>()
//    private val repository = RepositoryOrderItems()

    fun fetchOrderLists(){
        viewState.showProgress(true)
        CoroutineScope(Dispatchers.IO).launch {
            val resp = _jsonRespOrderItems(success = true, error = null, data = mutableListOf(
                         OrderItem(id = 12487545,createAt = "12-05-2020",updateAt = "12-06-2020",totalPrice = "2144",currency = "gbp",status = 4)
                        ,OrderItem(id = 56546546,createAt = "12-02-2020",updateAt = "12-02-2020",totalPrice = "6547",currency = "eur",status = 1)
                        ,OrderItem(id = 24356765,createAt = "12-07-2020",updateAt = "12-07-2020",totalPrice = "9547",currency = "usd",status = 2)
                        ,OrderItem(id = 97874878,createAt = "12-03-2020",updateAt = "12-04-2020",totalPrice = "1245",currency = "usd",status = 3)
                        ,OrderItem(id = 86784935,createAt = "12-06-2020",updateAt = "12-07-2020",totalPrice = "9547",currency = "gbp",status = 4)
                        ,OrderItem(id = 49895464,createAt = "12-09-2020",updateAt = "12-09-2020",totalPrice = "2157",currency = "eur",status = 2)
            ))

            delay(2_500L)

            if(resp.success){
                mOrders.clear()
                mOrders.addAll(resp.data as MutableList<OrderItem>)
                withContext(Dispatchers.Main){
                    viewState.fetchOrdersSuccess()
                }
            }else{
                withContext(Dispatchers.Main){
                    viewState.fetchOrdersFailure(resp.error)
                }
            }
            viewState.showProgress(false)
        }
    }

    fun getOrders() = mOrders

}