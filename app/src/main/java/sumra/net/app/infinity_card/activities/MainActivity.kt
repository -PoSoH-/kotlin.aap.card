//package sumra.net.app.infinity_card.activities
//
//import android.annotation.TargetApi
//import android.app.PendingIntent
//import android.content.Intent
//import android.content.SharedPreferences
//import android.os.Build
//import android.os.Bundle
//import android.text.Editable
//import android.text.TextUtils
//import android.text.TextWatcher
//import android.util.Log
//import android.view.View
//import android.widget.Toast
//import androidx.annotation.ColorRes
//import androidx.annotation.Nullable
//import androidx.browser.customtabs.CustomTabsIntent
//import com.google.android.material.appbar.AppBarLayout
//import com.google.android.material.floatingactionbutton.FloatingActionButton
//import com.google.android.material.snackbar.Snackbar
//import kotlinx.coroutines.CoroutineScope
//import kotlinx.coroutines.Dispatchers
//import kotlinx.coroutines.launch
//import kotlinx.coroutines.withContext
//import moxy.MvpAppCompatActivity
//import net.openid.appauth.*
//import net.openid.appauth.AuthorizationServiceConfiguration
//import net.openid.appauth.browser.AnyBrowserMatcher
//import net.openid.appauth.browser.BrowserMatcher
//import sumra.net.app.infinity_card.R
//import sumra.net.app.infinity_card.wsoi.AuthStateManager
//import sumra.net.app.infinity_card.wsoi.ConfigManager
//import java.util.concurrent.CountDownLatch
//import java.util.concurrent.ExecutorService
//import java.util.concurrent.atomic.AtomicBoolean
//import java.util.concurrent.atomic.AtomicReference
//
//
//class MainActivity : MvpAppCompatActivity() {
//
//    private val TAG = this::class.java.simpleName
//
//    private val EXTRA_FAILED = "failed"
//    private val RC_AUTH = 100
//
//    private val mClientId = AtomicReference<String>()
//    private val mAuthRequest = AtomicReference<AuthorizationRequest>()
//    private val mAuthIntent: AtomicReference<CustomTabsIntent> = AtomicReference<CustomTabsIntent>()
//
//    private var mAuthStateManager : AuthStateManager? = null
//    private var mConfiguration: ConfigManager? = null
//    private var mAuthService: AuthorizationService? = null
//
//    private var mAuthIntentLatch = CountDownLatch(1)
//    private val mExecutor: ExecutorService? = null
//    private val mUsePendingIntents = false
//
//    private val mBrowserMatcher: BrowserMatcher = AnyBrowserMatcher.INSTANCE
//
//    private var accessToken: String? = null
//    private var idToken: String? = null
//    private var state: String? = null
//
//    private var mPrefs: SharedPreferences? = null
//
//
//    private final val redirect_uri = "sumra.infinity://oauth";
//    private final val authorization_scope = "openid, profile";
//    private final val authorization_endpoint = "https://apim.sumra.net/oauth2/authorize";
//    private final val end_session_endpoint   = "https://apim.sumra.net/oidc/logout";
//
//    private final val token_endpoint  = "https://apim.sumra.net/oauth2/token";
//    private final val revoke_endpoint = "https://apim.sumra.net/oauth2/revoke";
//
//    private final val userinfo_endpoint = "https://apim.sumra.net/oauth2/userinfo";
//    private final val https_required = true;
//    private final val uri_shem = "sumra.infinity";
//
//    val user : String = "p.s.g.sirogk"
//    val password : String = "5BUdeKe8sn9FWYgL"
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)
//        setSupportActionBar(findViewById(R.id.toolbar))
//
//        mAuthStateManager = AuthStateManager.getInstance(this)
//        mConfiguration = ConfigManager.getInstance(this)
//
//        findViewById<FloatingActionButton>(R.id.fab).apply {
//            visibility = View.GONE
//            setOnClickListener { view ->
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                    .setAction("Action", null).show()
//            }
//        }
//
//        findViewById<AppBarLayout>(R.id.app_bar_activity).visibility = View.GONE
//
//        if (mAuthStateManager!!.currentState.isAuthorized() && !mConfiguration!!.hasConfigurationChanged()
//        ) {
//            Log.i(TAG, "User is already authenticated, proceeding to token activity")
//            // todo start all card fragment
////            startActivity(Intent(this, TokenActivity::class.java))
////            finish()
//            return
//        }
//
////        configureBrowserSelector()
//        if (mConfiguration!!.hasConfigurationChanged()) {
//            // discard any existing authorization state due to the change of configuration
//            Log.i(TAG, "Configuration change detected, discarding old state")
//            mAuthStateManager!!.replaceState(AuthState())
//            mConfiguration!!.acceptConfiguration()
//        }
//
//        if (intent.getBooleanExtra(EXTRA_FAILED, false)) {
//            displayAuthCancelled()
//        }
//
//        displayLoading("Initializing")
//
//        CoroutineScope(Dispatchers.IO).launch {
//            initializeAppAuth()
//        }
//
////        checkAuthState()
////
////        val resp = AuthorizationResponse.fromIntent(intent)
////        val ex = AuthorizationException.fromIntent(intent)
////        if (resp != null) {
////            UtilLog.error(TAG, resp.jsonSerializeString())
////        } else {
////            UtilLog.error(TAG, ex!!.localizedMessage)
////        }
//
//    }
//
//    private fun doAuth() {
//        try {
//            mAuthIntentLatch.await()
//        } catch (ex: InterruptedException) {
//            Log.w(TAG, "Interrupted while waiting for auth intent")
//        }
//        if (mUsePendingIntents) {
//            val completionIntent = Intent(this, null)
//            val cancelIntent = Intent(this, null)
//            cancelIntent.putExtra(EXTRA_FAILED, true)
//            cancelIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
//            mAuthService?.performAuthorizationRequest(
//                mAuthRequest.get(),
//                PendingIntent.getActivity(this, 0, completionIntent, 0),
//                PendingIntent.getActivity(this, 0, cancelIntent, 0),
//                mAuthIntent.get()
//            )
//        } else {
//            val intent: Intent = mAuthService?.getAuthorizationRequestIntent(
//                mAuthRequest.get(),
//                mAuthIntent.get()
//            )!!
//            startActivityForResult(intent, RC_AUTH)
//        }
//    }
//
//    override fun onDestroy() {
//        super.onDestroy()
//        if (mAuthService != null) {
//            mAuthService?.dispose();
//        }
//    }
//
//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        displayAuthOptions()
//        if (resultCode == RESULT_CANCELED) {
//            displayAuthCancelled()
//        } else {
//            // todo start to all cards
////            val intent = Intent(this, TokenActivity::class.java)
////            intent.putExtras(data.extras!!)
////            startActivity(intent)
//        }
//    }
//
//    fun startAuth() {
//        displayLoading("Making authorization request")
////        mUsePendingIntents = (findViewById<View>(R.id.pending_intents_checkbox) as CheckBox).isChecked
//        CoroutineScope(Dispatchers.IO).launch{
//            doAuth()
//        }
//    }
//
//    private suspend fun initializeAppAuth() {
//        Log.i(TAG, "Initializing AppAuth")
//        recreateAuthorizationService()
//        if (mAuthStateManager?.currentState?.getAuthorizationServiceConfiguration() != null) {
//            // configuration is already created, skip to client initialization
//            Log.i(TAG, "auth config already established")
//            initializeClient()
//            return
//        }
//
//        // if we are not using discovery, build the authorization service configuration directly
//        // from the static configuration values.
//        if (mConfiguration?.authEndpointUri == null) {
//            Log.i(TAG, "Creating auth config from res/raw/auth_config.json")
//            val config = AuthorizationServiceConfiguration(
//                mConfiguration?.authEndpointUri!!,
//                mConfiguration?.tokenEndpointUri!!
//            )
//            mAuthStateManager?.replaceState(AuthState(config))
//            initializeClient()
//            return
//        }
//
//        // WrongThread inference is incorrect for lambdas
//        // noinspection WrongThread
//        runOnUiThread { displayLoading("Retrieving discovery document") }
//        Log.i(TAG, "Retrieving OpenID discovery doc")
//        AuthorizationServiceConfiguration.fetchFromUrl(
//            mConfiguration?.authEndpointUri!!,
//            object : AuthorizationServiceConfiguration.RetrieveConfigurationCallback {
//                override fun onFetchConfigurationCompleted(
//                    serviceConfiguration: AuthorizationServiceConfiguration?,
//                    ex: AuthorizationException?
//                ) {
//                    if (serviceConfiguration == null) {
//                        Log.i(TAG, "Failed to retrieve discovery document", ex)
//                        displayError("Failed to retrieve discovery document: " + ex?.message, true)
//                    }
//                    Log.i(TAG, "Discovery document retrieved")
//                    mAuthStateManager?.replaceState(AuthState(serviceConfiguration!!))
//                    CoroutineScope(Dispatchers.IO).launch {
//                        initializeClient()
//                    }
//                }
//            },
//            mConfiguration?.connectionBuilder!!
//        )
//    }
//
////    private fun handleConfigurationRetrievalResult(
////        config: AuthorizationServiceConfiguration?,
////        ex: AuthorizationException
////    ) : AuthorizationServiceConfiguration.RetrieveConfigurationCallback {
////        if (config == null) {
////            Log.i(TAG, "Failed to retrieve discovery document", ex)
////            displayError("Failed to retrieve discovery document: " + ex.message, true)
////            return config.fe
////        }
////        Log.i(TAG, "Discovery document retrieved")
////        mAuthStateManager?.replaceState(AuthState(config))
////        CoroutineScope(Dispatchers.IO).launch {
////           initializeClient()
////        }
////    }
//
//    private suspend fun initializeClient() {
//        if (mConfiguration!!.getClientId() != null) {
//            Log.i(TAG, "Using static client ID: " + mConfiguration!!.getClientId())
//            // use a statically configured client ID
//            mClientId.set(mConfiguration!!.getClientId())
//            withContext(Dispatchers.Main) {
//                initializeAuthRequest()
//            }
//            return
//        }
//        val lastResponse: RegistrationResponse = mAuthStateManager?.currentState?.lastRegistrationResponse!!
//        if (lastResponse != null) {
//            Log.i(TAG, "Using dynamic client ID: " + lastResponse.clientId)
//            // already dynamically registered a client ID
//            mClientId.set(lastResponse.clientId)
//            withContext(Dispatchers.Main) {
//                initializeAuthRequest()
//            }
//            return
//        }
//
//        withContext(Dispatchers.Main) {
//            displayLoading("Dynamically registering client")
//        }
//        Log.i(TAG, "Dynamically registering client")
//        val registrationRequest = RegistrationRequest.Builder(
//            mAuthStateManager?.currentState?.authorizationServiceConfiguration!!,
//            listOf(mConfiguration!!.getRedirectUri())
//        )
//            .setTokenEndpointAuthenticationMethod(ClientSecretBasic.NAME)
//            .build()
//        mAuthService!!.performRegistrationRequest(
//            registrationRequest,
////            handleRegistrationResponse()
//            object : AuthorizationService.RegistrationResponseCallback {
//                override fun onRegistrationRequestCompleted(
//                    response: RegistrationResponse?,
//                    ex: AuthorizationException?
//                ) {
//                    mAuthStateManager?.updateAfterRegistration(response, ex)
//                    if (response == null) {
//                        Log.i(TAG, "Failed to dynamically register client", ex)
////                        displayErrorLater("Failed to register client: " + ex?.message, true)
//                        return
//                    }
//                    Log.i(TAG, "Dynamically registered client: " + response.clientId)
//                    mClientId.set(response.clientId)
//                    initializeAuthRequest()
//                }
//
//            }
//        )
//    }
//
//
//    private fun recreateAuthorizationService() {
//        if (mAuthService != null) {
//            Log.i(TAG, "Discarding existing AuthService instance")
//            mAuthService!!.dispose()
//        }
//        mAuthService = createAuthorizationService()
//        mAuthRequest.set(null)
//        mAuthIntent.set(null)
//    }
//
//    private fun createAuthorizationService(): AuthorizationService? {
//        Log.i(TAG, "Creating authorization service")
//        val builder = AppAuthConfiguration.Builder()
//        builder.setBrowserMatcher(mBrowserMatcher)
//        builder.setConnectionBuilder(mConfiguration!!.connectionBuilder)
//        return AuthorizationService(this, builder.build())
//    }
//
//    private fun displayLoading(loadingMessage: String) {
////        findViewById<View>(R.id.loading_container).visibility = View.VISIBLE
////        findViewById<View>(R.id.auth_container).visibility = View.GONE
////        findViewById<View>(R.id.error_container).visibility = View.GONE
////        (findViewById<View>(R.id.loading_description) as TextView).text = loadingMessage
//    }
//
//    private fun displayError(error: String, recoverable: Boolean) {
////        findViewById<View>(R.id.error_container).visibility = View.VISIBLE
////        findViewById<View>(R.id.loading_container).visibility = View.GONE
////        findViewById<View>(R.id.auth_container).visibility = View.GONE
////        (findViewById<View>(R.id.error_description) as TextView).text = error
////        findViewById<View>(R.id.retry).visibility = if (recoverable) View.VISIBLE else View.GONE
//    }
//
//    private suspend fun displayErrorLater(error: String, recoverable: Boolean) {
//        withContext(Dispatchers.Main) {
//            displayError(error, recoverable)
//        }
//    }
//
//    private fun initializeAuthRequest() {
//        getLoginHint()?.let { createAuthRequest(it) }
//        warmUpBrowser()
//        displayAuthOptions()
//    }
//
//    private fun displayAuthOptions() {
////        findViewById<View>(R.id.auth_container).visibility = View.VISIBLE
////        findViewById<View>(R.id.loading_container).visibility = View.GONE
////        findViewById<View>(R.id.error_container).visibility = View.GONE
//        val state: AuthState = mAuthStateManager?.currentState!!
//        val config = state.authorizationServiceConfiguration
//        var authEndpointStr: String
//        authEndpointStr = if (config!!.discoveryDoc != null) {
//            "Discovered auth endpoint: \n"
//        } else {
//            "Static auth endpoint: \n"
//        }
//        authEndpointStr += config.authorizationEndpoint
////        (findViewById<View>(R.id.auth_endpoint) as TextView).text = authEndpointStr
//        var clientIdStr: String
//        clientIdStr = if (state.lastRegistrationResponse != null) {
//            "Dynamic client ID: \n"
//        } else {
//            "Static client ID: \n"
//        }
//        clientIdStr += mClientId
////        (findViewById<View>(R.id.client_id) as TextView).text = clientIdStr
//    }
//
//    private fun displayAuthCancelled() {
////        Snackbar.make(
////            findViewById(R.id.coordinator),
////            "Authorization canceled",
////            Snackbar.LENGTH_SHORT
////        )
////            .show()
//        Toast.makeText(this, "Authorization canceled", Toast.LENGTH_LONG).show()
//    }
//
//    private fun warmUpBrowser() {
//        mAuthIntentLatch = CountDownLatch(1)
//        mExecutor!!.execute {
//            Log.i(TAG, "Warming up browser instance for auth request")
//            val intentBuilder =
//                mAuthService!!.createCustomTabsIntentBuilder(mAuthRequest.get().toUri())
//            intentBuilder.setToolbarColor(getColorCompat(R.color.colGradStart))
//            mAuthIntent.set(intentBuilder.build())
//            mAuthIntentLatch.countDown()
//        }
//    }
//
//    private fun createAuthRequest(@Nullable loginHint: String) {
//        Log.i(TAG, "Creating auth request for login hint: $loginHint")
//        val authRequestBuilder = AuthorizationRequest.Builder(
//            mAuthStateManager?.currentState?.authorizationServiceConfiguration!!,
//            mClientId.get(),
//            ResponseTypeValues.CODE,
//            mConfiguration!!.redirectUri
//        )
//            .setScope(mConfiguration!!.scope)
//        if (!TextUtils.isEmpty(loginHint)) {
//            authRequestBuilder.setLoginHint(loginHint)
//        }
//        mAuthRequest.set(authRequestBuilder.build())
//    }
//
//    private fun getLoginHint(): String? {
//        return "this hint"
////        return (findViewById<View>(R.id.login_hint_value) as EditText)
////            .text
////            .toString()
////            .trim { it <= ' ' }
//    }
//
//    @TargetApi(Build.VERSION_CODES.M)
//    private fun getColorCompat(@ColorRes color: Int): Int {
//        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            getColor(color)
//        } else {
//            resources.getColor(color)
//        }
//    }
//
//    private class LoginHintChangeHandler internal constructor() : TextWatcher {
////        private val mHandler: Handler
////        private var mTask: RecreateAuthRequestTask
//        override fun beforeTextChanged(cs: CharSequence?, start: Int, count: Int, after: Int) {}
//        override fun onTextChanged(cs: CharSequence?, start: Int, before: Int, count: Int) {
////            mTask.cancel()
////            mTask = RecreateAuthRequestTask()
////            mHandler.postDelayed(mTask, DEBOUNCE_DELAY_MS)
//        }
//
//        override fun afterTextChanged(ed: Editable?) {}
////
////        companion object {
////            private const val DEBOUNCE_DELAY_MS = 500
////        }
////
////        init {
////            mHandler = Handler(Looper.getMainLooper())
////            mTask = RecreateAuthRequestTask()
////        }
//    }
//
//    private class RecreateAuthRequestTask : Runnable {
//        private val mCanceled: AtomicBoolean = AtomicBoolean()
//        override fun run() {
//            if (mCanceled.get()) {
//                return
//            }
////            createAuthRequest(getLoginHint())
////            warmUpBrowser()
//        }
//
//        fun cancel() {
//            mCanceled.set(true)
//        }
//    }
//
//
//
//
//
//
//
//
////    private fun handleRegistrationResponse(
////        response: RegistrationResponse?,
////        ex: AuthorizationException
////    ) {
////        mAuthStateManager?.updateAfterRegistration(response, ex)
////        if (response == null) {
////            Log.i(TAG, "Failed to dynamically register client", ex)
////            displayErrorLater("Failed to register client: " + ex.message, true)
////            return
////        }
////        Log.i(TAG, "Dynamically registered client: " + response.clientId)
////        mClientId.set(response.clientId)
////        initializeAuthRequest()
////    }
//
////    private fun configureBrowserSelector() {
//////        val spinner = findViewById<View>(R.id.browser_selector) as Spinner
//////        val adapter = BrowserSelectionAdapter(this)
//////        spinner.adapter = adapter
//////        spinner.onItemSelectedListener = object : OnItemSelectedListener {
//////            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
//////                val info: BrowserInfo = adapter.getItem(position)
//////                if (info == null) {
//////                    mBrowserMatcher = AnyBrowserMatcher.INSTANCE
//////                    return
//////                } else {
//////                    mBrowserMatcher = ExactBrowserMatcher(info.mDescriptor)
//////                }
//////                recreateAuthorizationService()
//////                createAuthRequest(getLoginHint())
//////                warmUpBrowser()
//////            }
//////
//////            override fun onNothingSelected(parent: AdapterView<*>?) {
//////                mBrowserMatcher = AnyBrowserMatcher.INSTANCE
//////            }
//////        }
////    }
//
//
////    override fun onCreateOptionsMenu(menu: Menu): Boolean {
////        // Inflate the menu; this adds items to the action bar if it is present.
////        menuInflater.inflate(R.menu.menu_main, menu)
////        return true
////    }
////
////    override fun onOptionsItemSelected(item: MenuItem): Boolean {
////        // Handle action bar item clicks here. The action bar will
////        // automatically handle clicks on the Home/Up button, so long
////        // as you specify a parent activity in AndroidManifest.xml.
////        return when (item.itemId) {
////            R.id.action_settings -> true
////            else -> super.onOptionsItemSelected(item)
////        }
////    }
//
////    private fun checkAuthStateSign(){
////        if (authStateManager!!.currentState.isAuthorized) {
////
////        } else {
////            val response = AuthorizationResponse.fromIntent(this.intent)
////            val ex = AuthorizationException.fromIntent(this.intent)
////            if (response != null || ex != null) {
////                authStateManager!!.updateAfterAuthorization(
////                    response,
////                    ex
////                )
////                if (response?.authorizationCode != null) { // Authorization code exchange is required.
////                    exchangeAuthorizationCode(response)
////                } else if (ex != null) {
////                    Log.e(TAG, "Authorization request failed: " + ex.message)
////                    Toast.makeText(this, "Authorization request failed!", Toast.LENGTH_SHORT).show()
////                    LogoutRequest().getInstance()!!.signOut(this)
//////                    finish()
////                }
////            } else {
////                Log.e(
////                    TAG,
////                    "No authorization state retained - re-authorization required.")
////                Toast.makeText(this, "Re-authorization required!", Toast.LENGTH_SHORT).show()
//////                LogoutRequest().getInstance()!!.signOut(this)
//////                finish()
////            }
////        }
////    }
//
////    private  fun exchangeAuthorizationCode(authorizationResponse: AuthorizationResponse) { // Makes the extra parameters for access token request.
////        val additionalParameters: MutableMap<String, String?> =
////            HashMap()
////        additionalParameters["client_secret"] = mConfiguration!!.clientSecret
////        // State is stored to generate the logout endpoint request.
////        state = authorizationResponse.state
////        performTokenRequest(authorizationResponse.createTokenExchangeRequest(additionalParameters),
////            AuthorizationService.TokenResponseCallback { tokenResponse: TokenResponse?, authException: AuthorizationException? ->
////                this.handleCodeExchangeResponse(
////                    tokenResponse,
////                    authException
////                )
////            }
////        )
////    }
//
////    private fun handleCodeExchangeResponse(
////        tokenResponse: TokenResponse?,
////        authException: AuthorizationException?
////    ) {
////        mAuthStateManager!!.updateAfterTokenResponse(
////            tokenResponse,
////            authException
////        )
////        if (mAuthStateManager!!.getCurrentState().isAuthorized()) {
////            Log.e(TAG, "Authorization code exchange failed.")
////        } else {
////            idToken = tokenResponse?.idToken
////            accessToken = tokenResponse?.accessToken
////        }
////    }
//
////    private fun performTokenRequest(
////        request: TokenRequest,
////        callback: AuthorizationService.TokenResponseCallback
////    ) {
////        val clientAuthentication: ClientAuthentication
////        clientAuthentication = try {
////            mAuthStateManager!!.getCurrentState().getClientAuthentication()
////        } catch (ex: ClientAuthentication.UnsupportedAuthenticationMethod) {
////            UtilLog.error(
////                tag = TAG,
////                text = "Token request cannot be made, client authentication " +
////                        "for the token endpoint could not be constructed (%s)., " +
////                        "$ex"
////            )
////            return
////        }
////        mAuthService!!.performTokenRequest(request, clientAuthentication, callback)
////    }
//
////    private fun checkAuthState(){
//////        authStateManager = AuthStateManager.getInstance(this)
//////        configuration = ConfigManager.getInstance(this)
////
////        val accesTime = mAuthStateManager!!.currentState.accessTokenExpirationTime
////        val sysTime = System.currentTimeMillis()
////        val timeRes = sysTime - (accesTime ?: 0)
////
////        if(timeRes < 0 || timeRes == sysTime){
////            if (mAuthStateManager!!.currentState.isAuthorized && !mConfiguration!!.hasConfigurationChanged()) {
//////                startActivity(Intent(this, ActivityHome::class.java))
////                //todo show for fragment all card view
//////                finish()
////            } else { // If the user is not authorized, the LoginActivity view will be launched.
////                //            supportActionBar!!.title = ""
////                // Checks if the configuration is valid.
////                if (!mConfiguration!!.isValid) {
////                    Toast.makeText(
////                        this,
////                        "Configuration is not valid!",
////                        Toast.LENGTH_SHORT
////                    ).show()
////                    finish()
////                } else { // Discards any existing authorization state due to the change of configuration.
////                    if (mConfiguration!!.hasConfigurationChanged()) {
////                        mAuthStateManager!!.replaceState(AuthState())
////                        // Stores the current configuration as the last known valid configuration.
////                        mConfiguration!!.acceptConfiguration()
////
////                    }
////                }
////                AuthRequest.getInstance(this).doAuth(
////                    Intent(this, null) //ActivityLogin::class.java)
////                )
////            }
////        }else{
////            mAuthStateManager!!.replaceState(AuthState())
////            // Stores the current configuration as the last known valid configuration.
////            mConfiguration!!.acceptConfiguration()
////            val aTime = mAuthStateManager!!.currentState.accessTokenExpirationTime
////            val sTime = System.currentTimeMillis()
////            val rTime = sysTime - (accesTime ?: 0)
////            if(rTime < 0) {
//////                startActivity(Intent(this, ActivityHome::class.java))
//////                finish()
////            }else{
//////                startActivity(Intent(this, ActivityLogin::class.java))
//////                finish()
////                AuthRequest.getInstance(this).doAuth(
////                    Intent(this, null) //ActivityLogin::class.java)
////                )
////            }
////        }
////
////    }
//
//}
