package sumra.net.app.infinity_card.core

import android.annotation.SuppressLint
import com.google.common.net.HttpHeaders
import okhttp3.*
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.internal.addHeaderLenient
import okhttp3.internal.http2.Header
import org.apache.http.HttpException
import java.util.*


object NetOauthTest {
    @SuppressLint("NewApi")
    fun postFetchToken():String {

        val base = "https://apim.sumra.net/oauth2/token".toHttpUrlOrNull()!!

        val httpUrl = HttpUrl.Builder().apply {
            scheme(EHost.HTTPS.value)
            host(base.host)
//            addQueryParameter("grant_type","password")
//            addQueryParameter("username","p.s.g.sirogk")
//            addQueryParameter("password","5BUdeKe8sn9FWYgL")
        }.build()

//        val credentials = Credentials.basic("p.s.g.sirogk", "5BUdeKe8sn9FWYgL")

        val byteArray = "8cvnmMyUR1MhCSxNQu3jYXaK0Sga:B4pjhmVW6SAr6tJ5rFpr1nGJdq4a".toByteArray()
        val res0 = Base64.getEncoder().encodeToString(byteArray)

        val header = Headers
            .headersOf(
//                "content-type", "application/json",
                HttpHeaders.AUTHORIZATION, "Basic $res0")

        val body0 = FormBody.Builder().build() //("application/json; charset=utf-8".toMediaTypeOrNull()!!).build()
        val bodyData = "{\"grant_type\":\"password\",\"username\":\"p.s.g.sirogk\",\"password\":\"5BUdeKe8sn9FWYgL\"}"
        val body2 = bodyData.toRequestBody("application/json; charset=utf-8".toMediaTypeOrNull()!!)

        val httpRequest: Request = Request.Builder()
            .url(httpUrl)
            .headers(header)
            .post(body2)
            .build()

        try{
            val response = OkHttpClient.builder() //   builderByAuthoryze()
                .newCall(httpRequest).execute()
            if(response.isSuccessful){
                return NetworkCore.resultSuccess(response)
            }else{
                return NetworkCore.resultFailure(response)
            }
        } catch (e : HttpException) {
            return NetworkCore.resultFailure(e as Response)
        }
    }
}