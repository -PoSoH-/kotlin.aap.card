package sumra.net.app.infinity_card.enums

enum class ERespCode (val type: String){
    ERROR   (type = "error"),
    SUCCESS (type = "success");

    companion object {
        fun compareType(type: String): Boolean{
            return when(type){
                ERROR.type -> false
                SUCCESS.type -> true
                else -> false
            }
        }
    }
}
