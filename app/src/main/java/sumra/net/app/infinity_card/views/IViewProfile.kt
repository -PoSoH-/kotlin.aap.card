package sumra.net.app.infinity_card.views

import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(value = AddToEndSingleStrategy::class)
interface IViewProfile : IViewCommon {

    fun fetchMeSuccess()
    fun fetchMeFailure(error: String?)

    fun fetchTariffsSuccess()
    fun fetchTariffsFailure(error: String?)

    fun fetchPayDetailsSuccess()
    fun fetchPayDetailsFailure(error: String?)

}