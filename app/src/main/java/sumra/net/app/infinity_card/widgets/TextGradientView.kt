package sumra.net.app.infinity_card.widgets

import android.R.attr
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.LinearGradient
import android.graphics.Shader
import android.graphics.Shader.TileMode
import android.util.AttributeSet
import android.widget.TextView


@SuppressLint("AppCompatCustomView")
class TextGradientView : TextView {

    var c1 = 0
    var c2: kotlin.Int = 0
    var shader: Shader? = null

    constructor (context: Context?) : super(context)
    constructor (context: Context, attrs: AttributeSet?) : super(context, attrs) {
//        if (!isInEditMode) createShader(context, attrs, 0)
    }

    constructor (context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
//        if (!isInEditMode) createShader(context, attrs, defStyleAttr)
    }
//    constructor (context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
//        if (!isInEditMode) createShader(context, attrs, defStyleAttr)
//    }

//    fun createShader(context: Context, attrs: AttributeSet?, defStyleAttr: Int) {
//        val a = context.obtainStyledAttributes(attrs, R.styleable.GradientTextView, defStyleAttr, 0)
//        try {
//            c1 = a.getInt(R.styleable.GradientTextView_gradientStart, 0)
//            c2 = a.getInt(R.styleable.GradientTextView_gradientEnd, 0)
//            shader = LinearGradient(0, 0, 0, textSize, intArrayOf(c1, c2), floatArrayOf(0f, 1f), Shader.TileMode.CLAMP)
//            paint.shader = shader
//        } finally {
//            a.recycle()
//        }
//    }

    @SuppressLint("DrawAllocation")
    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        if (changed) {
//            ShapeDrawable.setShaderFactory(ShapeDrawable.ShaderFactory factory)
//            val textShader: Shader = LinearGradient(
//                0F,
//                0F,
//                0F,
//                20F,
//                intArrayOf(Color.GREEN, Color.BLUE),
//                TileMode.CLAMP
//            )
            val textShader: Shader = LinearGradient(
                0.0F, 0.0F, attr.width.toFloat(), height.toFloat(), intArrayOf(
                    Color.parseColor("#ffCA8F31"),
                    Color.parseColor("#ffFEF89E")
                ), floatArrayOf(
                    0.0f,
                    1.0f
                ), TileMode.CLAMP
            )
            paint.shader = textShader
        }
    }
}