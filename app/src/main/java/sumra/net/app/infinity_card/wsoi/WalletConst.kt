package com.example.domain.wsoi

class WalletConst {
    companion object{
        // Constants related to User Info Request.
        const val AUTHORIZATION = "Authorization"
        const val BEARER = "Bearer "
        const val UTF_8 = "UTF-8"

        // Constants related to Configuration.
        const val PREFS_NAME = "config"
        const val KEY_LAST_HASH = "lastHash"
        const val HTTP = "http"
        const val HTTPS = "https"

        // Constants related to AuthState.
        const val STORE_NAME = "AuthState"
        const val KEY_STATE = "state"

        // Constants related to Connection Builder.
        const val PROTOCOL = "TLSv1.2"

        // Constants related to the Application.
        const val APP_NAME = "PICKUP"

    }
}