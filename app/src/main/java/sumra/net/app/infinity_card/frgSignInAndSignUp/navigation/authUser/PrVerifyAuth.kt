package sumra.net.app.infinity_card.frgSignInAndSignUp.navigation.authUser

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import moxy.MvpPresenter
import sumra.net.app.infinity_card.models.responses.AuthUser
import sumra.net.app.infinity_card.models.responses.VerifyAuth

class PrVerifyAuth: MvpPresenter<IViewVerifyAuth>() {

    private val repository = RepositoryVerifyAuth()
    private lateinit var result: VerifyAuth

    fun fetchAuthSignUp(data: AuthUser){
        viewState.showProgress(true)
        CoroutineScope(Dispatchers.IO).launch {
            val resp = repository.fetchAuthSignUp(data).await()
            if (resp.success){
                withContext(Dispatchers.Main){
                    viewState.fetchUserAuthSuccess()
                }
            }else{
                withContext(Dispatchers.Main){
                    viewState.fetchUserAuthFailure(resp.error)
                }
            }
            viewState.showProgress(false)
        }
    }

    fun fetchAuthSignIn(data: AuthUser){
        viewState.showProgress(true)
        CoroutineScope(Dispatchers.IO).launch {
            val resp = repository.fetchAuthSignUp(data).await()
            if (resp.success){
                withContext(Dispatchers.Main){
                    viewState.fetchUserAuthSuccess()
                }
            }else{
                withContext(Dispatchers.Main){
                    viewState.fetchUserAuthFailure(resp.error)
                }
            }
            viewState.showProgress(false)
        }
    }

    fun refreshToken(){
        viewState.showProgress(true)
        CoroutineScope(Dispatchers.IO).launch {
            //read refresh token from local storange and send to server....
            val resp = repository.fetchTokenRefresh(
                token = "token",
                tokenRefresh = "token_refresh"
            ).await()
            if (resp.success){
                withContext(Dispatchers.Main){
                    viewState.fetchTokenRefreshSuccess()
                }
            }else{
                withContext(Dispatchers.Main){
                    viewState.fetchTokenRefreshFailure(resp.error)
                }
            }
            viewState.showProgress(false)
        }
    }

    fun fetchAuthSignOut(){
        viewState.showProgress(true)
        CoroutineScope(Dispatchers.IO).launch {
            //read bearer token from local storange and send to server....
            val resp = repository.fetchAuthSignOut("Bearer token").await()
            if (resp.success){
                withContext(Dispatchers.Main){
                    viewState.fetchLogoutSuccess()
                }
            }else{
                withContext(Dispatchers.Main){
                    viewState.fetchLogoutFailure(resp.error)
                }
            }
            viewState.showProgress(false)
        }

    }
}