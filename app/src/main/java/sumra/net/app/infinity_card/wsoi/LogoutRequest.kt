package net.sumra.wallet.wsoi

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.browser.customtabs.CustomTabsIntent
import net.openid.appauth.AuthState
import sumra.net.app.infinity_card.wsoi.AuthStateManager
import sumra.net.app.infinity_card.wsoi.ConfigManager
import java.lang.ref.WeakReference
import java.util.concurrent.atomic.AtomicReference

class LogoutRequest {
    var idToken: String? = null
    var state: String? = null
    private val instance =
        AtomicReference(
            WeakReference<LogoutRequest?>(null)
        )

    fun getInstance(): LogoutRequest? {
        var logoutRequest = instance.get().get()
        if (logoutRequest == null) {
            logoutRequest = LogoutRequest()
            instance.set(
                WeakReference(
                    logoutRequest
                )
            )
        }
        return logoutRequest
    }

    fun signOut(context: Context?) {
        val authStateManager = AuthStateManager.getInstance(context)
        val configuration = ConfigManager.getInstance(context)

        // Discards the authorization and token states, but retains the configuration.
        val currentState = authStateManager.currentState
        val clearedState = AuthState(currentState.authorizationServiceConfiguration!!)
        if (currentState.lastRegistrationResponse != null) {
            clearedState.update(currentState.lastRegistrationResponse)
        }
        authStateManager.replaceState(clearedState)
        val logout_uri = configuration.logoutEndpointUri.toString()
        val redirect = configuration.redirectUri.toString()
        val url = StringBuffer()
        url.append(logout_uri)
        url.append("?id_token_hint=")
        url.append(currentState.idToken)
        url.append("&post_logout_redirect_uri=")
        url.append(redirect)
        url.append("&state=")
        url.append(state)
        val builder = CustomTabsIntent.Builder()
        val customTabsIntent = builder.build()
        customTabsIntent.intent.flags =
            (Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_NEW_TASK
                    or Intent.FLAG_ACTIVITY_SINGLE_TOP)
        customTabsIntent.launchUrl(context, Uri.parse(url.toString()))
    }
}