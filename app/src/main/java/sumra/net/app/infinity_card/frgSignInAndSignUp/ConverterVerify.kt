package sumra.net.app.infinity_card.frgSignInAndSignUp

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import kotlinx.coroutines.delay
import sumra.net.app.infinity_card.models.responses.*

object ConverterVerify {

    suspend fun convertRespPhoneToModel(args: String): RespNetVerifySign{
        delay(2_500)

        val args = getSuccess()

        val isGreat = Gson().fromJson<_jsonRespVerifyPhone>(args, _jsonRespVerifyPhone::class.java)
        var isResult : RespNetVerifySign

        if(isGreat != null && isGreat.success){
            isResult = RespNetVerifySign(isGreat.success, isGreat.data, isGreat.error)
        }else{
            isResult = RespNetVerifySign(false, null, "Verify phone ERROR!")
        }
        return isResult
    }

    suspend fun convertRespCodeToModel(args: String): VerifyCode{
        delay(2_500)

        val args = getCodeSuccess()

        val isGreat = Gson().fromJson<_jsonRespVerifyCode>(args, _jsonRespVerifyCode::class.java)
        var isResult : VerifyCode

        if(isGreat != null && isGreat.success){
            isResult = VerifyCode(isGreat.success, isGreat.error)
        }else{
            isResult = VerifyCode(false, "Verify phone ERROR!")
        }
        return isResult
    }

    suspend fun convertReqstModelToCode(user: AuthUser): String{
       return Gson().toJson(user)
    }

    suspend fun convertRespCodeToUser(args: String): RespVerifyPhone {
        delay(2_500)

        val args = getAuthSuccess()

        val isGreat = Gson().fromJson<_jsonRespVerifyAuth>(args, _jsonRespVerifyAuth::class.java)
        var isResult: RespVerifyPhone

        if (isGreat != null && isGreat.success) {
            isResult = RespVerifyPhone(isGreat.success, isGreat.data, isGreat.error)
        } else {
            isResult = RespVerifyPhone(false, null,"Verify phone ERROR!")
        }
        return isResult
    }

    fun getAuthSuccess():String{
        return """{ 
                        "success": true,
                        "data"   : { 
                                        "token":"ADDRESS", 
                                        "token_expire":"1c4ER2", 
                                        "refresh_token":"1c4ER2", 
                                        "scope":"ncnJH7GS867JHJDfL" 
                                   } ,
                        "error"  : null
                    }"""
    }

    fun getCodeSuccess():String{
        return """{ 
                        "success": true,
                        "error"  : null
                    }"""
    }

    fun getSuccess():String{
        return """{ 
                        "success": true,
                        "data"   : { "link":"ADDRESS", "code":"1c4ER2", "id_code":"ncnJH7GS867JHJDfL" } ,
                        "error"  : null
                    }"""
    }

    private data class _jsonRespVerifyPhone(
        val success: Boolean,
        val data: VerifyPhone?,
        val error: String?,
    )

    private data class _jsonRespVerifyCode(
        val success: Boolean,
        val error: String?,
    )

    private data class _jsonRespVerifyAuth(
        val success: Boolean,
        val data: VerifyAuth?,
        val error: String?,
    )
}
