package sumra.net.app.infinity_card.frgSignInAndSignUp.navigation.verifyPhone

import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType
import sumra.net.app.infinity_card.views.IViewCommon

@StateStrategyType(value = AddToEndSingleStrategy::class)
interface IViewVerifyPhone: IViewCommon {
    fun verifyPhoneSuccess()
    fun verifyPhoneFailure()
}