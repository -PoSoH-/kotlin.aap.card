package sumra.net.app.infinity_card.interfaces

interface OnOrderItemClickListener {
    fun onOrderItemClick(orderId: Long)
}