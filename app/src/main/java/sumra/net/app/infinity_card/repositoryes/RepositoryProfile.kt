package sumra.net.app.infinity_card.repositoryes

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import sumra.net.app.infinity_card.core.NetProfile
import sumra.net.app.infinity_card.models.UserPatch
import sumra.net.app.infinity_card.models.responses._respNetProfile
import sumra.net.app.infinity_card.models.responses._respUserPatch
import sumra.net.app.infinity_card.repositoryes.converters.ConverterProfileUser

class RepositoryProfile {

    fun fetchProfileUser(token: String) : Deferred<_respNetProfile>{
        val resp = MockUser.getMockUser() // NetProfile.getClientUsers(token)  // MockUser.getMockUser()
        return GlobalScope.async {
            ConverterProfileUser.convertToUserModel(resp)
        }
    }

    fun patchProfileUser(token: String, user: UserPatch) : Deferred<_respUserPatch>{
        val resp = NetProfile.patchProfileUser(token, ConverterProfileUser.convertRequestUserPatch(user))
        return GlobalScope.async {
            ConverterProfileUser.convertRespUserPatch(resp)
        }
    }

    private class MockUser(){
        companion object{
            fun getMockUser() = """{
                    "success": true,
                    "user": {
                            "id": 1,
                            "type_id": 5,
                            "parent_id": 0,
                            "ownership_id": 1,
                            "tariff_id": 1,
                            "first_name": "Julia",
                            "last_name": "Colossaless",
                            "avatar": "https://cdna.artstation.com/p/assets/images/images/018/430/746/large/anime-attics-illustration30n.jpg?1559333706",
                            "phone": "380452136253",
                            "email": "julia_colossales_vanga@coloss.net",
                            "passport_number": "PG254598",
                            "passport_date_issue": "2029-10-14 00:00:00",
                            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                            "status": 1,
                            "country": "ua",
                            "created_at": "2020-10-14 13:05:11",
                            "updated_at": "2020-10-14 13:05:11",
                            "contacts": [
                                {   "id": 1,
                                    "user_id": 1,
                                    "name": "fdfd",
                                    "value": "fdfd",
                                    "description": "fdfd",
                                    "created_at": "2020-10-15 14:47:53",
                                    "updated_at": "2020-10-15 14:47:53"
                                },
                                {   "id": 2,
                                    "user_id": 1,
                                    "name": "",
                                    "value": "",
                                    "description": "",
                                    "created_at": "2020-10-15 14:49:27",
                                    "updated_at": "2020-10-15 14:49:27"
                                },
                                {   "id": 3,
                                    "user_id": 1,
                                    "name": "",
                                    "value": "",
                                    "description": "",
                                    "created_at": "2020-10-15 14:53:27",
                                    "updated_at": "2020-10-15 14:53:27"
                                },
                                {   "id": 6,
                                    "user_id": 1,
                                    "name": "dsdsds",
                                    "value": "dsdsds",
                                    "description": "dsdsdsu",
                                    "created_at": "2020-10-15 15:33:23",
                                    "updated_at": "2020-10-15 15:33:23"
                                }
                            ],
                            "requisites": [
                                {   "id": 1,
                                    "name": "Bank account",
                                    "description": "bank account test",
                                    "country": "ar",
                                    "created_at": "2020-10-15 07:33:16",
                                    "updated_at": "2020-10-15 07:33:16",
                                    "pivot": {
                                        "user_id": 1,
                                        "requisite_id": 1,
                                        "value": "s1236542856"
                                    }
                                }
                            ],
                            "cards": [
                                {   "id": 3,
                                    "type_id": 1,
                                    "number": "7788895414056688",
                                    "code": "31393839-3035-7452-8dce-302e39353132",
                                    "activation_code": "3830",
                                    "activate_before": "2021-10-15 00:00:00",
                                    "activated_at": "2020-10-20 10:59:55",
                                    "creator_id": 1,
                                    "owner_id": 1,
                                    "status": 3,
                                    "balance": "568.25",
                                    "currency": "usd",
                                    "balance_at": "2020-10-15 11:12:25",
                                    "created_at": "2020-10-15 11:12:25",
                                    "updated_at": "2020-10-20 10:59:55",
                                    "numberFormatted": "7788-8954-1405-6688"
                                },
                                {   "id": 4,
                                    "type_id": 3,
                                    "number": "1212910478723075",
                                    "code": "31323532-3433-7458-5FgR-302e32323835",
                                    "activation_code": "313937",
                                    "activate_before": "2021-10-15 00:00:00",
                                    "activated_at": "2020-10-20 10:54:16",
                                    "creator_id": 1,
                                    "owner_id": 1,
                                    "status": 3,
                                    "balance": "1005.25",
                                    "currency": "eur",
                                    "balance_at": "2020-10-15 11:39:41",
                                    "created_at": "2020-10-15 11:39:41",
                                    "updated_at": "2020-10-20 10:54:16",
                                    "numberFormatted": "1212-9104-7872-3075"
                                },
                                {   "id": 9,
                                    "type_id": 8,
                                    "number": "1212910478722975",
                                    "code": "31323532-3433-9654-8dce-302e32323835",
                                    "activation_code": "313937",
                                    "activate_before": "2021-10-15 00:00:00",
                                    "activated_at": "2020-10-20 10:54:16",
                                    "creator_id": 1,
                                    "owner_id": 1,
                                    "status": 3,
                                    "balance": "1005.25",
                                    "currency": "eur",
                                    "balance_at": "2020-10-15 11:39:41",
                                    "created_at": "2020-10-15 11:39:41",
                                    "updated_at": "2020-10-20 10:54:16",
                                    "numberFormatted": "1212-9104-7872-2975"
                                },
                                {   "id": 15,
                                    "type_id": 7,
                                    "number": "1212910478722975",
                                    "code": "31323532-3433-9652-8dce-302e32323835",
                                    "activation_code": "313937",
                                    "activate_before": "2021-10-15 00:00:00",
                                    "activated_at": "2020-10-20 10:54:16",
                                    "creator_id": 1,
                                    "owner_id": 1,
                                    "status": 3,
                                    "balance": "1005.25",
                                    "currency": "eur",
                                    "balance_at": "2020-10-15 11:39:41",
                                    "created_at": "2020-10-15 11:39:41",
                                    "updated_at": "2020-10-20 10:54:16",
                                    "numberFormatted": "1212-9104-7872-2975"
                                },
                                {   "id": 20,
                                    "type_id": 8,
                                    "number": "1215000454162445",
                                    "code": "31373531-3433-9631-8d0c-2e3034383137",
                                    "activation_code": "3437",
                                    "activate_before": "2021-10-15 00:00:00",
                                    "activated_at": "2020-10-20 09:39:57",
                                    "creator_id": 1,
                                    "owner_id": 1,
                                    "status": 3,
                                    "balance": "68715.22",
                                    "currency": "gbp",
                                    "balance_at": "2020-10-15 11:40:54",
                                    "created_at": "2020-10-15 11:40:54",
                                    "updated_at": "2020-10-20 09:39:57",
                                    "numberFormatted": "1215-0004-5416-2445"
                                },
                                {   "id": 66,
                                    "type_id": 4,
                                    "number": "1215827302652368",
                                    "code": "31343034-3035-1458-8c0c-2e3633383835",
                                    "activation_code": "3137",
                                    "activate_before": "2021-10-15 00:00:00",
                                    "activated_at": "2020-10-20 11:04:38",
                                    "creator_id": 1,
                                    "owner_id": 1,
                                    "status": 3,
                                    "balance": "4850.00",
                                    "currency": "gbp",
                                    "balance_at": "2020-10-15 14:09:16",
                                    "created_at": "2020-10-15 14:09:16",
                                    "updated_at": "2020-10-20 11:04:38",
                                    "numberFormatted": "1215-8273-0265-2368"
                                },
                                {   "id": 55,
                                    "type_id": 9,
                                    "number": "1212910478722975",
                                    "code": "31323532-3433-3214-8dce-302e32323835",
                                    "activation_code": "313937",
                                    "activate_before": "2021-10-15 00:00:00",
                                    "activated_at": "2020-10-20 10:54:16",
                                    "creator_id": 1,
                                    "owner_id": 1,
                                    "status": 3,
                                    "balance": "1005.25",
                                    "currency": "eur",
                                    "balance_at": "2020-10-15 11:39:41",
                                    "created_at": "2020-10-15 11:39:41",
                                    "updated_at": "2020-10-20 10:54:16",
                                    "numberFormatted": "1212-9104-7872-2975"
                                },
                                {   "id": 24,
                                    "type_id": 2,
                                    "number": "1212910478722975",
                                    "code": "31323532-3433-7458-8dce-302e32323835",
                                    "activation_code": "313937",
                                    "activate_before": "2021-10-15 00:00:00",
                                    "activated_at": "2020-10-20 10:54:16",
                                    "creator_id": 1,
                                    "owner_id": 1,
                                    "status": 3,
                                    "balance": "1005.25",
                                    "currency": "eur",
                                    "balance_at": "2020-10-15 11:39:41",
                                    "created_at": "2020-10-15 11:39:41",
                                    "updated_at": "2020-10-20 10:54:16",
                                    "numberFormatted": "1212-9104-7872-2975"
                                },
                                {   "id": 27,
                                    "type_id": 6,
                                    "number": "4563340340867687",
                                    "code": "31323338-3037-6874-8e4d-302e39373630",
                                    "activation_code": "313234",
                                    "activate_before": "2021-10-16 00:00:00",
                                    "activated_at": "2020-10-20 08:31:46",
                                    "creator_id": 1,
                                    "owner_id": 1,
                                    "status": 3,
                                    "balance": "245987.99",
                                    "currency": "usd",
                                    "balance_at": "2020-10-16 07:06:21",
                                    "created_at": "2020-10-16 07:06:22",
                                    "updated_at": "2020-10-20 08:31:46",
                                    "numberFormatted": "4563-3403-4086-7687"
                                },
                                {   "id": 8,
                                    "type_id": 3,
                                    "number": "9889408076673769",
                                    "code": "31313933-3135-6854-8ccc-302e34373734",
                                    "activation_code": "3737",
                                    "activate_before": "2021-10-16 00:00:00",
                                    "activated_at": "2020-10-20 11:09:58",
                                    "creator_id": 1,
                                    "owner_id": 1,
                                    "status": 3,
                                    "balance": "145.55",
                                    "currency": "eur",
                                    "balance_at": "2020-10-16 07:14:25",
                                    "created_at": "2020-10-16 07:14:25",
                                    "updated_at": "2020-10-20 11:09:58",
                                    "numberFormatted": "9889-4080-7667-3769"
                                },
                                {   "id": 96,
                                    "type_id": 1,
                                    "number": "1212910478722975",
                                    "code": "31323532-3433-5555-8dce-302e32323835",
                                    "activation_code": "313937",
                                    "activate_before": "2021-10-15 00:00:00",
                                    "activated_at": "2020-10-20 10:54:16",
                                    "creator_id": 1,
                                    "owner_id": 1,
                                    "status": 3,
                                    "balance": "1005.25",
                                    "currency": "eur",
                                    "balance_at": "2020-10-15 11:39:41",
                                    "created_at": "2020-10-15 11:39:41",
                                    "updated_at": "2020-10-20 10:54:16",
                                    "numberFormatted": "1212-9104-7872-2975"
                                },
                                {   "id": 18,
                                    "type_id": 7,
                                    "number": "1854354051495849",
                                    "code": "31373235-3131-4444-8c0c-302e32303138",
                                    "activation_code": "3831",
                                    "activate_before": "2021-10-20 00:00:00",
                                    "activated_at": "2020-10-20 11:37:27",
                                    "creator_id": 1,
                                    "owner_id": 1,
                                    "status": 3,
                                    "balance": "1459.87",
                                    "currency": "usd",
                                    "balance_at": "2020-10-20 11:36:29",
                                    "created_at": "2020-10-20 11:36:29",
                                    "updated_at": "2020-10-20 11:37:27",
                                    "numberFormatted": "1854-3540-5149-5849"
                                }
                            ],
                            "balances": [
                                {   "id": 145,
                                    "user_id": 1,
                                    "balance": "1220.00",
                                    "currency": "usd",
                                    "created_at": "2020-10-29 15:25:31",
                                    "updated_at": "2020-10-29 15:25:31"
                                },
                                {   "id": 146,
                                    "user_id": 1,
                                    "balance": "1129.52",
                                    "currency": "eur",
                                    "created_at": "2020-10-29 15:25:31",
                                    "updated_at": "2020-10-29 15:25:31"
                                },
                                {   "id": 147,
                                    "user_id": 1,
                                    "balance": "1095.71",
                                    "currency": "gbp",
                                    "created_at": "2020-10-29 15:25:31",
                                    "updated_at": "2020-10-29 15:25:31"
                                }
                            ]
                        }
                    }"""
        }
    }
}