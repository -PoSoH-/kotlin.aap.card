package sumra.net.app.infinity_card.models


import com.google.gson.annotations.SerializedName

data class _jsonNetRespFiles(
    @SerializedName("data")
    val respFiles: ResponseFile
)

data class ResponseFile(
    @SerializedName("attributes")
    val attributes: AttributeFiles,
    @SerializedName("id")
    val id: Long,
    @SerializedName("type")
    val type: String
)

data class AttributeFiles(
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("entity")
    val entity: String,
    @SerializedName("entity_id")
    val entityId: Long?,
    @SerializedName("folder_path")
    val folderPath: String,
    @SerializedName("id")
    val id: Long,
    @SerializedName("path")
    val path: String,
    @SerializedName("server_path")
    val serverPath: String,
    @SerializedName("updated_at")
    val updatedAt: String,
    @SerializedName("user_id")
    val userId: String,
    @SerializedName("value")
    val value: String
)


//{
//    "data": {
//        "type":"files",
//        "id":14,
//        "attributes":{
//            "entity":"avatar",
//            "entity_id":null,
//            "user_id":"1",
//            "server_path":"\/",
//            "folder_path":"aa635dc42386ef73ac05da105aeba272",
//            "updated_at":"2020-12-01 15:22:43",
//            "created_at":"2020-12-01 15:22:43",
//            "id":14,
//            "path":"\/avatar\/aa635dc42386ef73ac05da105aeba272\/original.jpeg",
//            "value":"original.jpeg"
//        }
//    }
//}