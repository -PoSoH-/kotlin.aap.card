package sumra.net.app.infinity_card.frgSignInAndSignUp.navigation.verifyPhone

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import moxy.MvpPresenter
import sumra.net.app.infinity_card.models.responses.RespNetVerifySign
import sumra.net.app.infinity_card.models.responses.VerifyPhone

class PrVerifyPhone: MvpPresenter<IViewVerifyPhone>() {

    private val repository = RepositoryVerifyPhone()
    private lateinit var resultat: VerifyPhone

    fun sendPhoneForVerify(token: String, phone: String){
        viewState.showProgress(true)
        CoroutineScope(Dispatchers.IO).launch {
            val resp = repository.sendVerifyPhone(token, phone).await()
            if (resp.success) {
                resultat = resp.data as VerifyPhone
                withContext(Dispatchers.Main) {
                    viewState.showProgress(false)
                    viewState.verifyPhoneSuccess()
                }
            } else {
                withContext(Dispatchers.Main) {
                    viewState.showProgress(false)
                    viewState.verifyPhoneFailure()
                }
            }
        }
    }

    fun getCode() = resultat.code

    fun getCodeID() = resultat.id_code
}