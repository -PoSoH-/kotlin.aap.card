package sumra.net.app.infinity_card.models.responses

import com.google.gson.annotations.SerializedName
import sumra.net.app.infinity_card.models.Country

data class _respUserPatch(
    val success: String,
    var data: Any?,
    var error: String?) //_jsonInfinityError.InfinityError?)