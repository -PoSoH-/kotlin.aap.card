package sumra.net.app.infinity_card.frgSignInAndSignUp.navigation.verifyPhone

import android.os.Bundle
import android.view.View
import androidx.navigation.Navigation
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.frg_auth_phone.*
import kotlinx.android.synthetic.main.frg_login_screen.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter
import sumra.net.app.infinity_card.AppCard
import sumra.net.app.infinity_card.R
import sumra.net.app.infinity_card.enums.EKeys
import sumra.net.app.infinity_card.utils.extention.hideKeyboard
import sumra.net.app.infinity_card.utils.extention.inputText
import sumra.net.app.infinity_card.widgets.CustomSnackBar.CustomSnackBar

class FrgVerifyPhone : MvpAppCompatFragment(R.layout.frg_auth_phone), IViewVerifyPhone {

    @InjectPresenter
    lateinit var mPrPhone: PrVerifyPhone

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cppNumberPhoneInput.registerCarrierNumberEditText(txtFrgLoginUserPhone)

        btnFrgVerifyPhone.apply {
            setOnClickListener {
                if(txtFrgLoginUserPhone.text.isNullOrEmpty()){
                    CustomSnackBar.make(
                        view = rootFrgAuthPhone,
                        typeMessage = CustomSnackBar.EMessageType.ERROR,
                        header = resources.getString(R.string.txt_login_section_header_phone),
                        message = resources.getString(R.string.error_phone_not_number),
                        duration = Snackbar.LENGTH_SHORT,
                        listener = null,
                        action_lable = null
                    )?.show()
                }else {
                    hideKeyboard()
                    mPrPhone.sendPhoneForVerify(
                        "_000_",
                        "${cppNumberPhoneInput.selectedCountryCode}${
                            txtFrgLoginUserPhone.text.toString()
                                .replace(" ", "")
                        }"
                    )
                }
            }
        }
    }

    override fun verifyPhoneSuccess() {
        val bundle = Bundle()
        bundle.putString(
            EKeys.AUTH_USER_PHONE,
            "${cppNumberPhoneInput.selectedCountryCode}${
                txtFrgLoginUserPhone.text.toString()
                    .replace(" ", "")
            }"
        )
        bundle.putString(EKeys.AUTH_USER_CODE, mPrPhone.getCode())
        bundle.putString(EKeys.AUTH_USER_CODE_ID, mPrPhone.getCodeID())
        txtFrgLoginUserPhone.inputText("")
        Navigation.findNavController(
            requireActivity(),
            R.id.nav_host_authorization
        ).navigate(R.id._frg_auth_code_verify, bundle)
    }

    override fun verifyPhoneFailure() {
        CustomSnackBar.make(
            view = rootSignUpAndSignIn,
            typeMessage = CustomSnackBar.EMessageType.ERROR,
            header = resources.getString(R.string.txt_login_section_header_phone),
            message = resources.getString(R.string.error_phone_verification),
            duration = Snackbar.LENGTH_SHORT,
            listener = null,
            action_lable = null
        )?.show()
    }

    override fun showProgress(state: Boolean) {
        when (state) {
            true ->
                AppCard.showProgress()
            else ->
                AppCard.hideProgress()
        }
    }

}