package sumra.net.app.infinity_card.views

import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType
import sumra.net.app.infinity_card.models.responses._jsonInfinityError

@StateStrategyType ( value = AddToEndSingleStrategy::class)
interface IViewCountries : IViewCommon {
    fun fetchAllCountrySuccess()
    fun fetchAllCountryFailure(error: _jsonInfinityError.InfinityError)
}