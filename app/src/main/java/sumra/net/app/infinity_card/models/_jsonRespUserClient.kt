package sumra.net.app.infinity_card.models


import com.google.gson.annotations.SerializedName

data class _jsonRespUserClient(
    @SerializedName("current_page")
    val currentPage: Int,
    @SerializedName("data")
    val clients: List<Client>,
    @SerializedName("fields")
    val fields: List<Field>,
    @SerializedName("first_page_url")
    val firstPageUrl: String,
    @SerializedName("from")
    val from: Int,
    //url pagination
    @SerializedName("last_page")
    val lastPage: Int,
    @SerializedName("last_page_url")
    val lastPageUrl: String,
    @SerializedName("next_page_url")
    val nextPageUrl: String,
    @SerializedName("path")
    val path: String,
    @SerializedName("per_page")
    val perPage: String,
    @SerializedName("prev_page_url")
    val prevPageUrl: Any,
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("to")
    val to: Int,
    @SerializedName("total")
    val total: Int
)

data class Client(
    @SerializedName("country")
    val country: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("id")
    val id: Long,
    @SerializedName("name")
    val name: String,
    @SerializedName("status")
    val status: Int,
    @SerializedName("tariff_id")
    val tariffId: Int,
    @SerializedName("type_id")
    val typeId: Int
)

data class Field(
    @SerializedName("key")
    val key: String,
    @SerializedName("label")
    val label: String
)