package sumra.net.app.infinity_card.presenter

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import moxy.MvpPresenter
import sumra.net.app.infinity_card.repositoryes.RepositoryCheck
import sumra.net.app.infinity_card.views.IViewCheckCard

class PrCardCheck:MvpPresenter<IViewCheckCard>() {

    private val mRepositoryCheck = RepositoryCheck()

    fun fetchCardCheck(token: String, arg: String){
        viewState.showProgress(true)
        CoroutineScope(Dispatchers.IO).launch {
            val resp = mRepositoryCheck.fetchCheckCardNet(token, arg).await()
            if(!resp.isNotEmpty()){
                withContext(Dispatchers.Main){
                    viewState.fetchCardCheckSuccess()
                }
            }else{
                withContext(Dispatchers.Main){
                    viewState.fetchCardCheckFailure()
                }
            }
            withContext(Dispatchers.Main){
                viewState.showProgress(false)
            }
        }
    }

}