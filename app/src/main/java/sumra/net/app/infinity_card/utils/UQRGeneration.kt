package sumra.net.app.infinity_card.utils

import android.graphics.Bitmap
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.journeyapps.barcodescanner.BarcodeEncoder

object UQRGeneration {
    fun generationQRCode(args: String, sizeQrCode: Int): Bitmap? {
        val multiFormatWriter = MultiFormatWriter()
        var bitmap : Bitmap? = null
        try {
            val bitMatrix =
                multiFormatWriter.encode(
                    args,
                    BarcodeFormat.QR_CODE,
                    sizeQrCode,
                    sizeQrCode
                )
            val barcodeEncoder = BarcodeEncoder()
            bitmap = barcodeEncoder.createBitmap(bitMatrix)
        } catch (e: WriterException) {
            e.printStackTrace()
        }
        return bitmap
    }
}