package sumra.net.app.infinity_card.models.responses

import com.google.gson.annotations.SerializedName

data class _respErrorMax(
    @SerializedName("type")
    val type: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("message")
    val message: String
)

//data class Data(
//    @SerializedName("message")
//    val message: String,
//    @SerializedName("title")
//    val title: String,
//    @SerializedName("type")
//    val type: String
//)

//{
//  "data": {
//    "type": "error",
//    "title": "Auth error",
//    "message": "Unauthorized access"
//  }
//}