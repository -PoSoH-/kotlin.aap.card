package sumra.net.app.infinity_card.core

object NetError{
    fun error(type:Int):String{
        return when(type){
            400 -> "Bad Request"
            401 -> "Unauthorized"
            402 -> "Payment Required"
            403 -> "Forbidden"
            404 -> "Not Found"
            405 -> "Method Not Allowed"
            500 -> "A server error has occurred"
            else -> "An unknown error occurred"
        }
    }
}