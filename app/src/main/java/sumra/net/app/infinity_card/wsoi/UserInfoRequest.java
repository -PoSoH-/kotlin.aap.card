package sumra.net.app.infinity_card.wsoi;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.example.domain.wsoi.WalletConst;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;

import okio.Okio;

/**
 * This class facilitates the invoking of userinfo endpoint of WSO2 IS in order to obtain user information.
 */
public class UserInfoRequest {
    public static final AtomicReference<JSONObject> userInfoJson = new AtomicReference<>();
    private static final String TAG = UserInfoRequest.class.getSimpleName();
    private static final AtomicReference<WeakReference<UserInfoRequest>> instance =
            new AtomicReference<>(new WeakReference<UserInfoRequest>(null));

    private final CountDownLatch authIntentLatch = new CountDownLatch(1);

    private ConfigManager configuration;
    private boolean val = false;

    private UserInfoRequest() {

    }

    /**
     * Returns an instance of the UserInfoRequest class.
     *
     * @return UserInfoRequest instance.
     */
    public static UserInfoRequest getInstance() {

        UserInfoRequest userInfoRequest = instance.get().get();
        if (userInfoRequest == null) {
            userInfoRequest = new UserInfoRequest();
            instance.set(new WeakReference<>(userInfoRequest));
        }
        return userInfoRequest;
    }

    /**
     * Fetches user information by invoking the userinfo endpoint of the WSO2 IS.
     *
     * @param accessToken Access token of the current session.
     * @param context Application context.
     * @param user User object that stores details of the user.
     * @return Returns a boolean to represent success or failure of the user info fetch.
     */
    public boolean fetchUserInfo(String accessToken, Context context, User user) {

        configuration = ConfigManager.getInstance(context);
        ExecutorService executor = Executors.newSingleThreadExecutor();

        URL userInfoEndpoint;

        try {
            userInfoEndpoint = new URL(configuration.getUserInfoEndpointUri().toString());

        } catch (MalformedURLException urlEx) {
            Log.e(TAG, "Failed to construct user info endpoint URL: ", urlEx);
            userInfoJson.set(null);
            return false;
        }

        executor.submit(() -> {
            try {
                HttpURLConnection conn;
                if (configuration.isHttpsRequired()) {
                    conn = (HttpURLConnection) userInfoEndpoint.openConnection();
                } else {
                    conn = ConnectionBuilderForTesting.INSTANCE.openConnection(Uri.parse(userInfoEndpoint.toString()));
                }
                conn.setRequestProperty(WalletConst.AUTHORIZATION, WalletConst.BEARER + accessToken);
                conn.setInstanceFollowRedirects(false);
                String response = Okio.buffer(Okio.source(conn.getInputStream())).readString(Charset.forName(WalletConst.UTF_8));
                userInfoJson.set(new JSONObject(response));

                // Sets values for the user object.
                user.setUsername(userInfoJson.get().getString("sub"));
                user.setEmail(userInfoJson.get().getString("sub"));
                val = true;

            } catch (IOException ioEx) {
                Log.e(TAG, "Network error when querying userinfo endpoint: ", ioEx);

            } catch (JSONException jsonEx) {
                Log.e(TAG, "Failed to parse userinfo response: ", jsonEx);

            } finally {
                authIntentLatch.countDown();
            }
        });

        try {
            authIntentLatch.await();
        } catch (InterruptedException ex) {
            Log.w(TAG, "Interrupted while waiting for auth intent: ", ex);
        }
        return val;
    }
}
