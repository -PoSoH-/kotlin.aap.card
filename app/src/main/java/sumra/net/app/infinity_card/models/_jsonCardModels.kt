package sumra.net.app.infinity_card.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class _jsonCardModels(
    @SerializedName("current_page")
    val currentPage: Int,
    @SerializedName("data")
    val cardTypes: List<CardTypeItem>,
    @SerializedName("fields")
    val fields: List<CardKey>,
    @SerializedName("first_page_url")
    val firstPageUrl: String,
    @SerializedName("from")
    val from: Int,
    @SerializedName("last_page")
    val lastPage: Int,
    @SerializedName("last_page_url")
    val lastPageUrl: String,
    @SerializedName("next_page_url")
    val nextPageUrl: String,
    @SerializedName("path")
    val path: String,
    @SerializedName("per_page")
    val perPage: Int,
    @SerializedName("prev_page_url")
    val prevPageUrl: String,
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("to")
    val to: Int,
    @SerializedName("total")
    val total: Int
)  : Serializable

data class CardTypeItem(
    @SerializedName("currency")
    val currency: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("id")
    val id: Long,
    @SerializedName("months")
    val months: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("price_finish")
    val priceFinish: String,
    @SerializedName("price_start")
    val priceStart: String,
    @SerializedName("series")
    val series: Series?,
    @SerializedName("series_id")
    val seriesId: Long,
    var countSend: Int = 0
) : Serializable

data class Series(
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("id")
    val id: Long,
    @SerializedName("name")
    val name: String,
    @SerializedName("updated_at")
    val updatedAt: String
) : Serializable

data class CardKey(
    @SerializedName("key")
    val key: String,
    @SerializedName("label")
    val label: String
) : Serializable
