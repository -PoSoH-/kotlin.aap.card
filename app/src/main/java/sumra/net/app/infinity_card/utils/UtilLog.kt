package sumra.net.app.infinity_card.utils

import android.util.Log
import android.view.View
import com.google.android.material.snackbar.Snackbar
import sumra.net.app.infinity_card.BuildConfig
import sumra.net.app.infinity_card.R
import sumra.net.app.infinity_card.utils.extention.showSnackBarError
import sumra.net.app.infinity_card.widgets.CustomSnackBar.CustomSnackBar

class UtilLog {
    companion object{
        fun error(tag:String, text:String){
            if(BuildConfig.DEBUG) Log.e(tag, text)
        }
        fun error(tag:String, text:String, root: View){
            if(BuildConfig.DEBUG) {
                Log.e(tag, text)
                CustomSnackBar.make(
                    view = root,
                    message = text,
                    duration = Snackbar.LENGTH_SHORT,
                    listener = null,
                    action_lable = "",
                    typeMessage = CustomSnackBar.EMessageType.ERROR,
                    header = "$tag"
                )?.show()
            }
        }
        fun info(tag:String, text:String){
            if(BuildConfig.DEBUG) Log.i(tag, text)
        }
        fun info(tag:String, text:String, root: View){
            if(BuildConfig.DEBUG) {
                Log.i(tag, text)
                CustomSnackBar.make(
                    view = root,
                    message = text,
                    duration = Snackbar.LENGTH_SHORT,
                    listener = null,
                    action_lable = "",
                    typeMessage = CustomSnackBar.EMessageType.OTHER,
                    header = "$tag"
                )?.show()
            }
        }
    }
}