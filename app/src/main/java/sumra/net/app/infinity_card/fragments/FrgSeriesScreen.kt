package sumra.net.app.infinity_card.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.frg_series_screen.*
import moxy.MvpAppCompatFragment
import sumra.net.app.infinity_card.R
import sumra.net.app.infinity_card.adapters.AdapterCardSeries
import sumra.net.app.infinity_card.interfaces.ISeriesItemSelectListener
import sumra.net.app.infinity_card.models.CardTypeItem
import sumra.net.app.infinity_card.enums.EKeys

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FrgSeriesScreen : MvpAppCompatFragment(R.layout.frg_series_screen) {

//    override fun onCreateView(
//        inflater: LayoutInflater, container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View? {
//        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.frg_all_infinity_card, container, false)
//    }

    private val mAdapterSeries = AdapterCardSeries(object: ISeriesItemSelectListener {
        override fun cardItemSelect(itemSelect: CardTypeItem) {
//            TODO("Not yet implemented")
        }
    })

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val args = arguments?.getSerializable(EKeys.CARD_TYPES_SHOW) as ArrayList<CardTypeItem>

        mAdapterSeries.bindCard(args = args)
        list_series_card_types_jfhjdhfjsdhfj.apply {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            adapter = mAdapterSeries
        }

        Toast.makeText(requireContext(), "${args.size}", Toast.LENGTH_LONG).show()

//        val r = Random.nextInt(0, 255)
//        val g = Random.nextInt(0, 255)
//        val b = Random.nextInt(0, 255)
//        val col = Color.argb(255, r, g, b).toString()
//        val color = Color.parseColor("#$col")
//        mmm.setBackgroundColor(color)
    }
}