package sumra.net.app.infinity_card.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import sumra.net.app.infinity_card.R
import sumra.net.app.infinity_card.adapters.holders.HolderCardSeries
import sumra.net.app.infinity_card.interfaces.ISeriesItemSelectListener
import sumra.net.app.infinity_card.models.CardTypeItem

class AdapterCardSeries(val listener: ISeriesItemSelectListener): RecyclerView.Adapter<HolderCardSeries>() {

    private val series = mutableListOf<CardTypeItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderCardSeries {
        return HolderCardSeries(LayoutInflater.from(parent.context).inflate(R.layout.cell_card_series_show, parent, false))
    }

    override fun onBindViewHolder(holder: HolderCardSeries, position: Int) {
        holder.bindSeries(this.series[position], listener)
    }

    override fun getItemCount(): Int {
        return this.series.size
    }

    fun bindCard(args: ArrayList<CardTypeItem>){
        this.series.clear()
        this.series.addAll(args)
        notifyDataSetChanged()
    }
}