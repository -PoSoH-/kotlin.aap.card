package sumra.net.app.infinity_card.enums

import sumra.net.app.infinity_card.R

enum class EOrderStatus (val code: Int, val value: String, val color: Int){
    CREATED (code = 1, value = "CREATED", color = R.color.colStatusCreate),
    PROCESS (code = 2, value = "PROCESSED", color = R.color.colStatusProcess),
    COMPLETED (code = 3, value = "COMPLETED", color = R.color.colStatusComplete),
    FAILED  (code = 4, value = "FAILED", color = R.color.colStatusFailed);

    companion object{
        fun checkColor(status: Int): Int{
            return when(status){
                CREATED.code -> CREATED.color
                PROCESS.code -> PROCESS.color
                COMPLETED.code -> COMPLETED.color
                else -> FAILED.color
            }
        }
        fun checkStatus(status: Int): String{
            return when(status){
                CREATED.code -> CREATED.value
                PROCESS.code -> PROCESS.value
                COMPLETED.code -> COMPLETED.value
                else -> FAILED.value
            }
        }
    }
}