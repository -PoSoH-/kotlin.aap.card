package sumra.net.app.infinity_card.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.frg_clients_screen.*
import kotlinx.android.synthetic.main.frg_profile_contacts.*
import kotlinx.android.synthetic.main.frg_profile_screen.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter
import sumra.net.app.infinity_card.AppCard
import sumra.net.app.infinity_card.R
import sumra.net.app.infinity_card.adapters.AdapterClientsList
import sumra.net.app.infinity_card.presenter.PClientUsers
import sumra.net.app.infinity_card.utils.UtilLog
import sumra.net.app.infinity_card.views.IViewClientUsers

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FrgProfileContacts : MvpAppCompatFragment(R.layout.frg_profile_contacts),
    IViewClientUsers{

    private val TAG = this::class.java.simpleName

    @InjectPresenter
    lateinit var mPrClients : PClientUsers

    private val mAdapterClients = AdapterClientsList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppCard.hideBottomNavView()
        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true /* enabled by default */) {
                override fun handleOnBackPressed() {
                    UtilLog.error(TAG, "BackPressed -> Click")
                    onBackPressed()
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // setup list clients
        listRecProfileContacts.apply {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            adapter = mAdapterClients
        }

        mPrClients.fetchClientUsers()
    }

    override fun loadClientsSuccess() {
        Toast.makeText(requireContext(), "Success", Toast.LENGTH_SHORT).show()
        mAdapterClients.bindClientsList(mPrClients.getClients())
    }

    override fun loadClientsFailure(error: String?) {
        when (error){
            null -> Toast.makeText(requireContext(), "Error", Toast.LENGTH_SHORT).show()
            else -> Toast.makeText(requireContext(), error, Toast.LENGTH_SHORT).show()
        }
    }

    override fun showProgress(state: Boolean) {
        when(state){
            true -> AppCard.showProgress() // Toast.makeText(requireContext(), "SHOW PROGRESS", Toast.LENGTH_SHORT).show()
            else -> AppCard.hideProgress() // Toast.makeText(requireContext(), "HIDE PROGRESS", Toast.LENGTH_SHORT).show()
        }
    }

    private fun onBackPressed(){
        UtilLog.info(TAG
            , "Button back click..."
            , rootContactElement)
        AppCard.showBottomNavView()
        Navigation.findNavController(
            requireActivity(),
            R.id.nav_host_profile
        ).popBackStack() //(R.id._fragment_profile_screen, false)
//        AppCard.getNavigation().popBackStack()
    }
}