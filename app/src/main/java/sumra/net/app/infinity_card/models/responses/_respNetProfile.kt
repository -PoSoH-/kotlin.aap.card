package sumra.net.app.infinity_card.models.responses

import sumra.net.app.infinity_card.models.User

class _respNetProfile(val success: Boolean, val data: User?, val error: String?)