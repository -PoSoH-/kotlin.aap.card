package sumra.net.app.infinity_card.fragments

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.frg_card_pay_screen.*
import kotlinx.android.synthetic.main.lay_balance_show.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter
import sumra.net.app.infinity_card.AppCard
import sumra.net.app.infinity_card.R
import sumra.net.app.infinity_card.adapters.AdapterCardTypeSelect
import sumra.net.app.infinity_card.enums.ECurrency
import sumra.net.app.infinity_card.enums.EKeys
import sumra.net.app.infinity_card.interfaces.ICardTypeSelectListener
import sumra.net.app.infinity_card.models.Balance
import sumra.net.app.infinity_card.models.CardTypeItem
import sumra.net.app.infinity_card.presenter.PrBuyCard
import sumra.net.app.infinity_card.presenter.PrCards
import sumra.net.app.infinity_card.utils.UString
import sumra.net.app.infinity_card.utils.extention.hideUtoD
import sumra.net.app.infinity_card.utils.extention.showDtoU
import sumra.net.app.infinity_card.utils.extention.showUtoD
import sumra.net.app.infinity_card.views.IViewCard
import sumra.net.app.infinity_card.views.IViewCards

class FrgCardPayScreen : MvpAppCompatFragment(R.layout.frg_card_pay_screen)
    , IViewCard
    , IViewCards{

    @InjectPresenter
    lateinit var mPrBuyCard : PrBuyCard
    @InjectPresenter
    lateinit var mPrCardType : PrCards

    private val mBalance = mutableListOf<Balance>()
//    private lateinit var mCurrencyBalance : Balance

    private val mCardTypeSelectAdapter = AdapterCardTypeSelect(object : ICardTypeSelectListener {
        override fun cardItemTypeSelect(itemTypeSelect: CardTypeItem, typeOperation: Boolean) {
            if(typeOperation) {
                mBalance[0].balance = (mBalance[0].balance.toDouble() - itemTypeSelect.priceStart.toDouble()).toString()
                mPrBuyCard.addTypeCard(itemTypeSelect.id.toString())
            }
            else {
                mBalance[0].balance = (mBalance[0].balance.toDouble() + itemTypeSelect.priceStart.toDouble()).toString()
                mPrBuyCard.removeTypeCard(itemTypeSelect.id.toString())
            }
            mBalance.forEach {
                updateBalance(it.currency, it.balance)
            }
            updateNotify(mBalance[0].balance.toDouble())
            buttonGenerateCardState()
            text_replenish_balance_show.apply {
                setOnClickListener {  }
                setOnLongClickListener(object:View.OnLongClickListener{override fun onLongClick(v: View?) = true })
            }
        }
    })

    private fun updateNotify(data: Double){
        mCardTypeSelectAdapter.updateBalance(data)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mBalance.clear()
        mBalance.addAll(requireArguments().get(EKeys.PROF_BALANCE) as List<Balance>)

        list_card_type_by_selected.apply {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            adapter = mCardTypeSelectAdapter
        }

        mPrCardType.fetchCardTypes("_000_")

        mBalance.forEach {
            if(it.balance.isNullOrEmpty()){
                infinity_card_bay_card_balance.findViewById<TextView>(R.id.lbl_all_cards_balance_aa).text = ECurrency.configureResult(ECurrency.USA.compare, "0.0")
                infinity_card_bay_card_balance.findViewById<TextView>(R.id.lbl_all_cards_balance_bb).text = ECurrency.configureResult(ECurrency.EUR.compare, "0.0")
                infinity_card_bay_card_balance.findViewById<TextView>(R.id.lbl_all_cards_balance_cc).text = ECurrency.configureResult(ECurrency.GBP.compare, "0.0")
            }else {
                updateBalance(it.currency, it.balance)
            }
        }

        action_button_buy_select_card.apply {
            setOnClickListener {
                mPrBuyCard.postGenerateCard("_oooo_")
            }
        }

    }

    private fun updateBalance(currency: String, balance: String){
        when(currency) {
            ECurrency.USA.compare -> infinity_card_bay_card_balance.findViewById<TextView>(R.id.lbl_all_cards_balance_aa).text = ECurrency.configureResult(currency, balance)
            ECurrency.EUR.compare -> infinity_card_bay_card_balance.findViewById<TextView>(R.id.lbl_all_cards_balance_bb).text = ECurrency.configureResult(currency, balance)
            ECurrency.GBP.compare -> infinity_card_bay_card_balance.findViewById<TextView>(R.id.lbl_all_cards_balance_cc).text = ECurrency.configureResult(currency, balance)
        }

        if(!balance.isNullOrEmpty()) {
            if (UString.isEmptyBalance(balance)) text_replenish_balance_show.hideUtoD()
            else text_replenish_balance_show.showDtoU()
        }
    }

    private fun buttonGenerateCardState(){
        if(mPrBuyCard.emptyCardList()) {
            action_button_buy_select_card.setEnabled(true)// show button
        }else {
            action_button_buy_select_card.setEnabled(false)//hide button
        }
    }

    override fun fetchCardsSuccess() {
//        TODO("Not yet implemented")
    }

    override fun fetchCardsFailure(error: String) {
//        TODO("Not yet implemented")
    }

    override fun loadAllCardTypesSuccess() {
//        mBalance.forEach {
//
//        }
        mCardTypeSelectAdapter.bindTypes(mPrCardType.getListCardType(), mBalance[0])
//        mCardTypeSelectAdapter.updateBalance
    }

    override fun loadAllCardTypesFailure(error: String?) {
//        TODO("Not yet implemented")
    }

    override fun showProgress(state: Boolean) {
        when(state){
            true ->  AppCard.showProgress()
            else ->  AppCard.hideProgress()
        }
    }
}