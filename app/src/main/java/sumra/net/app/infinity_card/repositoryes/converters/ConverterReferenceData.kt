package sumra.net.app.infinity_card.repositoryes.converters

import com.google.gson.Gson
import sumra.net.app.infinity_card.models.Country
import sumra.net.app.infinity_card.models._jsonCountries
import sumra.net.app.infinity_card.models.responses._jsonInfinityError
import sumra.net.app.infinity_card.models.responses._respCountries
import sumra.net.app.infinity_card.enums.ERespCode
import java.lang.RuntimeException

object ConverterReferenceData {

    fun convertToCountriesModel(args: String): _respCountries {
        var isGreat: _jsonCountries? = null
        var isError: _jsonInfinityError? = null

        try{
            isGreat = Gson().fromJson(args, _jsonCountries::class.java)
        } catch (ex: RuntimeException){
            try{
                isError = Gson().fromJson(args, _jsonInfinityError::class.java)
            } catch (ex: RuntimeException){
                isError = _jsonInfinityError(error = _jsonInfinityError.InfinityError(
                    type = ERespCode.ERROR.type,
                    title = "",
                    message = ex.message.toString()
                ))
            }
        }
        if(isGreat.isNullOrEmpty()){
            return _respCountries(success = "error", error = isError!!.error!!, data = null)
        }else{
            return _respCountries( success = "success", data = isGreat as List<Country>, error = null)
        }
        return _respCountries(success = "error", data = null, error = null)
    }

}