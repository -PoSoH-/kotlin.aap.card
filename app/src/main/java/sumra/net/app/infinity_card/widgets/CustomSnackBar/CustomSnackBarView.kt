package sumra.net.app.infinity_card.widgets.CustomSnackBar

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.animation.OvershootInterpolator
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.material.snackbar.ContentViewCallback
import kotlinx.android.synthetic.main.lay_custom_snack_bar_show.view.*
import sumra.net.app.infinity_card.R

class CustomSnackBarView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr), ContentViewCallback {

    val tvMessage: TextView
    val tvHeader: TextView
    val tvAction: Button
    val imLeft: ImageView
    val layRoot: ConstraintLayout

    init {
        View.inflate(context, R.layout.lay_custom_snack_bar_show, this)
        clipToPadding = false
        this.tvMessage = findViewById(R.id.txtInfinityCardLabelBody)
        this.tvHeader = findViewById(R.id.txtInfinityCardLabelHeader)
        this.tvAction = findViewById(R.id.tv_action)
        this.imLeft = findViewById(R.id.imgInfinityCardIconApp)
        this.layRoot = findViewById(R.id.infinityCardContainer)
    }

    override fun animateContentIn(delay: Int, duration: Int) {
        val scaleX = ObjectAnimator.ofFloat(imgInfinityCardIconApp, View.SCALE_X, 0f, 1f)
        val scaleY = ObjectAnimator.ofFloat(imgInfinityCardIconApp, View.SCALE_Y, 0f, 1f)
        val animatorSet = AnimatorSet().apply {
            interpolator = OvershootInterpolator()
            setDuration(300)
            playTogether(scaleX, scaleY)
        }
        animatorSet.start()
    }

    override fun animateContentOut(delay: Int, duration: Int) {
    }
}