package sumra.net.app.infinity_card.repositoryes

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import sumra.net.app.infinity_card.core.NetFiles
import sumra.net.app.infinity_card.models.responses._respNetFiles
import sumra.net.app.infinity_card.repositoryes.converters.ConverterFiles

class RepositoryFiles {

//    fun getFilesAvatar(token: String, image: String, name: String): Deferred<_respNetFiles>{
//        return GlobalScope.async {
//            ConverterFiles.convertRespToLink(NetFiles.postFiles(token, ConverterFiles.convertLinkFotRequest(image, name)))
//        }
//    }

    fun postFilesAvatar(token: String, image: String, name: String): Deferred<_respNetFiles>{
        return GlobalScope.async {
            ConverterFiles.convertRespToLink(NetFiles.postFiles(token, ConverterFiles.convertLinkForRequest(image, name)))
        }
    }

//    fun patchFilesAvatar(token: String, image: String, name: String): Deferred<_respNetFiles>{
//
//        return GlobalScope.async {
//
//        }
//    }
//
//    fun deleteFilesAvatar(token: String, image: String, name: String): Deferred<_respNetFiles>{
//
//        return GlobalScope.async {
//
//        }
//    }

}