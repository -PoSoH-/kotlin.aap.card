package sumra.net.app.infinity_card.repositoryes.converters

import com.google.gson.Gson
import sumra.net.app.infinity_card.models._jsonCardModels
import sumra.net.app.infinity_card.models.responses._jsonInfinityError
import sumra.net.app.infinity_card.models.responses._respCardTypes
import sumra.net.app.infinity_card.enums.ERespCode
import sumra.net.app.infinity_card.models.requests.ModelCardGenerate
import java.lang.RuntimeException

object ConverterCard {

    fun convertToUseApp(args: String): _respCardTypes {  //ArrayList<CardTypeItem> {
        var isGreat: _jsonCardModels? = null
        var isError: _jsonInfinityError? = null
        try {
            isGreat = Gson().fromJson(args, _jsonCardModels::class.java)
        }catch (isGreatError: RuntimeException){
            try {
                isError = Gson().fromJson(args, _jsonInfinityError::class.java)
            }catch (ex: RuntimeException){
                isError = _jsonInfinityError(
                    error = _jsonInfinityError.InfinityError(
                        message = ex.message.toString(),
                        type = ERespCode.ERROR.type,
                        title = ""))
            }
        }

        if(isGreat == null){
            // error block
            if(isError != null) _respCardTypes(success = isError.error!!.type!!, data = null, error = isError.error!!.message)
        } else {
            if(isGreat.success) return _respCardTypes(success = ERespCode.SUCCESS.type, data = isGreat, error = null)
        }
        return _respCardTypes(success = ERespCode.ERROR.type, data = null, error = null)
    }

    fun fromTypeToSend(args: ModelCardGenerate) : String {
        return Gson().toJson(args)
    }
}