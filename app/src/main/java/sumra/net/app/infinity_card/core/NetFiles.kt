package sumra.net.app.infinity_card.core

import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.Response
import org.apache.http.HttpException
import sumra.net.app.infinity_card.BuildConfig
import sumra.net.app.infinity_card.utils.UtilLog

object NetFiles {

    private val TAG = NetFiles::class.java.simpleName

    fun getFiles(token: String, id: Int):String { // avatar code in to Base64
        val host = BuildConfig.SUMRA_API_FILES_URL.toHttpUrlOrNull()!!
        val httpUrl = HttpUrl.Builder().apply {
            scheme(EHost.HTTPS.value)
            host(host.toString())
            addPathSegment(BuildConfig.SUMRA_API_FILE_FILE)
            addPathSegment(BuildConfig.SUMRA_API_FILES_NUM)
            addPathSegment(BuildConfig.SUMRA_API_FILES_VER)
            addPathSegment("files")
            addPathSegment(id.toString())
        }.build()

        val localHostUrl = "http://192.168.188.137:8095/files/1/v1/files"   //"https://api.sumra.net/files/1/v1/files"

        try{
            val response = OkHttpClient.builder()
                .newCall(NetworkCore.GET_RequestContent(/*httpUrl.toString()*/localHostUrl)).execute()
            if(response.isSuccessful){
                return NetworkCore.resultSuccess(response)
            }else{
                return NetworkCore.resultFailure(response)
            }
        } catch (e : HttpException) {
            return NetworkCore.resultFailure(e as Response)
        }
    }

    fun postFiles(token: String, avatar: String): String { // avatar code in to Base64
        val host = BuildConfig.SUMRA_API_FILES_HOST.toHttpUrlOrNull()!!
        val httpUrl = HttpUrl.Builder().apply {
            scheme(EHost.HTTPS.value)
            host(host.host)
            addPathSegment(BuildConfig.SUMRA_API_FILE_FILE)
            addPathSegment(BuildConfig.SUMRA_API_FILES_NUM)
            addPathSegment(BuildConfig.SUMRA_API_FILES_VER)
            addPathSegment("files")
        }.build()

        UtilLog.info(TAG, httpUrl.toString())
        val localHostUrl = "http://192.168.188.137:8080/v1/files"   //"https://api.sumra.net/files/1/v1/files"

        try{
//            val response = OkHttpClient.builder()
//                .newCall(NetworkCore.POST_RequestContent(httpUrl.toString(), token, avatar)).execute()
            val response = OkHttpClient.builder()
                .newCall(NetworkCore.POST_RequestContentByUserID(localHostUrl, token, avatar)).execute()
            if(response.isSuccessful){
                return NetworkCore.resultSuccess(response)
            }else{
                return NetworkCore.resultFailure(response)
            }
        } catch (e : HttpException) {
            return NetworkCore.resultFailure(e as Response)
        }
    }

    fun patchFiles(token: String, avatar: String):String { // avatar code in to Base64
        val host = BuildConfig.SUMRA_API_FILES_URL.toHttpUrlOrNull()!!
        val httpUrl = HttpUrl.Builder().apply {
            scheme(EHost.HTTPS.value)
            host(host.toString())
            addPathSegment(BuildConfig.SUMRA_API_FILE_FILE)
            addPathSegment(BuildConfig.SUMRA_API_FILES_NUM)
            addPathSegment(BuildConfig.SUMRA_API_FILES_VER)
            addPathSegment("files")
        }.build()

        try{
            val response = OkHttpClient.builder()
                .newCall(NetworkCore.GET_RequestContent(httpUrl.toString())).execute()
            if(response.isSuccessful){
                return NetworkCore.resultSuccess(response)
            }else{
                return NetworkCore.resultFailure(response)
            }
        } catch (e : HttpException) {
            return NetworkCore.resultFailure(e as Response)
        }
    }

    fun deleteFiles(token: String, id: Int):String { // avatar code in to Base64
        val host = BuildConfig.SUMRA_API_FILES_URL.toHttpUrlOrNull()!!
        val httpUrl = HttpUrl.Builder().apply {
            scheme(EHost.HTTPS.value)
            host(host.host)
            addPathSegment(BuildConfig.SUMRA_API_FILE_FILE)
            addPathSegment(BuildConfig.SUMRA_API_FILES_NUM)
            addPathSegment(BuildConfig.SUMRA_API_FILES_VER)
            addPathSegment("files")
            addPathSegment(id.toString())
        }.build()

        try{
            val response = OkHttpClient.builder()
                .newCall(NetworkCore.DELETE_RequestContent(httpUrl.toString(), token)).execute()
            if(response.isSuccessful){
                return NetworkCore.resultSuccess(response)
            }else{
                return NetworkCore.resultFailure(response)
            }
        } catch (e : HttpException) {
            return NetworkCore.resultFailure(e as Response)
        }
    }
}