package sumra.net.app.infinity_card.adapters.holders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.cell_country_item_show.view.*
import sumra.net.app.infinity_card.interfaces.ICountryItemSelectListener
import sumra.net.app.infinity_card.models.Country

class HolderCountryItem(val cell: View): RecyclerView.ViewHolder(cell) {
    fun bindItem(country: Country, listener: ICountryItemSelectListener){
        cell.cell_country_name.text = country.label
        cell.item_cell_background.apply {
            setOnClickListener {
                listener.countryItemSelect(country)
            }
        }
    }
}