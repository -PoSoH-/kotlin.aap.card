package sumra.net.app.infinity_card.oAuth2tests

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.annotation.AnyThread
import androidx.annotation.Nullable
import net.openid.appauth.*
import org.json.JSONException
import java.lang.ref.WeakReference
import java.util.concurrent.atomic.AtomicReference
import java.util.concurrent.locks.ReentrantLock


class AuthStateManager private constructor(context: Context) {

    private val TAG = "AuthStateManager"

    private val STORE_NAME = "AuthState"
    private val KEY_STATE = "state"

    private var mPrefs: SharedPreferences? = null
    private var mPrefsLock: ReentrantLock? = null
    private var mCurrentAuthState: AtomicReference<AuthState>? = null

    init {
        mPrefs = context.getSharedPreferences(STORE_NAME, Context.MODE_PRIVATE)
        mPrefsLock = ReentrantLock()
        mCurrentAuthState = AtomicReference()
    }

    companion object {
//        @AnyThread
        fun getInstance(context: Context): AuthStateManager? {
            val INSTANCE_REF = AtomicReference(WeakReference<AuthStateManager?>(null))
            var manager = INSTANCE_REF.get().get()
            if (manager == null) {
                manager = AuthStateManager(context.applicationContext)
                INSTANCE_REF.set(WeakReference(manager))
            }
            return manager
        }
    }

//    @AnyThread
    fun getCurrent(): AuthState {
        if (mCurrentAuthState!!.get() != null) {
            return mCurrentAuthState!!.get()
        }
        val state = readState()
        return if (mCurrentAuthState!!.compareAndSet(null, state)) {
            state
        } else {
            mCurrentAuthState!!.get()
        }
    }

//    @AnyThread
    fun replace(state: AuthState): AuthState {
        writeState(state)
        mCurrentAuthState!!.set(state)
        return state
    }

//    @AnyThread
    fun updateAfterAuthorization(
        @Nullable response: AuthorizationResponse?,
        @Nullable ex: AuthorizationException?
    ): AuthState {
        val current = getCurrent()
        current.update(response, ex)
        return replace(current)
    }

//    @AnyThread
    fun updateAfterTokenResponse(
        @Nullable response: TokenResponse?,
        @Nullable ex: AuthorizationException?
    ): AuthState {
        val current = getCurrent()
        current.update(response, ex)
        return replace(current)
    }

//    @AnyThread
    fun updateAfterRegistration(
        response: RegistrationResponse?,
        ex: AuthorizationException?
    ): AuthState {
        val current = getCurrent()
        if (ex != null) {
            return current
        }
        current.update(response)
        return replace(current)
    }

//    @AnyThread
    private fun readState(): AuthState {
        mPrefsLock!!.lock()
        return try {
            val currentState = mPrefs!!.getString(KEY_STATE, null) ?: return AuthState()
            try {
                AuthState.jsonDeserialize(currentState)
            } catch (ex: JSONException) {
                Log.w(TAG, "Failed to deserialize stored auth state - discarding")
                AuthState()
            }
        } finally {
            mPrefsLock!!.unlock()
        }
    }

//    @AnyThread
    private fun writeState(@Nullable state: AuthState?) {
        mPrefsLock!!.lock()
        try {
            val editor = mPrefs!!.edit()
            if (state == null) {
                editor.remove(KEY_STATE)
            } else {
                editor.putString(KEY_STATE, state.jsonSerializeString())
            }
            check(editor.commit()) { "Failed to write state to shared prefs" }
        } finally {
            mPrefsLock!!.unlock()
        }
    }

}