//package sumra.net.app.infinity_card.openIdmanagers
//
//import android.accounts.Account
//import android.accounts.AccountManager
//import android.content.Context
//import com.github.kevinsawicki.http.HttpRequest
//import com.google.gson.Gson
//import sumra.net.app.infinity_card.R
//import sumra.net.app.infinity_card.openIdmanagers.authenticator.Authenticator
//import sumra.net.app.infinity_card.openIdmanagers.authenticator.OIDCUtils
//import java.io.IOException
//import java.net.HttpURLConnection
//
//class APIUtility {
//
//    /**
//     * Makes a GET request and parses the received JSON string as a Map.
//     */
//    @Throws(IOException::class)
//    fun getJson(context: Context, url: String?, account: Account?): Map<*, *>? {
//        val jsonString = makeRequest(context, HttpRequest.METHOD_GET, url, account)
//        return Gson().fromJson(jsonString, MutableMap::class.java)
//    }
//
//    /**
//     * Makes an arbitrary HTTP request using the provided account.
//     *
//     * If the request doesn't execute successfully on the first try, the tokens will be refreshed
//     * and the request will be retried. If the second try fails, an exception will be raised.
//     */
//    @Throws(IOException::class)
//    fun makeRequest(context: Context, method: String, url: String?, account: Account?): String {
//        return makeRequest(context, method, url, account, true)
//    }
//
//    @Throws(IOException::class)
//    private fun makeRequest(
//        context: Context, method: String, url: String?, account: Account?,
//        doRetry: Boolean
//    ): String {
//        val accountManager = AccountManager.get(context)
//        val idToken: String
//
//        // Try retrieving an ID token from the account manager. The boolean true in the invocation
//        // tells Android to show a notification if the token can't be retrieved. When the
//        // notification is selected, it will launch the intent for re-authorisation. You could
//        // launch it automatically here if you wanted to by grabbing the intent from the bundle.
//        try {
//            val futureManager = accountManager.getAuthToken(
//                account,
//                Authenticator.TOKEN_TYPE_ID, null, true, null, null
//            )
//            idToken = futureManager.result.getString(AccountManager.KEY_AUTHTOKEN).toString()
//        } catch (e: Exception) {
//            throw IOException("Could not get ID token from account.", e)
//        }
//
//        // Prepare an API request using the token
//        var request = HttpRequest(url, method)
//        request = OIDCUtils.prepareApiRequest(request, idToken)
//        return if (request.ok()) {
//            request.body()
//        } else {
//            val code: Int = request.code()
//            if (doRetry && (code == HttpURLConnection.HTTP_UNAUTHORIZED || code == HttpURLConnection.HTTP_FORBIDDEN)) {
//                // We're being denied access on the first try, let's renew the token and retry
////                val accountType = context.getString(R.string.ACCOUNT_TYPE) //com.lnikkila.oidcsample.account
////                accountManager.invalidateAuthToken(accountType, idToken)
//                makeRequest(context, method, url, account, false)
//            } else {
//                // An unrecoverable error or the renewed token didn't work either
//                throw IOException(request.code().toString() + " " + request.message())
//            }
//        }
//    }
//
//}