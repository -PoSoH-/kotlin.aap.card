package sumra.net.app.infinity_card.enums

class EKeys {
    companion object{
        const val PROF_USER = "PROFILE.USER.ARGUMENTS"
        const val PROF_CARD = "PROFILE.CARDS.ARGUMENTS"
        const val PROF_BALANCE = "PROFILE.BALANCE.ARGUMENTS"

        const val CARD_TYPES_SHOW = "ARGUMENTS.CARD.TYPE.SERIES"

        const val PROF_USER_NAME_FIRST = "ARGUMENTS.PROFILE.USER.NAME_FIRST"
        const val PROF_USER_NAME_LAST = "ARGUMENTS.PROFILE.USER.NAME_LAST"

        const val AUTH_USER_PHONE = "ARGUMENTS.AUTH.USER.PHONE"
        const val AUTH_USER_CODE  = "ARGUMENTS.AUTH.USER.CODE"
        const val AUTH_USER_NAME  = "ARGUMENTS.AUTH.USER.NAME"
        const val AUTH_USER_CODE_ID  = "ARGUMENTS.AUTH.USER.CODE.ID"

        const val PROFILE_ORDER_ITEM_ID = "ARGUMENTS.PROFILE.ORDER.ITEM.ID"


        const val PROF_DATA = "PROFILE..."
        const val PROF_OTHER = "PROFILE..."
    }
}