package sumra.net.app.infinity_card.core

import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.Response
import org.apache.http.HttpException
import sumra.net.app.infinity_card.BuildConfig

object NetReferenceAll {

    private val localHostUrl = "http://192.168.188.137:8095/v1/infinity/reference/countries"

    fun getCountries(token: String):String {
        val host = BuildConfig.SUMRA_API_INITY_URL.toHttpUrlOrNull()!!
//        val host = "192.168.188.137:8095".toHttpUrlOrNull()!!
        val httpUrl = HttpUrl.Builder().apply {
            scheme(EHost.HTTPS.value)
            host(host.host)
//            addPathSegment(BuildConfig.SUMRA_VER_INTY_URL)
            addPathSegment("infinity")  // /infinity/reference/countries
            addPathSegment("reference")
            addPathSegment("countries")
        }.build()

        try{
//            val response = OkHttpClient.builder()
//                .newCall(NetworkCore.GET_RequestContent(httpUrl.toString())).execute()
            val response = OkHttpClient.builder()
                .newCall(NetworkCore.GET_RequestContentByUserId("${localHostUrl}")).execute()
            if(response.isSuccessful){
                return NetworkCore.resultSuccess(response)
            }else{
                return NetworkCore.resultFailure(response)
            }
        } catch (e : HttpException) {
            return NetworkCore.resultFailure(e as Response)
        }
    }
}