package sumra.net.app.infinity_card.interfaces

import sumra.net.app.infinity_card.models.Country

interface ICountryItemSelectListener {
    fun countryItemSelect(itemSelect: Country)
}