package sumra.net.app.infinity_card.repositoryes

import com.google.gson.Gson
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import sumra.net.app.infinity_card.core.NetClientUsers
import sumra.net.app.infinity_card.models._jsonRespUserClient

class RepositoryClientUsers {

    suspend fun fetchClientUsers() : Deferred<_jsonRespUserClient> {
        val resp =  MockClients.getClients()  // NetClientUsers.getClientUsers(null, null, null)
        return GlobalScope.async {Gson().fromJson<_jsonRespUserClient>(resp, _jsonRespUserClient::class.java)}
    }

    private class MockClients{
        companion object{
            fun getClients() = "{\n" +
                    "  \"success\": true,\n" +
                    "  \"fields\": [\n" +
                    "    {\n" +
                    "      \"key\": \"id\",\n" +
                    "      \"label\": \"ID\"\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"key\": \"name\",\n" +
                    "      \"label\": \"Name\"\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"key\": \"description\",\n" +
                    "      \"label\": \"Description\"\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"key\": \"type_id\",\n" +
                    "      \"label\": \"Type of user\"\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"key\": \"tariff_id\",\n" +
                    "      \"label\": \"Tariff\"\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"key\": \"country\",\n" +
                    "      \"label\": \"Country\"\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"key\": \"status\",\n" +
                    "      \"label\": \"Status\"\n" +
                    "    }\n" +
                    "  ],\n" +
                    "  \"current_page\": 1,\n" +
                    "  \"data\": [\n" +
                    "    {\n" +
                    "      \"id\": 14,\n" +
                    "      \"name\": \"df\",\n" +
                    "      \"description\": \"fdfdfd\",\n" +
                    "      \"type_id\": 2,\n" +
                    "      \"tariff_id\": 1,\n" +
                    "      \"country\": \"ad\",\n" +
                    "      \"status\": 1\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"id\": 15,\n" +
                    "      \"name\": \"dfdf\",\n" +
                    "      \"description\": \"df\",\n" +
                    "      \"type_id\": 2,\n" +
                    "      \"tariff_id\": 1,\n" +
                    "      \"country\": \"ai\",\n" +
                    "      \"status\": 1\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"id\": 22,\n" +
                    "      \"name\": \"dfdfd\",\n" +
                    "      \"description\": \"xcxcxcx\",\n" +
                    "      \"type_id\": 1,\n" +
                    "      \"tariff_id\": 4,\n" +
                    "      \"country\": \"az\",\n" +
                    "      \"status\": 1\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"id\": 13,\n" +
                    "      \"name\": \"dfdfd\",\n" +
                    "      \"description\": \"fdfd\",\n" +
                    "      \"type_id\": 2,\n" +
                    "      \"tariff_id\": 1,\n" +
                    "      \"country\": \"bd\",\n" +
                    "      \"status\": 1\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"id\": 9,\n" +
                    "      \"name\": \"dfdfd\",\n" +
                    "      \"description\": \"dfdf\",\n" +
                    "      \"type_id\": 1,\n" +
                    "      \"tariff_id\": 1,\n" +
                    "      \"country\": \"aw\",\n" +
                    "      \"status\": 1\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"id\": 7,\n" +
                    "      \"name\": \"dfdfdf\",\n" +
                    "      \"description\": \"fdfd\",\n" +
                    "      \"type_id\": 1,\n" +
                    "      \"tariff_id\": 1,\n" +
                    "      \"country\": \"dz\",\n" +
                    "      \"status\": 1\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"id\": 2,\n" +
                    "      \"name\": \"dfdfed\",\n" +
                    "      \"description\": \"dfdfd\",\n" +
                    "      \"type_id\": 1,\n" +
                    "      \"tariff_id\": 1,\n" +
                    "      \"country\": \"au\",\n" +
                    "      \"status\": 1\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"id\": 4,\n" +
                    "      \"name\": \"eere\",\n" +
                    "      \"description\": \"sdsds\",\n" +
                    "      \"type_id\": 2,\n" +
                    "      \"tariff_id\": 1,\n" +
                    "      \"country\": \"dz\",\n" +
                    "      \"status\": 1\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"id\": 3,\n" +
                    "      \"name\": \"fdfdf\",\n" +
                    "      \"description\": \"fdfdf\",\n" +
                    "      \"type_id\": 2,\n" +
                    "      \"tariff_id\": 1,\n" +
                    "      \"country\": \"ad\",\n" +
                    "      \"status\": 1\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"id\": 8,\n" +
                    "      \"name\": \"fdfdfd\",\n" +
                    "      \"description\": \"fdfd\",\n" +
                    "      \"type_id\": 2,\n" +
                    "      \"tariff_id\": 1,\n" +
                    "      \"country\": \"ao\",\n" +
                    "      \"status\": 1\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"id\": 5,\n" +
                    "      \"name\": \"fgfgf\",\n" +
                    "      \"description\": \"fgfgf\",\n" +
                    "      \"type_id\": 1,\n" +
                    "      \"tariff_id\": 1,\n" +
                    "      \"country\": \"al\",\n" +
                    "      \"status\": 1\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"id\": 12,\n" +
                    "      \"name\": \"gfgfgf\",\n" +
                    "      \"description\": \"fgfgf\",\n" +
                    "      \"type_id\": 2,\n" +
                    "      \"tariff_id\": 1,\n" +
                    "      \"country\": \"au\",\n" +
                    "      \"status\": 1\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"id\": 10,\n" +
                    "      \"name\": \"Ihor Porokhnenko\",\n" +
                    "      \"description\": \"gfgf\",\n" +
                    "      \"type_id\": 2,\n" +
                    "      \"tariff_id\": 1,\n" +
                    "      \"country\": \"az\",\n" +
                    "      \"status\": 1\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"id\": 11,\n" +
                    "      \"name\": \"Ihor Porokhnenko\",\n" +
                    "      \"description\": \"gfgfgf\",\n" +
                    "      \"type_id\": 2,\n" +
                    "      \"tariff_id\": 1,\n" +
                    "      \"country\": \"ax\",\n" +
                    "      \"status\": 1\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"id\": 16,\n" +
                    "      \"name\": \"Ihor Porokhnenko\",\n" +
                    "      \"description\": \"dfdf fdfd\",\n" +
                    "      \"type_id\": 2,\n" +
                    "      \"tariff_id\": 1,\n" +
                    "      \"country\": \"co\",\n" +
                    "      \"status\": 1\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"id\": 18,\n" +
                    "      \"name\": \"Ihor Porokhnenko 22222\",\n" +
                    "      \"description\": \"папап\",\n" +
                    "      \"type_id\": 1,\n" +
                    "      \"tariff_id\": 3,\n" +
                    "      \"country\": \"as\",\n" +
                    "      \"status\": 1\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"id\": 19,\n" +
                    "      \"name\": \"Ihor Porokhnenko 66666666666\",\n" +
                    "      \"description\": \"fdg fgfgfgfg fgffgf \",\n" +
                    "      \"type_id\": 2,\n" +
                    "      \"tariff_id\": 4,\n" +
                    "      \"country\": \"bs\",\n" +
                    "      \"status\": 1\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"id\": 21,\n" +
                    "      \"name\": \"referral\",\n" +
                    "      \"description\": \"gfgfgf\",\n" +
                    "      \"type_id\": 1,\n" +
                    "      \"tariff_id\": 1,\n" +
                    "      \"country\": \"dz\",\n" +
                    "      \"status\": 1\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"id\": 1,\n" +
                    "      \"name\": \"rtrt tr trt\",\n" +
                    "      \"description\": \"tr trtrtr\",\n" +
                    "      \"type_id\": 1,\n" +
                    "      \"tariff_id\": 1,\n" +
                    "      \"country\": \"cf\",\n" +
                    "      \"status\": 1\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"id\": 6,\n" +
                    "      \"name\": \"rtrtrt\",\n" +
                    "      \"description\": \"trtrt\",\n" +
                    "      \"type_id\": 1,\n" +
                    "      \"tariff_id\": 1,\n" +
                    "      \"country\": \"dz\",\n" +
                    "      \"status\": 1\n" +
                    "    }\n" +
                    "  ],\n" +
                    "  \"first_page_url\": \"http://192.168.188.137:8095/v1/giftcards/users?page=1\",\n" +
                    "  \"from\": 1,\n" +
                    "  \"last_page\": 2,\n" +
                    "  \"last_page_url\": \"http://192.168.188.137:8095/v1/giftcards/users?page=2\",\n" +
                    "  \"next_page_url\": \"http://192.168.188.137:8095/v1/giftcards/users?page=2\",\n" +
                    "  \"path\": \"http://192.168.188.137:8095/v1/giftcards/users\",\n" +
                    "  \"per_page\": \"20\",\n" +
                    "  \"prev_page_url\": null,\n" +
                    "  \"to\": 20,\n" +
                    "  \"total\": 22\n" +
                    "}"
        }
    }
}