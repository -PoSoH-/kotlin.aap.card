package sumra.net.app.infinity_card.models.responses

import sumra.net.app.infinity_card.models.AttributeFiles

data class _respNetFiles (
    val success: String,  // type response
    val data: AttributeFiles?,    // link for file of server
    val error: String?    // message error...
)