package sumra.net.app.infinity_card.models


import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class _jsonRespProfile (
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("user")
    val user: User)
//    ,
//    @SerializedName("error")
//    val error: String?)

data class User (
    @SerializedName("id")
    val id: Long,
    @SerializedName("first_name")
    var firstName: String?,
    @SerializedName("last_name")
    var lastName: String?,
    @SerializedName("avatar")
    var avatar: String?,
    @SerializedName("description")
    var description: String?,
    @SerializedName("country")
    var country: String?,
    @SerializedName("phone")
    var phone: String?,
    @SerializedName("email")
    var email: String?,
    @SerializedName("passport_number")
    var passportNumber: String?,
    @SerializedName("passport_date_issue")
    var passportDateIssue: String?,
    @SerializedName("type_id")
    val typeId: Long,
    @SerializedName("parent_id")
    val parentId: Long,
    @SerializedName("ownership_id")
    val ownershipId: Long,
    @SerializedName("tariff_id")
    val tariffId: Long,
    @SerializedName("status")
    val status: Int,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("updated_at")
    val updatedAt: String,
    @SerializedName("contacts")
    val contacts: List<Contact>,
    @SerializedName("requisites")
    val requisites: List<Requisite>,
    @SerializedName("cards")
    val cards: List<Card>,
    @SerializedName("balances")
    val balances: List<Balance>
) : Parcelable // : Serializable
{
    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readLong(),
        parcel.readLong(),
        parcel.readLong(),
        parcel.readLong(),
        parcel.readInt(),
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.createTypedArrayList(Contact)!!,
        parcel.createTypedArrayList(Requisite)!!,
        parcel.createTypedArrayList(Card)!!,
        parcel.createTypedArrayList(Balance)!!
    )

    override fun writeToParcel (parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(firstName)
        parcel.writeString(lastName)
        parcel.writeString(avatar)
        parcel.writeString(description)
        parcel.writeString(country)
        parcel.writeString(phone)
        parcel.writeString(email)
        parcel.writeString(passportNumber)
        parcel.writeString(passportDateIssue)
        parcel.writeLong(typeId)
        parcel.writeLong(parentId)
        parcel.writeLong(ownershipId)
        parcel.writeLong(tariffId)
        parcel.writeInt(status)
        parcel.writeString(createdAt)
        parcel.writeString(updatedAt)
        parcel.writeTypedList(contacts)
        parcel.writeTypedList(requisites)
        parcel.writeTypedList(cards)
        parcel.writeTypedList(balances)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<User> {
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }
}

data class Balance (
    @SerializedName("amount")
    var balance: String,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("currency_id")
    val currency: String,
    @SerializedName("id")
    val id: Long,
    @SerializedName("updated_at")
    val updatedAt: String,
    @SerializedName("user_id")
    val userId: Long
) : Parcelable//: Serializable
{
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readLong(),
        parcel.readString()!!,
        parcel.readLong()
    ) {

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(balance)
        parcel.writeString(createdAt)
        parcel.writeString(currency)
        parcel.writeLong(id)
        parcel.writeString(updatedAt)
        parcel.writeLong(userId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Balance> {
        override fun createFromParcel(parcel: Parcel): Balance {
            return Balance(parcel)
        }

        override fun newArray(size: Int): Array<Balance?> {
            return arrayOfNulls(size)
        }
    }
}

data class Card (
    @SerializedName("activate_before")
    val activateBefore: String,
    @SerializedName("activated_at")
    val activatedAt: String,
    @SerializedName("activation_code")
    val activationCode: String,
    @SerializedName("balance")
    val balance: String,
    @SerializedName("currency")
    val currency: String,
    @SerializedName("balance_at")
    val balanceAt: String,
    @SerializedName("code")
    val code: String,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("creator_id")
    val creatorId: Long,
    @SerializedName("id")
    val id: Long,
    @SerializedName("number")
    val number: String,
    @SerializedName("numberFormatted")
    val numberFormatted: String,
    @SerializedName("owner_id")
    val ownerId: Long,
    @SerializedName("status")
    val status: Int,
    @SerializedName("type_id")
    val typeId: Long,
    @SerializedName("updated_at")
    val updatedAt: String
) : Parcelable//: Serializable
{
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readLong(),
        parcel.readLong(),
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readLong(),
        parcel.readInt(),
        parcel.readLong(),
        parcel.readString()!!
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(activateBefore)
        parcel.writeString(activatedAt)
        parcel.writeString(activationCode)
        parcel.writeString(balance)
        parcel.writeString(currency)
        parcel.writeString(balanceAt)
        parcel.writeString(code)
        parcel.writeString(createdAt)
        parcel.writeLong(creatorId)
        parcel.writeLong(id)
        parcel.writeString(number)
        parcel.writeString(numberFormatted)
        parcel.writeLong(ownerId)
        parcel.writeInt(status)
        parcel.writeLong(typeId)
        parcel.writeString(updatedAt)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Card> {
        override fun createFromParcel(parcel: Parcel): Card {
            return Card(parcel)
        }

        override fun newArray(size: Int): Array<Card?> {
            return arrayOfNulls(size)
        }
    }
}

data class Contact (
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("id")
    val id: Long,
    @SerializedName("name")
    val name: String,
    @SerializedName("updated_at")
    val updatedAt: String,
    @SerializedName("user_id")
    val userId: Long,
    @SerializedName("value")
    val value: String
) : Parcelable//: Serializable
{
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readLong(),
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readLong(),
        parcel.readString()!!
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(createdAt)
        parcel.writeString(description)
        parcel.writeLong(id)
        parcel.writeString(name)
        parcel.writeString(updatedAt)
        parcel.writeLong(userId)
        parcel.writeString(value)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Contact> {
        override fun createFromParcel(parcel: Parcel): Contact {
            return Contact(parcel)
        }

        override fun newArray(size: Int): Array<Contact?> {
            return arrayOfNulls(size)
        }
    }
}

data class Requisite (
    @SerializedName("country")
    val country: String,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("id")
    val id: Long,
    @SerializedName("name")
    val name: String,
    @SerializedName("pivot")
    val pivot: Pivot,
    @SerializedName("updated_at")
    val updatedAt: String
) : Parcelable//: Serializable
{
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readLong(),
        parcel.readString()!!,
        parcel.readParcelable(Pivot::class.java.classLoader)!!,
        parcel.readString()!!
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(country)
        parcel.writeString(createdAt)
        parcel.writeString(description)
        parcel.writeLong(id)
        parcel.writeString(name)
        parcel.writeParcelable(pivot, flags)
        parcel.writeString(updatedAt)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Requisite> {
        override fun createFromParcel(parcel: Parcel): Requisite {
            return Requisite(parcel)
        }

        override fun newArray(size: Int): Array<Requisite?> {
            return arrayOfNulls(size)
        }
    }
}

data class Pivot (
    @SerializedName("requisite_id")
    val requisiteId: Long,
    @SerializedName("user_id")
    val userId: Long,
    @SerializedName("value")
    val value: String
) : Parcelable//: Serializable
{
    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readLong(),
        parcel.readString()!!
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(requisiteId)
        parcel.writeLong(userId)
        parcel.writeString(value)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Pivot> {
        override fun createFromParcel(parcel: Parcel): Pivot {
            return Pivot(parcel)
        }

        override fun newArray(size: Int): Array<Pivot?> {
            return arrayOfNulls(size)
        }
    }
}

data class UserPatch (
    val id: Long,
    @SerializedName("first_name")
    var first_name: String,
    @SerializedName("last_name")
    var last_name: String,
    val avatar: String,
    var description: String,
    @SerializedName("country_id")
    var country_id: Long,
    var phone: String,
    var email: String,
    @SerializedName("passport_number")
    var passport_number: String,
    @SerializedName("passport_date_issue")
    var passport_date_issue: String,
    @SerializedName("type_id")
    val type_id: Long,
//    val parentId: Long,
//    val ownershipId: Long,
//    val tariffId: Long,
    val status: Int
)