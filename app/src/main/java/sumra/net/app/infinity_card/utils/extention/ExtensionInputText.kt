package sumra.net.app.infinity_card.utils.extention

import android.text.Editable
import android.widget.EditText
import androidx.appcompat.widget.AppCompatEditText
import java.lang.StringBuilder

fun EditText.inputText(text: String){
    this.text = Editable.Factory.getInstance().newEditable(text)
}
fun EditText.inputText(text: StringBuilder){
    this.text = Editable.Factory.getInstance().newEditable(text.toString())
}
fun EditText.inputHint(text: String){
    this.hint = Editable.Factory.getInstance().newEditable(text)
}


fun AppCompatEditText.inputText(text: String){
    this.text = Editable.Factory.getInstance().newEditable(text)
}
fun AppCompatEditText.inputText(text: Char){
    this.text = Editable.Factory.getInstance().newEditable(text.toString())
}
fun AppCompatEditText.inputText(text: StringBuilder){
    this.text = Editable.Factory.getInstance().newEditable(text.toString())
}