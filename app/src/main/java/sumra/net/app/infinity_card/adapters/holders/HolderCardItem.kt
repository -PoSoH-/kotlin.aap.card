package sumra.net.app.infinity_card.adapters.holders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.cell_card_show.view.*
import sumra.net.app.infinity_card.enums.ECurrency
import sumra.net.app.infinity_card.models.Card
import sumra.net.app.infinity_card.utils.UQRGeneration
import sumra.net.app.infinity_card.utils.UString

class HolderCardItem(val cell: View) : RecyclerView.ViewHolder(cell) {
    fun bind(
        card: Card, rangeCard: String, nameCard: String, nameFirst: String, nameLast: String, codeCard: String, bg: Int
    ) {

        cell.image_card_background.setImageResource(bg)

        cell.img_infinity_card_qr_code
            .setImageBitmap(
                UQRGeneration
                    .generationQRCode(
                        codeCard, cell.img_infinity_card_qr_code.height * 2
                    )
            )

        cell.cell_infinity_card_series.text = rangeCard

        cell.cell_infinity_card_amount.text = card.balance
        cell.cell_infinity_card_currency.text = ECurrency.compareCurrencyByName(card.currency)

        cell.cell_infinity_card_number.text = card.numberFormatted
        cell.cell_infinity_card_base_type.text = nameCard //"Silver"

        cell.cell_infinity_card_user_name_first.text = nameFirst
        cell.cell_infinity_card_user_name_second.text = nameLast

        cell.cell_infinity_card_date_start.text = UString.setupDateByTime(card.activatedAt)
        cell.cell_infinity_card_date_final.text = UString.setupDateNotTime(card.activateBefore)
    }
}