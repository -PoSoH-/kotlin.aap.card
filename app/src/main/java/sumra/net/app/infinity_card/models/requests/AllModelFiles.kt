package sumra.net.app.infinity_card.models.requests

import com.google.gson.annotations.SerializedName

data class ModelGetRequest(
    @SerializedName("entity")
    val entity: String,
    @SerializedName("entity_id")
    val entityId: List<String>
)

data class ModelPostRequest(
    @SerializedName("entity")
    val entity: String,
    @SerializedName("entity_id")
    val entityId: Long? = null,
    @SerializedName("image")
    val image: String,
    @SerializedName("image_name")
    val name: String
)

data class ModelPatchRequest(
    @SerializedName("ids")
    val entity: String,
    @SerializedName("entity_id")
    val entityId: Long
)

data class ModelDeleteRequest(
    @SerializedName("id")
    val id: Long
)