//package sumra.net.app.infinity_card.openIdmanagers.authenticator
//
//import android.accounts.Account
//import android.accounts.AccountAuthenticatorActivity
//import android.accounts.AccountManager
//import android.app.AlertDialog
//import android.content.Intent
//import android.graphics.Bitmap
//import android.net.Uri
//import android.os.AsyncTask
//import android.os.Bundle
//import android.text.TextUtils
//import android.util.Log
//import android.view.View
//import android.webkit.WebView
//import android.webkit.WebViewClient
//import sumra.net.app.infinity_card.R
//import sumra.net.app.infinity_card.openIdmanagers.Config
//import java.io.IOException
//
//import com.google.api.client.auth.openidconnect.IdTokenResponse
//import com.google.api.client.json.gson.GsonFactory
//
//class AuthenticatorActivity : AccountAuthenticatorActivity() {
//
//    private val TAG = javaClass.simpleName
//
//    companion object {
//        val KEY_AUTH_URL = "umra.net.app.infinity_card.KEY_AUTH_URL"
//        val KEY_IS_NEW_ACCOUNT = "umra.net.app.infinity_card.KEY_IS_NEW_ACCOUNT"
//        val KEY_ACCOUNT_OBJECT = "umra.net.app.infinity_card.KEY_ACCOUNT_OBJECT"
//    }
//
//    private var accountManager: AccountManager? = null
//    private var account: Account? = null
//    private var isNewAccount = false
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
////        setContentView(R.layout.activity_authentication)
//        accountManager = AccountManager.get(this)
//        val extras = intent.extras
//
//        // Are we supposed to create a new account or renew the authorisation of an old one?
//        isNewAccount = extras!!.getBoolean(KEY_IS_NEW_ACCOUNT, false)
//
//        // In case we're renewing authorisation, we also got an Account object that we're supposed
//        // to work with.
//        account = extras.getParcelable(KEY_ACCOUNT_OBJECT)
//
//        // Fetch the authentication URL that was given to us by the calling activity
//        val authUrl = extras.getString(KEY_AUTH_URL)
//        Log.d(
//            TAG, String.format(
//                "Initiated activity for getting authorisation with URL '%s'.",
//                authUrl
//            )
//        )
//
//        // Initialise the WebView
////        val webView = findViewById<View>(R.id.WebView) as WebView
////
////        // TODO: Enable this if your authorisation page requires JavaScript
////        // webView.getSettings().setJavaScriptEnabled(true);
////        webView.loadUrl(authUrl!!)
////        webView.webViewClient = object : WebViewClient() {
////            override fun onPageStarted(view: WebView, urlString: String, favicon: Bitmap) {
////                super.onPageStarted(view, urlString, favicon)
////                val url = Uri.parse(urlString)
////                val parameterNames = url.queryParameterNames
////                val extractedFragment = url.RequestIdTokenFromFragmentPartTask
////                if (parameterNames.contains("error")) {
////                    view.stopLoading()
////
////                    // In case of an error, the `error` parameter contains an ASCII identifier, e.g.
////                    // "temporarily_unavailable" and the `error_description` *may* contain a
////                    // human-readable description of the error.
////                    //
////                    // For a list of the error identifiers, see
////                    // http://tools.ietf.org/html/rfc6749#section-4.1.2.1
////                    val error = url.getQueryParameter("error")
////                    val errorDescription = url.getQueryParameter("error_description")
////
////                    // If the user declines to authorise the app, there's no need to show an error
////                    // message.
////                    if (error != "access_denied") {
////                        showErrorDialog(
////                            String.format(
////                                "Error code: %s\n\n%s", error,
////                                errorDescription
////                            )
////                        )
////                    }
////                } else if (urlString.startsWith(Config.redirectUrl)) {
////                    // We won't need to keep loading anymore. This also prevents errors when using
////                    // redirect URLs that don't have real protocols (like app://) that are just
////                    // used for identification purposes in native apps.
////                    view.stopLoading()
////                    when (Config.flowType) {
////                        Config.Flows.Implicit -> {
////                            if (!TextUtils.isEmpty(extractedFragment)) {
//////                                val task = CreateIdTokenFromFragmentPartTask()
//////                                task.execute(extractedFragment)
////
////
////                            } else {
////                                Log.e(
////                                    TAG, String.format(
////                                        "urlString '%1\$s' doesn't contain fragment part; can't extract tokens",
////                                        urlString
////                                    )
////                                )
////                            }
////                        }
////                        Config.Flows.Hybrid -> {
////                            if (!TextUtils.isEmpty(extractedFragment)) {
//////                                val task = RequestIdTokenFromFragmentPartTask()
//////                                task.execute(extractedFragment)
////
////
////                            } else {
////                                Log.e(
////                                    TAG, String.format(
////                                        "urlString '%1\$s' doesn't contain fragment part; can't request tokens",
////                                        urlString
////                                    )
////                                )
////                            }
////                        }
////                        Config.Flows.AuthorizationCode -> {
////
////                            // The URL will contain a `code` parameter when the user has been authenticated
////                            if (parameterNames.contains("code")) {
////                                val authToken = url.getQueryParameter("code")
////
////                                // Request the ID token
//////                                val task = RequestIdTokenTask()
//////                                task.execute(authToken)
////
////
////
////                            } else {
////                                Log.e(
////                                    TAG, String.format(
////                                        "urlString '%1\$s' doesn't contain code param; can't extract authCode",
////                                        urlString
////                                    )
////                                )
////                            }
////                        }
////                        else -> {
////                            if (parameterNames.contains("code")) {
////                                val authToken = url.getQueryParameter("code")
//////                                val task = RequestIdTokenTask()
//////                                task.execute(authToken)
////
////
////                            } else {
////                                Log.e(
////                                    TAG, String.format(
////                                        "urlString '%1\$s' doesn't contain code param; can't extract authCode",
////                                        urlString
////                                    )
////                                )
////                            }
////                        }
////                    }
////                }
////                // else : should be an intermediate url, load it and keep going
////            }
////        }
//    }
//
////    private class CreateIdTokenFromFragmentPartTask : AsyncTask<String?, Void?, Boolean>() {
////        override fun doInBackground(vararg args: String?): Boolean {
////            val fragmentPart = args[0]
////            val tokenExtrationUrl = Uri.Builder().encodedQuery(fragmentPart).build()
////            val accessToken = tokenExtrationUrl.getQueryParameter("access_token")
////            val idToken = tokenExtrationUrl.getQueryParameter("id_token")
////            val tokenType = tokenExtrationUrl.getQueryParameter("token_type")
////            val expiresInString = tokenExtrationUrl.getQueryParameter("expires_in")
////            val expiresIn = if (!TextUtils.isEmpty(expiresInString)) java.lang.Long.decode(expiresInString) else null
////            val scope = tokenExtrationUrl.getQueryParameter("scope")
////            if (TextUtils.isEmpty(accessToken) || TextUtils.isEmpty(idToken) || TextUtils.isEmpty(tokenType) || expiresIn == null) {
////                return false
////            } else {
////                Log.i("TAG", "AuthToken : $accessToken")
////                val response = IdTokenResponse()
////                response.setAccessToken(accessToken)
////                response.setIdToken(idToken)
////                response.setTokenType(tokenType)
////                response.setExpiresInSeconds(expiresIn)
////                response.setScope(scope)
////                response.setFactory(GsonFactory())
////                if (isNewAccount) {
////                    createAccount(response)
////                } else {
////                    setTokens(response)
////                }
////            }
////            return true
////        }
////
////        override fun onPostExecute(wasSuccess: Boolean) {
////            if (wasSuccess) {
////                // The account manager still wants the following information back
////                val intent = Intent()
////                intent.putExtra(AccountManager.KEY_ACCOUNT_NAME, account.name)
////                intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, account.type)
////                setAccountAuthenticatorResult(intent.extras)
////                setResult(RESULT_OK, intent)
////                finish()
////            } else {
////                showErrorDialog("Could not get ID Token.")
////            }
////        }
////    }
//
//    /**
//     * Hybrid flow
//     */
////    private class RequestIdTokenFromFragmentPartTask : AsyncTask<String?, Void?, Boolean>() {
////        override fun doInBackground(vararg args: String?): Boolean {
////            val fragmentPart = args[0]
////            val tokenExtrationUrl = Uri.Builder().encodedQuery(fragmentPart).build()
////            val idToken = tokenExtrationUrl.getQueryParameter("id_token")
////            val authCode = tokenExtrationUrl.getQueryParameter("code")
////            if (TextUtils.isEmpty(idToken) || TextUtils.isEmpty(authCode)) {
////                return false
////            } else {
////                val response: IdTokenResponse
////                Log.i("TAG", "Requesting access_token with AuthCode : $authCode")
////                response = try {
////                    OIDCUtils.requestTokens(
////                        Config.tokenServerUrl,
////                        Config.redirectUrl,
////                        Config.clientId,
////                        Config.clientSecret,
////                        authCode
////                    )
////                } catch (e: IOException) {
////                    Log.e("TAG", "Could not get response.")
////                    e.printStackTrace()
////                    return false
////                }
////                if (isNewAccount) {
////                    createAccount(response)
////                } else {
////                    setTokens(response)
////                }
////            }
////            return true
////        }
////
////        override fun onPostExecute(wasSuccess: Boolean) {
////            if (wasSuccess) {
////                // The account manager still wants the following information back
////                val intent = Intent()
////                intent.putExtra(AccountManager.KEY_ACCOUNT_NAME, account.name)
////                intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, account.type)
////                setAccountAuthenticatorResult(intent.extras)
////                setResult(RESULT_OK, intent)
////                finish()
////            } else {
////                showErrorDialog("Could not get ID Token.")
////            }
////        }
////
////    }
//
//    /**
//     * Requests the ID Token asynchronously.
//     */
////    private class RequestIdTokenTask : AsyncTask<String?, Void?, Boolean>() {
////        override fun doInBackground(vararg args: String?): Boolean {
////            val authToken = args[0]
////            val response: IdTokenResponse
////            Log.d("TAG", "Requesting ID token.")
////            response = try {
////                OIDCUtils.requestTokens(
////                    Config.tokenServerUrl,
////                    Config.redirectUrl,
////                    Config.clientId,
////                    Config.clientSecret,
////                    authToken
////                )
////            } catch (e: IOException) {
////                Log.e("TAG", "Could not get response.")
////                e.printStackTrace()
////                return false
////            }
////            if (isNewAccount) {
////                createAccount(response)
////            } else {
////                setTokens(response)
////            }
////            return true
////        }
////
////        override fun onPostExecute(wasSuccess: Boolean) {
////            if (wasSuccess) {
////                // The account manager still wants the following information back
////                val intent = Intent()
////                intent.putExtra(AccountManager.KEY_ACCOUNT_NAME, account.name)
////                intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, account.type)
////                setAccountAuthenticatorResult(intent.extras)
////                setResult(RESULT_OK, intent)
////                finish()
////            } else {
////                showErrorDialog("Could not get ID Token.")
////            }
////        }
////    }
//
//    private fun createAccount(response: IdTokenResponse) {
//        Log.d(TAG, "Creating account.")
//        val accountType = "open_id"  // getString(R.string.ACCOUNT_TYPE)
//
//        // AccountManager expects that each account has a unique username. If a new account has the
//        // same username as a previously created one, it will overwrite the older account.
//        //
//        // Unfortunately the OIDC spec cannot guarantee[1] that any user information is unique,
//        // save for the user ID (i.e. the ID Token subject) which is hardly human-readable. This
//        // makes choosing between multiple accounts difficult.
//        //
//        // We'll resort to naming each account `preferred_username (ID)`. This is a neat solution
//        // if the user ID is short enough.
//        //
//        // [1]: http://openid.net/specs/openid-connect-basic-1_0.html#ClaimStability
//
//        // Use the app name as a fallback if the other information isn't available for some reason.
//        var accountName: String? = getString(R.string.app_name)
//        var accountId: String? = null
//        try {
//            accountId = response.parseIdToken().getPayload().getSubject()
//        } catch (e: IOException) {
//            Log.e(TAG, "Could not get ID Token subject.")
//            e.printStackTrace()
//        }
//
//        // Get the user information so we can grab the `preferred`_username`
//        var userInfo: Map<*, *>? = emptyMap<Any, Any>()
//        try {
//            userInfo = OIDCUtils.getUserInfo(Config.userInfoUrl, response.getIdToken())
//        } catch (e: IOException) {
//            Log.e(TAG, "Could not get UserInfo.")
//            e.printStackTrace()
//        }
//        if (userInfo!!.containsKey("preferred_username")) {
//            accountName = userInfo["preferred_username"] as String?
//        }
//        account = Account(String.format("%s (%s)", accountName, accountId), accountType)
//        accountManager!!.addAccountExplicitly(account, null, null)
//
//        // Store the tokens in the account
//        setTokens(response)
//        Log.d(TAG, "Account created.")
//    }
//
//    private fun setTokens(response: IdTokenResponse) {
//        accountManager!!.setAuthToken(account, Authenticator.TOKEN_TYPE_ID, response.getIdToken())
//        accountManager!!.setAuthToken(account, Authenticator.TOKEN_TYPE_ACCESS, response.getAccessToken())
//        accountManager!!.setAuthToken(account, Authenticator.TOKEN_TYPE_REFRESH, response.getRefreshToken())
//    }
//
//    /**
//     * TODO: Improve error messages.
//     */
//    private fun showErrorDialog(message: String) {
//        AlertDialog.Builder(this@AuthenticatorActivity)
//            .setTitle("Sorry, there was an error")
//            .setMessage(message)
//            .setCancelable(true)
//            .setNeutralButton("Close") { dialogInterface, i ->
//                dialogInterface.dismiss()
//                finish()
//            }
//            .create()
//            .show()
//    }
//
//}